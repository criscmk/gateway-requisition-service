def label = "worker-${UUID.randomUUID().toString()}"
def ARCHITECTURE = ""

// define the pod templates
podTemplate(label: label, containers: [
        containerTemplate(name: 'maven', image: 'maven:3.6.1-jdk-11-slim', command: 'cat', ttyEnabled: true),
        containerTemplate(name: 'docker', image: 'docker:19.03', command: 'cat', ttyEnabled: true),
],
        volumes: [
                hostPathVolume(mountPath: '/var/run/docker.sock', hostPath: '/var/run/docker.sock')
        ]) {
    node(label) {
        //checkout the code
        repository = checkout scm

        // source control variables
        ARCHITECTURE = "amd64"
        def COMMIT_ID = repository.GIT_COMMIT
        def BRANCH = repository.GIT_BRANCH
        def SHORT_COMMIT_ID = "${COMMIT_ID[0..5]}"

        //get instance of a pom file to get properties
        def pom = readMavenPom()

        //extract image properties
        def DOCKER_AMD_BASE_IMAGE = pom.properties['docker.image.amd.base']
        def DOCKER_REPOSITORY_NAME = pom.properties['docker.image.repository']

        // fetch version and application name
        def VERSION = readMavenPom().getVersion()
        def APPLICATION_NAME = readMavenPom().getArtifactId()

        // build and sonar analysis
        stage("Build and SonarQube analysis") {
            print "Building branch: ${BRANCH}"
            print "Using GIT commitID: ${COMMIT_ID}"
            print "Application name:  ${APPLICATION_NAME}"
            print "Version: ${VERSION}"
            withSonarQubeEnv('sonarqube-server') {
                container('maven') {
                    sh "mvn clean package -DskipTests package sonar:sonar -Dsonar.projectKey=${APPLICATION_NAME}"
                }
            }
        }

        // quality control gate to ensure pipeline only continues when sonarqube tests complete
        stage("Code Quality Gate") {
            print "Waiting for SonarQube quality analysis results."
            timeout(time: 10, unit: 'MINUTES') {
                def qg = waitForQualityGate()
                if (qg.status != 'OK') {
                    error "Pipeline aborted due to quality gate failure: ${qg.status}"
                }
                print "Quality control gate passed."
            }
        }

        // build the docker images
        stage('Build docker image') {
            print "Building docker image."
            print "Base image: ${DOCKER_AMD_BASE_IMAGE}."
            print "Image architecture: ${ARCHITECTURE}."
            container('docker') {
                // docker build command
                sh("docker build --build-arg BASE_IMAGE=${DOCKER_AMD_BASE_IMAGE} " +
                        " --build-arg APPLICATION_NAME=${APPLICATION_NAME}-${VERSION} " +
                        " -t ${DOCKER_REPOSITORY_NAME}/${APPLICATION_NAME}:v${VERSION}-${SHORT_COMMIT_ID}-${ARCHITECTURE} .")
            }
        }

        // push image to docker hub repo
        stage('Push image to the repository') {
            print "Pushing image into docker repository"
            print "Image name: cellulant/${APPLICATION_NAME}:v${VERSION}-${SHORT_COMMIT_ID}-${ARCHITECTURE}"

            container('docker') {
                withCredentials([[$class          : 'UsernamePasswordMultiBinding',
                                  credentialsId   : '1747d824-3e68-4669-91e5-fc0933824aa8',
                                  usernameVariable: 'DOCKER_HUB_USER',
                                  passwordVariable: 'DOCKER_HUB_PASSWORD']]) {
                    sh """
                        docker login -u ${DOCKER_HUB_USER} -p ${DOCKER_HUB_PASSWORD}
                        docker push cellulant/${APPLICATION_NAME}:v${VERSION}-${SHORT_COMMIT_ID}-${ARCHITECTURE}
                        """
                }
            }
        }
    }
}

node(label: 'pc64le-windows') {
    print "Completed building normal image, building ppc-image"
    //checkout the code
    repository = checkout scm

    // source control variables
    def COMMIT_ID = repository.GIT_COMMIT
    def BRANCH = repository.GIT_BRANCH
    def SHORT_COMMIT_ID = "${COMMIT_ID[0..5]}"

    //get instance of a pom file to get properties
    def pom = readMavenPom()

    //extract image properties
    ARCHITECTURE = "ppc64le"
    def DOCKER_PPC_BASE_IMAGE = pom.properties['docker.image.ppc64le.base']
    def DOCKER_REPOSITORY_NAME = pom.properties['docker.image.repository']

    // fetch version and application name
    def VERSION = readMavenPom().getVersion()
    def APPLICATION_NAME = readMavenPom().getArtifactId()

    // build and sonar analysis
    stage("Building the jar file") {
        print "Building branch: ${BRANCH}"
        print "Using GIT commitID: ${COMMIT_ID}"
        print "Application name:  ${APPLICATION_NAME}"
        print "Version: ${VERSION}"
        bat "mvn clean package -DskipTests package"
    }

    // build the docker images
    stage('Build docker image') {
        print "Building docker image."
        print "Base image: ${DOCKER_PPC_BASE_IMAGE}."
        print "Image architecture: ${ARCHITECTURE}."
        // docker build command
        bat("docker build --build-arg BASE_IMAGE=${DOCKER_PPC_BASE_IMAGE} " +
                " --build-arg APPLICATION_NAME=${APPLICATION_NAME}-${VERSION} " +
                " -t ${DOCKER_REPOSITORY_NAME}/${APPLICATION_NAME}:v${VERSION}-${SHORT_COMMIT_ID}-${ARCHITECTURE} .")

    }

    // push image to docker hub repo
    stage('Push image to the repository') {
        print "Pushing image into docker repository"
        print "Image name: cellulant/${APPLICATION_NAME}:v${VERSION}-${SHORT_COMMIT_ID}-${ARCHITECTURE}"

        withCredentials([[$class          : 'UsernamePasswordMultiBinding',
                          credentialsId   : '1747d824-3e68-4669-91e5-fc0933824aa8',
                          usernameVariable: 'DOCKER_HUB_USER',
                          passwordVariable: 'DOCKER_HUB_PASSWORD']]) {
            bat """
                        docker login -u ${DOCKER_HUB_USER} -p ${DOCKER_HUB_PASSWORD}
                        docker push cellulant/${APPLICATION_NAME}:v${VERSION}-${SHORT_COMMIT_ID}-${ARCHITECTURE}
                        """

        }
    }
}