package com.cellulant.gatewayrequisitionservices.card.services;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class Payload {

    @JsonProperty(value = "ACSUrl")
    private String acsUrl;
    @JsonProperty("Payload")
    private String payload;
    @JsonProperty("TransactionId")
    private String transactionId;
}
