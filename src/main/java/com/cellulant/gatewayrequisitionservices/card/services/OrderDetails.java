package com.cellulant.gatewayrequisitionservices.card.services;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Data
@AllArgsConstructor
public class OrderDetails {

    private String OrderNumber;
    private String Amount;
    private String CurrencyCode;
}

