package com.cellulant.gatewayrequisitionservices.card.services;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

@RequiredArgsConstructor
@Data
@AllArgsConstructor
public class CreateJWTRequest {

    @NotBlank(message = "{returnUrl.required}")
    private String returnUrl;
    @NotBlank(message = "{referenceId.required}")
    private String referenceId;
}
