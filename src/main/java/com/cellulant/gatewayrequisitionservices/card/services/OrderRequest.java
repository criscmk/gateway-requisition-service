package com.cellulant.gatewayrequisitionservices.card.services;

import lombok.*;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderRequest {
    private String orderNumber;
    private String amount;
    private String currencyCode;
}