package com.cellulant.gatewayrequisitionservices.card.services;


import com.cellulant.gatewayrequisitionservices.cards.StepUpJWTRequest;
import com.cellulant.gatewayrequisitionservices.configurations.Configs;
import com.cellulant.gatewayrequisitionservices.configurations.Constants;
import com.cellulant.gatewayrequisitionservices.dtos.responses.ResponseArray;
import com.cellulant.gatewayrequisitionservices.models.ServicesEntity;
import com.cellulant.gatewayrequisitionservices.services.ServiceImpl;
import com.cellulant.gatewayrequisitionservices.utils.SharedUtilities;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

/**
 * Created by eyo
 * Project gateway-lookup-services
 * User: eyo
 * Date: 25/09/2019
 * Time: 2:01 PM
 */
@Service
@RequiredArgsConstructor
@SuppressWarnings("Duplicates")
@Slf4j
public class ValidateCardNumberService {
    // Initialize SharedUtilities resource
    // Which is auto-generated by Spring, we will use it to load SharedUtilities

    @NonNull
    private final SharedUtilities sharedUtilities;
    // This means to get the bean called ServiceImpl
    // Which is auto-generated by Spring, we will use it to load ServiceImpl

    @NonNull
    private final ServiceImpl serviceImpl;
    @NonNull
    private final Configs configs;

    /**
     * Validate card number response array.
     *
     * @param headerParams the header params
     * @param cardNumber   the card number
     * @return the response array
     * @throws Exception the exception
     */
    public Mono<ResponseArray> validateCardNumber(Map headerParams, String cardNumber) throws Exception {

        //Get action from headers
        String action = sharedUtilities.retrieveActionFromHeaders(headerParams);
        //Retrieve the Service Details form Database
        final Mono<ServicesEntity> servicesEntityMono = serviceImpl.getServiceDetails((String.valueOf(headerParams.get(Constants.SERVICE))));


        return servicesEntityMono.flatMap(servicesEntity -> {
            // Get msisdn
            final Mono<String> msisdnMono = sharedUtilities.getMsisdn(headerParams, action, servicesEntity)
                    .doOnNext(msisdn -> MDC.put(Constants.MSISDN, String.valueOf(msisdn)));
            // Retrieve Wallet URL from Database using provided action
            final Mono<String> walletUrlMono = sharedUtilities.getWalletUrl(action, servicesEntity);


            return Mono.zip(msisdnMono, walletUrlMono).flatMap(data -> {
                final String msisdn = data.getT1();
                final String walletUrl = data.getT2();

                // update headerParams with updated msisdn
                final Mono<Map> headerParamsMono = Mono.just(headerParams).map(map -> {
                    map.replace(Constants.MSISDN, msisdn);
                    return map;
                });

                return headerParamsMono.flatMap(headers -> {
                    final Mono<String> requestIDMono = sharedUtilities.getRequestID(null, msisdn, headers)
                            .doOnNext(requestID -> MDC.put(Constants.CLOUD_REQUEST_ID, String.valueOf(requestID)));

                    return requestIDMono
                            .flatMap(requestID -> sharedUtilities.getIntegrationLayerResponse(cardNumber, requestID, walletUrl))
                            .flatMap(sharedUtilities::handleResponse);
                });
            });
        });
    }


    public Mono<ResponseArray> createJWT(Map headerParams, OrderRequest createJWTRequest) throws Exception {

        //Get action from headers
        String action = sharedUtilities.retrieveActionFromHeaders(headerParams);
        //Retrieve the Service Details form Database
        final Mono<ServicesEntity> servicesEntityMono = serviceImpl.getServiceDetails((String.valueOf(headerParams.get(Constants.SERVICE))));


        return servicesEntityMono.flatMap(servicesEntity -> {
            // Get msisdn
            final Mono<String> msisdnMono = sharedUtilities.getMsisdn(headerParams, action, servicesEntity)
                    .doOnNext(msisdn -> MDC.put(Constants.MSISDN, String.valueOf(msisdn)));

            return msisdnMono.flatMap(msisdn -> {
                headerParams.replace(Constants.MSISDN, msisdn);
                Mono<String> requestIDMono = sharedUtilities.getRequestID(null, msisdn, headerParams)
                        .doOnNext(requestID -> MDC.put(Constants.CLOUD_REQUEST_ID, String.valueOf(requestID)));

                return requestIDMono.map(requestID -> {
                    // The JWT signature algorithm we will be using to sign the token
                    SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

                    long nowMillis = System.currentTimeMillis();

                    // We will sign our JWT with our API Key
                    byte[] apiKeySecretBytes = configs.getJwtApiKey().getBytes();
                    Key signingKey = new SecretKeySpec(apiKeySecretBytes,
                            signatureAlgorithm.getJcaName());
                    var transactionId = String.valueOf(UUID.randomUUID());
                    // Let's set the JWT Claims
                    var order = new OrderDetails(createJWTRequest.getOrderNumber(),createJWTRequest.getAmount(),createJWTRequest.getCurrencyCode());

                    JwtBuilder builder = Jwts.builder()
                            .setId(transactionId)
                            .setIssuedAt(new Date(nowMillis))
                            .setIssuer(configs.getJwtApiIndentifier())
                            .claim("OrgUnitId", configs.getJwtApiOrgUnitId())
                            .claim("Payload", order)
                            .signWith(signatureAlgorithm, signingKey);
                    final Claims claims = Jwts.parser()
                            .setSigningKey(signingKey)
                            .parseClaimsJws(builder.compact()).getBody();

                    // Add the expiration or TTL (Time To Live) for this JWT
                    long expMillis = nowMillis + configs.getJwtApiExpiration();
                    Date exp = new Date(expMillis);
                    builder.setExpiration(exp);

                    // Builds the JWT and serializes it to a compact, URL-safe string
                    return sharedUtilities.handleJwtResponse(builder.compact());
                });
            });

        });

    }

    public Mono<ResponseArray> createStepUpJwt(Map headerParams, StepUpJWTRequest stepUpJWTRequest) throws Exception {

        //Get action from headers
        String action = sharedUtilities.retrieveActionFromHeaders(headerParams);
        //Retrieve the Service Details form Database
        final Mono<ServicesEntity> servicesEntityMono = serviceImpl.getServiceDetails((String.valueOf(headerParams.get(Constants.SERVICE))));


        return servicesEntityMono.flatMap(servicesEntity -> {
            // Get msisdn
            final Mono<String> msisdnMono = sharedUtilities.getMsisdn(headerParams, action, servicesEntity)
                    .doOnNext(msisdn -> MDC.put(Constants.MSISDN, String.valueOf(msisdn)));

            return msisdnMono.flatMap(msisdn -> {
                headerParams.replace(Constants.MSISDN, msisdn);
                Mono<String> requestIDMono = sharedUtilities.getRequestID(null, msisdn, headerParams)
                        .doOnNext(requestID -> MDC.put(Constants.CLOUD_REQUEST_ID, String.valueOf(requestID)));

                return requestIDMono.map(requestID -> {
                    // The JWT signature algorithm we will be using to sign the token
                    SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

                    long nowMillis = System.currentTimeMillis();

                    // We will sign our JWT with our API Key
                    byte[] apiKeySecretBytes = configs.getJwtApiKey().getBytes();
                    Key signingKey = new SecretKeySpec(apiKeySecretBytes,
                            signatureAlgorithm.getJcaName());
                    var transactionId = String.valueOf(UUID.randomUUID());
                    var order = new Payload(stepUpJWTRequest.getAcsUrl(), stepUpJWTRequest.getPareq(), stepUpJWTRequest.getTransactionId());
                    // Let's set the JWT Claims
                    JwtBuilder builder = Jwts.builder()
                            .setId(transactionId)
                            .setIssuedAt(new Date(nowMillis))
                            .setIssuer(configs.getJwtApiIndentifier())
                            .claim("OrgUnitId", configs.getJwtApiOrgUnitId())
                            .claim("Payload", order)
                            .claim("ReturnUrl", stepUpJWTRequest.getReturnUrl())
                            .claim("ReferenceId", stepUpJWTRequest.getReferenceId())
                            .signWith(signatureAlgorithm, signingKey);

                    final Claims claims = Jwts.parser()
                            .setSigningKey(signingKey)
                            .parseClaimsJws(builder.compact()).getBody();

                    log.info("Get Claims {} {}", claims.get("Payload"), claims.get("jti"));

                    // Add the expiration or TTL (Time To Live) for this JWT
                    long expMillis = nowMillis + configs.getJwtApiExpiration();
                    Date exp = new Date(expMillis);
                    builder.setExpiration(exp);

                    // Builds the JWT and serializes it to a compact, URL-safe string
                    return sharedUtilities.handleJwtResponse(builder.compact());
                });
            });

        });

    }
}
