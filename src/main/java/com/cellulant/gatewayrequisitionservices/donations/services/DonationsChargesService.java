package com.cellulant.gatewayrequisitionservices.donations.services;

import com.cellulant.gatewayrequisitionservices.configurations.Constants;
import com.cellulant.gatewayrequisitionservices.donations.dtos.DonationsChargesRequest;
import com.cellulant.gatewayrequisitionservices.dtos.responses.ResponseArray;
import com.cellulant.gatewayrequisitionservices.models.ServicesEntity;
import com.cellulant.gatewayrequisitionservices.services.ServiceImpl;
import com.cellulant.gatewayrequisitionservices.utils.SharedUtilities;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.Map;

@Service
@RequiredArgsConstructor
@Slf4j
public class DonationsChargesService {

    @NonNull
    private final SharedUtilities sharedUtilities;

    @NonNull
    private final ServiceImpl serviceImpl;

    public Mono<ResponseArray> processFetchDonationCharges(Map headerParams, DonationsChargesRequest donationsChargesRequest) throws Exception{
        //Get action from headers
        final String action = sharedUtilities.retrieveActionFromHeaders(headerParams);
        final String uuid = String.valueOf(headerParams.get(Constants.UUID));
        //Retrieve the Service Details form Database
        final Mono<ServicesEntity> servicesEntityMono = serviceImpl.getServiceDetails((String.valueOf(headerParams.get(Constants.SERVICE))));

        return servicesEntityMono.flatMap(servicesEntity -> {
            // Get msisdn
            final Mono<String> msisdnMono = sharedUtilities.getMsisdn(headerParams, action, servicesEntity)
                    .doOnNext(msisdn -> MDC.put(Constants.MSISDN, String.valueOf(msisdn)));

            // Retrieve Wallet URL from Database using provided action
            final Mono<String> walletUrlMono = sharedUtilities.getWalletUrl(action, servicesEntity);

            final Mono<String> ecobankDomesticJsonMono = Mono.just(donationsChargesRequest)
                    .flatMap(chargesRequest -> Mono.fromCallable(() -> {
                        log.info("This is the charges request [{}]", chargesRequest);
                        // Encrypt the Pin
                        //chargesRequest.setTransactionType(sharedUtilities.mapServiceToEcobankTransactionTypes(String.valueOf(donationsChargesRequest.getTransactionType())));
                        float amount = sharedUtilities.cleanupDecimalAmount(Float.parseFloat(donationsChargesRequest.getAmount()));
                        chargesRequest.setAmount(String.valueOf(amount));
                        // Formulate Wallet Payload
                        ObjectMapper objectMapper = new ObjectMapper();
                        objectMapper.disable(MapperFeature.USE_ANNOTATIONS);
                        return objectMapper.writeValueAsString(chargesRequest);
                    }));

            return Mono.zip(msisdnMono, walletUrlMono, ecobankDomesticJsonMono)
                    .flatMap(data -> {

                        final String msisdn = data.getT1();
                        final String walletUrl = data.getT2();
                        final String ecobankDomesticJson = data.getT3();

                        // update headerParams with updated msisdn
                        final Mono<Map> headerParamsMono = Mono.just(headerParams).map(map -> {
                            map.replace(Constants.MSISDN, msisdn);
                            return map;
                        });

                        return headerParamsMono.flatMap(headers -> {
                            final Mono<String> requestIDMono = sharedUtilities.getRequestID(donationsChargesRequest, msisdn, headers)
                                    .doOnNext(requestID -> MDC.put(Constants.CLOUD_REQUEST_ID, String.valueOf(requestID)));

                            //Validate Uuid
                            final Mono<Void> validateUUID = sharedUtilities.validateUUID(servicesEntity, msisdn, action, uuid);

                            return validateUUID.then(requestIDMono)
                                    .flatMap(requestID -> sharedUtilities
                                            .processIntegrationResponse(headers, msisdn, requestID, servicesEntity, walletUrl, ecobankDomesticJson));
                        });

                    });
        });
    }
}
