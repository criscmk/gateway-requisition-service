package com.cellulant.gatewayrequisitionservices.donations.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DonationsChargesRequest implements Serializable {
    @NotNull(message = "{accountAlias.required}")
    private String accountAlias;
    @NotNull(message = "{amount.required}")
    private String amount;
    @NotNull(message = "{amount.charityAffiliateCode}")
    private String charityAffiliateCode;
    @NotNull(message = "{amount.charityCode}")
    private String charityCode;
    private String transactionType;
    private String transactionCode;


}
