package com.cellulant.gatewayrequisitionservices.donations.dtos;

public enum TransactionTypes {

    otherlocalbanks("INTERBANK"),
    billpayment("BILLPAY"),
    mobilemoney("MOBILEMONEY"),
    ecobankdomestic("ECOBANKDOMESTIC"),
    ecobankafrica("ECOBANKAFRICA"),
    international("INTERNATIONAL"),
    visadirect("VISADIRECT"),
    prepaidcard("PREPAIDCARD"),
    creditcard("CREDITCARD"),
    merchant("MERCHANT"),
    airtime("AIRTIME");

    private final String transactionType;

    TransactionTypes(String transactionType) {
        this.transactionType = transactionType;
    }

    /**
     * This has to be in place to Override
     * Required to return the Value of the Enum
     */
    @Override
    public String toString() {
        return transactionType;
    }
}
