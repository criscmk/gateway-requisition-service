package com.cellulant.gatewayrequisitionservices.utils;


import com.cellulant.gatewayrequisitionservices.configurations.Configs;
import com.cellulant.gatewayrequisitionservices.configurations.StatusCodeConfig;
import com.cellulant.gatewayrequisitionservices.configurations.StatusMessageServiceConfig;
import com.cellulant.gatewayrequisitionservices.configurations.aspects.annotations.NoLogging;
import com.cellulant.gatewayrequisitionservices.configurations.aspects.annotations.UtilitiesLoggable;
import com.cellulant.gatewayrequisitionservices.dtos.responses.ResponseArray;
import com.cellulant.gatewayrequisitionservices.dtos.responses.ResponseArrayResultDTO;
import com.cellulant.gatewayrequisitionservices.dtos.walletdtos.WalletResponse;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * Created by sylvester
 * Project transfers-gateway-api
 * User: sylvester
 * Date: 4/2/19
 * Time: 8:37 AM
 */
@Component
@UtilitiesLoggable
@RequiredArgsConstructor
public class ResponseManager implements MessageSourceAware {

    @NonNull
    private final Configs configs;
    @NonNull
    private final StatusCodeConfig statusCodeConfig;
    @NonNull
    private final StatusMessageServiceConfig statusMessageService;

    // Initialize Resource Configs
    private MessageSource messageSource;
    // Initialize Default Locale
    private Locale locale = Locale.US;
    private ResponseArrayResultDTO responseArrayResultDTO;


    /**
     * Handle wallet response response array.
     *
     * @param walletResponse the wallet response
     * @return the response array
     */
    @NoLogging
    public ResponseArray handleWalletResponse(WalletResponse walletResponse) {
        ResponseArray responseArray = new ResponseArray();
        if (walletResponse.getStatusCode() == configs.getWalletSyncSuccessCode()) {
            int statusCode = statusCodeConfig.getGatewaySyncSuccessStatusCode();
            responseArray.setStatusCode(statusCode);
            responseArray.setStatusMessage(walletResponse.getStatusMessage());
            responseArray.setResult(walletResponse.getResult());
        } else if (walletResponse.getStatusCode() == configs.getWalletAsyncSuccessCode()) {
            int statusCode = statusCodeConfig.getGatewayAsyncSuccessStatusCode();
            responseArray.setStatusCode(statusCode);
            responseArray.setStatusMessage(walletResponse.getStatusMessage());

            responseArray.setResult(walletResponse.getResult());
        } else if (walletResponse.getStatusCode() == configs.
                getWalletFailureCode()) {
            int statusCode = statusCodeConfig.getGatewayFailureStatusCode();
            responseArray.setStatusCode(statusCode);
            responseArray.setStatusMessage(walletResponse.getStatusMessage());
            responseArray.setResult(walletResponse.getResult());
        } else if (walletResponse.getStatusCode() == configs.getWalletNoRecordFoundStatusCode()) {
            int statusCode = statusCodeConfig.getGatewayNoRecordFoundSuccessStatusCode();
            responseArray.setStatusCode(statusCode);
            responseArray.setStatusMessage(walletResponse.getStatusMessage());
            responseArray.setResult(walletResponse.getResult());
        } else {
            int statusCode = walletResponse.getStatusCode();
            responseArray.setStatusCode(statusCode);
            responseArray.setStatusMessage(walletResponse.getStatusMessage());
            responseArray.setResult(walletResponse.getResult());
        }
        return responseArray;
    }

    /**
     * Handle exception message response array.
     *
     * @param message the message
     * @return the response array
     */
    public ResponseArray handleExceptionMessage(String message) {
        ResponseArray responseArray = new ResponseArray();
        responseArrayResultDTO = new ResponseArrayResultDTO();
        responseArray.setStatusCode(statusCodeConfig.getGatewayFailureStatusCode());
        responseArray.setStatusMessage(messageSource.getMessage(message, new Object[0], locale));
        responseArrayResultDTO.setMessage(messageSource.getMessage(message, new Object[0], locale));
        responseArray.setResult(responseArrayResultDTO);
        return responseArray;
    }


    /**
     * Handle exception message response array.
     *
     * @param message the message
     * @param results the result
     * @return the response array
     */
    public ResponseArray handleExceptionMessage(String message, List results) {
        ResponseArray responseArray = new ResponseArray();
        responseArrayResultDTO = new ResponseArrayResultDTO();
        responseArray.setStatusCode(statusCodeConfig.getGatewayFailureStatusCode());
        responseArray.setStatusMessage(messageSource.getMessage(message, new Object[0], locale));
        responseArrayResultDTO.setMessage(messageSource.getMessage(message, new Object[0], locale));
        responseArrayResultDTO.setData(results);
        responseArray.setResult(responseArrayResultDTO);
        return responseArray;
    }

    /**
     * Handle custom exception message response array.
     *
     * @param statusCode the status code
     * @param message    the message
     * @return the response array
     */
    public ResponseArray handleCustomExceptionMessage(int statusCode, String message) {
        ResponseArray responseArray = new ResponseArray();
        responseArrayResultDTO = new ResponseArrayResultDTO();
        responseArray.setStatusCode(statusCode);
        responseArray.setStatusMessage(message);
        responseArrayResultDTO.setMessage(message);
        responseArray.setResult(responseArrayResultDTO);
        return responseArray;
    }

    /**
     * Handle exception message response array.
     *
     * @param message the message
     * @param locale  the locale
     * @return the response array
     */
    public ResponseArray handleExceptionMessage(String message, Locale locale) {
        ResponseArray responseArray = new ResponseArray();
        responseArray.setStatusCode(statusCodeConfig.getGatewayFailureStatusCode());
        responseArray.setStatusMessage(messageSource.getMessage(message, new Object[0], locale));
        return responseArray;
    }

    /**
     * Handle exception message response array.
     *
     * @return the response array
     */
    public ResponseArray handleExceptionMessage() {
        ResponseArray responseArray = new ResponseArray();
        responseArray.setStatusCode(statusCodeConfig.getGatewayFailureStatusCode());
        responseArray.setStatusMessage("No message found");
        return responseArray;
    }


    @Override
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }


    /**
     * Return success response response array.
     *
     * @param response the response
     * @return the response array
     */
    public ResponseArray returnSuccessResponse(List response) {

        ResponseArray responseArray = new ResponseArray();
        responseArrayResultDTO = new ResponseArrayResultDTO();
        int statusCode = statusCodeConfig.getGatewaySyncSuccessStatusCode();
        responseArray.setStatusCode(statusCode);
        responseArray.setStatusMessage(statusMessageService.formulateResponseMessage(statusCode));
        responseArrayResultDTO.setMessage(statusMessageService.formulateResponseMessage(statusCode));
        responseArrayResultDTO.setData(response);
        responseArray.setResult(responseArrayResultDTO);

        return responseArray;
    }


    public ResponseArray returnAsyncSuccessResponse(String languageCode, List response, String requestID) {
        String message = getAsyncMessage(languageCode);
        ResponseArray responseArray = new ResponseArray();
        responseArrayResultDTO = new ResponseArrayResultDTO();
        responseArray.setStatusCode(statusCodeConfig.getGatewayAsyncSuccessStatusCode());
        responseArray.setStatusMessage(message);
        responseArrayResultDTO.setMessage(message);
        responseArrayResultDTO.setReferenceID(String.valueOf(requestID));
        responseArrayResultDTO.setData(response);
        responseArray.setResult(responseArrayResultDTO);
        return responseArray;
    }

    private String getAsyncMessage(String languageCode) {
        String message = statusMessageService.formulateResponseMessage(statusCodeConfig.getGatewayAsyncSuccessStatusCode());
        if (message != null) {

            return message;
        }
        return configs.getDefaultSuccessMessage();
    }

    public ResponseArray handleCustomeJWTSuccess(String compact) {

        ResponseArray responseArray = new ResponseArray();
        var hashMap = new HashMap<String, Object>();
        hashMap.put("token", Collections.singletonList(compact));
        List<Object> responseDataList = new ArrayList<>();
        responseDataList.add(hashMap);
        responseArrayResultDTO = new ResponseArrayResultDTO();
        responseArray.setStatusCode(statusCodeConfig.getGatewaySyncSuccessStatusCode());
        responseArray.setStatusMessage(messageSource.getMessage("jwt.success", new Object[0], locale));
        responseArrayResultDTO.setMessage(messageSource.getMessage("jwt.success", new Object[0], locale));
        responseArrayResultDTO.setData(responseDataList);
        responseArray.setResult(responseArrayResultDTO);
        return responseArray;
    }
}
