package com.cellulant.gatewayrequisitionservices.utils;

import com.cellulant.gatewayrequisitionservices.configurations.aspects.annotations.NoLogging;
import com.cellulant.gatewayrequisitionservices.configurations.aspects.annotations.UtilitiesLoggable;
import org.springframework.stereotype.Component;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;

/**
 * Created by sylvester
 * Project requisition-gateway-api
 * User: sylvester
 * Date: 4/2/19
 * Time: 7:50 PM
 */
@Component
@UtilitiesLoggable
public class AESEncrypterCBC {

    private static final String TAG = "AESEncrypterCBC";

    /**
     * This generates the secret key to use for the
     * encryption process
     *
     * @param keyValue the key value
     * @return a secret key
     * @throws NoSuchAlgorithmException NoSuchAlgorithmException
     */
    public Key getEncryptionKey(String keyValue) throws NoSuchAlgorithmException {
        return new SecretKeySpec(keyValue.getBytes(), "AES");
    }

    /**
     * This function encrypts the data and encodes it in
     * base64
     *
     * @param data      the data we need to encrypt
     * @param secretKey the secretKey to use in the encryption
     * @return base64Encoded encrypted data
     * @throws NoSuchPaddingException             NoSuchPaddingException
     * @throws NoSuchAlgorithmException           NoSuchAlgorithmException
     * @throws InvalidKeyException                InvalidKeyException
     * @throws BadPaddingException                BadPaddingException
     * @throws IllegalBlockSizeException          IllegalBlockSizeException
     * @throws InvalidAlgorithmParameterException the invalid algorithm parameter exception
     */
    public String encrypt(String data, Key secretKey) throws NoSuchPaddingException,
            NoSuchAlgorithmException, InvalidKeyException, BadPaddingException,
            IllegalBlockSizeException, InvalidAlgorithmParameterException {
        IvParameterSpec iv = new IvParameterSpec("TheBestSecretKey".getBytes(StandardCharsets.UTF_8));
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, secretKey, iv);
        return new String(
                Base64.getEncoder().encode(cipher.doFinal(data.getBytes())),
                StandardCharsets.UTF_8
        );
    }

    /**
     * This function decrypts the data using the secret Key.We start by
     * decoding the base64 encoded string then decrypt
     *
     * @param encryptedData the encrypted data
     * @param secretKey     the shared secret key used to encrypt the data
     * @return the string that was encrypted
     * @throws NoSuchPaddingException             NoSuchPaddingException
     * @throws NoSuchAlgorithmException           NoSuchAlgorithmException
     * @throws InvalidKeyException                InvalidKeyException
     * @throws BadPaddingException                BadPaddingException
     * @throws IllegalBlockSizeException          IllegalBlockSizeException
     * @throws IOException                        the io exception
     * @throws InvalidAlgorithmParameterException the invalid algorithm parameter exception
     */
    public String decrypt(String encryptedData, Key secretKey) throws NoSuchPaddingException,
            NoSuchAlgorithmException, InvalidKeyException, BadPaddingException,
            IllegalBlockSizeException, IOException, InvalidAlgorithmParameterException {
        String key = "TheBestSecretKey";
        IvParameterSpec iv = new IvParameterSpec(new byte[16]);
        SecretKeySpec spec = new SecretKeySpec(key.getBytes(StandardCharsets.UTF_8), "AES");
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, secretKey, iv);
        byte[] decodedValue = Base64.getDecoder().decode(encryptedData);
        System.out.println(TAG + " Decoded Value Bytes " + Arrays.toString(decodedValue));
        return new String(cipher.doFinal(decodedValue));
    }

    /**
     * Handles Decryption of Strings
     *
     * @param value String
     * @return String decrypted value
     */
    public static String getDecryptedValue(String value) {

        try {
            AESEncrypterCBC encrypter = new AESEncrypterCBC();
            Key key = encrypter.getEncryptionKey("TheBestSecretKey");
            return encrypter.decrypt(value, key);
        } catch (Exception ex) {
            System.out.println(TAG + ex.toString());
            return "";
        }
    }

    /**
     * Handles Encryption of Strings
     *
     * @param value String
     * @return String encrypted value
     */
    @NoLogging
    String getEncryptedValue(String value) {

        try {
            AESEncrypterCBC encrypter = new AESEncrypterCBC();
            Key key = encrypter.getEncryptionKey("TheBestSecretKey");
            return encrypter.encrypt(value, key);
        } catch (Exception ex) {
            System.out.println(TAG + ex.toString());
            return "";
        }
    }

    /**
     * Test Class For Encryption and Decryption
     *
     * @param args args
     */
    public static void main(String[] args) {
        AESEncrypterCBC encrypter = new AESEncrypterCBC();
        try {
            Key key = encrypter.getEncryptionKey("TheBestSecretKey");
            String temp = encrypter.encrypt("3.0", key);
            System.out.println(TAG + "Original Text : " + "3.0");
            System.out.println(TAG + "Encrypted text : " + temp);
            System.out.println(TAG + "Decrypted text : " + encrypter.decrypt(temp, key));
        } catch (Exception ex) {
            System.out.println(TAG + "Exception" + ex.toString());
        }
    }
}
