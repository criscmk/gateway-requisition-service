package com.cellulant.gatewayrequisitionservices.dtos.core;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Range;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CoreRequest implements Serializable {

    @NotNull
    @Min(value = 1, message = "{cloudPacketID.notNull}")
    private String cloudPacketID;

    @NotNull
    @Min(value = 1, message = "{requestID.notNull}")
    private String cloudRequestID;

    @NotNull
    @Range(min = 0, max = 1, message = "{requestMode.invalid}")
    private int requestMode;

    @NotNull(message = "{msisdn.notNull}")
    private String msisdn;

    @NotNull(message = "{channel.notNull}")
    private String channelName;

    @NotNull(message = "{destination.notNull}")
    private String destination;

    @NotNull
    @Min(value = 1, message = "{requestIMC.notNull}")
    private int requestIMC;

    @NotNull(message = "{cloudDateReceived.notNull}")
    private String cloudDateReceived;

    @NotNull
    @Min(value = 1, message = "{networkID.notNull}")
    private int networkID;

    @NotNull(message = "{languageCode.notNull}")
    private String languageCode;

    @NotNull(message = "{username.notNull}")
    private String username;

    @NotNull(message = "{password.notNull}")
    private String password;

    @Valid
    @NotNull(message = "{payload.notNull}")
    private ServiceRequest payload;
}