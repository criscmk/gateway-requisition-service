package com.cellulant.gatewayrequisitionservices.dtos.core;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ServiceRequest implements Serializable {

    private String pin;
    private String accountAlias;
    private String moduleName;
    private String serviceTag;
    private double amount;
    private String serviceAction;
    Map<String, Object> extraData = new LinkedHashMap<>();

    /**
     * Sets extra data.
     *
     * @param key   the key
     * @param value the value
     */
    @JsonAnySetter
    public void setExtraData(String key, Object value) {
        extraData.put(key, value);
    }

}