package com.cellulant.gatewayrequisitionservices.dtos.walletdtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by sylvester
 * Project requisition-gateway-api
 * User: sylvester
 * Date: 4/2/19
 * Time: 7:54 PM
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class WalletResponse {

    private int statusType;
    private int statusCode;
    private String statusMessage;
    private Object result;

}

