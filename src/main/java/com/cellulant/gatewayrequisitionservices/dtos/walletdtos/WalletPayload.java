package com.cellulant.gatewayrequisitionservices.dtos.walletdtos;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by sylvester
 * Project requisition-gateway-api
 * User: sylvester
 * Date: 4/2/19
 * Time: 7:52 PM
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class WalletPayload {

    private String cloudRequestID;
    private int channelID;
    private String msisdn;
    private String payload;
    private int networkID;
    private String cloudDateReceived;
    private int requestMode;
    private String accessPoint;
    private int canFallBack;
    private String languageCode;
    private int cloudUserID;
}
