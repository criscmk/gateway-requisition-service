package com.cellulant.gatewayrequisitionservices.dtos.walletdtos;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by  IntelliJ IDEA.
 * Project enquiries-gateway-api
 * User: Sylvester Musyoki
 * Date: 5/21/19
 * Time: 6:38 PM
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Payload implements Serializable {

    /**
     * The Service key.
     */
    private String serviceTag;
    /**
     * The Account alias.
     */
    private String accountAlias;
    /**
     * The Module name.
     */
    private String moduleName;
    /**
     * The Pin.
     */
    private String pin;
    /**
     * The Amount.
     */
    private Float amount;

    private String serviceAction;
    /**
     * The Extra data.
     */
    Map<String, Object> extraData = new LinkedHashMap<>();

    /**
     * Sets extra data.
     *
     * @param key   the key
     * @param value the value
     */
    @JsonAnySetter
    public void setExtraData(String key, Object value) {
        extraData.put(key, value);
    }
}
