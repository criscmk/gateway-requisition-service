package com.cellulant.gatewayrequisitionservices.dtos.responses;

import lombok.*;

/**
 * Created by sylvester
 * Project requisition-gateway-api
 * User: sylvester
 * Date: 4/2/19
 * Time: 7:50 PM
 */
@Builder
@Data
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
public class ResponseArray {
    @NonNull
    private Integer statusCode;
    @NonNull
    private String statusMessage;
    private Object result;

}
