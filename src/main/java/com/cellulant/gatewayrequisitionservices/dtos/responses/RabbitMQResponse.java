package com.cellulant.gatewayrequisitionservices.dtos.responses;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RabbitMQResponse {

    private int statusCode;
    private int statusType;
    private String statusMessage;
    private List<Object> resultsList;
}
