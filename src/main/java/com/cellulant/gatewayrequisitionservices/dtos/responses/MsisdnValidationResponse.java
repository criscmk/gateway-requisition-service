package com.cellulant.gatewayrequisitionservices.dtos.responses;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by eyo
 * Project enquiries-gateway-api
 * User: eyo
 * Date: 2019-07-03
 * Time: 15:49
 */
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MsisdnValidationResponse {

    private String originalNumber;
    private String numberType;
    private String localNumber;
    private String internationalNumber;
    private String countryCode;
    private String countryDialCode;
    private String country;
    private String network;
    private String hasNumberPortability;
    private String note;


}
