package com.cellulant.gatewayrequisitionservices.dtos.responses;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by eyo
 * Project payment-gateway-api
 * User: eyo
 * Date: 2019-07-22
 * Time: 08:46
 */
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseArrayResultDTO {
    private String message;
    private String referenceID;
    private List data;
}
