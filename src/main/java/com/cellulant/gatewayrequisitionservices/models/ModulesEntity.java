package com.cellulant.gatewayrequisitionservices.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The type Modules entity.
 */
@Entity
@Table(name = "modules", schema = "api_gateway_db")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Where(clause = "active = '1'")
public class ModulesEntity {
    @Id
    private int moduleId;
    private String moduleName;
    private String moduleDescription;
}
