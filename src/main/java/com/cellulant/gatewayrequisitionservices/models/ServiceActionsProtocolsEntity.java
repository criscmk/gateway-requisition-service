package com.cellulant.gatewayrequisitionservices.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Where;

import javax.persistence.*;

@Entity
@Table(name = "serviceActionsProtocolsCloud", schema = "api_gateway_db")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Where(clause = "active = '1'")
public class ServiceActionsProtocolsEntity {
    @Id
    private int serviceActionsProtocolId;
    private String serviceUrl;
    private int serviceType;
    private int requireUuidValidation;
    private boolean isValidateMsisdn;

    @ManyToOne()
    @JoinColumn(name = "actionId", insertable = false, updatable = false)
    private ActionsEntity actionEntity;

//    @ManyToOne()
//    @JoinColumn(name = "serviceId", insertable = false, updatable = false)
//    private ServicesEntity servicesEntity;



}
