package com.cellulant.gatewayrequisitionservices.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Created by sylvester
 * Project requisition-gateway-api
 * User: sylvester
 * Date: 4/2/19
 * Time: 7:54 PM
 */
@Builder
@Entity
@Table(name = "requestLogs")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RequestLogs {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String requestLogID;
    private int clientID;
    private String service;
    private String clientRequestID;
    private int channelId;
    private int walletChannelId;
    private String MSISDN;
    private String payload;

}
