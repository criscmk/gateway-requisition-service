package com.cellulant.gatewayrequisitionservices.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "actions", schema = "api_gateway_db")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Where(clause = "active = '1'")
public class ActionsEntity {
    @Id
    private int actionId;
    private String actionName;


}
