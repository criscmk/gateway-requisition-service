package com.cellulant.gatewayrequisitionservices.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.List;

/**
 * The type Services entity.
 */
@Entity
@Table(name = "services", schema = "api_gateway_db")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Where(clause = "active = '1'")
public class ServicesEntity {
    @Id
    private int serviceId;
    private String serviceName;
    private String serviceDescription;
    private String serviceCode;
    private int serviceType;
    private int moduleId;
    private Byte isFallback;
    private Integer minAmount;
    private Integer maxAmount;
    private byte isReversal;
    private boolean isValidateMsisdn;


    @OneToMany()
    @JoinColumn(name = "serviceId", insertable = false, updatable = false)
    private List<ServiceActionsProtocolsEntity> serviceActionsProtocolsEntityList;

    @ManyToOne()
    @JoinColumn(name = "moduleId", insertable = false, updatable = false)
    private ModulesEntity modulesEntity;

    private int active;
}
