package com.cellulant.gatewayrequisitionservices.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

/**
 * Created by kamauwamatu
 * Project api-gateway-core
 * User: kamauwamatu
 * Date: 2019-05-08
 * Time: 14:28
 */
@Entity
@Table(name = "statusCodes")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class StatusCodesEntity {

    @Id
    private long statusCodeID;
    private String description;
    private String scenario;
    private int statusCode;

    /**
     * The Response template list.
     */
    @OneToMany
    @JoinColumn(name = "statusCodeID", insertable = false, updatable = false)
    List<ResponseTemplatesEntity> responseTemplateList;

}
