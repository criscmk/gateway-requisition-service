package com.cellulant.gatewayrequisitionservices.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Created by kamauwamatu
 * Project lookup-gateway-api
 * User: kamauwamatu
 * Date: 2019-03-22
 * Time: 12:01
 */

//Data from the database (api_gateway_db)
//Table name "contactInfo"

@Entity
@Table(name = "contactInfo")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ContactInfo {
    @Id
    private long contactInfoID;
    private int clientID;
    private int languageID;
    private String title;
    private String mission;
    private String vision;
    private String location;
    private String phone;
    private String fax;
    private String email;
    private String address;
    private String twitter;
    private String facebook;
    private String website;
    private String linkedIn;
    private String link;
    private String youtube;
    private String active;
    private String dateCreated;
    private String dateModified;

    @OneToOne
    @JoinColumn(name = "languageID", insertable = false, updatable = false)
    private LanguagesEntity languagesEntity;

}




