package com.cellulant.gatewayrequisitionservices.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by kamauwamatu
 * Project api-gateway-core
 * User: kamauwamatu
 * Date: 2019-05-08
 * Time: 13:14
 */
@Entity
@Table(name = "responseTemplates")
@Where(clause = "active='1'")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResponseTemplatesEntity {
    @Id
    private long responseID;
    private int languageID;
    private long statusCodeID;
    private String responseMessage;
}
