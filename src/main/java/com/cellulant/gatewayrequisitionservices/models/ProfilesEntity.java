package com.cellulant.gatewayrequisitionservices.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "profiles", schema = "api_gateway_db")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Where(clause = "active = '1'")
public class ProfilesEntity {
    @Id
    private int profileID;
    private String msisdn;
    private String uuid;
    private int activationRequestID;
    private String parseInstallationID;
    private String devicePlatform;
    private String deviceName;
    private int active;
}
