package com.cellulant.gatewayrequisitionservices.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Where;

import javax.persistence.*;


@Entity
@Table(name = "articles")
@Where(clause = "active='1'")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Products {
    @Id
    private int articleID;
    private int articleTypeID;
    private String articleTitle;
    private String article;
    private String articleUrl;
    private String imageUrl;
    private int languageID;
    private int active;
    private String dateCreated;
    private String dateModified;

    @OneToOne
    @JoinColumn(name = "languageID", insertable = false, updatable = false)
    private LanguagesEntity languagesEntity;
}