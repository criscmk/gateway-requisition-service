package com.cellulant.gatewayrequisitionservices.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by sylvester
 * Project transfers-gateway-api
 * User: sylvester
 * Date: 6/20/19
 * Time: 2:59 PM
 */
@Entity
@Table(name = "languages", schema = "api_gateway_db")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Where(clause = "active = '1'")
public class LanguagesEntity {
    @Id
    private int languageID;
    private String languageName;
    private String description;
    private String abbreviation;
}
