package com.cellulant.gatewayrequisitionservices.enaira.services;
import com.cellulant.gatewayrequisitionservices.balanceenquiry.requests.BalanceEnquiryRequest;
import com.cellulant.gatewayrequisitionservices.configurations.Constants;
import com.cellulant.gatewayrequisitionservices.dtos.responses.ResponseArray;
import com.cellulant.gatewayrequisitionservices.enaira.requests.*;
import com.cellulant.gatewayrequisitionservices.ministatement.requests.MiniStatementRequest;
import com.cellulant.gatewayrequisitionservices.models.ServicesEntity;
import com.cellulant.gatewayrequisitionservices.services.ServiceImpl;
import com.cellulant.gatewayrequisitionservices.utils.SharedUtilities;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.slf4j.MDC;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class ENairaService {

    @NonNull
    private final SharedUtilities sharedUtilities;

    @NonNull
    private final ServiceImpl serviceImpl;

    /**
     * Process enaira authenticate request mono.
     *
     * @param headerParams              the header params
     * @param eNairaAuthenticateRequest enaira authenticate request
     * @return the mono
     * @throws Exception the exception
     */
    public Mono<ResponseArray> processENairaAuthenticate(Map headerParams, ENairaAuthenticateRequest eNairaAuthenticateRequest) throws Exception {
        //Get action from headers
        final String action = sharedUtilities.retrieveActionFromHeaders(headerParams);
        final String uuid = String.valueOf(headerParams.get(Constants.UUID));
        //Retrieve the Service Details form Database
        final Mono<ServicesEntity> servicesEntityMono = serviceImpl.getServiceDetails((String.valueOf(headerParams.get(Constants.SERVICE))));


        return servicesEntityMono.flatMap(servicesEntity -> {
            // Get msisdn
            final Mono<String> msisdnMono = sharedUtilities.getMsisdn(headerParams, action, servicesEntity)
                    .doOnNext(msisdn -> MDC.put(Constants.MSISDN, String.valueOf(msisdn)));

            // Retrieve Wallet URL from Database using provided action
            final Mono<String> walletUrlMono = sharedUtilities.getWalletUrl(action, servicesEntity);

            final Mono<String> enairaAuthenticateRequestJsonMono = Mono.just(eNairaAuthenticateRequest)
                    .flatMap(eNairaAuthenticateRequest1 -> Mono.fromCallable(() -> {

                        // Formulate Wallet Payload
                        ObjectMapper objectMapper = new ObjectMapper();
                        objectMapper.disable(MapperFeature.USE_ANNOTATIONS);
                        return objectMapper.writeValueAsString(eNairaAuthenticateRequest1);
                    }));


            return Mono.zip(msisdnMono, walletUrlMono, enairaAuthenticateRequestJsonMono)
                    .flatMap(data -> {
                        final String msisdn = data.getT1();
                        final String walletUrl = data.getT2();
                        final String enairaAuthenticateRequestJson = data.getT3();

                        // update headerParams with updated msisdn
                        final Mono<Map> headerParamsMono = Mono.just(headerParams).map(map -> {
                            map.replace(Constants.MSISDN, msisdn);
                            return map;
                        });

                        return headerParamsMono.flatMap(headers -> {
                            final Mono<String> requestIDMono = sharedUtilities.getRequestID(eNairaAuthenticateRequest, msisdn, headers)
                                    .doOnNext(requestID -> MDC.put(Constants.CLOUD_REQUEST_ID, String.valueOf(requestID)));

                            //Validate Uuid
                            final Mono<Void> validateUUID = sharedUtilities.validateUUID(servicesEntity, msisdn, action, uuid);

                            return validateUUID.then(requestIDMono)
                                    .flatMap(requestID -> sharedUtilities
                                            .processIntegrationResponse(headers, msisdn, requestID, servicesEntity, walletUrl, enairaAuthenticateRequestJson));

                        });
                    });
        });

    }

    /**
     * Fetch hard token delivery modes mono.
     *
     * @param headerParams the header params
     * @return the mono
     * @throws Exception the exception
     */
    public Mono<ResponseArray> fetchENairaDeliveryModes(Map headerParams) throws Exception {
        //Get action from headers
        String action = sharedUtilities.retrieveActionFromHeaders(headerParams);
        //Retrieve the Service Details form Database
        final Mono<ServicesEntity> servicesEntityMono = serviceImpl.getServiceDetails((String.valueOf(headerParams.get(Constants.SERVICE))));

        return servicesEntityMono.flatMap(servicesEntity -> {
            // Get msisdn
            final Mono<String> msisdnMono = sharedUtilities.getMsisdn(headerParams, action, servicesEntity)
                    .doOnNext(msisdn -> MDC.put(Constants.MSISDN, String.valueOf(msisdn)));

            // Retrieve Wallet URL from Database using provided action
            final Mono<String> walletUrlMono = sharedUtilities.getWalletUrl(action, servicesEntity);


            return msisdnMono.flatMap(msisdn -> {
                // update headerParams with updated msisdn
                final Mono<Map> headerParamsMono = Mono.just(headerParams).map(map -> {
                    map.replace(Constants.MSISDN, msisdn);
                    return map;
                });

                return headerParamsMono.then(walletUrlMono)
                        .flatMap(sharedUtilities::getIntegrationLayerResponse)
                        .flatMap(sharedUtilities::handleResponse);
            });
        });
    }

    /**
     * Process enaira balance enquiry response array.
     *
     * @param headerParams                    the header params
     * @param eNairaBalanceEnquiryBulkRequest the balance enquiry request
     *                                        * @return the response array
     *                                        * @throwsxception the exception
     */
    public Mono<ResponseArray> processEnairaBalanceEnquiry(Map headerParams, ENairaBalanceEnquiryBulkRequest eNairaBalanceEnquiryBulkRequest) throws Exception {
        //Get action from headers
        final String action = sharedUtilities.retrieveActionFromHeaders(headerParams);
        final String uuid = String.valueOf(headerParams.get(Constants.UUID));
        //Retrieve the Service Details form Database
        final Mono<ServicesEntity> servicesEntityMono = serviceImpl.getServiceDetails((String.valueOf(headerParams.get(Constants.SERVICE))));
        return servicesEntityMono.flatMap(servicesEntity -> {
            // Get msisdn
            final Mono<String> msisdnMono = sharedUtilities.getMsisdn(headerParams, action, servicesEntity)
                    .doOnNext(msisdn -> MDC.put(Constants.MSISDN, String.valueOf(msisdn)));

            // Retrieve Wallet URL from Database using provided action
            final Mono<String> walletUrlMono = sharedUtilities.getWalletUrl(action, servicesEntity);

            final Mono<String> enairaBalanceEnquiryBulkRequestMono = Mono.just(eNairaBalanceEnquiryBulkRequest)
                    .flatMap(enairaBalanceEnquiryBulk -> Mono.fromCallable(() -> {

                        // Formulate Wallet Payload
                        ObjectMapper objectMapper = new ObjectMapper();
                        objectMapper.disable(MapperFeature.USE_ANNOTATIONS);
                        return objectMapper.writeValueAsString(enairaBalanceEnquiryBulk);
                    }));

            return Mono.zip(msisdnMono, walletUrlMono, enairaBalanceEnquiryBulkRequestMono)
                    .flatMap(data -> {
                        final String msisdn = data.getT1();
                        final String walletUrl = data.getT2();
                        final String enairaBalanceEnquiryBulkRequestJson = data.getT3();

                        // update headerParams with updated msisdn
                        final Mono<Map> headerParamsMono = Mono.just(headerParams).map(map -> {
                            map.replace(Constants.MSISDN, msisdn);
                            return map;
                        });

                        return headerParamsMono.flatMap(headers -> {
                            final Mono<String> requestIDMono = sharedUtilities.getRequestID(enairaBalanceEnquiryBulkRequestMono, msisdn, headers)
                                    .doOnNext(requestID -> MDC.put(Constants.CLOUD_REQUEST_ID, String.valueOf(requestID)));

                            //Validate Uuid
                            final Mono<Void> validateUUID = sharedUtilities.validateUUID(servicesEntity, msisdn, action, uuid);

                            return validateUUID.then(requestIDMono).flatMap(requestID -> sharedUtilities
                                    .processIntegrationResponse(headers, msisdn, requestID, servicesEntity, walletUrl, enairaBalanceEnquiryBulkRequestJson));
                        });
                    });


        });
    }

    /**
     * Fetch enaira mini statement response array.
     *
     * @param headerParams               the header params
     * @param eNairaMiniStatementRequest the enaira mini statement request
     * @return the response array
     * @throws Exception the exception
     */
    public Mono<ResponseArray> fetchMiniStatement(Map headerParams, ENairaMiniStatementRequest eNairaMiniStatementRequest) throws Exception {

        //Get action from headers
        final String action = sharedUtilities.retrieveActionFromHeaders(headerParams);
        final String uuid = String.valueOf(headerParams.get(Constants.UUID));
        //Retrieve the Service Details form Database
        final Mono<ServicesEntity> servicesEntityMono = serviceImpl.getServiceDetails((String.valueOf(headerParams.get(Constants.SERVICE))));

        return servicesEntityMono.flatMap(servicesEntity -> {
            // Get msisdn
            final Mono<String> msisdnMono = sharedUtilities.getMsisdn(headerParams, action, servicesEntity)
                    .doOnNext(msisdn -> MDC.put(Constants.MSISDN, String.valueOf(msisdn)));

            // Retrieve Wallet URL from Database using provided action
            final Mono<String> walletUrlMono = sharedUtilities.getWalletUrl(action, servicesEntity);

            // Generate a mini statement Request Json Mono
            final Mono<String> miniStatementRequestJsonMono = Mono.just(eNairaMiniStatementRequest)
                    .flatMap(eNairaMiniStatementRequest1 -> Mono.fromCallable(() -> {

                        // Formulate Wallet Payload
                        ObjectMapper objectMapper = new ObjectMapper();
                        objectMapper.disable(MapperFeature.USE_ANNOTATIONS);
                        return objectMapper.writeValueAsString(eNairaMiniStatementRequest1);
                    }));

            return Mono.zip(msisdnMono, walletUrlMono, miniStatementRequestJsonMono)
                    .flatMap(data -> {
                        final String msisdn = data.getT1();
                        final String walletUrl = data.getT2();
                        final String miniStatementRequestJson = data.getT3();

                        // update headerParams with updated msisdn
                        final Mono<Map> headerParamsMono = Mono.just(headerParams).map(map -> {
                            map.replace(Constants.MSISDN, msisdn);
                            return map;
                        });

                        return headerParamsMono.flatMap(headers -> {
                            final Mono<String> requestIDMono = sharedUtilities.getRequestID(eNairaMiniStatementRequest, msisdn, headers)
                                    .doOnNext(requestID -> MDC.put(Constants.CLOUD_REQUEST_ID, String.valueOf(requestID)));

                            //Validate Uuid
                            final Mono<Void> validateUUID = sharedUtilities.validateUUID(servicesEntity, msisdn, action, uuid);

                            return validateUUID.then(requestIDMono)
                                    .flatMap(requestID -> sharedUtilities
                                            .processIntegrationResponse(headerParams, msisdn, requestID, servicesEntity, walletUrl, miniStatementRequestJson));
                        });
                    });

        });

    }

    /**
     * process enaira preauthorization response array.
     *
     * @param headerParams                  the header params
     * @param eNairaPreAuthorizationRequest the enaira preauthorization request
     * @return the response array
     * @throws Exception the exception
     */

    public Mono<ResponseArray> processEnairaPreAuthorization(Map headerParams, ENairaPreAuthorizationRequest eNairaPreAuthorizationRequest) throws Exception {
        //Get action from headers
        final String action = sharedUtilities.retrieveActionFromHeaders(headerParams);
        final String uuid = String.valueOf(headerParams.get(Constants.UUID));
        //Retrieve the Service Details form Database
        final Mono<ServicesEntity> servicesEntityMono = serviceImpl.getServiceDetails((String.valueOf(headerParams.get(Constants.SERVICE))));

        return servicesEntityMono.flatMap(servicesEntity -> {
            // Get msisdn
            final Mono<String> msisdnMono = sharedUtilities.getMsisdn(headerParams, action, servicesEntity)
                    .doOnNext(msisdn -> MDC.put(Constants.MSISDN, String.valueOf(msisdn)));

            // Retrieve Wallet URL from Database using provided action
            final Mono<String> walletUrlMono = sharedUtilities.getWalletUrl(action, servicesEntity);

            final Mono<String> enairaPreAuthorizationRequestMono = Mono.just(eNairaPreAuthorizationRequest)
                    .flatMap(eNairaPreAuthorizationRequest1 -> Mono.fromCallable(() -> {
                        // Formulate Wallet Payload
                        ObjectMapper objectMapper = new ObjectMapper();
                        objectMapper.disable(MapperFeature.USE_ANNOTATIONS);
                        return objectMapper.writeValueAsString(eNairaPreAuthorizationRequest1);
                    }));


            return Mono.zip(msisdnMono, walletUrlMono, enairaPreAuthorizationRequestMono)
                    .flatMap(data -> {
                        final String msisdn = data.getT1();
                        final String walletUrl = data.getT2();
                        final String enairaPreAuthorizationRequestJson = data.getT3();

                        // update headerParams with updated msisdn
                        final Mono<Map> headerParamsMono = Mono.just(headerParams).map(map -> {
                            map.replace(Constants.MSISDN, msisdn);
                            return map;
                        });

                        return headerParamsMono.flatMap(headers -> {
                            final Mono<String> requestIDMono = sharedUtilities.getRequestID(eNairaPreAuthorizationRequest, msisdn, headers)
                                    .doOnNext(requestID -> MDC.put(Constants.CLOUD_REQUEST_ID, String.valueOf(requestID)));

                            //Validate Uuid
                            final Mono<Void> validateUUID = sharedUtilities.validateUUID(servicesEntity, msisdn, action, uuid);

                            return validateUUID.then(requestIDMono).flatMap(requestID -> sharedUtilities
                                    .processIntegrationResponse(headers, msisdn, requestID, servicesEntity, walletUrl, enairaPreAuthorizationRequestJson));
                        });
                    });
        });


    }

    /**
     * process enaira enaira name inquiry response array.
     *
     * @param headerParams                  the header params
     * @param eNairaNameInquiryRequest the enaira name inquiry request
     * @return the response array
     * @throws Exception the exception
     */

    public Mono<ResponseArray> processNameInquiry(Map headerParams, ENairaNameInquiryRequest eNairaNameInquiryRequest) throws Exception {
        //Get action from headers
        final String action = sharedUtilities.retrieveActionFromHeaders(headerParams);
        final String uuid = String.valueOf(headerParams.get(Constants.UUID));
        //Retrieve the Service Details form Database
        final Mono<ServicesEntity> servicesEntityMono = serviceImpl.getServiceDetails((String.valueOf(headerParams.get(Constants.SERVICE))));

        return servicesEntityMono.flatMap(servicesEntity -> {
            // Get msisdn
            final Mono<String> msisdnMono = sharedUtilities.getMsisdn(headerParams, action, servicesEntity)
                    .doOnNext(msisdn -> MDC.put(Constants.MSISDN, String.valueOf(msisdn)));

            // Retrieve Wallet URL from Database using provided action
            final Mono<String> walletUrlMono = sharedUtilities.getWalletUrl(action, servicesEntity);

            final Mono<String> enairaNameInquiryRequestMono = Mono.just(eNairaNameInquiryRequest)
                    .flatMap(eNairaPreAuthorizationRequest1 -> Mono.fromCallable(() -> {
                        // Formulate Wallet Payload
                        ObjectMapper objectMapper = new ObjectMapper();
                        objectMapper.disable(MapperFeature.USE_ANNOTATIONS);
                        return objectMapper.writeValueAsString(eNairaPreAuthorizationRequest1);
                    }));


            return Mono.zip(msisdnMono, walletUrlMono, enairaNameInquiryRequestMono)
                    .flatMap(data -> {
                        final String msisdn = data.getT1();
                        final String walletUrl = data.getT2();
                        final String enairaPreAuthorizationRequestJson = data.getT3();

                        // update headerParams with updated msisdn
                        final Mono<Map> headerParamsMono = Mono.just(headerParams).map(map -> {
                            map.replace(Constants.MSISDN, msisdn);
                            return map;
                        });

                        return headerParamsMono.flatMap(headers -> {
                            final Mono<String> requestIDMono = sharedUtilities.getRequestID(eNairaNameInquiryRequest, msisdn, headers)
                                    .doOnNext(requestID -> MDC.put(Constants.CLOUD_REQUEST_ID, String.valueOf(requestID)));

                            //Validate Uuid
                            final Mono<Void> validateUUID = sharedUtilities.validateUUID(servicesEntity, msisdn, action, uuid);

                            return validateUUID.then(requestIDMono).flatMap(requestID -> sharedUtilities
                                    .processIntegrationResponse(headers, msisdn, requestID, servicesEntity, walletUrl, enairaPreAuthorizationRequestJson));
                        });
                    });
        });

    }

    /**
     * Fetch enaira transfer  modes mono.
     *
     * @param headerParams the header params
     * @return the mono
     * @throws Exception the exception
     */
    public Mono<ResponseArray> fetchENairaTransferModes(Map headerParams) throws Exception {
        //Get action from headers
        String action = sharedUtilities.retrieveActionFromHeaders(headerParams);
        //Retrieve the Service Details form Database
        final Mono<ServicesEntity> servicesEntityMono = serviceImpl.getServiceDetails((String.valueOf(headerParams.get(Constants.SERVICE))));

        return servicesEntityMono.flatMap(servicesEntity -> {
            // Get msisdn
            final Mono<String> msisdnMono = sharedUtilities.getMsisdn(headerParams, action, servicesEntity)
                    .doOnNext(msisdn -> MDC.put(Constants.MSISDN, String.valueOf(msisdn)));

            // Retrieve Wallet URL from Database using provided action
            final Mono<String> walletUrlMono = sharedUtilities.getWalletUrl(action, servicesEntity);

            return msisdnMono.flatMap(msisdn -> {
                // update headerParams with updated msisdn
                final Mono<Map> headerParamsMono = Mono.just(headerParams).map(map -> {
                    map.replace(Constants.MSISDN, msisdn);
                    return map;
                });

                return headerParamsMono.then(walletUrlMono)
                        .flatMap(sharedUtilities::getIntegrationLayerResponse)
                        .flatMap(sharedUtilities::handleResponse);

            });
        });
    }
    /**
     * Fetch enaira destination options mono.
     *
     * @param headerParams the header params
     * @return the mono
     * @throws Exception the exception
     */
    public Mono<ResponseArray> fetchENairaDestinationOptions(Map headerParams) throws Exception {
        //Get action from headers
        String action = sharedUtilities.retrieveActionFromHeaders(headerParams);
        //Retrieve the Service Details form Database
        final Mono<ServicesEntity> servicesEntityMono = serviceImpl.getServiceDetails((String.valueOf(headerParams.get(Constants.SERVICE))));

        return servicesEntityMono.flatMap(servicesEntity -> {
            // Get msisdn
            final Mono<String> msisdnMono = sharedUtilities.getMsisdn(headerParams, action, servicesEntity)
                    .doOnNext(msisdn -> MDC.put(Constants.MSISDN, String.valueOf(msisdn)));

            // Retrieve Wallet URL from Database using provided action
            final Mono<String> walletUrlMono = sharedUtilities.getWalletUrl(action, servicesEntity);

            return msisdnMono.flatMap(msisdn -> {
                // update headerParams with updated msisdn
                final Mono<Map> headerParamsMono = Mono.just(headerParams).map(map -> {
                    map.replace(Constants.MSISDN, msisdn);
                    return map;
                });

                return headerParamsMono.then(walletUrlMono)
                        .flatMap(sharedUtilities::getIntegrationLayerResponse)
                        .flatMap(sharedUtilities::handleResponse);

            });
        });
    }

}
