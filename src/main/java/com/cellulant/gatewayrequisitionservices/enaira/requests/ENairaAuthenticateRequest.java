package com.cellulant.gatewayrequisitionservices.enaira.requests;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ENairaAuthenticateRequest {
    @NotNull(message = "account Alias is required")
    private String accountAlias;
    @NotNull(message = "username is required")
    private String username;
    @NotNull(message = "password is required")
    private String password;
}
