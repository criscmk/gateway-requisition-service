package com.cellulant.gatewayrequisitionservices.enaira.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ENairaBalanceEnquiryBulkRequest implements Serializable {

    @NotNull(message = "{accountAlias.required}")
    private String accountAlias;

    @NotNull(message = "{authorizationToken.required}")
    private String authorizationToken;

}
