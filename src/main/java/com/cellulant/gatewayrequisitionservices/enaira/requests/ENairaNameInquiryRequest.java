package com.cellulant.gatewayrequisitionservices.enaira.requests;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class ENairaNameInquiryRequest implements Serializable {

    @NotNull(message = "accountAlias is required")
    private String accountAlias;
    @NotNull(message = "accountInfo is required")
    private String accountInfo;
}
