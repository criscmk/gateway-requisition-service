package com.cellulant.gatewayrequisitionservices.enaira.requests;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ENairaPreAuthorizationRequest implements Serializable {

    @NotNull(message = "accountAlias is required")
    String accountAlias;

    @NotNull(message = "authorizationToken is required")
    String authorizationToken;
}
