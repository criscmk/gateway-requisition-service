package com.cellulant.gatewayrequisitionservices.products.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by kamauwamatu
 * Project lookup-gateway-api
 * User: kamauwamatu
 * Date: 2019-06-16
 * Time: 23:47
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RespProducts {

    private String productTitle;
    private String productDescription;
    private String imageUrl;
    private String date;
    private String learnMore;

}
