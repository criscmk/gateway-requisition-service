package com.cellulant.gatewayrequisitionservices.inappbanner.services;

import com.cellulant.gatewayrequisitionservices.configurations.Configs;
import com.cellulant.gatewayrequisitionservices.configurations.Constants;
import com.cellulant.gatewayrequisitionservices.dtos.responses.ResponseArray;
import com.cellulant.gatewayrequisitionservices.models.ServicesEntity;
import com.cellulant.gatewayrequisitionservices.services.ServiceImpl;
import com.cellulant.gatewayrequisitionservices.utils.SharedUtilities;
import lombok.RequiredArgsConstructor;
import org.slf4j.MDC;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import java.util.Map;


@Service
@RequiredArgsConstructor
public class InAppBannerService {
    private final SharedUtilities sharedUtilities;

    private final ServiceImpl serviceImpl;

    private final Configs configs;

    public Mono<ResponseArray> fetchActiveBanner(Map headerParams) throws Exception {

        //Get action from headers
        final String action = sharedUtilities.retrieveActionFromHeaders(headerParams);
        //Retrieve the Service Details form Database

        final Mono<ServicesEntity> servicesEntityMono = serviceImpl.getServiceDetails((String.valueOf(headerParams.get(Constants.SERVICE))));
        return servicesEntityMono.flatMap(servicesEntity -> {
            // Get msisdn
            final Mono<String> msisdnMono = sharedUtilities.getMsisdn(headerParams, action, servicesEntity)
                    .doOnNext(msisdn -> MDC.put(Constants.MSISDN, String.valueOf(msisdn)));

            // Retrieve Wallet URL from Database using provided action
            final Mono<String> walletUrlMono = sharedUtilities.getWalletUrl(action, servicesEntity);

            return Mono.zip(msisdnMono, walletUrlMono).flatMap(data -> {

                final String msisdn = data.getT1();
                final String walletUrl = data.getT2();

                // update headerParams with updated msisdn
                final Mono<Map> headerParamsMono = Mono.just(headerParams).map(map -> {
                    map.replace(Constants.MSISDN, msisdn);
                    return map;
                });

                return headerParamsMono.flatMap(headers -> {
                    final Mono<String> requestIDMono = sharedUtilities.getRequestID(null, msisdn, headers)
                            .doOnNext(requestID -> MDC.put(Constants.CLOUD_REQUEST_ID, String.valueOf(requestID)));

                    return requestIDMono
                            .flatMap(requestID -> sharedUtilities.getIntegrationLayerResponse(walletUrl))
                            .flatMap(sharedUtilities::handleResponse);
                });
            });
        });

    }
}
