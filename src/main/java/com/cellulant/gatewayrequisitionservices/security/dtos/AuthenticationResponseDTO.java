package com.cellulant.gatewayrequisitionservices.security.dtos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthenticationResponseDTO {
	private ResultDTO<UserDetailsDTO> result;
	private String statusMessage;
	private Integer statusCode;
}
