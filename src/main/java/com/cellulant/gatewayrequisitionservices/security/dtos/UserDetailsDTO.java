package com.cellulant.gatewayrequisitionservices.security.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDetailsDTO implements UserDetails {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private Integer userID;
	private String sourceIPAddress;
	private String userName;
	private String email;
	private List<RolesDTO> roles;

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return roles;
	}
	
	@Override
	public String getPassword()
	{
		return null;
	}
	@Override
	public String getUsername()
	{
		return userName;
	}
	@Override
	public boolean isAccountNonExpired() 
	{
		return false;
	}
	@Override
	public boolean isAccountNonLocked() 
	{
		return false;
	}
	@Override
	public boolean isCredentialsNonExpired() 
	{
		return false;
	}
	@Override
	public boolean isEnabled() 
	{
		return true;
	}
}
