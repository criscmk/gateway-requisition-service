package com.cellulant.gatewayrequisitionservices.security.dtos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TokenDTO {
	private String token;
}
