package com.cellulant.gatewayrequisitionservices.security.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResultDTO<T> {
    private String message;
    private String referenceID = "0";
    private List<T> data = new ArrayList<>();

    public ResultDTO(String message) {
        this.message = message;
    }

    public ResultDTO(String message, List<T> data) {
        this.message = message;
        this.data = data;
    }
}

