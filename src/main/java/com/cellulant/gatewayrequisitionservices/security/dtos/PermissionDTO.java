package com.cellulant.gatewayrequisitionservices.security.dtos;

import org.springframework.security.core.GrantedAuthority;

public class PermissionDTO implements GrantedAuthority
{

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private String moduleName;
	private String entityActionGroupName;
	private String entityActionPowerName;

	// Constructor
	public PermissionDTO() {

	}

	public PermissionDTO(
			String moduleName,
			String entityActionGroupName,
			String entityActionPowerName
	) {
		this.moduleName = moduleName;
		this.entityActionPowerName = entityActionPowerName;
		this.entityActionGroupName = entityActionGroupName;
	}

	public String getEntityActionPowerName() {
		return entityActionPowerName;
	}

	public void setEntityActionPowerName(String entityActionPowerName) {
		this.entityActionPowerName = entityActionPowerName;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getEntityActionGroupName() {
		return entityActionGroupName;
	}

	public void setEntityActionGroupName(String entityActionGroupName) {
		this.entityActionGroupName = entityActionGroupName;
	}

	@Override
	public String getAuthority() {
		if (moduleName == null || moduleName.isBlank()) {
			return null;
		}
		return "ROLE_" + moduleName;
	}

}
