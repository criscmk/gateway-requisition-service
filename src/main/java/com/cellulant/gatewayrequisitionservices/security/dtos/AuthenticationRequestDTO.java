package com.cellulant.gatewayrequisitionservices.security.dtos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthenticationRequestDTO
{
	private String userName;
	private String password;
	private String sourceIPAddress;

}
