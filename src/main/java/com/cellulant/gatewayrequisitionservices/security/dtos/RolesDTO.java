package com.cellulant.gatewayrequisitionservices.security.dtos;

import org.springframework.security.core.GrantedAuthority;

public class RolesDTO implements GrantedAuthority
{
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private Integer moduleID;
	private String moduleName;
	private Integer entityActionID;
	private String entityActionGroupName;
	private String entityActionPowerName;

	public RolesDTO() {

	}

	public Integer getModuleID() {
		return moduleID;
	}

	public void setModuleID(Integer moduleID) {
		this.moduleID = moduleID;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public Integer getEntityActionID() {
		return entityActionID;
	}

	public void setEntityActionID(Integer entityActionID) {
		this.entityActionID = entityActionID;
	}

	public String getEntityActionGroupName() {
		return entityActionGroupName;
	}

	public void setEntityActionGroupName(String entityActionGroupName) {
		this.entityActionGroupName = entityActionGroupName;
	}

	public String getEntityActionPowerName() {
		return entityActionPowerName;
	}

	public void setEntityActionPowerName(String entityActionPowerName) {
		this.entityActionPowerName = entityActionPowerName;
	}

	@Override
	public String getAuthority() {
		if (moduleName == null || moduleName.isBlank()) {
			return null;
		}
		return "ROLE_" + moduleName;
	}
	
	
	
}
