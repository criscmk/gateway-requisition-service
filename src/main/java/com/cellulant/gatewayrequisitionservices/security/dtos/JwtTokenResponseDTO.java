package com.cellulant.gatewayrequisitionservices.security.dtos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JwtTokenResponseDTO {
	private ResultDTO<TokenDTO> result;
	private String statusMessage;
	private Integer statusCode;
}
