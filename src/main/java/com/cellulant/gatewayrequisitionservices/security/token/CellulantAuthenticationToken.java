package com.cellulant.gatewayrequisitionservices.security.token;

import com.cellulant.gatewayrequisitionservices.security.dtos.PermissionDTO;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.List;

@Getter
@Setter
public class CellulantAuthenticationToken
		extends UsernamePasswordAuthenticationToken
{
	private static final long serialVersionUID = 1L;
	private String sourceIPAddress;
	private List<PermissionDTO> requestedPermissions;

	public CellulantAuthenticationToken(
			String sourceIPAddress,
			Object principal,
			Object credentials,
			Collection<? extends GrantedAuthority> authorities
	) {
		super(principal, credentials, authorities);
		this.sourceIPAddress = sourceIPAddress;
	}

	public CellulantAuthenticationToken(
			String sourceIPAddress,
			Object principal,
			Object credentials
	) {
		super(principal, credentials);
		this.sourceIPAddress = sourceIPAddress;
	}
}
