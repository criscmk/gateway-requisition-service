package com.cellulant.gatewayrequisitionservices.security.token;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

@Getter
@Setter
public class CellulantJwtAuthenticationToken extends AbstractAuthenticationToken
{
	private static final long serialVersionUID = 1L;
	private String sourceIPAddress;
	private String token;
	private String userName;
	private UserDetails principal;
	

	public CellulantJwtAuthenticationToken(
			String sourceIPAddress,
			String token,
			UserDetails principal,
			Collection<? extends GrantedAuthority> authorities
	)
	{
		super(authorities);
		this.principal = principal;
		this.token = token;
		this.sourceIPAddress = sourceIPAddress;
		super.setAuthenticated(true);
	}

	public CellulantJwtAuthenticationToken(
				String sourceIPAddress,
				String token
	)
	{
		super(null);
		this.token = token;
		this.sourceIPAddress = sourceIPAddress;
		setAuthenticated(false);
	}

	@Override
	public Object getCredentials() {

		return null;
	}

	@Override
	public Object getPrincipal() {
		return this.principal;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString()).append(": ");
		sb.append("UserName: ").append(getUserName()).append("; ");
		sb.append("Authenticated: ").append(this.isAuthenticated()).append("; ");
		sb.append("Details: ").append(this.getDetails()).append("; ");

		if (!getAuthorities().isEmpty()) {
			sb.append("Granted Authorities: ");

			int i = 0;
			for (GrantedAuthority authority : getAuthorities()) {
				if (i++ > 0) {
					sb.append(", ");
				}

				sb.append(authority);
			}
		} else {
			sb.append("Not granted any authorities");
		}

		return sb.toString();
	}
}
