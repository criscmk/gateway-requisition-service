package com.cellulant.gatewayrequisitionservices.security.managers;

import com.cellulant.gatewayrequisitionservices.configurations.Configs;
import com.cellulant.gatewayrequisitionservices.security.dtos.AuthenticationRequestDTO;
import com.cellulant.gatewayrequisitionservices.security.dtos.AuthenticationResponseDTO;
import com.cellulant.gatewayrequisitionservices.security.dtos.RolesDTO;
import com.cellulant.gatewayrequisitionservices.security.dtos.UserDetailsDTO;
import com.cellulant.gatewayrequisitionservices.security.token.CellulantAuthenticationToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Denis Gitonga
 */
public class CellulantAuthenticationManager implements ReactiveAuthenticationManager {

    private final Logger logger = LoggerFactory.getLogger(CellulantAuthenticationManager.class);

    private final WebClient webClient;
    private final Configs configs;

    public CellulantAuthenticationManager(WebClient.Builder builder, Configs configs) {
        this.webClient = builder.build();
        this.configs = configs;
    }

    @Override
    public Mono<Authentication> authenticate(Authentication authentication) {
        logger.info("cellulant provider for basic auth");
        // convert to our specific authentication request type
        final CellulantAuthenticationToken auth = (CellulantAuthenticationToken) authentication;
        logger.info("About to make authentication call to auth service");
        logger.info("User to authenticate: {}", auth.getPrincipal().toString());

        final AuthenticationRequestDTO request = new AuthenticationRequestDTO();
        request.setPassword(auth.getCredentials().toString());
        request.setUserName(auth.getPrincipal().toString());
        request.setSourceIPAddress(auth.getSourceIPAddress());

        return webClient.post()
                .uri(configs.getAuthenticationService())
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(request)
                .retrieve()
                .bodyToMono(AuthenticationResponseDTO.class)
                .map(authResponse -> {
                    logger.debug("received authentication status code: {}", authResponse.getStatusCode());

                    return this.buildAuthenticationToken(authResponse, auth.getSourceIPAddress());
                })
                .onErrorResume(e -> {
                    logger.error("exception during authentication: {}", e.getMessage());

                    return Mono.error(new AuthenticationServiceException(e.getMessage()));
                })
                .cast(Authentication.class);
    }

    private CellulantAuthenticationToken buildAuthenticationToken(AuthenticationResponseDTO authResponse, String sourceIPAddress) {
        if (authResponse.getStatusCode().equals(configs.getAuthenticationSuccessStatusCode())) {
            logger.debug("received successful authentication status code");
            logger.debug("creating new authentication principal");
            UserDetailsDTO user = this.mapUserDetails(authResponse);
            final CellulantAuthenticationToken successfulAuthentication = new CellulantAuthenticationToken(
                    sourceIPAddress,
                    user,
                    "",
                    this.mapGrantedAuthorities(user.getRoles())
            );
            logger.debug("created authentication pricipal: {}", successfulAuthentication);

            return successfulAuthentication;
        } else {
            logger.error("Authentication failed: {}", authResponse.getStatusMessage());
            throw new BadCredentialsException(authResponse.getStatusMessage());
        }
    }

    private List<GrantedAuthority> mapGrantedAuthorities(
            List<RolesDTO> roles
    ) {
        List<GrantedAuthority> authorities = roles.stream()
                .map(role -> new SimpleGrantedAuthority(role.getAuthority()))
                .collect(Collectors.toList());

        logger.info("Roles mapped from authentication response: {}",
                authorities);
        return authorities;
    }

    private UserDetailsDTO mapUserDetails(
            AuthenticationResponseDTO authResponse
    ) {
        List<UserDetailsDTO> users = authResponse.getResult().getData();
        List<RolesDTO> roles = Optional.ofNullable(users.get(0).getRoles())
                .orElse(Collections.emptyList());
        List<GrantedAuthority> authorities = roles.stream()
                .map(role -> new SimpleGrantedAuthority(role.getAuthority()))
                .collect(Collectors.toList());

        logger.debug("Roles mapped from authentication response: {}",
                authorities);
        UserDetailsDTO user = new UserDetailsDTO();
        user.setRoles(roles);
        user.setEmail(users.get(0).getEmail());
        user.setUserID(users.get(0).getUserID());
        user.setUserName(users.get(0).getUsername());

        return user;
    }
}
