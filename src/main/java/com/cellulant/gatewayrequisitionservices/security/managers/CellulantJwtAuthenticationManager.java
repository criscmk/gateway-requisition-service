package com.cellulant.gatewayrequisitionservices.security.managers;

import com.cellulant.gatewayrequisitionservices.configurations.Configs;
import com.cellulant.gatewayrequisitionservices.security.dtos.RolesDTO;
import com.cellulant.gatewayrequisitionservices.security.dtos.UserDetailsDTO;
import com.cellulant.gatewayrequisitionservices.security.token.CellulantJwtAuthenticationToken;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.core.Authentication;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Denis Gitonga
 */
public class CellulantJwtAuthenticationManager implements ReactiveAuthenticationManager {
    private final Logger logger = LoggerFactory.getLogger(CellulantJwtAuthenticationManager.class);

    private final Configs configs;

    public CellulantJwtAuthenticationManager(Configs configs) {
        this.configs = configs;
    }

    @Override
    public Mono<Authentication> authenticate(Authentication authentication) {
        return Mono.fromCallable(() -> {
            logger.info("jwt provider to authentication and authorization request");
            // convert to our specific authentication request type
            CellulantJwtAuthenticationToken auth = (CellulantJwtAuthenticationToken) authentication;
            String jwt = auth.getToken();

            try {
                final Claims claims = Jwts.parser()
                        .setSigningKey(configs.getJwtSecret().getBytes())
                        .parseClaimsJws(jwt).getBody();

                logger.debug("ID: " + claims.getId());
                logger.debug("Subject: " + claims.getSubject());
                logger.debug("Issuer: " + claims.getIssuer());
                logger.debug("Expiration: " + claims.getExpiration());

                final ArrayList<Object> roles = (ArrayList<Object>) claims.get("roles");

                final List<RolesDTO> rolesDTO = roles.stream().map(role -> {
                    LinkedHashMap<String, Object> keys = (LinkedHashMap<String, Object>) role;
                    RolesDTO roleDTO = new RolesDTO();
                    keys.forEach((key, value) -> {
                        if (key.equals("moduleID")) {
                            roleDTO.setModuleID((Integer) value);
                        }
                        if (key.equals("moduleName")) {
                            roleDTO.setModuleName((String) value);
                        }
                        if (key.equals("entityActionID")) {
                            roleDTO.setEntityActionID((Integer) value);
                        }
                        if (key.equals("entityActionGroupName")) {
                            roleDTO.setEntityActionGroupName((String) value);
                        }
                        if (key.equals("entityActionPowerName")) {
                            roleDTO.setEntityActionPowerName((String) value);
                        }
                    });

                    return roleDTO;
                }).collect(Collectors.toList());

                UserDetailsDTO user = new UserDetailsDTO();
                user.setEmail(claims.get("email", String.class));
                user.setUserID(claims.get("userId", Integer.class));
                user.setUserName(claims.getSubject());
                user.setRoles(rolesDTO);

                logger.debug("authentication process completed");
                return new CellulantJwtAuthenticationToken(
                        auth.getSourceIPAddress(),
                        jwt,
                        user,
                        rolesDTO
                );
            } catch (JwtException | ClassCastException e) {
                logger.error("Exception during JWT validation: " + e.getMessage());
                throw new AuthenticationServiceException(e.getMessage());
            }
        });
    }
}
