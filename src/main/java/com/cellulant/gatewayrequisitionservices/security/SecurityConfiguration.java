package com.cellulant.gatewayrequisitionservices.security;


import com.cellulant.gatewayrequisitionservices.configurations.Configs;
import com.cellulant.gatewayrequisitionservices.security.filters.CellulantAuthenticationFilter;
import com.cellulant.gatewayrequisitionservices.security.filters.CellulantJwtAuthenticationFilter;
import com.cellulant.gatewayrequisitionservices.security.providers.CellulantAuthenticationProvider;
import com.cellulant.gatewayrequisitionservices.security.providers.CellulantJwtAuthenticationProvider;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.client.RestTemplate;

@Configuration
@EnableWebSecurity
@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
	private Logger logger = LoggerFactory.getLogger(SecurityConfiguration.class);
	private Configs configs;
	private RestTemplate restTemplate;
	private ObjectMapper objectMapper;

	public SecurityConfiguration(
			Configs configs,
			ObjectMapper objectMapper,
			RestTemplate restTemplate
	) {
		this.configs = configs;
		this.objectMapper = objectMapper;
		this.restTemplate = restTemplate;
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		logger.debug("configuring HttpSecurity for endpoint security");
		// @formatter: off


		http.authorizeRequests()
				.antMatchers("/actuator/health").permitAll()
				.antMatchers("/**").access("hasRole('gateway')")
				.anyRequest()
				.authenticated()
				.and()
				.authenticationProvider(getProvider())
				.addFilterBefore(preAuthenticationFilter(), BasicAuthenticationFilter.class)
				.httpBasic()
				.and()
				.authenticationProvider(getJwtProvider())
				.addFilterBefore(preJwtAuthenticationFilter(), CellulantAuthenticationFilter.class)
				.httpBasic()
				.and()
				.headers().disable()
				.csrf().disable()
				.formLogin().disable()
				.logout().disable()
				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
		;

	}

	@Bean
	public CellulantAuthenticationFilter preAuthenticationFilter()
			throws Exception {
		logger.debug("creating the cellulant authentication filter to be used");
		return new CellulantAuthenticationFilter(
				authenticationManagerBean(),
				getEntryPoint()
		);
	}

	@Bean
	public CellulantJwtAuthenticationFilter preJwtAuthenticationFilter()
			throws Exception {
		logger.debug("creating the cellulant jwt authentication filter to be used");
		return new CellulantJwtAuthenticationFilter(
				authenticationManagerBean(),
				getEntryPoint()
		);
	}

	@Bean
	public CellulantJwtAuthenticationProvider getJwtProvider() //throws Exception
	{
		logger.debug("creating the cellulant JWT authentication provider to be used");
		return new CellulantJwtAuthenticationProvider(configs);
	}

	@Bean
	public CellulantAuthenticationProvider getProvider() //throws Exception
	{
		logger.debug("creating the cellulant authentication provider to be used");
		return new CellulantAuthenticationProvider(
				restTemplate,
				configs);
	}

	@Bean
	public CellulantAuthenticationEntryPoint getEntryPoint() //throws Exception
	{
		logger.debug("creating the cellulant authentication entry point to be used");
		return new CellulantAuthenticationEntryPoint(configs, objectMapper);
	}
}
