package com.cellulant.gatewayrequisitionservices.security.providers;

import com.cellulant.gatewayrequisitionservices.configurations.Configs;
import com.cellulant.gatewayrequisitionservices.security.dtos.AuthenticationRequestDTO;
import com.cellulant.gatewayrequisitionservices.security.dtos.AuthenticationResponseDTO;
import com.cellulant.gatewayrequisitionservices.security.dtos.RolesDTO;
import com.cellulant.gatewayrequisitionservices.security.dtos.UserDetailsDTO;
import com.cellulant.gatewayrequisitionservices.security.token.CellulantAuthenticationToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.stream.Collectors;

public class CellulantAuthenticationProvider implements AuthenticationProvider {
	private RestTemplate restTemplate;
	private Configs configs;

	private Logger logger = LoggerFactory
			.getLogger(CellulantAuthenticationProvider.class);

	public CellulantAuthenticationProvider(
			RestTemplate restTemplate,
			Configs configs
	) {
		this.configs = configs;
		this.restTemplate = restTemplate;
	}


	@Override
	public Authentication authenticate(Authentication authentication) {
		logger.info("cellulant provider for basic auth");
		// convert to our specific authentication request type
		CellulantAuthenticationToken auth =
				(CellulantAuthenticationToken) authentication;
		logger.info("About to make authentication call to auth service");
		logger.info("User to authenticate: {}", auth.getPrincipal().toString());

		AuthenticationRequestDTO request = new AuthenticationRequestDTO();
		request.setPassword(auth.getCredentials().toString());
		request.setUserName(auth.getPrincipal().toString());
		request.setSourceIPAddress(auth.getSourceIPAddress());

		Authentication successfulAuthentication = null;

		try {
			logger.info("about to make auth call");

			AuthenticationResponseDTO authResponse = restTemplate.postForObject(
					configs.getAuthenticationService(),
					request,
					AuthenticationResponseDTO.class);

			logger.debug("received authentication status code: {}",
					authResponse.getStatusCode());
			if (authResponse.getStatusCode().equals(configs.getAuthenticationSuccessStatusCode())) {
				logger.debug("received successful authentication status code");
				logger.debug("creating new authentication pricipal");
				UserDetailsDTO user = this.mapUserDetails(authResponse);
				successfulAuthentication = new CellulantAuthenticationToken(
						auth.getSourceIPAddress(),
						user,
						"",
						this.mapGrantedAuthorities(user.getRoles())
				);
				logger.debug("created authentication pricipal: {}", successfulAuthentication);
			} else {
				logger.error("Authentication failed: {}", authResponse.getStatusMessage());
				throw new BadCredentialsException(authResponse.getStatusMessage());
			}

			logger.debug("authentication result: {}", successfulAuthentication.isAuthenticated());
			return successfulAuthentication;

		} catch (Exception e) {
			logger.error("exception during authentication: {}",
					e.getMessage());
			throw new AuthenticationServiceException(e.getMessage());
		}
	}

	@Override
	public boolean supports(Class<?> authentication) {
		logger.debug("The token class type is: {}", authentication.getName());
		return authentication.equals(CellulantAuthenticationToken.class);
	}

	private List<GrantedAuthority> mapGrantedAuthorities(
			List<RolesDTO> roles
	) {
		List<GrantedAuthority> authorities = roles.stream()
				.map(role -> new SimpleGrantedAuthority(role.getAuthority()))
				.collect(Collectors.toList());

		logger.info("Roles mapped from authentication response: {}",
				authorities);
		return authorities;
	}

	private UserDetailsDTO mapUserDetails(
			AuthenticationResponseDTO authResponse
	) {
		List<UserDetailsDTO> users = authResponse.getResult().getData();
		List<RolesDTO> roles = users.get(0).getRoles();
		List<GrantedAuthority> authorities = roles.stream()
				.map(role -> new SimpleGrantedAuthority(role.getAuthority()))
				.collect(Collectors.toList());

		logger.debug("Roles mapped from authentication response: {}",
				authorities);
		UserDetailsDTO user = new UserDetailsDTO();
		user.setRoles(roles);
		user.setEmail(users.get(0).getEmail());
		user.setUserID(users.get(0).getUserID());
		user.setUserName(users.get(0).getUsername());

		return user;
	}
}
