package com.cellulant.gatewayrequisitionservices.security.providers;


import com.cellulant.gatewayrequisitionservices.configurations.Configs;
import com.cellulant.gatewayrequisitionservices.security.dtos.RolesDTO;
import com.cellulant.gatewayrequisitionservices.security.dtos.UserDetailsDTO;
import com.cellulant.gatewayrequisitionservices.security.token.CellulantJwtAuthenticationToken;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.Authentication;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class CellulantJwtAuthenticationProvider implements AuthenticationProvider {
	private Logger loggerBean = LoggerFactory
			.getLogger(CellulantJwtAuthenticationProvider.class);

	@Autowired
	private Configs configs;

	public CellulantJwtAuthenticationProvider(
			Configs configs
	) {
		this.configs = configs;
	}

	@Override
	public Authentication authenticate(Authentication authentication) {
		loggerBean.info("jwt provider to authentication and authorization request");
		// convert to our specific authentication request type
		CellulantJwtAuthenticationToken auth = (CellulantJwtAuthenticationToken) authentication;
		String jwt = auth.getToken();
		Authentication successfulAuthentication = null;

		try {
			//This line will throw an exception if jwt not valid
			Claims claims = Jwts.parser()
					.setSigningKey(configs.getJwtSecret().getBytes())
					.parseClaimsJws(jwt).getBody();

			loggerBean.debug("ID: " + claims.getId());
			loggerBean.debug("Subject: " + claims.getSubject());
			loggerBean.debug("Issuer: " + claims.getIssuer());
			loggerBean.debug("Expiration: " + claims.getExpiration());
			ArrayList<Object> roles = (ArrayList<Object>) claims.get("roles");
			List<RolesDTO> rolesDTO = new ArrayList<>();
			roles.stream()
					.forEach(role -> {
						LinkedHashMap<String, Object> keys = (LinkedHashMap<String, Object>) role;
						RolesDTO roleDTO = new RolesDTO();
						keys.forEach((key, value) -> {
							if (key.equals("moduleID")) {
								roleDTO.setModuleID((Integer) value);
							}
							if (key.equals("moduleName")) {
								roleDTO.setModuleName((String) value);
							}
							if (key.equals("entityActionID")) {
								roleDTO.setEntityActionID((Integer) value);
							}
							if (key.equals("entityActionGroupName")) {
								roleDTO.setEntityActionGroupName((String) value);
							}
							if (key.equals("entityActionPowerName")) {
								roleDTO.setEntityActionPowerName((String) value);
							}
						});
						rolesDTO.add(roleDTO);
					});
			UserDetailsDTO user = new UserDetailsDTO();
			user.setEmail(claims.get("email", String.class));
			user.setUserID(claims.get("userId", Integer.class));
			user.setUserName(claims.getSubject());
			user.setRoles(rolesDTO);
			successfulAuthentication = new CellulantJwtAuthenticationToken(
					auth.getSourceIPAddress(),
					jwt,
					user,
					rolesDTO
			);
		} catch (JwtException | ClassCastException e) {
			loggerBean.error("Exception during JWT validation: " + e.getMessage());
			throw new AuthenticationServiceException(e.getMessage());
		}
		loggerBean.debug("authentication process completed");
		return successfulAuthentication;
	}

	@Override
	public boolean supports(Class<?> authentication) {
		loggerBean.debug("checking if request should be handled by this provider");
		loggerBean.debug("The token class type is: " + authentication.getName());
		return authentication.equals(CellulantJwtAuthenticationToken.class);
	}
}
