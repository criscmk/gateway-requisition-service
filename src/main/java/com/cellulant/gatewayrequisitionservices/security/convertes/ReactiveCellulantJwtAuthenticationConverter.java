package com.cellulant.gatewayrequisitionservices.security.convertes;


import com.cellulant.gatewayrequisitionservices.security.token.CellulantJwtAuthenticationToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.server.authentication.ServerAuthenticationConverter;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Optional;

/**
 * @author Denis Gitonga
 */
public class ReactiveCellulantJwtAuthenticationConverter implements ServerAuthenticationConverter {

    private final Logger logger = LoggerFactory.getLogger(ReactiveCellulantJwtAuthenticationConverter.class);

    @Override
    public Mono<Authentication> convert(ServerWebExchange exchange) {

        final ServerHttpRequest request = exchange.getRequest();

        // check if request should be processed by this filter
        if (!hasValidJwtToken(request)) {
            logger.debug("skipping jwt validation");
            return Mono.empty();
        }

        // token that will be passed to provider
        final String sourceIPAddress = Optional.ofNullable(request.getLocalAddress())
                .map(InetSocketAddress::getAddress)
                .map(InetAddress::getHostAddress)
                .orElse(null);
        final CellulantJwtAuthenticationToken auth = new CellulantJwtAuthenticationToken(
                sourceIPAddress,
                getRequestBearerTokenValue(request));

        return Mono.just(auth);
    }


    private boolean hasValidJwtToken(ServerHttpRequest request) {
        logger.debug("checking jwt token is in request");
        final String jwtHeader = request.getHeaders().getFirst(HttpHeaders.AUTHORIZATION);
        // check the auth header is present
        if (jwtHeader == null) {
            logger.debug("authorization header not found");
            return false;
        }

        // check the token is a bearer token
        if (!jwtHeader.toLowerCase().startsWith("bearer ")) {
            logger.debug("bearer token not found");
            return false;
        }


        String[] jwtTokenArray = jwtHeader.trim().split(" ");

        // check the token structure is correct
        if (jwtTokenArray.length != 2) {
            logger.debug("authorization header not expected structure");
            return false;
        }

        String jwtToken = jwtTokenArray[1];

        // check value of authorization header present
        if (jwtToken == null || jwtToken.trim().isEmpty()) {
            logger.debug("authorization header not found");
            return false;
        }

        logger.debug("authorization header value: {} ", jwtHeader);

        logger.debug("bearer token: {}", jwtHeader);
        return true;
    }

    private String getRequestBearerTokenValue(ServerHttpRequest request) {
        String jwtHeader = request.getHeaders().getFirst(HttpHeaders.AUTHORIZATION);
        String[] jwtTokenArray = jwtHeader.trim().split(" ");
        return jwtTokenArray[1];
    }
}
