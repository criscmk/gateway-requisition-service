package com.cellulant.gatewayrequisitionservices.security.convertes;

import com.cellulant.gatewayrequisitionservices.security.token.CellulantAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.server.authentication.ServerHttpBasicAuthenticationConverter;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Optional;

/**
 * @author Denis Gitonga
 */
public class ReactiveCellulantAuthenticationConverter extends ServerHttpBasicAuthenticationConverter {
    @Override
    public Mono<Authentication> convert(ServerWebExchange exchange) {
        return super.convert(exchange).map(authentication -> {
            final UsernamePasswordAuthenticationToken token = (UsernamePasswordAuthenticationToken) authentication;
            final String ipAddress = Optional.ofNullable(exchange.getRequest().getLocalAddress())
                    .map(InetSocketAddress::getAddress)
                    .map(InetAddress::getHostAddress)
                    .orElse(null);
            final String userName = token.getPrincipal().toString();
            final String password = token.getCredentials().toString();

            return new CellulantAuthenticationToken(
                    ipAddress,
                    userName,
                    password
            );
        });
    }
}
