package com.cellulant.gatewayrequisitionservices.security;

import com.cellulant.gatewayrequisitionservices.dtos.responses.ResponseArray;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.server.ServerAuthenticationEntryPoint;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * @author Denis Gitonga
 */

public class ReactiveCellulantAuthenticationEntryPoint implements ServerAuthenticationEntryPoint {

    private final ObjectMapper objectMapper;

    public ReactiveCellulantAuthenticationEntryPoint(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    private final Logger logger = LoggerFactory.getLogger(ReactiveCellulantAuthenticationEntryPoint.class);

    @Override
    public Mono<Void> commence(ServerWebExchange exchange, AuthenticationException e) {
        return Mono.defer(() -> {
            final ResponseArray responseJson = ResponseArray.builder()
                    .result(e.getMessage())
                    .statusMessage(e.getMessage())
                    .statusCode(601)
                    .build();

            ServerHttpResponse response = exchange.getResponse();
            response.setStatusCode(HttpStatus.OK);
            response.getHeaders().setContentType(MediaType.APPLICATION_JSON);
            final DataBufferFactory bufferFactory = response.bufferFactory();
            final Mono<DataBuffer> dataBufferMono = Mono.fromCallable(() -> objectMapper.writeValueAsBytes(responseJson))
                    .map(bufferFactory::wrap);
            return response.writeWith(dataBufferMono);
        });
    }
}
