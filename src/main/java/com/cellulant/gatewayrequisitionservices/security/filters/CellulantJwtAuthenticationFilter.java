package com.cellulant.gatewayrequisitionservices.security.filters;


import com.cellulant.gatewayrequisitionservices.security.token.CellulantJwtAuthenticationToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CellulantJwtAuthenticationFilter extends BasicAuthenticationFilter {
	private Logger loggerBean = LoggerFactory
			.getLogger(CellulantJwtAuthenticationFilter.class);

	protected CellulantJwtAuthenticationFilter(
			final AuthenticationManager requiresAuth
	) {
		super(requiresAuth);
		loggerBean.debug("created the cellulant jwt authentication filter");
	}

	public CellulantJwtAuthenticationFilter(
			AuthenticationManager authenticationManager,
			AuthenticationEntryPoint authenticationEntryPoint
	) {
		super(authenticationManager, authenticationEntryPoint);
		loggerBean.debug("created the cellulant jwt authentication filter");
	}

	@Override
	public void doFilterInternal(
			HttpServletRequest request,
			HttpServletResponse response,
			FilterChain chain
	) throws java.io.IOException, javax.servlet.ServletException {
		// check if request should be processed by this filter
		if (!hasValidJwtToken(request)) {
			loggerBean.debug("skipping jwt validation");
			chain.doFilter(request, response);
			return;
		}

		// token that will be passed to provider 
		CellulantJwtAuthenticationToken auth = new CellulantJwtAuthenticationToken(
				request.getLocalAddr(),
				getRequestBearerTokenValue(request));
		try {
			// authenticate request 
			Authentication authResult = this.getAuthenticationManager()
					.authenticate(auth);

			// store authentication result
			SecurityContextHolder.getContext().setAuthentication(authResult);
		} catch (AuthenticationException failed) {
			loggerBean.error("exception during authentication: {} ", failed.getMessage());
			SecurityContextHolder.clearContext();
			this.getAuthenticationEntryPoint().commence(request, response, failed);
			return;
		} catch (Exception e) {
			loggerBean.error("exception during authentication: {} ", e.getMessage());
		}

		chain.doFilter(request, response);
		SecurityContextHolder.clearContext();
	}

	private boolean hasValidJwtToken(HttpServletRequest request) {
		loggerBean.debug("checking jwt token is in request");
		String jwtHeader = request.getHeader(HttpHeaders.AUTHORIZATION);

		// check the auth header is present 
		if (jwtHeader == null) {
			loggerBean.debug("authorization header not found");
			return false;
		}
		String[] jwtTokenArray = jwtHeader.trim().split(" ");

		// check the token structure is correct
		if (jwtTokenArray.length != 2) {
			loggerBean.debug("authorization header not expected structure");
			return false;
		}

		String jwtToken = jwtTokenArray[1];

		// check value of authorization header present
		if (jwtToken == null || jwtToken.trim().isEmpty()) {
			loggerBean.debug("authorization header not found");
			return false;
		}

		loggerBean.debug("authorization header value: {} ", jwtHeader);

		// check the token is a bearer token
		if (!jwtHeader.toLowerCase().startsWith("bearer ")) {
			loggerBean.debug("bearer token not found");
			return false;
		}

		loggerBean.debug("bearer token: {}", jwtHeader);
		return true;
	}

	private String getRequestBearerTokenValue(HttpServletRequest request) {
		String jwtHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
		String[] jwtTokenArray = jwtHeader.trim().split(" ");
		return jwtTokenArray[1];
	}
}
