package com.cellulant.gatewayrequisitionservices.security.filters;

import com.cellulant.gatewayrequisitionservices.security.token.CellulantAuthenticationToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;


public class CellulantAuthenticationFilter extends BasicAuthenticationFilter {

    private Logger loggerBean = LoggerFactory
            .getLogger(CellulantAuthenticationFilter.class);

    protected CellulantAuthenticationFilter(
            final AuthenticationManager requiresAuth
    ) {
        super(requiresAuth);
        loggerBean.debug("created the cellulant authentication filter");
    }

    public CellulantAuthenticationFilter(
            AuthenticationManager authenticationManager,
            AuthenticationEntryPoint authenticationEntryPoint
    ) {
        super(authenticationManager, authenticationEntryPoint);
        logger.debug("created the cellulant authentication filter");
    }

    @Override
    public void doFilterInternal(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain chain
    ) throws java.io.IOException, javax.servlet.ServletException {
        loggerBean.debug("inside customer Cellulant basic auth filter");
        String authorizationToken = request.getHeader(HttpHeaders.AUTHORIZATION);
        SecurityContext context = SecurityContextHolder.getContext();
        if (authorizationToken != null && !authorizationToken.trim().isEmpty()) {
            loggerBean.debug("authorization header found in request");
            loggerBean.debug("authorization header value: {}", authorizationToken);

            if (!authorizationToken.toLowerCase().startsWith("basic ")) {
                loggerBean.debug("authorization is NOT basic auth");
                chain.doFilter(request, response);
                return;
            }
            loggerBean.debug("authorization is basic auth");
            try {
                String[] tokens = extractAndDecodeHeader2(
                        authorizationToken,
                        request
                );
                assert tokens.length == 2;

                String userName = tokens[0];
                String password = tokens[1];
                String ipAddress = request.getLocalAddr();
                CellulantAuthenticationToken auth = new CellulantAuthenticationToken(
                        ipAddress,
                        userName,
                        password
                );
                Authentication authResult = getAuthenticationManager()
                        .authenticate(auth);

                SecurityContextHolder.getContext().setAuthentication(authResult);
            } catch (AuthenticationException failed) {
                loggerBean.error("Exception during authentication:  {}", failed.getMessage());
                SecurityContextHolder.clearContext();
                getAuthenticationEntryPoint().commence(request, response, failed);
                return;
            } catch (Exception e) {
                loggerBean.error("Exception during authentication: {} ", e.getMessage());
            }
        }
        chain.doFilter(request, response);
    }

    private String[] extractAndDecodeHeader2(
            String header,
            HttpServletRequest request
    ) throws IOException {
        byte[] base64Token = header.substring(6).getBytes(StandardCharsets.UTF_8);
        byte[] decoded;
        try {
            decoded = Base64.getDecoder().decode(base64Token);
        } catch (IllegalArgumentException e) {
            throw new BadCredentialsException(
                    "Failed to decode basic authentication token"
            );
        }

        String token = new String(decoded, getCredentialsCharset(request));

        int delim = token.indexOf(":");

        if (delim == -1) {
            throw new BadCredentialsException(
                    "Invalid basic authentication token"
            );
        }
        return new String[]{token.substring(0, delim),
                token.substring(delim + 1)};
    }
}
