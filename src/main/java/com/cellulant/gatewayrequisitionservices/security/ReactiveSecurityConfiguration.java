package com.cellulant.gatewayrequisitionservices.security;

import com.cellulant.gatewayrequisitionservices.configurations.Configs;
import com.cellulant.gatewayrequisitionservices.security.convertes.ReactiveCellulantAuthenticationConverter;
import com.cellulant.gatewayrequisitionservices.security.convertes.ReactiveCellulantJwtAuthenticationConverter;
import com.cellulant.gatewayrequisitionservices.security.managers.CellulantAuthenticationManager;
import com.cellulant.gatewayrequisitionservices.security.managers.CellulantJwtAuthenticationManager;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.SecurityWebFiltersOrder;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.security.web.server.authentication.AuthenticationWebFilter;
import org.springframework.security.web.server.context.NoOpServerSecurityContextRepository;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

/**
 * @author Denis Gitonga
 */
@Configuration
@AllArgsConstructor
@EnableWebFluxSecurity
@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.REACTIVE)
public class ReactiveSecurityConfiguration {

    private final Logger logger = LoggerFactory.getLogger(ReactiveSecurityConfiguration.class);

    private final ObjectMapper objectMapper;
    private final Configs configs;
    private final WebClient.Builder builder;

    @Bean
    SecurityWebFilterChain springSecurityWebFilterChain(ServerHttpSecurity http) {

        // @formatter:off
        http.authorizeExchange()
           .pathMatchers("/actuator/health").permitAll()
           .pathMatchers("/**").hasAnyRole("gateway")
           .anyExchange().authenticated();
        // @formatter:on

        http.httpBasic().disable();
        http.headers().disable();
        http.csrf().disable();
        http.formLogin().disable();
        http.logout().disable();

        // @formatter:off
        return http
                .securityContextRepository(NoOpServerSecurityContextRepository.getInstance())
                .exceptionHandling()
                    .accessDeniedHandler((exchange, denied) -> Mono.error(denied))
                    .authenticationEntryPoint(getEntryPoint())
                .and()
                    .addFilterAt(preAuthenticationFilter(), SecurityWebFiltersOrder.AUTHENTICATION)
                    .addFilterAt(preJwtAuthenticationFilter(), SecurityWebFiltersOrder.AUTHENTICATION)
                .build();
        // @formatter:on
    }

    public AuthenticationWebFilter preAuthenticationFilter() {

        final AuthenticationWebFilter filter = new AuthenticationWebFilter(authenticationManager());
        filter.setServerAuthenticationConverter(new ReactiveCellulantAuthenticationConverter());

        return filter;
    }

    public AuthenticationWebFilter preJwtAuthenticationFilter() {
        final AuthenticationWebFilter filter = new AuthenticationWebFilter(jwtAuthenticationManager());
        filter.setServerAuthenticationConverter(new ReactiveCellulantJwtAuthenticationConverter());

        return filter;
    }

    public ReactiveAuthenticationManager jwtAuthenticationManager() {
        return new CellulantJwtAuthenticationManager(configs);
    }

    public ReactiveAuthenticationManager authenticationManager() {
        return new CellulantAuthenticationManager(builder, configs);
    }


    private ReactiveCellulantAuthenticationEntryPoint getEntryPoint() {
        logger.debug("creating the cellulant authentication entry point to be used");
        return new ReactiveCellulantAuthenticationEntryPoint(objectMapper);
    }
}
