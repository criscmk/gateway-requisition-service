package com.cellulant.gatewayrequisitionservices.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

public class CellulantUserPrincipal extends User
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String sourceIPAddress;
	public CellulantUserPrincipal(
				String username,
				String password,
				String sourceIPAddress,
				Collection<? extends GrantedAuthority> authorities
	)
	{
		super(username, password, authorities);
		this.sourceIPAddress = sourceIPAddress;
	}
	
	public String getSourceIPAddress()
	{
		return sourceIPAddress;
	}
	public void setSourceIPAddress(String sourceIPAddress)
	{
		this.sourceIPAddress = sourceIPAddress;
	}

}
