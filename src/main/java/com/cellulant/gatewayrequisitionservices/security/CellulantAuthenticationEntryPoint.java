package com.cellulant.gatewayrequisitionservices.security;


import com.cellulant.gatewayrequisitionservices.configurations.Configs;
import com.cellulant.gatewayrequisitionservices.dtos.responses.ResponseArray;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class CellulantAuthenticationEntryPoint
		extends BasicAuthenticationEntryPoint {
	private final Logger logger =
			LoggerFactory.getLogger(CellulantAuthenticationEntryPoint.class);
	private final String REALM_NAME = "customer management service";
	private ObjectMapper objectMapper;

	public CellulantAuthenticationEntryPoint(
			Configs configs, ObjectMapper objectMapper
	) {
		this.objectMapper = objectMapper;
	}


	@Override
	public void commence(
			HttpServletRequest request,
			HttpServletResponse httpServletResponse,
			AuthenticationException authException
	) throws IOException {
		logger.error("error occured authentication request: {}",
				authException.getMessage());
		ResponseArray responseJson = new ResponseArray();
		responseJson.setResult(authException.getMessage());
		responseJson.setStatusMessage(authException.getMessage());
		responseJson.setStatusCode(601);
		String responseBody = objectMapper.writeValueAsString(responseJson);
		httpServletResponse.setStatus(HttpServletResponse.SC_OK);
		httpServletResponse.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
		httpServletResponse.getWriter().write(responseBody);
		httpServletResponse.getWriter().flush();
		httpServletResponse.getWriter().close();
	}

	@Override
	public void afterPropertiesSet() {
		this.setRealmName(REALM_NAME);
	}
}
