package com.cellulant.gatewayrequisitionservices.security.schedular;

import com.cellulant.gatewayrequisitionservices.configurations.Configs;
import com.cellulant.gatewayrequisitionservices.security.service.RefreshTokenService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import reactor.core.scheduler.Schedulers;

import java.text.SimpleDateFormat;
import java.util.Date;


@Component
public class TokenRefreshScheduler {
    // 600000 Milliseconds = 10 minutes
    private static final long TOKEN_REFRESH_PERIOD_MILLISECONDS = 600000L;
    private static final int TOKEN_REFRESH_MAX_RETRY_ATTEMPTS = 3;
    // 2000 Milliseconds = 2 seconds
    private static final long RETRY_DELAY_PERIOD_MILLISECONDS = 2000;

    Configs configs;
    RefreshTokenService fetchTokenService;
    private final Logger logger = LoggerFactory
            .getLogger(TokenRefreshScheduler.class);

    public TokenRefreshScheduler(
            RefreshTokenService fetchTokenService,
            Configs configs
    ) {
        this.configs = configs;
        this.fetchTokenService = fetchTokenService;
    }

    @Scheduled(fixedDelay = TOKEN_REFRESH_PERIOD_MILLISECONDS)
    @Retryable(
            value = {Exception.class},
            maxAttempts = TOKEN_REFRESH_MAX_RETRY_ATTEMPTS,
            backoff = @Backoff(delay = RETRY_DELAY_PERIOD_MILLISECONDS))
    public void cronJobSch() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        fetchTokenService.refreshToken()
                .doOnSubscribe(subscription -> {
                    Date start = new Date();
                    logger.debug("running token refresh job run at: {}", sdf.format(start));
                })
                .doOnTerminate(() -> {
                    Date end = new Date();
                    logger.debug("Finished running token refresh job run at: {}",
                            sdf.format(end));
                })
                .subscribeOn(Schedulers.boundedElastic())
                .subscribe();

    }

    @Recover
    public void recover(Exception e) {
        logger.info(e.getMessage(), e);
        logger.info("Service recovered from token refresh failure");
    }
}
