package com.cellulant.gatewayrequisitionservices.security.utils;

import com.cellulant.gatewayrequisitionservices.security.dtos.RolesDTO;
import com.cellulant.gatewayrequisitionservices.security.dtos.UserDetailsDTO;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class CellulantSecurityUtils {
    private CellulantSecurityUtils() {
    }

    public static Mono<Integer> getCurrentUserID() {
        return getReactiveCurrentUser()
                .map(UserDetailsDTO::getUserID);
    }

    public static Mono<UserDetailsDTO> getReactiveCurrentUser() {
        return ReactiveSecurityContextHolder.getContext()
                .map(SecurityContext::getAuthentication)
                .map(Authentication::getPrincipal)
                .cast(UserDetailsDTO.class);
    }

    public static UserDetailsDTO getCurrentUser() {
        UserDetailsDTO user = (UserDetailsDTO) SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getPrincipal();

        return user;
    }

    public static List<RolesDTO> getUserModuleAuthority(String module) {
        UserDetailsDTO user = (UserDetailsDTO) SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getPrincipal();

        return user.getRoles().stream()
                .filter(role -> role.getModuleName().equals(module))
                .collect(Collectors.toList());
    }

    public static boolean isUserModuleAuthorityPower(String module, String power) {
        UserDetailsDTO user = (UserDetailsDTO) SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getPrincipal();

        return user.getRoles().stream()
                .filter(role -> role.getModuleName().equals(module))
                .anyMatch(role -> role.getEntityActionPowerName().equals(power));
    }

    public static Integer getModuleIDByModuleName(String module) {
        UserDetailsDTO user = (UserDetailsDTO) SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getPrincipal();

        Optional<Integer> moduleId = user.getRoles().stream()
                .filter(role -> role.getModuleName().equals(module))
                .map(role -> role.getModuleID())
                .findFirst();

        if (moduleId.isPresent()) {
            return moduleId.get();
        } else {
            return null;
        }
    }

    public static Integer getEntityActionIDByModuleNameAndEntityActionGroup(
            String module,
            String entityActionGroup
    ) {
        UserDetailsDTO user = (UserDetailsDTO) SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getPrincipal();

        Optional<Integer> entityActionId = user.getRoles().stream()
                .filter(role -> role.getModuleName().equals(module))
                .filter(role -> role.getEntityActionGroupName().equals(entityActionGroup))
                .map(role -> role.getEntityActionID())
                .findFirst();
        if (entityActionId.isPresent()) {
            return entityActionId.get();
        } else {
            return null;
        }
    }

    public static String getUserModuleAuthorityPower(String module, String entityGroup) {
        UserDetailsDTO user = (UserDetailsDTO) SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getPrincipal();

        RolesDTO roleFound = user.getRoles().stream()
                .filter(role -> {
                    return role.getModuleName().equals(module) &&
                            role.getEntityActionGroupName().equals(entityGroup);
                })
                .findFirst()
                //.get();
                .orElseThrow();

        return roleFound.getEntityActionPowerName();
    }

    public static String getUserSourceIPAddress() {
        UserDetailsDTO user = (UserDetailsDTO) SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getPrincipal();

        return user.getSourceIPAddress();
    }
}
