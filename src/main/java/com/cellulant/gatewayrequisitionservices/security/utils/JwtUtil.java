package com.cellulant.gatewayrequisitionservices.security.utils;

import com.cellulant.gatewayrequisitionservices.security.dtos.UserDetailsDTO;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

import java.io.UnsupportedEncodingException;
import java.security.Key;
import java.util.Date;

public class JwtUtil {

    //@Value("${aes.key}")
    private String secret = "TheBestSecretKeyTheBestSecretKeyTheBestSecretKey";

    //@Value("${jwt.ttl}")
    private long ttlMillis = 300000L;

    /**
     * Tries to parse specified String as a JWT token. If successful, returns User object with username, id and role prefilled (extracted from token).
     * If unsuccessful (token is invalid or not containing all required user properties), simply returns null.
     *
     * @param token the JWT token to parse
     * @return the User object extracted from specified token or null if a token is invalid.
     */
    public UserDetailsDTO parseToken(String token) {
        try {
            Claims body = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody();

            UserDetailsDTO u = new UserDetailsDTO();
//            u.setUsername(body.getSubject());
//            u.setId(Long.parseLong((String) body.get("userId")));
//            u.setRole((String) body.get("role"));

            return u;

        } catch (JwtException | ClassCastException e) {
            return null;
        }
    }

    /**
     * Generates a JWT token containing username as subject, and userId and role as additional claims. These properties are taken from the specified
     * User object. Tokens validity is infinite.
     *
     * @param user the user for which the token will be generated
     * @return the JWT token
     * @throws UnsupportedEncodingException
     */
    public String generateToken(UserDetailsDTO user) throws UnsupportedEncodingException {
        Claims claims = Jwts.claims();
        claims.setIssuedAt(new Date());
        claims.setSubject(user.getUsername());
        claims.put("userId", user.getUserID());
        claims.put("email", user.getEmail());
        claims.put("roles", user.getRoles());

        // set token experation period
        long expMillis = System.currentTimeMillis() + ttlMillis;
        Date exp = new Date(expMillis);
        claims.setExpiration(exp);

        // TODO update the 
        Key key = Keys.hmacShaKeyFor(secret.getBytes());
        return Jwts.builder()
                .setClaims(claims)
                .signWith(key, SignatureAlgorithm.HS256)
                .compact();
    }
}
