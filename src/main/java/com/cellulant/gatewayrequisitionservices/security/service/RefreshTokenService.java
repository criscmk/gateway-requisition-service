package com.cellulant.gatewayrequisitionservices.security.service;


import com.cellulant.gatewayrequisitionservices.configurations.Configs;
import com.cellulant.gatewayrequisitionservices.security.TokenStore;
import com.cellulant.gatewayrequisitionservices.security.dtos.AuthenticationRequestDTO;
import com.cellulant.gatewayrequisitionservices.security.dtos.JwtTokenResponseDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Optional;

@Component
public class RefreshTokenService {
    private final ObjectMapper objectMapper;
    private final Configs configs;
    private final WebClient webClient;

    private final Logger logger = LoggerFactory
            .getLogger(RefreshTokenService.class);

    public RefreshTokenService(
            ObjectMapper objectMapper,
            Configs configs,
            WebClient webClient
    ) {
        this.configs = configs;
        this.objectMapper = objectMapper;
        this.webClient = webClient;
    }

    public Mono<Void> refreshToken() {

        return this.fetchToken()
                .doOnSuccess(jwt -> jwt.ifPresentOrElse(
                        this::storeToken,
                        () -> logger.error("failed to refresh token")
                ))
                .doOnError(throwable -> logger.error(throwable.getMessage()))
                .then();
    }

    private Mono<Optional<String>> fetchToken() {
        AuthenticationRequestDTO request = new AuthenticationRequestDTO();
        request.setPassword(configs.getAuthenticationPassword());
        request.setUserName(configs.getAuthenticationUsername());
        request.setSourceIPAddress("127.0.0.1");
        Mono<JwtTokenResponseDTO> authResponseMono = this.getAuthenticationToken(request);

        return authResponseMono.flatMap(authResponse -> {
            try {
                logger.debug("Response from auth service: {}", objectMapper.writeValueAsString(authResponse));
            } catch (Exception e) {
                // do nothing
            }
            logger.debug("received authentication status code: {} ", authResponse.getStatusCode());
            if (authResponse.getStatusCode().equals(configs.getAuthenticationSuccessStatusCode())) {
                logger.debug("received successful authentication status code");
                return Mono.just(Optional.ofNullable(authResponse.getResult().getData().get(0).getToken()));
            } else {
                logger.error("Authentication failed: {}", authResponse.getStatusMessage());
                return Mono.just(Optional.empty());
            }
        });
    }

    private Mono<JwtTokenResponseDTO> getAuthenticationToken(
            AuthenticationRequestDTO request
    ) {

        return Mono.defer(() -> webClient.post()
                .uri(configs.getAuthenticationService())
                .bodyValue(request)
                .retrieve()
                .bodyToMono(JwtTokenResponseDTO.class));
    }

    private void storeToken(String jwt) {
        logger.debug("token to be stored: {}", jwt);
        TokenStore.clear();
        logger.debug("token store after clearing: {}", TokenStore.getToken());
        TokenStore.setToken(jwt);
        logger.debug("token store after saving to context: {}", TokenStore.getToken());
    }


}
