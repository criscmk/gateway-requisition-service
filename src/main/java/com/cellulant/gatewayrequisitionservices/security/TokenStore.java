package com.cellulant.gatewayrequisitionservices.security;

import org.springframework.stereotype.Component;

@Component
public final class TokenStore {
    private static String token;

    public static void setToken(String stoken) {
        TokenStore.token = stoken;
    }

    public static String getToken() {
        return TokenStore.token;
    }

    public static void clear() {
        TokenStore.token = "";
    }
}