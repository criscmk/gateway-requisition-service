package com.cellulant.gatewayrequisitionservices.forex.requests;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

/**
 * Created by sylvester
 * Project gateway-enquiry-services
 * User: sylvester
 * Date: 7/8/19
 * Time: 9:45 AM
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ForexRequest implements java.io.Serializable {
    @NotNull(message = "{baseCurrency.required}")
    private String baseCurrency;
    @NotBlank(message = "{quoteCurrency.required}")
    private String quoteCurrency;
}
