package com.cellulant.gatewayrequisitionservices.fullstatement.services;

import com.cellulant.gatewayrequisitionservices.configurations.Constants;
import com.cellulant.gatewayrequisitionservices.dtos.responses.ResponseArray;
import com.cellulant.gatewayrequisitionservices.fullstatement.requests.FullStatementRequest;
import com.cellulant.gatewayrequisitionservices.models.ServicesEntity;
import com.cellulant.gatewayrequisitionservices.services.ServiceImpl;
import com.cellulant.gatewayrequisitionservices.utils.SharedUtilities;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.slf4j.MDC;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.Map;

/**
 * Created by sylvester
 * Project requisition-gateway-api
 * User: sylvester
 * Date: 4/4/19
 * Time: 7:21 PM
 */
@Service
@RequiredArgsConstructor
public class FullStatementService {

    @NonNull
    private final SharedUtilities sharedUtilities;

    @NonNull
    private final ServiceImpl serviceImpl;

    /**
     * Process request response array.
     *
     * @param headerParams         the header params
     * @param fullStatementRequest the full statement request
     * @return the response array
     * @throws Exception the exception
     */
    @SuppressWarnings("Duplicates")
    public Mono<ResponseArray> processFullStatementRequest(Map headerParams, FullStatementRequest fullStatementRequest) throws Exception {
        //Get action from headers
        final String action = sharedUtilities.retrieveActionFromHeaders(headerParams);
        final String uuid = String.valueOf(headerParams.get(Constants.UUID));
        //Retrieve the Service Details form Database
        final Mono<ServicesEntity> servicesEntityMono = serviceImpl.getServiceDetails((String.valueOf(headerParams.get(Constants.SERVICE))));

        return servicesEntityMono.flatMap(servicesEntity -> {
            // Get msisdn
            final Mono<String> msisdnMono = sharedUtilities.getMsisdn(headerParams, action, servicesEntity)
                    .doOnNext(msisdn -> MDC.put(Constants.MSISDN, String.valueOf(msisdn)));

            // Retrieve Wallet URL from Database using provided action
            final Mono<String> walletUrlMono = sharedUtilities.getWalletUrl(action, servicesEntity);

            final Mono<String> fullStatementRequestJsonMono = Mono.just(fullStatementRequest)
                    .flatMap(fullStatement -> Mono.fromCallable(() -> {

                        fullStatement.setPin(sharedUtilities.encrypt(fullStatement.getPin()));

                        // Formulate Wallet Payload
                        ObjectMapper objectMapper = new ObjectMapper();
                        objectMapper.disable(MapperFeature.USE_ANNOTATIONS);
                        return objectMapper.writeValueAsString(fullStatement);
                    }));


            return Mono.zip(msisdnMono, walletUrlMono, fullStatementRequestJsonMono)
                    .flatMap(data -> {
                        final String msisdn = data.getT1();
                        final String walletUrl = data.getT2();
                        final String fullStatementRequestJson = data.getT3();

                        // update headerParams with updated msisdn
                        final Mono<Map> headerParamsMono = Mono.just(headerParams).map(map -> {
                            map.replace(Constants.MSISDN, msisdn);
                            return map;
                        });

                        return headerParamsMono.flatMap(headers -> {
                            final Mono<String> requestIDMono = sharedUtilities.getRequestID(fullStatementRequest, msisdn, headers)
                                    .doOnNext(requestID -> MDC.put(Constants.CLOUD_REQUEST_ID, String.valueOf(requestID)));

                            //Validate Uuid
                            final Mono<Void> validateUUID = sharedUtilities.validateUUID(servicesEntity, msisdn, action, uuid);

                            return validateUUID.then(requestIDMono)
                                    .flatMap(requestID -> sharedUtilities
                                            .processIntegrationResponse(headers, msisdn, requestID, servicesEntity, walletUrl, fullStatementRequestJson));

                        });
                    });
        });
    }
}
