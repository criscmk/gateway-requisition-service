package com.cellulant.gatewayrequisitionservices.fullstatement.requests;


import com.cellulant.gatewayrequisitionservices.validation.ValidStartEndDates;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * Created by sylvester
 * Project requisition-gateway-api
 * User: sylvester
 * Date: 4/4/19
 * Time: 7:27 PM
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FullStatementRequest implements Serializable {

    @NotNull(message = "{accountAlias.required}")
    private String accountAlias;
    @NotBlank(message = "{pin.required}")
    //@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String pin;
    @NotNull(message = "{startDate.required}")
//    @Pattern(regexp = "^[0-9]{4}-(((0[13578]|(10|12))-(0[1-9]|[1-2][0-9]|3[0-1]))|(02-(0[1-9]|[1-2][0-9]))|((0[469]|11)-(0[1-9]|[1-2][0-9]|30)))$")
    private String startDate;
    @NotNull(message = "{endDate.required}")
//    @Pattern(regexp = "^[0-9]{4}-(((0[13578]|(10|12))-(0[1-9]|[1-2][0-9]|3[0-1]))|(02-(0[1-9]|[1-2][0-9]))|((0[469]|11)-(0[1-9]|[1-2][0-9]|30)))$")
    private String endDate;
    @NotNull(message = "{email.required}")
    private String email;
}
