package com.cellulant.gatewayrequisitionservices.termdeposit.requests;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;

/**
 * Created by  IntelliJ IDEA.
 * Project enquiries-gateway-api
 * User: Sylvester Musyoki
 * Date: 5/11/19
 * Time: 12:38 PM
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TermDepositRequest implements Serializable {
    @NotBlank(message = "{accountAlias.required}")
    private String accountAlias;
    @NotBlank(message = "{customerId.required}")
    private String customerId;
}
