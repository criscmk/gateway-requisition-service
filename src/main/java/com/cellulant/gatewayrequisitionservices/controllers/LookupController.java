package com.cellulant.gatewayrequisitionservices.controllers;


import com.cellulant.gatewayrequisitionservices.binlist.services.ValidateBinService;
import com.cellulant.gatewayrequisitionservices.card.services.OrderRequest;
import com.cellulant.gatewayrequisitionservices.card.services.ValidateCardNumberService;
import com.cellulant.gatewayrequisitionservices.cardfail.dtos.FailedRequest;
import com.cellulant.gatewayrequisitionservices.cardfail.services.FailedService;
import com.cellulant.gatewayrequisitionservices.cards.StepUpJWTRequest;
import com.cellulant.gatewayrequisitionservices.configurations.Constants;
import com.cellulant.gatewayrequisitionservices.contacts.services.ContactsServiceImpl;
import com.cellulant.gatewayrequisitionservices.countries.services.CountriesServiceImpl;
import com.cellulant.gatewayrequisitionservices.customerfeedback.services.CustomerFeedbackService;
import com.cellulant.gatewayrequisitionservices.deliverymodes.services.DeliveryModesServiceImpl;
import com.cellulant.gatewayrequisitionservices.dtos.responses.ResponseArray;
import com.cellulant.gatewayrequisitionservices.enaira.services.ENairaService;
import com.cellulant.gatewayrequisitionservices.identitification.services.IdentificationTypesServiceImpl;
import com.cellulant.gatewayrequisitionservices.locateus.services.LocateUsServiceImpl;
import com.cellulant.gatewayrequisitionservices.products.services.ProductsServiceImpl;
import com.cellulant.gatewayrequisitionservices.remoteconfigs.services.RemoteConfigsService;
import com.cellulant.gatewayrequisitionservices.utils.SharedUtilities;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.util.Map;

/**
 * Created by LoiseKinyua
 * Project lookup_gateway_api
 * User: LoiseKinyua
 * Date: 3/18/19
 * Time: 11:23 AM
 */
@RestController
@RequiredArgsConstructor
public class LookupController {

    public static final String LOCATE_US_BRANCH_LOCATIONS = "/locate-us/branch-locations";
    public static final String LOCATE_US_ATM_LOCATIONS = "/locate-us/atm-locations";
    public static final String LOCATE_US_POS_LOCATIONS = "/locate-us/pos-locations";
    public static final String LOCATE_US_LOCATION_CATEGORIES = "/locate-us/location-categories";
    public static final String COUNTRIES_ACTIVE_COUNTRIES = "/countries/active-countries";
    public static final String IDENTITY_TYPES_FETCH = "/identity-types/fetch";
    public static final String ONBOARDING_IDENTITY_TYPES_FETCH = "/onboarding-identity-types/fetch";
    public static final String DELIVERY_MODES_FETCH = "/delivery-modes/fetch/{affiliate}";
    public static final String PRODUCTS_FETCH = "/products/fetch";
    public static final String CONTACT_INFO_FETCH = "/contact-info/fetch";
    public static final String VALIDATION_VALIDATE_CARD_NUMBER = "/validation/validate-card-number/{cardNumber}";
    public static final String VALIDATION_CREATE_CARD_JWT = "validation/create-card-jwt";
    public static final String FAILED_REPORT = "/failed/report";
    public static final String REMOTE_CONFIGS_FETCH = "/remote-configs/fetch";
    public static final String BIN_VALIDATE_REQUEST_BIN = "bin-validate/request/{bin}";
    private static final String VALIDATION_CREATE_STEP_UP_CARD_JWT = "validation/create-card-step-up-jwt";
    public static final String ENAIRA_TRANSFER_MODES = "/enaira/transfer-modes";
    public static final String ENAIRA_DESTINATION_OPTIONS = "/enaira/destination-options";
    public static final String CUSTOMER_FEEDBACK_SERVICES_FETCH = "/customer-feedback/service-codes";
    @NonNull
    private final SharedUtilities sharedUtilities;
    @NonNull
    private final LocateUsServiceImpl locateUsService;
    @NonNull
    private final CountriesServiceImpl countriesService;
    @NonNull
    private final IdentificationTypesServiceImpl identificationTypesService;
    @NonNull
    private final DeliveryModesServiceImpl deliveryModesService;
    @NonNull
    private final ProductsServiceImpl productsService;
    @NonNull
    private final ContactsServiceImpl contactsService;
    @NonNull
    private final ValidateCardNumberService validateCardNumberService;
    @NonNull
    private final FailedService failedServiceImpl;
    @NonNull
    private final RemoteConfigsService remoteConfigsServiceImpl;
    @NonNull
    private final ValidateBinService validateBinServiceImpl;
    @NonNull
    private final ENairaService eNairaServiceImpl;
    private final CustomerFeedbackService customerFeedbackService;

    /**
     * Fetch branch locations response entity.
     *
     * @param headParamsString the head params string
     * @return the response entity
     * @throws Exception the exception
     */
    @GetMapping(value = LOCATE_US_BRANCH_LOCATIONS)
    public Mono<ResponseArray> fetchBranchLocations(@RequestHeader(Constants.X_HEADERS) String headParamsString) throws Exception {
        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            try {
                return locateUsService.fetchBranchLocations(headerParams);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });

    }


    /**
     * Fetch atm locations response entity.
     *
     * @param headParamsString the head params string
     * @return the response entity
     * @throws Exception the exception
     */
    @GetMapping(value = LOCATE_US_ATM_LOCATIONS)
    public Mono<ResponseArray> fetchAtmLocations(@RequestHeader(Constants.X_HEADERS) String headParamsString) throws Exception {
        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            try {

                // Recent Balance Enquiry Response
                return locateUsService.fetchAtmLocations(headerParams);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });

    }


    /**
     * Fetch pos locations response entity.
     *
     * @param headParamsString the head params string
     * @return the response entity
     * @throws Exception the exception
     */
    @GetMapping(value = LOCATE_US_POS_LOCATIONS)
    public Mono<ResponseArray> fetchPosLocations(@RequestHeader(Constants.X_HEADERS) String headParamsString) throws Exception {
        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            try {

                // Recent Balance Enquiry Response
                return locateUsService.fetchPosLocations(headerParams);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }

    /**
     * Fetch location categories response entity.
     *
     * @param headParamsString the head params string
     * @return the response entity
     * @throws Exception the exception
     */
    @GetMapping(value = LOCATE_US_LOCATION_CATEGORIES)
    public Mono<ResponseArray> fetchLocationCategories(@RequestHeader(Constants.X_HEADERS) String headParamsString) throws Exception {
        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            try {

                // Recent Balance Enquiry Response
                return locateUsService.fetchLocationCategories(headerParams);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }

    /**
     * Fetch pos locations response entity.
     *
     * @param headParamsString the head params string
     * @return the response entity
     * @throws Exception the exception
     */
    @GetMapping(value = COUNTRIES_ACTIVE_COUNTRIES)
    public Mono<ResponseArray> fetchCountries(@RequestHeader(Constants.X_HEADERS) String headParamsString) throws Exception {
        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            try {

                // Recent Balance Enquiry Response
                return countriesService.fetchCountries(headerParams);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }


    /**
     * Fetch identity types response entity.
     *
     * @param headParamsString the head params string
     * @return the response entity
     * @throws Exception the exception
     */
    @GetMapping(value = IDENTITY_TYPES_FETCH)
    public Mono<ResponseArray> fetchIdentityTypes(@RequestHeader(Constants.X_HEADERS) String headParamsString) throws Exception {
        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            try {

                // Recent Balance Enquiry Response
                return identificationTypesService.fetchIdentityTypes(headerParams);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }

    /**
     * Fetch identity types response entity.
     *
     * @param headParamsString the head params string
     * @return the response entity
     * @throws Exception the exception
     */
    @GetMapping(value = ONBOARDING_IDENTITY_TYPES_FETCH)
    public Mono<ResponseArray> fetchOnboardingIdentityTypes(@RequestHeader(Constants.X_HEADERS) String headParamsString) throws Exception {
        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            try {
                // Recent Balance Enquiry Response
                return identificationTypesService.fetchOnboardingIdentityTypes(headerParams);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }


    /**
     * Fetch delivery modes for affiliate response entity.
     *
     * @param headParamsString the head params string
     * @param affiliate        the affiliate
     * @return the response entity
     * @throws Exception the exception
     */
    @GetMapping(value = DELIVERY_MODES_FETCH)
    public Mono<ResponseArray> fetchDeliveryModesForAffiliate(@RequestHeader(Constants.X_HEADERS) String headParamsString, @PathVariable String affiliate) throws Exception {
        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            try {

                // Recent Balance Enquiry Response
                return deliveryModesService.fetchDeliveryModesForAffiliate(headerParams, affiliate);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }


    /**
     * Fetch products service response entity.
     *
     * @param headParamsString the head params string
     * @return the response entity
     * @throws Exception the exception
     */
    @GetMapping(value = PRODUCTS_FETCH)
    public Mono<ResponseArray> fetchProductsService(@RequestHeader(Constants.X_HEADERS) String headParamsString) throws Exception {
        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            try {

                // Recent Balance Enquiry Response
                return productsService.fetchProducts(headerParams);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }


    /**
     * Fetch contact info response entity.
     *
     * @param headParamsString the head params string
     * @return the response entity
     * @throws Exception the exception
     */
    @GetMapping(value = CONTACT_INFO_FETCH)
    public Mono<ResponseArray> fetchContactInfo(@RequestHeader(Constants.X_HEADERS) String headParamsString) throws Exception {
        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            try {

                // Recent Balance Enquiry Response
                return contactsService.fetchContactInfo(headerParams);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }

    /**
     * Validate card number response entity.
     *
     * @param headParamsString the head params string
     * @param cardNumber       the card number
     * @return the response entity
     * @throws Exception the exception
     */
    @GetMapping(value = VALIDATION_VALIDATE_CARD_NUMBER)
    public Mono<ResponseArray> validateCardNumber(@RequestHeader(Constants.X_HEADERS) String headParamsString,
                                                  @PathVariable String cardNumber) throws Exception {
        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            try {
                // Recent Balance Enquiry Response
                return validateCardNumberService.validateCardNumber(headerParams, cardNumber);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }

    /**
     * Create step up jwt mono.
     *
     * @param headParamsString the head params string
     * @param stepUpJWTRequest the step up jwt request
     * @return the mono
     * @throws Exception the exception
     */
    @PostMapping(value = VALIDATION_CREATE_STEP_UP_CARD_JWT)
    public Mono<ResponseArray> createStepUpJwt(@RequestHeader(Constants.X_HEADERS) String headParamsString, @RequestBody @Valid StepUpJWTRequest stepUpJWTRequest) throws Exception {
        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            try {
                return validateCardNumberService.createStepUpJwt(headerParams, stepUpJWTRequest);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }

    /**
     * Create jwt mono.
     *
     * @param headParamsString the head params string
     * @param createJWTRequest the create jwt request
     * @return the mono
     * @throws Exception the exception
     */
    @PostMapping(value = VALIDATION_CREATE_CARD_JWT)
    public Mono<ResponseArray> createJWT(@RequestHeader(Constants.X_HEADERS) String headParamsString, @RequestBody @Valid OrderRequest createJWTRequest) throws Exception {
        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            // Recent Balance Enquiry Response
            try {
                return validateCardNumberService.createJWT(headerParams, createJWTRequest);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }

    /**
     * Log failed request response entity.
     *
     * @param headParamsString the head params string
     * @param failedRequest    the failed request
     * @return the response entity
     * @throws Exception the exception
     */
    @PostMapping(value = FAILED_REPORT)
    public Mono<ResponseArray> logFailedRequest(@RequestHeader(Constants.X_HEADERS) String headParamsString,
                                                @RequestBody @Valid FailedRequest failedRequest) throws Exception {
        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            try {

                // Recent Balance Enquiry Response
                return failedServiceImpl.logFailedRequests(headerParams, failedRequest);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }

    /**
     * Fetch remote configs response entity.
     *
     * @param headParamsString the head params string
     * @return the response entity
     * @throws Exception the exception
     */
    @GetMapping(value = REMOTE_CONFIGS_FETCH)
    public Mono<ResponseArray> fetchRemoteConfigs(@RequestHeader(Constants.X_HEADERS) String headParamsString) throws Exception {
        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            try {
                // Recent Balance Enquiry Response
                return remoteConfigsServiceImpl.fetchRemoteConfigs(headerParams);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }

    /**
     * Validate bin response entity.
     *
     * @param headParamsString the head params string
     * @param bin              the bin
     * @return the response entity
     * @throws Exception the exception
     */
    @GetMapping(value = "bin-validate/request/{bin}")
    public Mono<ResponseArray> validateBin(@RequestHeader(Constants.X_HEADERS) String headParamsString,
                                           @PathVariable String bin) throws Exception {
        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            try {
                return validateBinServiceImpl.validateBin(headerParams, bin);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }


    /**
     * Fetch enaira transfer modes mono.
     *
     * @param headerParamsString the header params string
     * @return the mono
     * @throws Exception the exception
     */
    @GetMapping(ENAIRA_TRANSFER_MODES)
    public Mono<ResponseArray> enairaTransferModes(@RequestHeader(Constants.X_HEADERS) String headerParamsString) throws Exception {
        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headerParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            try {
                //ENaira Transfer Modes
                return eNairaServiceImpl.fetchENairaTransferModes(headerParams);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }

    /**
     * Fetch enaira destination options  mono.
     *
     * @param headerParamsString the header params string
     * @return the mono
     * @throws Exception the exception
     */
    @GetMapping(ENAIRA_DESTINATION_OPTIONS)
    public Mono<ResponseArray> enairaDestinationOptions(@RequestHeader(Constants.X_HEADERS) String headerParamsString) throws Exception {
        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headerParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            try {
                //ENaira Destination Options
                return eNairaServiceImpl.fetchENairaDestinationOptions(headerParams);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }

    /**
     * Fetch customer feedback services  mono.
     * @param headerParamsString the header params string
     * @return the mono
     * @throws Exception the exception
     */
    @GetMapping(CUSTOMER_FEEDBACK_SERVICES_FETCH)
    public Mono<ResponseArray> customerFeedbackServices(@RequestHeader(Constants.X_HEADERS) String headerParamsString) throws Exception {
        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headerParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            try {
                //Customer Feedback Services
                return customerFeedbackService.fetchCustomerFeedbackServiceCodes(headerParams);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }


}


