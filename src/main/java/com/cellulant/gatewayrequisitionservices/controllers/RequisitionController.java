package com.cellulant.gatewayrequisitionservices.controllers;
import com.cellulant.gatewayrequisitionservices.authorizationoptions.AuthorizationOptionsService;
import com.cellulant.gatewayrequisitionservices.duplicatecheck.request.DuplicateTransactionCheckRequestDto;
import com.cellulant.gatewayrequisitionservices.duplicatecheck.services.DuplicateTransactionCheckService;
import com.cellulant.gatewayrequisitionservices.lookupvalues.LookupValuesService;
import com.cellulant.gatewayrequisitionservices.chequeservices.dtos.ConfirmChequeRequest;
import com.cellulant.gatewayrequisitionservices.chequeservices.dtos.RequestChequeBookRequest;
import com.cellulant.gatewayrequisitionservices.chequeservices.dtos.StopChequeBookRequest;
import com.cellulant.gatewayrequisitionservices.chequeservices.services.ChequeServicesImpl;
import com.cellulant.gatewayrequisitionservices.configurations.Constants;
import com.cellulant.gatewayrequisitionservices.donations.services.DonationService;
import com.cellulant.gatewayrequisitionservices.donations.dtos.DonationsChargesRequest;
import com.cellulant.gatewayrequisitionservices.donations.services.DonationsChargesService;
import com.cellulant.gatewayrequisitionservices.customerfeedback.requests.CustomerFeedBackRequest;
import com.cellulant.gatewayrequisitionservices.customerfeedback.services.CustomerFeedbackService;
import com.cellulant.gatewayrequisitionservices.dtos.responses.ResponseArray;
import com.cellulant.gatewayrequisitionservices.enaira.requests.ENairaAuthenticateRequest;
import com.cellulant.gatewayrequisitionservices.enaira.requests.ENairaNameInquiryRequest;
import com.cellulant.gatewayrequisitionservices.enaira.requests.ENairaPreAuthorizationRequest;
import com.cellulant.gatewayrequisitionservices.enaira.services.ENairaService;
import com.cellulant.gatewayrequisitionservices.fullstatement.requests.FullStatementRequest;
import com.cellulant.gatewayrequisitionservices.fullstatement.services.FullStatementService;
import com.cellulant.gatewayrequisitionservices.hardtoken.requests.*;
import com.cellulant.gatewayrequisitionservices.hardtoken.services.HardTokenService;
import com.cellulant.gatewayrequisitionservices.inappbanner.services.InAppBannerService;
import com.cellulant.gatewayrequisitionservices.internationaltransfers.request.InternationalTransferChargesRequestDto;
import com.cellulant.gatewayrequisitionservices.internationaltransfers.services.InternationalTransferChargesService;
import com.cellulant.gatewayrequisitionservices.loanterms.requests.LoanTermsRequest;
import com.cellulant.gatewayrequisitionservices.loanterms.services.LoanTermsServiceImpl;
import com.cellulant.gatewayrequisitionservices.pesalink.services.PesaLinkService;
import com.cellulant.gatewayrequisitionservices.termdeposit.requests.TermDepositRequest;
import com.cellulant.gatewayrequisitionservices.termdeposit.services.TermDepositServiceImpl;
import com.cellulant.gatewayrequisitionservices.utils.SharedUtilities;
import com.cellulant.gatewayrequisitionservices.xpresscashtoken.request.XpressCashTokenChargesRequestDto;
import com.cellulant.gatewayrequisitionservices.xpresscashtoken.services.XpressCashTokenChargesService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.util.Map;

/**
 * Created by sylvester
 * Project requisition-gateway-api
 * User: sylvester
 * Date: 4/2/19
 * Time: 9:43 PM
 */
@RestController
@RequiredArgsConstructor
public class RequisitionController {

    public static final String CHEQUE_SERVICES_STOP = "/cheque-services/stop";
    public static final String CHEQUE_SERVICES_CONFIRM = "/cheque-services/confirm";
    public static final String CHEQUE_SERVICES_BOOK_LEAVES = "/cheque-services/book-leaves";
    public static final String CHEQUE_SERVICES_REQUEST = "/cheque-services/request";
    public static final String FULL_STATEMENT_REQUEST = "/full-statement/request";
    public static final String TERM_DEPOSIT_REQUEST = "/term-deposit/request";
    public static final String LOAN_INFORMATION_REQUEST = "/loan-information/request";
    public static final String CHEQUE_SERVICES_BRANCHES_FETCH = "/cheque-services/branches-fetch";
    public static final String HARD_TOKEN_REQUEST_TYPES = "/hard-token/request-types";
    public static final String HARD_TOKEN_DELIVERY_MODES = "/hard-token/delivery-modes";
    public static final String HARD_TOKEN_BRANCHES = "/hard-token/branches";
    public static final String HARD_TOKEN_CHARGES = "/hard-token/charges";
    public static final String HARD_TOKEN_REQUEST = "/hard-token/request";
    public static final String HARD_TOKEN_ACTIVATE = "/hard-token/activate";
    public static final String HARD_TOKEN_VALIDATE = "/hard-token/validate-token";
    public static final String HARD_TOKEN_CHECK_PENDING_REQUEST = "/hard-token/check-pending-request";
    public static final String ENAIRA_AUTHENTICATE_REQUEST = "/enaira/authenticate";

    public static final String ENAIRA_PRE_AUTHORIZATION = "/enaira/preauthorization";
    public static final String ENAIRA_NAME_INQUIRY = "/enaira/name-inquiry";
    public static final String DONATIONS_CHARITY_ORGANISATIONS = "donations/charities";
    public static final String DONATIONS_CHARGES = "/donation/charges";

    public static final String CUSTOMERS_ACTIVE_BANNER = "/customers/active-banner";
    public static final String PESALINK_BANKS = "/pesalink/banks/{proxyAlias}";

    public static final String CUSTOMER_FEEDBACK_CREATE = "/customer-feedback/create";

    public static final String XPRESS_CASH_TOKEN_CHARGES = "/xpress-cash-token-account/charges";

    public static final String INTERNATIONAL_TRANSFER_CHARGES = "/international-transfer/charges";

    public static final String LOOKUP_VALUES_FETCH = "/lookup-values/fetch/{lookUpCategory}";

    public static final String AUTHORIZATION_OPTIONS_FETCH = "/authorization-options/fetch";

    public static final String DUPLICATE_TRANSACTIONS_CHECK = "/duplicate-transactions/check";

    @NonNull
    private final ChequeServicesImpl chequeService;

    @NonNull
    private final FullStatementService fullStatementService;

    @NonNull
    private final TermDepositServiceImpl termDepositServiceImpl;

    @NonNull
    private final LoanTermsServiceImpl loanTermsServiceImpl;

    @NonNull
    private final HardTokenService hardTokenServiceImpl;

    @NonNull
    private final SharedUtilities sharedUtilities;

    @NonNull
    private final ENairaService eNairaServiceImpl;

    private final DonationService donationServiceImpl;

    private final InAppBannerService inAppBannerServiceImpl;

    private final PesaLinkService pesaLinkServiceImpl;

    private final DonationsChargesService donationsChargesService;

    private final CustomerFeedbackService customerFeedbackService;

    private final XpressCashTokenChargesService xpressCashTokenChargesService;

    private final InternationalTransferChargesService internationalTransferChargesService;

    private final LookupValuesService lookupValuesService;

    private  final AuthorizationOptionsService authorizationOptionsService;

    private final DuplicateTransactionCheckService duplicateTransactionCheckService;

    /**
     * Process stop cheque book request response entity.
     *
     * @param headerParamsString    the header params string
     * @param stopChequeBookRequest the stop cheque book request
     * @return the response entity
     * @throws Exception the exception
     */
    @PostMapping(value = CHEQUE_SERVICES_STOP)
    public Mono<ResponseArray> processStopChequeBookRequest(@RequestHeader(Constants.X_HEADERS) String headerParamsString,
                                                            @Valid @RequestBody StopChequeBookRequest stopChequeBookRequest) throws Exception {
        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headerParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            try {
                //validation of parameters
                return chequeService.processStopChequeBookRequest(headerParams, stopChequeBookRequest);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }


    /**
     * Process confirm cheque request response entity.
     *
     * @param headerParamsString   the header params string
     * @param confirmChequeRequest the confirm cheque request
     * @return the response entity
     * @throws Exception the exception
     */
    @PostMapping(value = CHEQUE_SERVICES_CONFIRM)
    public Mono<ResponseArray> processConfirmChequeRequest(@RequestHeader(Constants.X_HEADERS) String headerParamsString,
                                                           @Valid @RequestBody ConfirmChequeRequest confirmChequeRequest) throws Exception {
        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headerParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            try {
                //validation of parameters
                return chequeService.processConfirmChequeRequest(headerParams, confirmChequeRequest);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }

    /**
     * Process cheque book leaves request response entity.
     *
     * @param headerParamsString the header params string
     * @return the response entity
     * @throws Exception the exception
     */
    @GetMapping(value = CHEQUE_SERVICES_BOOK_LEAVES)
    public Mono<ResponseArray> processChequeBookLeavesRequest(@RequestHeader(Constants.X_HEADERS) String headerParamsString) throws Exception {
        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headerParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            try {
                //validation of parameters
                return chequeService.processChequeBookLeavesRequest(headerParams);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }

    /**
     * Process request cheque book request response entity.
     *
     * @param headerParamsString       the header params string
     * @param requestChequeBookRequest the request cheque book request
     * @return the response entity
     * @throws Exception the exception
     */
    @PostMapping(value = CHEQUE_SERVICES_REQUEST)
    public Mono<ResponseArray> processRequestChequeBookRequest(@RequestHeader(Constants.X_HEADERS) String headerParamsString,
                                                               @Valid @RequestBody RequestChequeBookRequest requestChequeBookRequest) throws Exception {
        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headerParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            try {

                //validation of parameters
                return chequeService.processRequestChequeBookRequest(headerParams, requestChequeBookRequest);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }

    /**
     * Fetch check book branches response entity.
     *
     * @param headerParamsString the header params string
     * @return the response entity
     * @throws Exception the exception
     */
    @GetMapping(value = CHEQUE_SERVICES_BRANCHES_FETCH)
    public Mono<ResponseArray> fetchCheckBookBranches(@RequestHeader(Constants.X_HEADERS) String headerParamsString) throws Exception {
        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headerParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            try {
                //validation of parameters
                return chequeService.processFetchCheckBookBranches(headerParams);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }

    /**
     * Process full statement request response entity.
     *
     * @param headerParamsString   the header params string
     * @param fullStatementRequest the full statement request
     * @return the response entity
     * @throws Exception the exception
     */
    @PostMapping(value = FULL_STATEMENT_REQUEST)
    public Mono<ResponseArray> processFullStatementRequest(@RequestHeader(Constants.X_HEADERS) String headerParamsString,
                                                           @Valid @RequestBody FullStatementRequest fullStatementRequest) throws Exception {
        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headerParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            try {

                //validation of parameters
                return fullStatementService.processFullStatementRequest(headerParams, fullStatementRequest);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }

    /**
     * Process term deposit response entity.
     *
     * @param headParamsString   the head params string
     * @param termDepositRequest the term deposit request
     * @return the response entity
     * @throws Exception the exception
     */
    @PostMapping(value = TERM_DEPOSIT_REQUEST)
    public Mono<ResponseArray> processTermDeposit(@RequestHeader(Constants.X_HEADERS) String headParamsString,
                                                  @Valid @RequestBody TermDepositRequest termDepositRequest) throws Exception {
        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            try {
                // Recent Transaction Status Response
                return termDepositServiceImpl.processTermDepositRequest(headerParams, termDepositRequest);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }

    /**
     * Process loan terms response entity.
     *
     * @param headParamsString the head params string
     * @param loanTermsRequest the loan terms request
     * @return the response entity
     * @throws Exception the exception
     */
    @PostMapping(value = LOAN_INFORMATION_REQUEST)
    public Mono<ResponseArray> processLoanTerms(@RequestHeader(Constants.X_HEADERS) String headParamsString,
                                                @Valid @RequestBody LoanTermsRequest loanTermsRequest) throws Exception {
        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            try {
                // Loan terms request
                return loanTermsServiceImpl.processLoanTermsRequest(headerParams, loanTermsRequest);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });

    }

    /**
     * Fetch hard token request types mono.
     *
     * @param headerParamsString the header params string
     * @return the mono
     * @throws Exception the exception
     */
    @GetMapping(HARD_TOKEN_REQUEST_TYPES)
    public Mono<ResponseArray> fetchHardTokenRequestTypes(@RequestHeader(Constants.X_HEADERS) String headerParamsString) throws Exception {
        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headerParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            try {
                // Hard Token Request Types
                return hardTokenServiceImpl.fetchHardTokenRequestTypes(headerParams);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }

    /**
     * Fetch hard token delivery modes mono.
     *
     * @param headerParamsString the header params string
     * @return the mono
     * @throws Exception the exception
     */
    @GetMapping(HARD_TOKEN_DELIVERY_MODES)
    public Mono<ResponseArray> fetchHardTokenDeliveryModes(@RequestHeader(Constants.X_HEADERS) String headerParamsString) throws Exception {
        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headerParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            try {
                // Hard Token delivery modes
                return hardTokenServiceImpl.fetchHardTokenDeliveryModes(headerParams);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }


    /**
     * Fetch hard token branches mono.
     *
     * @param headerParamsString the header params string
     * @return the mono
     */
    @GetMapping(HARD_TOKEN_BRANCHES)
    public Mono<ResponseArray> fetchHardTokenBranches(@RequestHeader(Constants.X_HEADERS) String headerParamsString) {
        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headerParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            try {
                // Hard Token branches
                return hardTokenServiceImpl.fetchHardTokenBranches(headerParams);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }

    /**
     * Fetch hard token charges mono.
     *
     * @param headerParamsString the header params string
     * @return the mono
     */
    @PostMapping(HARD_TOKEN_CHARGES)
    public Mono<ResponseArray> fetchHardTokenCharges(@RequestHeader(Constants.X_HEADERS) String headerParamsString,
                                                     @Valid @RequestBody HardTokenChargeRequest hardTokenChargeRequest) {
        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headerParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            try {
                // Hard Token charges
                return hardTokenServiceImpl.fetchHardTokenCharges(headerParams, hardTokenChargeRequest);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }

    /**
     * Process hard token request mono.
     *
     * @param headerParamsString the header params string
     * @param hardTokenRequest   the hard token request
     * @return the mono
     */
    @PostMapping(HARD_TOKEN_REQUEST)
    public Mono<ResponseArray> processHardTokenRequest(@RequestHeader(Constants.X_HEADERS) String headerParamsString,
                                                       @Valid @RequestBody HardTokenRequest hardTokenRequest) {
        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headerParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            try {
                // Hard Token request
                return hardTokenServiceImpl.processHardTokenRequest(headerParams, hardTokenRequest);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }

    /**
     * Process hard token activate request mono.
     *
     * @param headerParamsString       the header params string
     * @param hardTokenActivateRequest the hard token activate request
     * @return the mono
     */
    @PostMapping(HARD_TOKEN_ACTIVATE)
    public Mono<ResponseArray> processHardTokenActivateRequest(@RequestHeader(Constants.X_HEADERS) String headerParamsString,
                                                               @Valid @RequestBody HardTokenActivateRequest hardTokenActivateRequest) {
        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headerParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            try {
                // Hard Token activate request
                return hardTokenServiceImpl.processHardTokenActivateRequest(headerParams, hardTokenActivateRequest);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }

    /**
     * Process hard token validate request mono.
     *
     * @param headerParamsString       the header params string
     * @param hardTokenValidateRequest the hard token validate request
     * @return the mono
     */
    @PostMapping(HARD_TOKEN_VALIDATE)
    public Mono<ResponseArray> processHardTokenValidateRequest(@RequestHeader(Constants.X_HEADERS) String headerParamsString,
                                                               @Valid @RequestBody HardTokenValidateRequest hardTokenValidateRequest) {
        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headerParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            try {
                // Hard Token activate request
                return hardTokenServiceImpl.processHardTokenCheckPendingRequest(headerParams, hardTokenValidateRequest);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }

    /**
     * Process hard token check pending request mono.
     *
     * @param headerParamsString           the header params string
     * @param hardTokenCheckPendingRequest the hard token check pending request
     * @return the mono
     */
    @PostMapping(HARD_TOKEN_CHECK_PENDING_REQUEST)
    public Mono<ResponseArray> processHardTokenCheckPendingRequest(@RequestHeader(Constants.X_HEADERS) String headerParamsString,
                                                                   @Valid @RequestBody HardTokenCheckPendingRequest hardTokenCheckPendingRequest) {
        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headerParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            try {
                // Hard Token activate request
                return hardTokenServiceImpl.processHardTokenCheckPendingRequest(headerParams, hardTokenCheckPendingRequest);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }


    /**
     * Process enaira authenticate request mono.
     *
     * @param headerParamsString       the header params string
     * @param eNairaAuthenticateRequest the enaira authenticate request
     * @return the mono
     */
    @PostMapping(ENAIRA_AUTHENTICATE_REQUEST)
    public Mono<ResponseArray> eNairaAuthenticate(@RequestHeader(Constants.X_HEADERS) String headerParamsString,
                                                               @Valid @RequestBody ENairaAuthenticateRequest eNairaAuthenticateRequest) {
        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headerParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            try {
                // Hard Token activate request
                return eNairaServiceImpl.processENairaAuthenticate(headerParams, eNairaAuthenticateRequest);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }


    /**
     * Process enaira preauthorization response entity.
     *
     * @param headParamsString          the head params string
     * @param eNairaPreAuthorizationRequest the enaira preauthorization request
     * @return the response entity
     * @throws Exception the exception
     */
    @PostMapping(value = ENAIRA_PRE_AUTHORIZATION)
    public Mono<ResponseArray> processENairaPreAuthorization(@RequestHeader(Constants.X_HEADERS) String headParamsString,
                                                               @Valid @RequestBody ENairaPreAuthorizationRequest eNairaPreAuthorizationRequest)
            throws Exception {

        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            try {
                return eNairaServiceImpl.processEnairaPreAuthorization(headerParams, eNairaPreAuthorizationRequest);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }


    /**
     * Process enaira name inquiry response entity.
     *
     * @param headParamsString          the head params string
     * @param eNairaNameInquiryRequest the enaira preauthorization request
     * @return the response entity
     * @throws Exception the exception
     */
    @PostMapping(value = ENAIRA_NAME_INQUIRY)
    public Mono<ResponseArray> processENairaNameInquiry(@RequestHeader(Constants.X_HEADERS) String headParamsString,
                                                             @Valid @RequestBody ENairaNameInquiryRequest eNairaNameInquiryRequest)
            throws Exception {

        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            try {
                return eNairaServiceImpl.processNameInquiry(headerParams, eNairaNameInquiryRequest);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }

    /**
     * Fetch Charity Organizations mono.
     *
     * @param headerParamsString the header params string
     * @return the mono
     */
    @GetMapping(DONATIONS_CHARITY_ORGANISATIONS)
    public Mono<ResponseArray> fetchCharityOrganizations(@RequestHeader(Constants.X_HEADERS) String headerParamsString) {
        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headerParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            try {
                // Donation Charities
                return donationServiceImpl.fetchCharityOrganisations(headerParams);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }


    @PostMapping(DONATIONS_CHARGES)
    public Mono<ResponseArray> fetchDonationCharges(@RequestHeader(Constants.X_HEADERS) String headerParamsString,
                                                    @Valid @RequestBody  DonationsChargesRequest donationsChargesRequest){
        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headerParamsString);

        return headerParamsMono.flatMap(headerParams -> {
            try {
                // Donation Charities
                return donationsChargesService.processFetchDonationCharges(headerParams, donationsChargesRequest);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }



    /**
     * Fetch Customer Active Banner mono.
     *
     * @param headerParamsString the header params string
     * @return the mono
     */
    @GetMapping(CUSTOMERS_ACTIVE_BANNER)
    public Mono<ResponseArray> fetchCustomerActiveBanner(@RequestHeader(Constants.X_HEADERS) String headerParamsString) {
        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headerParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            try {
                // Active banner
                return inAppBannerServiceImpl.fetchActiveBanner(headerParams);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }
    /**
     * Fetch Pesalink Banks mono.
     *
     * @param headParamsString the header params string
     * @return the mono
     */
    @GetMapping(PESALINK_BANKS)
    public Mono<ResponseArray> fetchCustomerPesaLinkBanks(@RequestHeader(Constants.X_HEADERS) String headParamsString,@PathVariable String proxyAlias)
                                                           {
        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            try {
                // PesaLink Banks
                return pesaLinkServiceImpl.fetchCustomerPesaLinkBanks(headerParams,proxyAlias);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }

    /**
     * Process Customer Feedback mono.
     * @param headParamsString the header params string
     * @return the mono
     */
    @PostMapping(CUSTOMER_FEEDBACK_CREATE)
    public Mono<ResponseArray> customerFeedBack(@RequestHeader(Constants.X_HEADERS) String headParamsString,
                                                @Valid @RequestBody CustomerFeedBackRequest customerFeedBackRequestDto)
                                                           {
        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            try {
                // process customer feedback
                return customerFeedbackService.processCustomerFeedback(headerParams,customerFeedBackRequestDto);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }

    /**
     *
     * @param headerParamsString
     * @param xpressCashTokenChargesRequestDto
     * @return the mono
     */
    @PostMapping(XPRESS_CASH_TOKEN_CHARGES)
    public Mono<ResponseArray> fetchXpressCashTokenCharges(@RequestHeader(Constants.X_HEADERS) String headerParamsString,
                                                    @Valid @RequestBody XpressCashTokenChargesRequestDto xpressCashTokenChargesRequestDto){
        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headerParamsString);

        return headerParamsMono.flatMap(headerParams -> {
            try {
                // Xpress Cash Token Charities
                return xpressCashTokenChargesService.processFetchXpressCashTokenCharges(headerParams, xpressCashTokenChargesRequestDto);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }

    /**
     *
     * @param headerParamsString
     * @param internationalTransferChargesRequestDto
     * @return the mono
     */
    @PostMapping(INTERNATIONAL_TRANSFER_CHARGES)
    public Mono<ResponseArray> fetchInternationalTrnaferCharges(@RequestHeader(Constants.X_HEADERS) String headerParamsString,
                                                           @Valid @RequestBody InternationalTransferChargesRequestDto internationalTransferChargesRequestDto){
        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headerParamsString);

        return headerParamsMono.flatMap(headerParams -> {
            try {
                // Fetch International Transfer Charges
                return internationalTransferChargesService.processFetchInternationalTransferCharges(headerParams, internationalTransferChargesRequestDto);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }

    /**
     * Fetch lookup values via lookUpCategory
     * @param headerParamsString x - headers
     * @param lookUpCategory - look up category to fetch lookup values with
     * @return
     */
    @GetMapping(LOOKUP_VALUES_FETCH)
    public Mono<ResponseArray> fetchAuthorizationOptions(@RequestHeader(Constants.X_HEADERS) String headerParamsString, @PathVariable String lookUpCategory){
        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headerParamsString);

        return headerParamsMono.flatMap(headerParams -> {
            try {
                // Fetch Authorization Options
                return lookupValuesService.processFetchLookupValues(headerParams, lookUpCategory);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }

    @GetMapping(AUTHORIZATION_OPTIONS_FETCH)
    public Mono<ResponseArray> fetchAuthorizationOptions(@RequestHeader(Constants.X_HEADERS) String headerParamsString) throws Exception {

        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headerParamsString);

        return headerParamsMono.flatMap(headerParams -> {
            try {
                // Fetch Authorization Options
                return authorizationOptionsService.processFetchAuthorizationOptions(headerParams);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }

    /**
     * Perform duplicate transaction request
     * @param headerParamsString - Header param string
     * @param duplicateTransactionCheckRequestDto - Duplicate transaction check request dto
     * @return - Mono of response array
     */
    @PostMapping(DUPLICATE_TRANSACTIONS_CHECK)
    public Mono<ResponseArray> handleDuplicateTransactionCheck(@RequestHeader(Constants.X_HEADERS) String headerParamsString,
                                                                @Valid @RequestBody DuplicateTransactionCheckRequestDto duplicateTransactionCheckRequestDto){
        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headerParamsString);

        return headerParamsMono.flatMap(headerParams -> {
            try {
                // Fetch International Transfer Charges
                return duplicateTransactionCheckService.processDuplicateTransactionCheck(headerParams, duplicateTransactionCheckRequestDto);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }

}
