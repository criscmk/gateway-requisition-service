package com.cellulant.gatewayrequisitionservices.controllers;


import com.cellulant.gatewayrequisitionservices.balanceenquiry.requests.BalanceEnquiryBulkRequest;
import com.cellulant.gatewayrequisitionservices.balanceenquiry.requests.BalanceEnquiryRequest;
import com.cellulant.gatewayrequisitionservices.balanceenquiry.services.BalanceEnquiryService;
import com.cellulant.gatewayrequisitionservices.configurations.Constants;
import com.cellulant.gatewayrequisitionservices.dtos.responses.ResponseArray;
import com.cellulant.gatewayrequisitionservices.enaira.requests.ENairaBalanceEnquiryBulkRequest;
import com.cellulant.gatewayrequisitionservices.enaira.requests.ENairaMiniStatementRequest;
import com.cellulant.gatewayrequisitionservices.enaira.services.ENairaService;
import com.cellulant.gatewayrequisitionservices.forex.requests.ForexRequest;
import com.cellulant.gatewayrequisitionservices.forex.services.ForexServiceImpl;
import com.cellulant.gatewayrequisitionservices.ministatement.requests.MiniStatementRequest;
import com.cellulant.gatewayrequisitionservices.ministatement.services.MiniStatementService;
import com.cellulant.gatewayrequisitionservices.recentactivity.services.RecentActivityService;
import com.cellulant.gatewayrequisitionservices.transactionstatus.dtos.TransactionStatusRequest;
import com.cellulant.gatewayrequisitionservices.transactionstatus.services.TransactionServiceImpl;
import com.cellulant.gatewayrequisitionservices.utils.SharedUtilities;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Map;

/**
 * The type Enquiries controller.
 */
@RestController
@RequiredArgsConstructor
public class EnquiriesController {

    public static final String BALANCE_ENQUIRY_REQUEST = "/balance-enquiry/request";
    public static final String BALANCE_ENQUIRY_MULTIPLE = "/balance-enquiry/multiple";
    public static final String RECENT_ACTIVITY_FETCH = "/recent-activity/fetch";
    public static final String MINI_STATEMENT_REQUEST = "/mini-statement/request";
    public static final String TRANSACTION_STATUS_FETCH = "/transaction-status/fetch";
    public static final String FOREX_RATES = "/forex/rates";
    public static final String ENAIRA_BALANCE_ENQUIRY_MULTIPLE = "/enaira/balance-enquiry";
    public static final String ENAIRA_MINI_STATEMENT_REQUEST = "/enaira/mini-statement";


    @NonNull
    private final MiniStatementService miniStatementService;
    @NonNull
    private final RecentActivityService recentActivityService;
    @NonNull
    private final BalanceEnquiryService balanceEnquiryService;
    @NonNull
    private final TransactionServiceImpl transactionService;
    @NonNull
    private final ForexServiceImpl forexServiceImpl;
    @NonNull
    private final SharedUtilities sharedUtilities;
    @NotNull
    private final ENairaService eNairaServiceImpl;


    /**
     * Process balance enquiry response entity.
     *
     * @param headParamsString      the header string params
     * @param balanceEnquiryRequest the balance enquiry request
     * @return the response entity
     * @throws Exception the exception
     */
    @PostMapping(value = BALANCE_ENQUIRY_REQUEST)
    public Mono<ResponseArray> processBalanceEnquiry(@RequestHeader(Constants.X_HEADERS) String headParamsString,
                                                     @Valid @RequestBody BalanceEnquiryRequest balanceEnquiryRequest)
            throws Exception {

        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            try {
                return balanceEnquiryService.processBalanceEnquiry(headerParams, balanceEnquiryRequest);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }


    /**
     * Process bulk balance enquiry response entity.
     *
     * @param headParamsString          the head params string
     * @param bulkbalanceEnquiryRequest the balance enquiry request
     * @return the response entity
     * @throws Exception the exception
     */
    @PostMapping(value = BALANCE_ENQUIRY_MULTIPLE)
    public Mono<ResponseArray> processBulkBalanceEnquiry(@RequestHeader(Constants.X_HEADERS) String headParamsString,
                                                         @Valid @RequestBody BalanceEnquiryBulkRequest bulkbalanceEnquiryRequest)
            throws Exception {

        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            try {
                return balanceEnquiryService.processBulkBalanceEnquiry(headerParams, bulkbalanceEnquiryRequest);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }



    /**
     * Process recent activity request response entity.
     *
     * @param headParamsString the header string params
     * @return the response entity
     * @throws Exception the exception
     */
    @GetMapping(value = RECENT_ACTIVITY_FETCH)
    public Mono<ResponseArray> processRecentActivityRequest(@RequestHeader(Constants.X_HEADERS) String headParamsString) throws Exception {

        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            try {
                return recentActivityService.fetchRecentActivity(headerParams);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }

    /**
     * Process mini statement request response entity.
     *
     * @param headParamsString     the header string params
     * @param miniStatementRequest the mini statement request
     * @return the response entity
     * @throws Exception the exception
     */
    @PostMapping(value = MINI_STATEMENT_REQUEST)
    public Mono<ResponseArray> processMiniStatementRequest(@RequestHeader(Constants.X_HEADERS) String headParamsString,
                                                           @Valid @RequestBody MiniStatementRequest miniStatementRequest
    ) throws Exception {

        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            try {
                return miniStatementService.fetchMiniStatement(headerParams, miniStatementRequest);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }


    /**
     * Process transaction status response entity.
     *
     * @param headParamsString the header string params
     * @return the response entity
     * @throws Exception the exception
     */
    @PostMapping(value = TRANSACTION_STATUS_FETCH)
    public Mono<ResponseArray> processTransactionStatus(@RequestHeader(Constants.X_HEADERS) String headParamsString,
                                                        @Valid @RequestBody TransactionStatusRequest transactionStatusRequest)
            throws Exception {
        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            try {
                return transactionService.processTransactionStatusRequest(headerParams, transactionStatusRequest);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }

    /**
     * Process fetch forex rates response entity.
     *
     * @param headParamsString the head params string
     * @param forexRequest     the forex request
     * @return the response entity
     * @throws Exception the exception
     */
    @PostMapping(value = FOREX_RATES)
    public Mono<ResponseArray> processFetchForexRates(@RequestHeader(Constants.X_HEADERS) String headParamsString,
                                                      @Valid @RequestBody ForexRequest forexRequest)
            throws Exception {

        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            try {
                return forexServiceImpl.processFetchForexRatesRequest(headerParams, forexRequest);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }

    /**
     * Process enaira bulk balance enquiry response entity.
     *
     * @param headParamsString          the head params string
     * @param eNairaBalanceEnquiryBulkRequest the balance enquiry request
     * @return the response entity
     * @throws Exception the exception
     */
    @PostMapping(value = ENAIRA_BALANCE_ENQUIRY_MULTIPLE)
    public Mono<ResponseArray> processENairaBulkBalanceEnquiry(@RequestHeader(Constants.X_HEADERS) String headParamsString,
                                                               @Valid @RequestBody ENairaBalanceEnquiryBulkRequest eNairaBalanceEnquiryBulkRequest)
            throws Exception {

        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            try {
                return eNairaServiceImpl.processEnairaBalanceEnquiry(headerParams, eNairaBalanceEnquiryBulkRequest);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }


    /**
     * Process enaira mini statement request response entity.
     *
     * @param headParamsString     the header string params
     * @param eNairaMiniStatementRequest the enaira mini statement request
     * @return the response entity
     * @throws Exception the exception
     */
    @PostMapping(value = ENAIRA_MINI_STATEMENT_REQUEST)
    public Mono<ResponseArray> processENairaMiniStatementRequest(@RequestHeader(Constants.X_HEADERS) String headParamsString,
                                                           @Valid @RequestBody ENairaMiniStatementRequest eNairaMiniStatementRequest
    ) throws Exception {

        Mono<Map> headerParamsMono = sharedUtilities.formulateHeaderHashMap(headParamsString);
        return headerParamsMono.flatMap(headerParams -> {
            try {
                return eNairaServiceImpl.fetchMiniStatement(headerParams, eNairaMiniStatementRequest);
            } catch (Exception e) {
                return Mono.error(e);
            }
        });
    }




}
