package com.cellulant.gatewayrequisitionservices.balanceenquiry.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * The type Balance enquiry request.
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BalanceEnquiryRequest implements Serializable {

    @NotNull(message = "{accountAlias.required}")
    private String accountAlias;
    @NotBlank(message = "{pin.required}")
//    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String pin;
    private String service;
}