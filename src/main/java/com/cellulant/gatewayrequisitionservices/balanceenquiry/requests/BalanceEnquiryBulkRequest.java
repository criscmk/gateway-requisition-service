package com.cellulant.gatewayrequisitionservices.balanceenquiry.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * The type Balance enquiry request.
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BalanceEnquiryBulkRequest implements Serializable {

    @NotNull(message = "{accountAlias.required}")
    private String accountAlias;
    @NotNull(message = "{pin.required}")
    //@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String pin;
    @NotNull(message = "{accountNumbers.required}")
    private String accountNumbers;

    private String service;
}