package com.cellulant.gatewayrequisitionservices.hardtoken.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * Created by eyo
 * Project gateway-requisition-services
 * User: eyo
 * Date: 03/03/2021
 * Time: 9:25 AM
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class HardTokenValidateRequest {
    @NotNull
    private String accountAlias;
    @NotNull
    private String tokenResponse;
    @NotNull(message = "{pin.required}")
    //@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String pin;
    private String requestType;
    private String beneficiaryID;

}
