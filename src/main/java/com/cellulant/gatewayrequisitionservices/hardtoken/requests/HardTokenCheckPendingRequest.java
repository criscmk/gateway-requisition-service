package com.cellulant.gatewayrequisitionservices.hardtoken.requests;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * Created by eyo
 * Project gateway-requisition-services
 * User: eyo
 * Date: 03/03/2021
 * Time: 9:25 AM
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class HardTokenCheckPendingRequest {
    @NotNull(message = "{account.required}")
    private String accountAlias;
}
