package com.cellulant.gatewayrequisitionservices.hardtoken.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * Created by eyo
 * Project gateway-requisition-services
 * User: eyo
 * Date: 03/03/2021
 * Time: 9:04 AM
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class HardTokenRequest {
    @NotNull
    private String accountAlias;
    @NotNull
    private String requestType;
    @NotNull
    private String modeOfDelivery;
    @NotNull
    private String pickUpBranch;
    @NotNull(message = "{pin.required}")
    //@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String pin;
}
