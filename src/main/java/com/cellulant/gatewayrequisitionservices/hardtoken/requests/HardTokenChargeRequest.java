package com.cellulant.gatewayrequisitionservices.hardtoken.requests;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * @author Denis Gitonga
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class HardTokenChargeRequest {

    @NotNull(message = "{modeOfDelivery.required}")
    private String modeOfDelivery;
}
