package com.cellulant.gatewayrequisitionservices.hardtoken.services;

import com.cellulant.gatewayrequisitionservices.beneficiary.UpdateBeneficiaryAuthenticationStatus;
import com.cellulant.gatewayrequisitionservices.configurations.Configs;
import com.cellulant.gatewayrequisitionservices.configurations.Constants;
import com.cellulant.gatewayrequisitionservices.configurations.StatusCodeConfig;
import com.cellulant.gatewayrequisitionservices.dtos.responses.ResponseArray;
import com.cellulant.gatewayrequisitionservices.dtos.walletdtos.WalletResponse;
import com.cellulant.gatewayrequisitionservices.hardtoken.requests.*;
import com.cellulant.gatewayrequisitionservices.models.ServicesEntity;
import com.cellulant.gatewayrequisitionservices.services.ServiceImpl;
import com.cellulant.gatewayrequisitionservices.utils.SharedUtilities;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.http.HttpMethod;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Created by eyo
 * Project gateway-requisition-services
 * User: eyo
 * Date: 03/03/2021
 * Time: 8:35 AM
 */
@Service
@RequiredArgsConstructor
@SuppressWarnings("Duplicates")
@Slf4j
public class HardTokenService {
    @NonNull
    private final SharedUtilities sharedUtilities;

    @NonNull
    private final ServiceImpl serviceImpl;

    @NonNull
    private final Configs configs;

    private final StatusCodeConfig statusCodeConfig;

    /**
     * Fetch hard token request types mono.
     *
     * @param headerParams the header params
     * @return the mono
     * @throws Exception the exception
     */
    public Mono<ResponseArray> fetchHardTokenRequestTypes(Map headerParams) throws Exception {
        //Get action from headers
        String action = sharedUtilities.retrieveActionFromHeaders(headerParams);
        //Retrieve the Service Details form Database
        final Mono<ServicesEntity> servicesEntityMono = serviceImpl.getServiceDetails((String.valueOf(headerParams.get(Constants.SERVICE))));

        return servicesEntityMono.flatMap(servicesEntity -> {
            // Get msisdn
            final Mono<String> msisdnMono = sharedUtilities.getMsisdn(headerParams, action, servicesEntity)
                    .doOnNext(msisdn -> MDC.put(Constants.MSISDN, String.valueOf(msisdn)));

            // Retrieve Wallet URL from Database using provided action
            final Mono<String> walletUrlMono = sharedUtilities.getWalletUrl(action, servicesEntity);


            return msisdnMono.flatMap(msisdn -> {
                // update headerParams with updated msisdn
                final Mono<Map> headerParamsMono = Mono.just(headerParams).map(map -> {
                    map.replace(Constants.MSISDN, msisdn);
                    return map;
                });

                return headerParamsMono.then(walletUrlMono)
                        .flatMap(sharedUtilities::getIntegrationLayerResponse)
                        .flatMap(sharedUtilities::handleResponse);
            });
        });
    }

    /**
     * Fetch hard token delivery modes mono.
     *
     * @param headerParams the header params
     * @return the mono
     * @throws Exception the exception
     */
    public Mono<ResponseArray>  fetchHardTokenDeliveryModes(Map headerParams) throws Exception {
        //Get action from headers
        String action = sharedUtilities.retrieveActionFromHeaders(headerParams);
        //Retrieve the Service Details form Database
        final Mono<ServicesEntity> servicesEntityMono = serviceImpl.getServiceDetails((String.valueOf(headerParams.get(Constants.SERVICE))));

        return servicesEntityMono.flatMap(servicesEntity -> {
            // Get msisdn
            final Mono<String> msisdnMono = sharedUtilities.getMsisdn(headerParams, action, servicesEntity)
                    .doOnNext(msisdn -> MDC.put(Constants.MSISDN, String.valueOf(msisdn)));

            // Retrieve Wallet URL from Database using provided action
            final Mono<String> walletUrlMono = sharedUtilities.getWalletUrl(action, servicesEntity);


            return msisdnMono.flatMap(msisdn -> {
                // update headerParams with updated msisdn
                final Mono<Map> headerParamsMono = Mono.just(headerParams).map(map -> {
                    map.replace(Constants.MSISDN, msisdn);
                    return map;
                });

                return headerParamsMono.then(walletUrlMono)
                        .flatMap(sharedUtilities::getIntegrationLayerResponse)
                        .flatMap(sharedUtilities::handleResponse);
            });
        });
    }

    /**
     * Fetch hard token branches mono.
     *
     * @param headerParams the header params
     * @return the mono
     * @throws Exception the exception
     */
    public Mono<ResponseArray> fetchHardTokenBranches(Map headerParams) throws Exception {
        //Get action from headers
        String action = sharedUtilities.retrieveActionFromHeaders(headerParams);
        //Retrieve the Service Details form Database
        final Mono<ServicesEntity> servicesEntityMono = serviceImpl.getServiceDetails((String.valueOf(headerParams.get(Constants.SERVICE))));

        return servicesEntityMono.flatMap(servicesEntity -> {
            // Get msisdn
            final Mono<String> msisdnMono = sharedUtilities.getMsisdn(headerParams, action, servicesEntity)
                    .doOnNext(msisdn -> MDC.put(Constants.MSISDN, String.valueOf(msisdn)));

            // Retrieve Wallet URL from Database using provided action
            final Mono<String> walletUrlMono = sharedUtilities.getWalletUrl(action, servicesEntity);

            return Mono.zip(msisdnMono, walletUrlMono).flatMap(data -> {

                final String msisdn = data.getT1();
                final String walletUrl = data.getT2();

                // update headerParams with updated msisdn
                final Mono<Map> headerParamsMono = Mono.just(headerParams).map(map -> {
                    map.replace(Constants.MSISDN, msisdn);
                    return map;
                });

                return headerParamsMono.flatMap(headers -> {
                    final Mono<String> requestIDMono = sharedUtilities.getRequestID(null, msisdn, headers)
                            .doOnNext(requestID -> MDC.put(Constants.CLOUD_REQUEST_ID, String.valueOf(requestID)));

                    return requestIDMono
                            .flatMap(requestID -> sharedUtilities.getIntegrationLayerResponse(walletUrl, requestID))
                            .flatMap(sharedUtilities::handleResponse);
                });
            });
        });
    }

    /**
     * Fetch hard token charges mono.
     *
     * @param headerParams the header params
     * @return the mono
     * @throws Exception the exception
     */
    public Mono<ResponseArray> fetchHardTokenCharges(Map headerParams, HardTokenChargeRequest hardTokenChargeRequest) throws Exception {
        //Get action from headers
        String action = sharedUtilities.retrieveActionFromHeaders(headerParams);
        final String uuid = String.valueOf(headerParams.get(Constants.UUID));
        //Retrieve the Service Details form Database
        final Mono<ServicesEntity> servicesEntityMono = serviceImpl.getServiceDetails((String.valueOf(headerParams.get(Constants.SERVICE))));

        return servicesEntityMono.flatMap(servicesEntity -> {
            // Get msisdn
            final Mono<String> msisdnMono = sharedUtilities.getMsisdn(headerParams, action, servicesEntity)
                    .doOnNext(msisdn -> MDC.put(Constants.MSISDN, String.valueOf(msisdn)));

            // Retrieve Wallet URL from Database using provided action
            final Mono<String> walletUrlMono = sharedUtilities.getWalletUrl(action, servicesEntity);

            return Mono.zip(msisdnMono, walletUrlMono).flatMap(data -> {

                final String msisdn = data.getT1();
                final String walletUrl = data.getT2();

                // update headerParams with updated msisdn
                final Mono<Map> headerParamsMono = Mono.just(headerParams).map(map -> {
                    map.replace(Constants.MSISDN, msisdn);
                    return map;
                });

                return headerParamsMono.flatMap(headers -> {
                    final Mono<String> requestIDMono = sharedUtilities.getRequestID(null, msisdn, headers)
                            .doOnNext(requestID -> MDC.put(Constants.CLOUD_REQUEST_ID, String.valueOf(requestID)));

                    return requestIDMono
                            .flatMap(requestID -> sharedUtilities.getIntegrationLayerResponse(hardTokenChargeRequest, requestID, walletUrl))
                            .flatMap(sharedUtilities::handleResponse);
                });
            });
        });
    }

    /**
     * Process hard token request mono.
     *
     * @param headerParams     the header params
     * @param hardTokenRequest the hard token request
     * @return the mono
     * @throws Exception the exception
     */
    public Mono<ResponseArray> processHardTokenRequest(Map headerParams, HardTokenRequest hardTokenRequest) throws Exception {
        //Get action from headers
        final String action = sharedUtilities.retrieveActionFromHeaders(headerParams);
        final String uuid = String.valueOf(headerParams.get(Constants.UUID));
        //Retrieve the Service Details form Database
        final Mono<ServicesEntity> servicesEntityMono = serviceImpl.getServiceDetails((String.valueOf(headerParams.get(Constants.SERVICE))));

        return servicesEntityMono.flatMap(servicesEntity -> {
            // Get msisdn
            final Mono<String> msisdnMono = sharedUtilities.getMsisdn(headerParams, action, servicesEntity)
                    .doOnNext(msisdn -> MDC.put(Constants.MSISDN, String.valueOf(msisdn)));

            // Retrieve Wallet URL from Database using provided action
            final Mono<String> walletUrlMono = sharedUtilities.getWalletUrl(action, servicesEntity);

            final Mono<String> hardTokenRequestJsonMono = Mono.just(hardTokenRequest)
                    .flatMap(hardTokenRequest1 -> Mono.fromCallable(() -> {
                        // Encrypt the Pin
                        hardTokenRequest1.setPin(sharedUtilities.encrypt(hardTokenRequest1.getPin()));

                        // Formulate Wallet Payload
                        ObjectMapper objectMapper = new ObjectMapper();
                        objectMapper.disable(MapperFeature.USE_ANNOTATIONS);
                        return objectMapper.writeValueAsString(hardTokenRequest1);
                    }));


            return Mono.zip(msisdnMono, walletUrlMono, hardTokenRequestJsonMono)
                    .flatMap(data -> {
                        final String msisdn = data.getT1();
                        final String walletUrl = data.getT2();
                        final String hardTokenRequestJson = data.getT3();

                        // update headerParams with updated msisdn
                        final Mono<Map> headerParamsMono = Mono.just(headerParams).map(map -> {
                            map.replace(Constants.MSISDN, msisdn);
                            return map;
                        });

                        return headerParamsMono.flatMap(headers -> {
                            final Mono<String> requestIDMono = sharedUtilities.getRequestID(hardTokenRequest, msisdn, headers)
                                    .doOnNext(requestID -> MDC.put(Constants.CLOUD_REQUEST_ID, String.valueOf(requestID)));

                            //Validate Uuid
                            final Mono<Void> validateUUID = sharedUtilities.validateUUID(servicesEntity, msisdn, action, uuid);

                            return validateUUID.then(requestIDMono)
                                    .flatMap(requestID -> sharedUtilities
                                            .processIntegrationResponse(headers, msisdn, requestID, servicesEntity, walletUrl, hardTokenRequestJson));

                        });
                    });
        });
    }

    /**
     * Process hard token activate request mono.
     *
     * @param headerParams             the header params
     * @param hardTokenActivateRequest the hard token activate request
     * @return the mono
     * @throws Exception the exception
     */
    public Mono<ResponseArray> processHardTokenActivateRequest(Map headerParams, HardTokenActivateRequest hardTokenActivateRequest) throws Exception {
        //Get action from headers
        final String action = sharedUtilities.retrieveActionFromHeaders(headerParams);
        final String uuid = String.valueOf(headerParams.get(Constants.UUID));
        //Retrieve the Service Details form Database
        final Mono<ServicesEntity> servicesEntityMono = serviceImpl.getServiceDetails((String.valueOf(headerParams.get(Constants.SERVICE))));

        return servicesEntityMono.flatMap(servicesEntity -> {
            // Get msisdn
            final Mono<String> msisdnMono = sharedUtilities.getMsisdn(headerParams, action, servicesEntity)
                    .doOnNext(msisdn -> MDC.put(Constants.MSISDN, String.valueOf(msisdn)));

            // Retrieve Wallet URL from Database using provided action
            final Mono<String> walletUrlMono = sharedUtilities.getWalletUrl(action, servicesEntity);

            final Mono<String> hardTokenActivateRequestJsonMono = Mono.just(hardTokenActivateRequest)
                    .flatMap(hardTokenActivateRequest1 -> Mono.fromCallable(() -> {
                        // Encrypt the Pin
                        hardTokenActivateRequest1.setPin(sharedUtilities.encrypt(hardTokenActivateRequest1.getPin()));

                        // Formulate Wallet Payload
                        ObjectMapper objectMapper = new ObjectMapper();
                        objectMapper.disable(MapperFeature.USE_ANNOTATIONS);
                        return objectMapper.writeValueAsString(hardTokenActivateRequest1);
                    }));


            return Mono.zip(msisdnMono, walletUrlMono, hardTokenActivateRequestJsonMono)
                    .flatMap(data -> {
                        final String msisdn = data.getT1();
                        final String walletUrl = data.getT2();
                        final String hardTokenActivateRequestJson = data.getT3();

                        // update headerParams with updated msisdn
                        final Mono<Map> headerParamsMono = Mono.just(headerParams).map(map -> {
                            map.replace(Constants.MSISDN, msisdn);
                            return map;
                        });

                        return headerParamsMono.flatMap(headers -> {
                            final Mono<String> requestIDMono = sharedUtilities.getRequestID(hardTokenActivateRequest, msisdn, headers)
                                    .doOnNext(requestID -> MDC.put(Constants.CLOUD_REQUEST_ID, String.valueOf(requestID)));

                            //Validate Uuid
                            final Mono<Void> validateUUID = sharedUtilities.validateUUID(servicesEntity, msisdn, action, uuid);

                            return validateUUID.then(requestIDMono)
                                    .flatMap(requestID -> sharedUtilities
                                            .processIntegrationResponse(headers, msisdn, requestID, servicesEntity, walletUrl, hardTokenActivateRequestJson));

                        });
                    });
        });
    }

    /**
     * Process hard token validate request mono.
     *
     * @param headerParams             the header params
     * @param hardTokenValidateRequest the hard token validate request
     * @return the mono
     * @throws Exception the exception
     */
    public Mono<ResponseArray> processHardTokenCheckPendingRequest(Map headerParams, HardTokenValidateRequest hardTokenValidateRequest) throws Exception {

        if(Boolean.parseBoolean(this.configs.getHardTokenMockEnabled())){
            // Formulate the token response.
            var statusHash = new HashMap<String, Object>();
            statusHash.put("status", true);
            var dataList = new ArrayList<>();
            dataList.add(statusHash);
            var resultHash = new HashMap<String, Object>();
            resultHash.put("message", "Dear customer, your transaction has been successfully processed");
            resultHash.put("referenceID","1329136908");
            resultHash.put("data",dataList);


            final var responseArray = ResponseArray.builder().statusCode(5000)
                    .statusMessage("SUCCESS")
                    .result(resultHash).build();
            var msisdn = String.valueOf(headerParams.get(Constants.MSISDN));

            if(hardTokenValidateRequest.getRequestType().equalsIgnoreCase(Constants.EXISTING_NOT_AUTHENTICATED_REQUEST_TYPE))
            {
                return updateBeneficiaryAuthenticationStatus(hardTokenValidateRequest.getBeneficiaryID(), hardTokenValidateRequest.getTokenResponse(), msisdn).flatMap(walletResponse -> Mono.just(responseArray));

            }
            return Mono.just(responseArray);
        }
        //Get action from headers
        final String action = sharedUtilities.retrieveActionFromHeaders(headerParams);
        final String uuid = String.valueOf(headerParams.get(Constants.UUID));
        //Retrieve the Service Details form Database
        final Mono<ServicesEntity> servicesEntityMono = serviceImpl.getServiceDetails((String.valueOf(headerParams.get(Constants.SERVICE))));

        return servicesEntityMono.flatMap(servicesEntity -> {
            // Get msisdn
            final Mono<String> msisdnMono = sharedUtilities.getMsisdn(headerParams, action, servicesEntity)
                    .doOnNext(msisdn -> MDC.put(Constants.MSISDN, String.valueOf(msisdn)));

            // Retrieve Wallet URL from Database using provided action
            final Mono<String> walletUrlMono = sharedUtilities.getWalletUrl(action, servicesEntity);

            final Mono<String> hardTokenValidateRequestJsonMono = Mono.just(hardTokenValidateRequest)
                    .flatMap(hardTokenValidateRequest1 -> Mono.fromCallable(() -> {
                        // Encrypt the Pin
                        hardTokenValidateRequest1.setPin(sharedUtilities.encrypt(hardTokenValidateRequest1.getPin()));

                        // Formulate Wallet Payload
                        ObjectMapper objectMapper = new ObjectMapper();
                        objectMapper.disable(MapperFeature.USE_ANNOTATIONS);
                        return objectMapper.writeValueAsString(hardTokenValidateRequest1);
                    }));


            return Mono.zip(msisdnMono, walletUrlMono, hardTokenValidateRequestJsonMono)
                    .flatMap(data -> {
                        final String msisdn = data.getT1();
                        final String walletUrl = data.getT2();
                        final String hardTokenValidateRequestJson = data.getT3();

                        // update headerParams with updated msisdn
                        final Mono<Map> headerParamsMono = Mono.just(headerParams).map(map -> {
                            map.replace(Constants.MSISDN, msisdn);
                            return map;
                        });

                        return headerParamsMono.flatMap(headers -> {
                            final Mono<String> requestIDMono = sharedUtilities.getRequestID(hardTokenValidateRequest, msisdn, headers)
                                    .doOnNext(requestID -> MDC.put(Constants.CLOUD_REQUEST_ID, String.valueOf(requestID)));

                            //Validate Uuid
                            final Mono<Void> validateUUID = sharedUtilities.validateUUID(servicesEntity, msisdn, action, uuid);

                            return validateUUID.then(requestIDMono)
                                    .flatMap(requestID -> sharedUtilities
                                            .processIntegrationResponse(headers, msisdn, requestID, servicesEntity, walletUrl, hardTokenValidateRequestJson));

                        }).flatMap(responseArray -> {
                            log.info("\n let's  check  request type [{} {}]",hardTokenValidateRequest.getRequestType(),responseArray.getStatusCode());
                            //let's check request type and response from esb
                            if(Objects.equals(hardTokenValidateRequest.getRequestType(), Constants.EXISTING_NOT_AUTHENTICATED_REQUEST_TYPE) && responseArray.getStatusCode() == statusCodeConfig.getGatewaySyncSuccessStatusCode())
                            {
                                log.info("let's fire to customer management to update beneficiary status");
                               return updateBeneficiaryAuthenticationStatus(hardTokenValidateRequest.getBeneficiaryID(), hardTokenValidateRequest.getTokenResponse(), msisdn).flatMap(walletResponse -> Mono.just(responseArray));

                            }
                            return Mono.just(responseArray);
                        });
                    });
        });
    }

    /**
     * Process hard token check pending request mono.
     *
     * @param headerParams             the header params
     * @param hardTokenCheckPendingRequest the hard token check pending request
     * @return the mono
     * @throws Exception the exception
     */
    public Mono<ResponseArray> processHardTokenCheckPendingRequest(Map headerParams, HardTokenCheckPendingRequest hardTokenCheckPendingRequest) throws Exception {
        if(Boolean.parseBoolean(this.configs.getHardTokenCheckPendingRequest())){
            // Formulate the token response.
            var statusHash = new HashMap<String, Object>();
            statusHash.put("hasActivatedToken", true);
            statusHash.put("hasPendingRequest", false);
            var dataList = new ArrayList<>();
            dataList.add(statusHash);
            var resultHash = new HashMap<String, Object>();
            resultHash.put("message", "Dear customer, your transaction has been successfully processed");
            resultHash.put("referenceID","1329136908");
            resultHash.put("data",dataList);


            final var responseArray = ResponseArray.builder().statusCode(5000)
                    .statusMessage("Status Fetched Successfully")
                    .result(resultHash).build();
            return Mono.just(responseArray);
        }
        //Get action from headers
        final String action = sharedUtilities.retrieveActionFromHeaders(headerParams);
        final String uuid = String.valueOf(headerParams.get(Constants.UUID));
        //Retrieve the Service Details form Database
        final Mono<ServicesEntity> servicesEntityMono = serviceImpl.getServiceDetails((String.valueOf(headerParams.get(Constants.SERVICE))));

        return servicesEntityMono.flatMap(servicesEntity -> {
            // Get msisdn
            final Mono<String> msisdnMono = sharedUtilities.getMsisdn(headerParams, action, servicesEntity)
                    .doOnNext(msisdn -> MDC.put(Constants.MSISDN, String.valueOf(msisdn)));

            // Retrieve Wallet URL from Database using provided action
            final Mono<String> walletUrlMono = sharedUtilities.getWalletUrl(action, servicesEntity);

            final Mono<String> hardTokenValidateRequestJsonMono = Mono.just(hardTokenCheckPendingRequest)
                    .flatMap(hardTokenValidateRequest1 -> Mono.fromCallable(() -> {
                        // any encryption here

                        // Formulate Wallet Payload
                        ObjectMapper objectMapper = new ObjectMapper();
                        objectMapper.disable(MapperFeature.USE_ANNOTATIONS);
                        return objectMapper.writeValueAsString(hardTokenValidateRequest1);
                    }));


            return Mono.zip(msisdnMono, walletUrlMono, hardTokenValidateRequestJsonMono)
                    .flatMap(data -> {
                        final String msisdn = data.getT1();
                        final String walletUrl = data.getT2();
                        final String hardTokenValidateRequestJson = data.getT3();

                        // update headerParams with updated msisdn
                        final Mono<Map> headerParamsMono = Mono.just(headerParams).map(map -> {
                            map.replace(Constants.MSISDN, msisdn);
                            return map;
                        });

                        return headerParamsMono.flatMap(headers -> {
                            final Mono<String> requestIDMono = sharedUtilities.getRequestID(hardTokenCheckPendingRequest, msisdn, headers)
                                    .doOnNext(requestID -> MDC.put(Constants.CLOUD_REQUEST_ID, String.valueOf(requestID)));

                            //Validate Uuid
                            final Mono<Void> validateUUID = sharedUtilities.validateUUID(servicesEntity, msisdn, action, uuid);

                            return validateUUID.then(requestIDMono)
                                    .flatMap(requestID -> sharedUtilities
                                            .processIntegrationResponse(headers, msisdn, requestID, servicesEntity, walletUrl, hardTokenValidateRequestJson));

                        });
                    });
        });
    }

    public Mono<WalletResponse> updateBeneficiaryAuthenticationStatus(String nominationID, String token, String msisdn)
    {
       var updateBeneficiaryStatus = UpdateBeneficiaryAuthenticationStatus.builder()
               .authenticationStatus(true)
               .nominationID(nominationID)
               .otp(sharedUtilities.encrypt(token))
               .otpType("hard-token")
               .build();

        log.info("lets update beneficiary authenticated status {}",updateBeneficiaryStatus);

        return this.sharedUtilities.getIntegrationLayerPutResponse(updateBeneficiaryStatus, msisdn, configs.getUpdateBeneficiaryUrl())
                .doOnSuccess(walletResponse -> log.info("customer management response {}",walletResponse))
                .doOnError(throwable -> log.error(throwable.getMessage()));

    }
}
