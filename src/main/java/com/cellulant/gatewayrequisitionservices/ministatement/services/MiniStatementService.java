package com.cellulant.gatewayrequisitionservices.ministatement.services;


import com.cellulant.gatewayrequisitionservices.configurations.Constants;
import com.cellulant.gatewayrequisitionservices.dtos.responses.ResponseArray;
import com.cellulant.gatewayrequisitionservices.ministatement.requests.MiniStatementRequest;
import com.cellulant.gatewayrequisitionservices.models.ServicesEntity;
import com.cellulant.gatewayrequisitionservices.services.ServiceImpl;
import com.cellulant.gatewayrequisitionservices.utils.SharedUtilities;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.slf4j.MDC;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.Map;

/**
 * The type Mini statement service.
 */
@SuppressWarnings("Duplicates")
@Service
@RequiredArgsConstructor
public class MiniStatementService {


    // This means to get the bean called SharedUtilities
    // Which is auto-generated by Spring, we will use it to load SharedUtilities
    @NonNull
    private final SharedUtilities sharedUtilities;

    // This means to get the bean called ServiceImpl
    // Which is auto-generated by Spring, we will use it to load ServiceImpl
    @NonNull
    private final ServiceImpl serviceImpl;

    /**
     * Fetch mini statement response array.
     *
     * @param headerParams         the header params
     * @param miniStatementRequest the mini statement request
     * @return the response array
     * @throws Exception the exception
     */
    public Mono<ResponseArray> fetchMiniStatement(Map headerParams, MiniStatementRequest miniStatementRequest) throws Exception {

        //Get action from headers
        final String action = sharedUtilities.retrieveActionFromHeaders(headerParams);
        final String uuid = String.valueOf(headerParams.get(Constants.UUID));
        //Retrieve the Service Details form Database
        final Mono<ServicesEntity> servicesEntityMono = serviceImpl.getServiceDetails((String.valueOf(headerParams.get(Constants.SERVICE))));

        return servicesEntityMono.flatMap(servicesEntity -> {
            // Get msisdn
            final Mono<String> msisdnMono = sharedUtilities.getMsisdn(headerParams, action, servicesEntity)
                    .doOnNext(msisdn -> MDC.put(Constants.MSISDN, String.valueOf(msisdn)));

            // Retrieve Wallet URL from Database using provided action
            final Mono<String> walletUrlMono = sharedUtilities.getWalletUrl(action, servicesEntity);

            final Mono<String> miniStatementRequestJsonMono = Mono.just(miniStatementRequest)
                    .flatMap(miniStatement -> Mono.fromCallable(() -> {
                        // Encrypt the Pin
                        miniStatement.setPin(sharedUtilities.encrypt(miniStatement.getPin()));

                        // Formulate Wallet Payload
                        ObjectMapper objectMapper = new ObjectMapper();
                        objectMapper.disable(MapperFeature.USE_ANNOTATIONS);
                        return objectMapper.writeValueAsString(miniStatement);
                    }));


            return Mono.zip(msisdnMono, walletUrlMono, miniStatementRequestJsonMono)
                    .flatMap(data -> {
                        final String msisdn = data.getT1();
                        final String walletUrl = data.getT2();
                        final String miniStatementRequestJson = data.getT3();

                        // update headerParams with updated msisdn
                        final Mono<Map> headerParamsMono = Mono.just(headerParams).map(map -> {
                            map.replace(Constants.MSISDN, msisdn);
                            return map;
                        });

                        return headerParamsMono.flatMap(headers -> {
                            final Mono<String> requestIDMono = sharedUtilities.getRequestID(miniStatementRequest, msisdn, headers)
                                    .doOnNext(requestID -> MDC.put(Constants.CLOUD_REQUEST_ID, String.valueOf(requestID)));

                            //Validate Uuid
                            final Mono<Void> validateUUID = sharedUtilities.validateUUID(servicesEntity, msisdn, action, uuid);

                            return validateUUID.then(requestIDMono)
                                    .flatMap(requestID -> sharedUtilities
                                            .processIntegrationResponse(headerParams, msisdn, requestID, servicesEntity, walletUrl, miniStatementRequestJson));
                        });
                    });
        });

    }


}
