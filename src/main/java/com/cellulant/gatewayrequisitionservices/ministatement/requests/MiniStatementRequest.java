package com.cellulant.gatewayrequisitionservices.ministatement.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

/**
 * The type Mini statement request.
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MiniStatementRequest implements java.io.Serializable {

    @NotNull(message = "{accountAlias.required}")
    private String accountAlias;
    @NotBlank(message = "{pin.required}")
   // @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String pin;

    private String fromDate;
    private String toDate;
}