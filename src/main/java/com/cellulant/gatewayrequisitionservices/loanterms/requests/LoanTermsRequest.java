package com.cellulant.gatewayrequisitionservices.loanterms.requests;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;

/**
 * Created by  IntelliJ IDEA.
 * Project enquiries-gateway-api
 * User: Sylvester Musyoki
 * Date: 5/11/19
 * Time: 12:48 PM
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoanTermsRequest implements Serializable {
    @NotBlank(message = "{pin.required}")
    private String pin;
    @NotBlank(message = "{accountAlias.required}")
    private String accountAlias;
    @NotBlank(message = "{customerId.required}")
    private String customerId;
}
