package com.cellulant.gatewayrequisitionservices.services;


import com.cellulant.gatewayrequisitionservices.configurations.StatusCodeConfig;
import com.cellulant.gatewayrequisitionservices.configurations.StatusMessageServiceConfig;
import com.cellulant.gatewayrequisitionservices.exceptions.InvalidUUIDException;
import com.cellulant.gatewayrequisitionservices.models.ProfilesEntity;
import com.cellulant.gatewayrequisitionservices.repositories.ProfilesRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

/**
 * Created by kamauwamatu
 * Project paymentsgatewayapi
 * User: kamauwamatu
 * Date: 2019-05-14
 * Time: 12:14
 */
@Service
@AllArgsConstructor
public class ProfilesImpl {

    /**
     * The Services repository.
     */
    private final ProfilesRepository profilesRepository;
    private final StatusCodeConfig statusCodeConfig;
    private final StatusMessageServiceConfig statusMessageService;

    /**
     * Validate uuid.
     *
     * @param msisdn the msisdn
     * @param uuid   the uuid
     */
    public Mono<ProfilesEntity> validateUuid(String msisdn, String uuid) {
        return Mono.fromCallable(() -> profilesRepository.findByMsisdnAndUuid(msisdn, uuid))
                .subscribeOn(Schedulers.boundedElastic())
                .flatMap(profilesEntity -> {
                    if (null == profilesEntity || profilesEntity.getProfileID() == 0) {
                        return Mono.error(new InvalidUUIDException(statusCodeConfig, statusMessageService));
                    }
                    return Mono.just(profilesEntity);
                });
    }
}
