package com.cellulant.gatewayrequisitionservices.services;


import com.cellulant.gatewayrequisitionservices.configurations.Configs;
import com.cellulant.gatewayrequisitionservices.dtos.core.CoreRequest;
import com.cellulant.gatewayrequisitionservices.dtos.responses.RabbitMQResponse;
import com.cellulant.gatewayrequisitionservices.dtos.walletdtos.WalletPayload;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * The type Rabbit mq service.
 *
 * @author kobe
 */
@AllArgsConstructor
@Service
public class RabbitMQService {

    private final Logger logger;
    private final RabbitTemplate rabbitTemplate;
    private final Configs configs;

    /**
     * Generates Correlation data for confirmations
     *
     * @return CorrelationDate Object
     */
    public CorrelationData getCorrelationData() {
        return new CorrelationData(UUID.randomUUID().toString());
    }

    /**
     * Dispatches Objects to RabbitMQ as a JSON string Handles Asynchronous
     * Processing Retries
     *
     * @param walletPayload the wallet payload
     * @return completable future
     * @throws InterruptedException the interrupted exception
     * @throws TimeoutException     the timeout exception
     * @throws ExecutionException   the execution exception
     */
    @Async("concurrentThreadPoolTaskExecutor")
    public CompletableFuture<RabbitMQResponse> dispatchToLoggerQueue(WalletPayload walletPayload) throws InterruptedException, TimeoutException, ExecutionException {
        rabbitTemplate.setMessageConverter(new Jackson2JsonMessageConverter());

        CorrelationData correlationData = getCorrelationData();
        rabbitTemplate.convertAndSend(
                configs.getExchangeName() + '.' + configs.getQueueNamePrefix()
                        + '.' + configs.getRequestLoggerQueue(),
                configs.getQueueNamePrefix() + "." + configs.getRequestLoggerQueue(),
                walletPayload,
                correlationData
        );

        // Listen For RabbitMQ Confirmations
//        boolean ack = correlationData.getFuture().get(configs.getConfirmTimeout(), TimeUnit.SECONDS).isAck();
//
//        if (!ack) {
//            logger.error("RabbitMQ Has Returned a negative Confirmation...");
//
//            return CompletableFuture.completedFuture(new RabbitMQResponse(configs.getFailedStatusType(),
//                    configs.getFailedStatusCode(), "", null));
//        }

        logger.info("Received confirm from RabbitMQ with result: true. Correlation Data is: {}", correlationData.getId());
        this.rabbitTemplate.setMandatory(true);
        return CompletableFuture.completedFuture(new RabbitMQResponse(configs.getSuccessStatusType(),
                configs.getSuccessStatusCode(), "", null));
    }

    /**
     * Dispatch to sync core queue completable future.
     *
     * @param coreRequest the core payload
     * @return the completable future
     * @throws InterruptedException the interrupted exception
     * @throws TimeoutException     the timeout exception
     * @throws ExecutionException   the execution exception
     */
    @Async("concurrentThreadPoolTaskExecutor")
    public CompletableFuture<RabbitMQResponse> dispatchToAsyncCoreQueue(CoreRequest coreRequest) throws InterruptedException, TimeoutException, ExecutionException {
        rabbitTemplate.setMessageConverter(new Jackson2JsonMessageConverter());
        CorrelationData correlationData = getCorrelationData();

        rabbitTemplate.convertAndSend(
                configs.getExchangeName() + '.' + configs.getQueueNamePrefix()
                        + '.' + configs.getSyncCoreQueue(),
                configs.getQueueNamePrefix() + "." + configs.getSyncCoreQueue(),
                coreRequest,
                correlationData
        );

//        // Listen For RabbitMQ Confirmations
//        boolean ack = correlationData.getFuture().get(configs.getConfirmTimeout(), TimeUnit.SECONDS).isAck();
//
//        if (!ack) {
//            logger.error("RabbitMQ Has Returned a negative Confirmation...");
//
//            return CompletableFuture.completedFuture(new RabbitMQResponse(configs.getFailedStatusType(),
//                    configs.getFailedStatusCode(), "", null));
//        }
        logger.info("Received confirm from RabbitMQ with result: true. Correlation Data is: {}", correlationData.getId());
        this.rabbitTemplate.setMandatory(true);
        return CompletableFuture.completedFuture(new RabbitMQResponse(configs.getSuccessStatusType(),
                configs.getSuccessStatusCode(), "", null));
    }

    /**
     * Checks if we got a confirmation from RabbitMQ
     *
     * @param correlationData Object
     * @return boolean
     * @throws InterruptedException Exception
     * @throws ExecutionException   Exception
     * @throws TimeoutException     Exception
     */
    private boolean rabbitHasNotConfirmed(CorrelationData correlationData) throws InterruptedException, ExecutionException, TimeoutException {
        return !correlationData.getFuture().get(configs.getConfirmTimeout(), TimeUnit.SECONDS).isAck();
    }

}
