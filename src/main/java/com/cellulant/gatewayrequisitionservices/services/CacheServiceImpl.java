package com.cellulant.gatewayrequisitionservices.services;


import com.cellulant.gatewayrequisitionservices.models.StatusCodesEntity;
import com.cellulant.gatewayrequisitionservices.repositories.StatusCodeRepository;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

/**
 * Only external method calls coming in through the proxy are intercepted.
 * This means that self-invocation, in effect, a method within the target object calling another method of the target object,
 * will not lead to an actual cache interception at runtime even if the invoked method is marked with @Cacheable.
 * <p>
 * Solution
 * you also can write a Service e.g. CacheService and put all your to cache methods into the service.
 * Autowire the Service where you need and call the methods.
 */
@Service
public class CacheServiceImpl {

    private StatusCodeRepository statusCodeRepository;

    /**
     * Instantiates a new Cache service.
     *
     * @param statusCodeRepository the status code repository
     */
    public CacheServiceImpl(StatusCodeRepository statusCodeRepository) {
        this.statusCodeRepository = statusCodeRepository;
    }

    /**
     * Fetch response template string.
     *
     * @param statusCode  the status code
     * @param finalLangId the final lang id
     * @return the string
     */
    @Cacheable(value = "responseMessage", keyGenerator = "responseMessageCustomKeyGenerator", unless = "#result==null")
    public String fetchResponseTemplate(int statusCode, int finalLangId) {

        StatusCodesEntity statusCodesEntity = statusCodeRepository.findByStatusCode(statusCode).orElse(null);
        // Handle if responseTemplate list in StatusCode Entity is empty
        if (null == statusCodesEntity || statusCodesEntity.getResponseTemplateList().isEmpty()) {
            // Return a null to prevent cache from saving default message.
            return null;
        }

        // Return the responseMessage from the responseTemplateList
        return String.valueOf(statusCodesEntity.getResponseTemplateList()
                .stream()
                .filter(
                        responseTemplate -> responseTemplate.getLanguageID() == finalLangId
                ).collect(Collectors.toList()).get(0).getResponseMessage()
        );
    }
}
