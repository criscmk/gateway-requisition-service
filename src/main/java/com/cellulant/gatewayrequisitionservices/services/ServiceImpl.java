package com.cellulant.gatewayrequisitionservices.services;

import com.cellulant.gatewayrequisitionservices.models.ServicesEntity;
import com.cellulant.gatewayrequisitionservices.repositories.ServicesRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

/**
 * Created by kamauwamatu
 * Project paymentsgatewayapi
 * User: kamauwamatu
 * Date: 2019-05-14
 * Time: 12:14
 */
@Service
public class ServiceImpl {

    /**
     * The Services repository.
     */
    private final ServicesRepository servicesRepository;


    /**
     * Instantiates a new Service.
     *
     * @param servicesRepository the services repository
     */
    public ServiceImpl(ServicesRepository servicesRepository) {

        this.servicesRepository = servicesRepository;
    }

    /**
     * Get service details services entity.
     *
     * @param serviceCode the service code
     * @return the services entity
     */
    public Mono<ServicesEntity> getServiceDetails(String serviceCode) {

        return Mono.defer(() -> Mono.fromCallable(() -> servicesRepository.findByServiceCode(serviceCode))
                .flatMap(Mono::justOrEmpty)
                .subscribeOn(Schedulers.boundedElastic())).cache();
    }
}
