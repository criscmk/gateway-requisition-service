package com.cellulant.gatewayrequisitionservices.cardfail.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by eyo
 * Project gateway-lookup-services
 * User: eyo
 * Date: 25/09/2019
 * Time: 4:23 PM
 */
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FailedRequest {
     @NotBlank(message = "{service.required}")
     private String service;
     @NotBlank(message = "{externalApi.required}")
     private String externalApi;
     @NotBlank(message = "{responseCode.required}")
     private String responseCode;
     @NotBlank(message = "{responseMessage.required}")
     private String responseMessage;

     private Object extraData;
}
