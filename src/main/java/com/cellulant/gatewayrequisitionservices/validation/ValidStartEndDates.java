package com.cellulant.gatewayrequisitionservices.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * The interface Valid start end dates.
 */
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {ValidStartEndDatesValidator.class})
public @interface ValidStartEndDates {
    /**
     * Message string.
     *
     * @return the string
     */
    String message() default "Start date cannot be after end date ";

    /**
     * Groups class [ ].
     *
     * @return the class [ ]
     */
    Class<?>[] groups() default {};

    /**
     * Payload class [ ].
     *
     * @return the class [ ]
     */
    Class<? extends Payload>[] payload() default {};

    /**
     * Start date string.
     *
     * @return the string
     */
    String startDate();

    /**
     * End date string.
     *
     * @return the string
     */
    String endDate();
}
