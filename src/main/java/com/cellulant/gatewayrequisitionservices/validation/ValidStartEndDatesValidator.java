package com.cellulant.gatewayrequisitionservices.validation;

import org.springframework.beans.BeanWrapperImpl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * The type Valid start end dates validator.
 */
public class ValidStartEndDatesValidator implements ConstraintValidator<ValidStartEndDates, Object> {
    private String startDate;
    private String endDate;

    @Override
    public void initialize(ValidStartEndDates constraintAnnotation) {
        this.startDate = constraintAnnotation.startDate();
        this.endDate = constraintAnnotation.endDate();
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {

        Object startDateValue = new BeanWrapperImpl(value).getPropertyValue(startDate);
        Object endDateValue = new BeanWrapperImpl(value).getPropertyValue(endDate);

        if (startDateValue != null && endDateValue != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date start = sdf.parse(startDateValue.toString());
                Date end = sdf.parse(endDateValue.toString());
                Date now = new Date();
                if (now.compareTo(end) < 0) {
                    return false;
                } else if (start.compareTo(end) > 0) {
                    return false;
                } else return start.compareTo(end) != 0;
            } catch (ParseException e) {
                return false;
            }

        }
        return false;
    }
}
