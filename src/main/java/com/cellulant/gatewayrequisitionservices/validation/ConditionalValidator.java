package com.cellulant.gatewayrequisitionservices.validation;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static org.springframework.util.ObjectUtils.isEmpty;

public class ConditionalValidator implements ConstraintValidator<Conditional, Object> {

    @Autowired
    private Validator validator;
    private String selected;
    private String[] required;
    private String[] onlyOneRequired;
    private String onlyOneRequiredMessage;
    private String message;
    private String[] values;

    @Override
    public void initialize(Conditional requiredIfChecked) {
        selected = requiredIfChecked.selected();
        required = requiredIfChecked.required();
        onlyOneRequired = requiredIfChecked.onlyOneRequired();
        onlyOneRequiredMessage = requiredIfChecked.onlyOneRequiredMessage();
        message = requiredIfChecked.message();
        values = requiredIfChecked.values();
    }

    @Override
    public boolean isValid(Object object, ConstraintValidatorContext context) {


        boolean valid = true;

        Object checkedValue = new BeanWrapperImpl(object).getPropertyValue(selected);
        if (Arrays.asList(values).contains(checkedValue)) {
            // implementation for only one required
            if (onlyOneRequired != null && onlyOneRequired.length >= 1) {
                final Stream<Pair<String, Object>> requiredValueStream = Stream.of(onlyOneRequired)
                        .map(propName -> Pair.of(propName, new BeanWrapperImpl(object).getPropertyValue(propName)))
                        .filter(pair -> Objects.nonNull(pair.getSecond()));

                final List<Pair<String, Object>> requiredValues = requiredValueStream.collect(Collectors.toList());
                if (requiredValues.size() != 1) {
                    context.disableDefaultConstraintViolation();
                    requiredValues.stream()
                            .map(Pair::getFirst)
                            .forEach(propName -> context.buildConstraintViolationWithTemplate(propName + onlyOneRequiredMessage)
                                    .addPropertyNode(propName)
                                    .addConstraintViolation());
                    valid = false;
                } else {
                    final Optional<Pair<String, Object>> requiredValue = requiredValues.stream()
                            .findFirst();
                    String propName = requiredValue
                            .map(Pair::getFirst)
                            .orElse(null);
                    Object requiredObject = requiredValue
                            .map(Pair::getSecond)
                            .orElse(null);
                    valid = evaluate(propName, requiredObject, context);
                }
            }
            // implementation for all required
            else {
                for (String propName : required) {
                    Object requiredValue = new BeanWrapperImpl(object).getPropertyValue(propName);
                    valid = evaluate(propName, requiredValue, context);
                }
            }
        }
        return valid;
    }


    private boolean evaluate(String propName,
                             Object object,
                             ConstraintValidatorContext context) {
        if (object != null) {
            final Set<ConstraintViolation<Object>> errors = validator.validate(object);
            if (!isEmpty(errors)) {
                for (ConstraintViolation<Object> violation : errors) {
                    context.disableDefaultConstraintViolation();
                    context.buildConstraintViolationWithTemplate(propName + violation.getMessageTemplate()).addConstraintViolation();
                }
                return false;
            }
        } else {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(propName + message).addPropertyNode(propName).addConstraintViolation();
            return false;
        }

        return true;
    }
}
