package com.cellulant.gatewayrequisitionservices.chequeservices.dtos;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by sylvester
 * Project requisition-gateway-api
 * User: sylvester
 * Date: 4/4/19
 * Time: 3:49 PM
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RequestChequeBookRequest implements Serializable {

    @NotBlank(message = "{pin.required}")
    //@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String pin;
    @NotNull(message = "{accountAlias.required}")
    private String accountAlias;
    @NotBlank(message = "{leaves.required}")
    private String leaves;
    @NotNull(message = "{reason.required}")
    private String reason;
    @NotNull(message = "{branchName.required}")
    private String branchName;
    @NotNull(message = "{branchCode.required}")
    private String branchCode;

}
