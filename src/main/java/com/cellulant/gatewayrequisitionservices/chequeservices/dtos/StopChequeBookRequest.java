package com.cellulant.gatewayrequisitionservices.chequeservices.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;

/**
 * Created by sylvester
 * Project requisition-gateway-api
 * User: sylvester
 * Date: 4/3/19
 * Time: 2:59 PM
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class StopChequeBookRequest implements Serializable {
    @NotBlank(message = "{pin.required}")
    //@JsonProperty(access = JsonProperty.Access.READ_WRITE)
    private String pin;
    @NotBlank(message = "{accountAlias.required}")
    private String accountAlias;
    private String chequeNumbers;
    private String startLeaveNo;
    private String endLeaveNo;
    @NotBlank(message = "{reason.required}")
    private String reason;
}
