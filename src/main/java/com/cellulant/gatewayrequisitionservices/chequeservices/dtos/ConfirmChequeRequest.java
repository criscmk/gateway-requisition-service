package com.cellulant.gatewayrequisitionservices.chequeservices.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by sylvester
 * Project requisition-gateway-api
 * User: sylvester
 * Date: 4/4/19
 * Time: 11:21 AM
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ConfirmChequeRequest implements Serializable {

    @NotBlank(message = "{pin.required}")
//    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String pin;
    @NotBlank(message = "{accountAlias.required}")
    private String accountAlias;
    @NotBlank(message = "{chequeNumber.required}")
    private String chequeNumber;
    @NotBlank(message = "{beneficiaryName.required}")
    private String beneficiaryName;
    @NotNull(message = "{amount.required}")
    private float amount;
    @NotBlank(message = "{reason.required}")
    private String reason;
    @NotBlank(message = "{chequeDate.required}")
    private String chequeDate;
}
