package com.cellulant.gatewayrequisitionservices.chequeservices.services;

import com.cellulant.gatewayrequisitionservices.chequeservices.dtos.ConfirmChequeRequest;
import com.cellulant.gatewayrequisitionservices.chequeservices.dtos.RequestChequeBookRequest;
import com.cellulant.gatewayrequisitionservices.chequeservices.dtos.StopChequeBookRequest;
import com.cellulant.gatewayrequisitionservices.configurations.Constants;
import com.cellulant.gatewayrequisitionservices.dtos.responses.ResponseArray;
import com.cellulant.gatewayrequisitionservices.models.ServicesEntity;
import com.cellulant.gatewayrequisitionservices.services.ServiceImpl;
import com.cellulant.gatewayrequisitionservices.utils.SharedUtilities;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.slf4j.MDC;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.Map;
import java.util.function.Supplier;

/**
 * Created by eyo
 * Project requisition-gateway-api
 * User: eyo
 * Date: 2019-06-15
 * Time: 12:57
 */
@SuppressWarnings("Duplicates")
@Service
@RequiredArgsConstructor
public class ChequeServicesImpl {

    // This means to get the bean called SharedUtilities
    // Which is auto-generated by Spring, we will use it to load SharedUtilities
    @NonNull
    private final SharedUtilities sharedUtilities;
    @NonNull
    private final ServiceImpl serviceImpl;


    /**
     * Process request cheque book request response array.
     *
     * @param headerParams             the header params
     * @param requestChequeBookRequest the request cheque book request
     * @return the response array
     * @throws Exception the exception
     */
    public Mono<ResponseArray> processRequestChequeBookRequest(Map headerParams, RequestChequeBookRequest requestChequeBookRequest) throws Exception {

        //Get action from headers
        final String action = sharedUtilities.retrieveActionFromHeaders(headerParams);
        final String uuid = String.valueOf(headerParams.get(Constants.UUID));
        //Retrieve the Service Details form Database
        final Mono<ServicesEntity> servicesEntityMono = serviceImpl.getServiceDetails((String.valueOf(headerParams.get(Constants.SERVICE))));

        return servicesEntityMono.flatMap(servicesEntity -> {
            // Get msisdn
            final Mono<String> msisdnMono = sharedUtilities.getMsisdn(headerParams, action, servicesEntity)
                    .doOnNext(msisdn -> MDC.put(Constants.MSISDN, String.valueOf(msisdn)));

            // Retrieve Wallet URL from Database using provided action
            final Mono<String> walletUrlMono = sharedUtilities.getWalletUrl(action, servicesEntity);

            final Mono<String> requestChequeBookRequestJsonMono = Mono.just(requestChequeBookRequest)
                    .flatMap(requestChequeBook -> Mono.fromCallable(() -> {

                        requestChequeBook.setPin(sharedUtilities.encrypt(requestChequeBook.getPin()));

                        // Formulate Wallet Payload
                        ObjectMapper objectMapper = new ObjectMapper();
                        objectMapper.disable(MapperFeature.USE_ANNOTATIONS);
                        return objectMapper.writeValueAsString(requestChequeBook);
                    }));


            return Mono.zip(msisdnMono, walletUrlMono, requestChequeBookRequestJsonMono)
                    .flatMap(data -> {
                        final String msisdn = data.getT1();
                        final String walletUrl = data.getT2();
                        final String requestChequeBookRequestJson = data.getT3();

                        // update headerParams with updated msisdn
                        final Mono<Map> headerParamsMono = Mono.just(headerParams).map(map -> {
                            map.replace(Constants.MSISDN, msisdn);
                            return map;
                        });

                        return headerParamsMono.flatMap(headers -> {
                            final Mono<String> requestIDMono = sharedUtilities.getRequestID(requestChequeBookRequest, msisdn, headers)
                                    .doOnNext(requestID -> MDC.put(Constants.CLOUD_REQUEST_ID, String.valueOf(requestID)));

                            //Validate Uuid
                            final Mono<Void> validateUUID = sharedUtilities.validateUUID(servicesEntity, msisdn, action, uuid);

                            return validateUUID
                                    .then(requestIDMono)
                                    .flatMap(requestID -> sharedUtilities
                                            .processIntegrationResponse(headers, msisdn, requestID, servicesEntity, walletUrl, requestChequeBookRequestJson));

                        });
                    });
        });

    }

    /**
     * Process stop cheque book request response array.
     *
     * @param headerParams          the header params
     * @param stopChequeBookRequest the stop cheque book request
     * @return the response array
     * @throws Exception the exception
     */
    public Mono<ResponseArray> processStopChequeBookRequest(Map headerParams, StopChequeBookRequest stopChequeBookRequest) throws Exception {
        //Get action from headers
        final String action = sharedUtilities.retrieveActionFromHeaders(headerParams);
        final String uuid = String.valueOf(headerParams.get(Constants.UUID));
        //Retrieve the Service Details form Database
        final Mono<ServicesEntity> servicesEntityMono = serviceImpl.getServiceDetails((String.valueOf(headerParams.get(Constants.SERVICE))));

        return servicesEntityMono.flatMap(servicesEntity -> {
            // Get msisdn
            final Mono<String> msisdnMono = sharedUtilities.getMsisdn(headerParams, action, servicesEntity)
                    .doOnNext(msisdn -> MDC.put(Constants.MSISDN, String.valueOf(msisdn)));

            // Retrieve Wallet URL from Database using provided action
            final Mono<String> walletUrlMono = sharedUtilities.getWalletUrl(action, servicesEntity);

            final Mono<String> stopChequeBookJsonMono = Mono.just(stopChequeBookRequest)
                    .flatMap(stopCheque -> Mono.fromCallable(() -> {

                        stopCheque.setPin(sharedUtilities.encrypt(stopCheque.getPin()));

                        // Formulate Wallet Payload
                        ObjectMapper objectMapper = new ObjectMapper();
                        objectMapper.disable(MapperFeature.USE_ANNOTATIONS);
                        return objectMapper.writeValueAsString(stopCheque);
                    }));


            return Mono.zip(msisdnMono, walletUrlMono, stopChequeBookJsonMono)
                    .flatMap(data -> {
                        final String msisdn = data.getT1();
                        final String walletUrl = data.getT2();
                        final String stopChequeBookJson = data.getT3();

                        // update headerParams with updated msisdn
                        final Mono<Map> headerParamsMono = Mono.just(headerParams).map(map -> {
                            map.replace(Constants.MSISDN, msisdn);
                            return map;
                        });

                        return headerParamsMono.flatMap(headers -> {
                            final Mono<String> requestIDMono = sharedUtilities.getRequestID(stopChequeBookRequest, msisdn, headers)
                                    .doOnNext(requestID -> MDC.put(Constants.CLOUD_REQUEST_ID, String.valueOf(requestID)));

                            //Validate Uuid
                            final Mono<Void> validateUUID = sharedUtilities.validateUUID(servicesEntity, msisdn, action, uuid);

                            return validateUUID.then(requestIDMono)
                                    .flatMap(requestID -> sharedUtilities
                                            .processIntegrationResponse(headers, msisdn, requestID, servicesEntity, walletUrl, stopChequeBookJson)
                                    );
                        });
                    });
        });
    }

    /**
     * Process confirm cheque request response array.
     *
     * @param headerParams         the header params
     * @param confirmChequeRequest the confirm cheque request
     * @return the response array
     * @throws Exception the exception
     */
    public Mono<ResponseArray> processConfirmChequeRequest(Map headerParams, ConfirmChequeRequest confirmChequeRequest) throws Exception {

        //Get action from headers
        final String action = sharedUtilities.retrieveActionFromHeaders(headerParams);
        final String uuid = String.valueOf(headerParams.get(Constants.UUID));
        //Retrieve the Service Details form Database
        final Mono<ServicesEntity> servicesEntityMono = serviceImpl.getServiceDetails((String.valueOf(headerParams.get(Constants.SERVICE))));

        return servicesEntityMono.flatMap(servicesEntity -> {
            // Get msisdn
            final Mono<String> msisdnMono = sharedUtilities.getMsisdn(headerParams, action, servicesEntity)
                    .doOnNext(msisdn -> MDC.put(Constants.MSISDN, String.valueOf(msisdn)));

            // Retrieve Wallet URL from Database using provided action
            final Mono<String> walletUrlMono = sharedUtilities.getWalletUrl(action, servicesEntity);

            final Mono<String> confirmChequeRequestJsonMono = Mono.just(confirmChequeRequest)
                    .flatMap(confirmCheque -> Mono.fromCallable(() -> {

                        confirmCheque.setPin(sharedUtilities.encrypt(confirmCheque.getPin()));

                        // Formulate Wallet Payload
                        ObjectMapper objectMapper = new ObjectMapper();
                        objectMapper.disable(MapperFeature.USE_ANNOTATIONS);
                        return objectMapper.writeValueAsString(confirmCheque);
                    }));


            return Mono.zip(msisdnMono, walletUrlMono, confirmChequeRequestJsonMono)
                    .flatMap(data -> {
                        final String msisdn = data.getT1();
                        final String walletUrl = data.getT2();
                        final String confirmChequeRequestJson = data.getT3();

                        // update headerParams with updated msisdn
                        final Mono<Map> headerParamsMono = Mono.just(headerParams).map(map -> {
                            map.replace(Constants.MSISDN, msisdn);
                            return map;
                        });

                        return headerParamsMono.flatMap(headers -> {
                            final Mono<String> requestIDMono = sharedUtilities.getRequestID(confirmChequeRequest, msisdn, headers)
                                    .doOnNext(requestID -> MDC.put(Constants.CLOUD_REQUEST_ID, String.valueOf(requestID)));

                            //Validate Uuid
                            final Mono<Void> validateUUID = sharedUtilities.validateUUID(servicesEntity, msisdn, action, uuid);

                            return validateUUID.then(requestIDMono)
                                    .flatMap(requestID -> sharedUtilities
                                            .processIntegrationResponse(headers, msisdn, requestID, servicesEntity, walletUrl, confirmChequeRequestJson)
                                    );
                        });
                    });
        });
    }

    /**
     * Process cheque book leaves request response array.
     *
     * @param headerParams the header params
     * @return the response array
     * @throws Exception the exception
     */
    public Mono<ResponseArray> processChequeBookLeavesRequest(Map headerParams) throws Exception {

        //Get action from headers
        final String action = sharedUtilities.retrieveActionFromHeaders(headerParams);
        final String uuid = String.valueOf(headerParams.get(Constants.UUID));
        //Retrieve the Service Details form Database
        final Mono<ServicesEntity> servicesEntityMono = serviceImpl.getServiceDetails((String.valueOf(headerParams.get(Constants.SERVICE))));

        return servicesEntityMono.flatMap(servicesEntity -> {
            // Get msisdn
            final Mono<String> msisdnMono = sharedUtilities.getMsisdn(headerParams, action, servicesEntity)
                    .doOnNext(msisdn -> MDC.put(Constants.MSISDN, String.valueOf(msisdn)));

            // Retrieve Wallet URL from Database using provided action
            final Mono<String> walletUrlMono = sharedUtilities.getWalletUrl(action, servicesEntity);

            return Mono.zip(msisdnMono, walletUrlMono)
                    .flatMap(data -> {
                        final String msisdn = data.getT1();
                        final String walletUrl = data.getT2();

                        // update headerParams with updated msisdn
                        final Mono<Map> headerParamsMono = Mono.just(headerParams).map(map -> {
                            map.replace(Constants.MSISDN, msisdn);
                            return map;
                        });

                        final Supplier<Mono<ResponseArray>> responseArraySupplier = () -> {
                            //Validate Uuid
                            final Mono<Void> validateUUID = sharedUtilities.validateUUID(servicesEntity, msisdn, action, uuid);

                            return validateUUID
                                    .then(sharedUtilities.getIntegrationLayerResponse(walletUrl))
                                    .flatMap(sharedUtilities::handleResponse);
                        };
                        return headerParamsMono.then(responseArraySupplier.get());
                    });
        });
    }

    /**
     * Process fetch check book branches response array.
     *
     * @param headerParams the header params
     * @return the response array
     * @throws Exception the exception
     */
    public Mono<ResponseArray> processFetchCheckBookBranches(Map headerParams) throws Exception {
        //Get action from headers
        final String action = sharedUtilities.retrieveActionFromHeaders(headerParams);
        final String uuid = String.valueOf(headerParams.get(Constants.UUID));
        //Retrieve the Service Details form Database
        final Mono<ServicesEntity> servicesEntityMono = serviceImpl.getServiceDetails((String.valueOf(headerParams.get(Constants.SERVICE))));

        return servicesEntityMono.flatMap(servicesEntity -> {
            // Get msisdn
            final Mono<String> msisdnMono = sharedUtilities.getMsisdn(headerParams, action, servicesEntity)
                    .doOnNext(msisdn -> MDC.put(Constants.MSISDN, String.valueOf(msisdn)));

            // Retrieve Wallet URL from Database using provided action
            final Mono<String> walletUrlMono = sharedUtilities.getWalletUrl(action, servicesEntity);

            return Mono.zip(msisdnMono, walletUrlMono)
                    .flatMap(data -> {
                        final String msisdn = data.getT1();
                        final String walletUrl = data.getT2();

                        // update headerParams with updated msisdn
                        final Mono<Map> headerParamsMono = Mono.just(headerParams).map(map -> {
                            map.replace(Constants.MSISDN, msisdn);
                            return map;
                        });

                        final Supplier<Mono<ResponseArray>> responseArraySupplier = () -> {//Validate Uuid
                            final Mono<Void> validateUUID = sharedUtilities.validateUUID(servicesEntity, msisdn, action, uuid);

                            return validateUUID
                                    .then(sharedUtilities.getIntegrationLayerResponse(walletUrl))
                                    .flatMap(sharedUtilities::handleResponse);
                        };

                        return headerParamsMono.then(responseArraySupplier.get());
                    });
        });
    }
}
