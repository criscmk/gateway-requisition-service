package com.cellulant.gatewayrequisitionservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.reactive.config.EnableWebFlux;
import reactor.blockhound.BlockHound;

/**
 * The type Requisition gateway api application.
 */
@EnableCaching
@EnableScheduling
@EnableAspectJAutoProxy
@SpringBootApplication
public class GatewayRequisitionServicesApplication {

    /*static {
        BlockHound.install();
    }*/

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        SpringApplication.run(GatewayRequisitionServicesApplication.class, args);
    }

}