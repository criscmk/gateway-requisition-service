package com.cellulant.gatewayrequisitionservices.cards.managecards.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.List;

/**
 * Created by eyo
 * Project card-services-gateway-api
 * User: eyo
 * Date: 2019-06-16
 * Time: 15:14
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FundPrepaidCardRequest implements Serializable {

    @NotBlank(message = "{pin.required}")
    //@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String pin;
    @NotBlank(message = "{accountAlias.required}")
    private String accountAlias;
    @NotNull(message = "{amount.required}")
    private Float amount;
    @NotBlank(message = "{billerCode.required}")
    private String billerCode;
    @NotNull(message = "{formDataValue.required}")
    private List<FormDataValue> formDataValue;
    @NotBlank(message = "{productCode.required}")
    private String productCode;
    @NotBlank(message = "{narration.required}")
    private String narration;
    @NotBlank(message = "{nominate.required}")
    @Pattern(regexp = "yes|no")
    private String nominate;


}
