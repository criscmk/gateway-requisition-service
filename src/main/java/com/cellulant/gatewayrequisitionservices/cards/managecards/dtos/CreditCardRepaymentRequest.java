package com.cellulant.gatewayrequisitionservices.cards.managecards.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * Created by eyo
 * Project card-services-gateway-api
 * User: eyo
 * Date: 2019-06-16
 * Time: 15:14
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreditCardRepaymentRequest implements Serializable {
    @NotBlank(message = "{accountAlias.required}")
    private String accountAlias;
    @NotNull(message = "{amount.required}")
    private double amount;
    @NotBlank(message = "{currency.required}")
    private String currency;
    @NotBlank(message = "{cardNumber.required}")
    private String cardNumber;
    @NotBlank(message = "{narration.required}")
    private String narration;
    @NotBlank(message = "{pin.required}")
    private String pin;
    @NotBlank(message = "{nominate.required}")
    @Pattern(regexp = "yes|no")
    private String nominate;
    @NotBlank(message = "{recipientAccountName.required}")
    private String recipientAccountName;
}
