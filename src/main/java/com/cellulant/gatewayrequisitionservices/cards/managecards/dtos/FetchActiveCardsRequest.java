package com.cellulant.gatewayrequisitionservices.cards.managecards.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by kamauwamatu
 * Project card-services-gateway-api
 * User: kamauwamatu
 * Date: 2019-08-07
 * Time: 10:37
 */
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FetchActiveCardsRequest {

    @NotBlank(message = "{accountAlias.required}")
    private String accountAlias;
}
