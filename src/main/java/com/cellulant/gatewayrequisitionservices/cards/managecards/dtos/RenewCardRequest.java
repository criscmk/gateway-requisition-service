package com.cellulant.gatewayrequisitionservices.cards.managecards.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RenewCardRequest implements Serializable {

    @NotBlank(message = "{pan.required}")
    private String pan;

    @NotBlank(message = "{pin.required}")
//    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String pin;

    @NotBlank(message = "{expiryDate.required}")
    private String expiryDate;

    @NotBlank(message = "{accountAlias.required}")
    private String accountAlias;

    @NotNull(message = "{requestType.required}")
    private String requestType;

    @NotNull(message = "{deliveryMode.required}")
    private String deliveryMode;

    @NotNull(message = "{cardAccountNumber.required}")
    private String cardAccountNumber;


}
