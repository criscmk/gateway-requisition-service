package com.cellulant.gatewayrequisitionservices.cards.managecards.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;

/**
 * Created by kamauwamatu
 * Project card-services-gateway-api
 * User: kamauwamatu
 * Date: 2019-08-20
 * Time: 11:11
 */
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FormDataValue implements Serializable {
    @NotBlank(message = "{fieldName.required}")
    private String fieldName;
    @NotBlank(message = "{dataType.required}")
    private String dataType;
    @NotBlank(message = "{fieldValue.required}")
    private String fieldValue;
}
