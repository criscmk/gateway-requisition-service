package com.cellulant.gatewayrequisitionservices.cards.managecards.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

/**
 * @author Denis Gitonga
 */
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FetchDebitCardLimitRequest {

    @NotBlank(message = "{pan.required}")
    private String pan;
    @NotBlank(message = "{expiryDate.required}")
    private String expiryDate;
    private String currency;
}
