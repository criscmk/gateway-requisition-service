package com.cellulant.gatewayrequisitionservices.cards.managecards.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;

/**
 * Created by eyo
 * Project card-services-gateway-api
 * User: eyo
 * Date: 2019-06-16
 * Time: 14:59
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TravelNotificationRequest implements Serializable {

    @NotBlank(message = "{pin.required}")
    // @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String pin;
    @NotBlank(message = "{accountAlias.required}")
    private String accountAlias;
    @NotBlank(message = "{cardAccounts.required}")
    private String cardAccounts;
    @NotBlank(message = "{travelDestination.required}")
    private String travelDestination;
    @NotBlank(message = "{travelDate.required}")
    private String travelDate;
    @NotBlank(message = "{returnDate.required}")
    private String returnDate;
    @NotBlank(message = "{purposeOfTravel.required}")
    private String purposeOfTravel;
}
