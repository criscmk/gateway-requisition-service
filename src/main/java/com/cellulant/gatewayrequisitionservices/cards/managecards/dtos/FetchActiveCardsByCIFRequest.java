package com.cellulant.gatewayrequisitionservices.cards.managecards.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FetchActiveCardsByCIFRequest implements Serializable {

    @NotBlank(message = "{accountAlias.required}")
    private String accountAlias;
}
