package com.cellulant.gatewayrequisitionservices.cards.managecards.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

/**
 * @author Denis Gitonga
 */
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateDebitCardLimitRequest {

    @NotBlank(message = "{pan.required}")
    private String pan;
    @NotBlank(message = "{expiryDate.required}")
    private String expiryDate;

    @NotBlank(message = "{pin.required}")
//    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String pin;

    @NotBlank(message = "{accountAlias.required}")
    private String accountAlias;

    private DebitCardLimitInfo atmTransactions;
    private DebitCardLimitInfo posTransactions;
    private DebitCardLimitInfo onlineTransactions;
}
