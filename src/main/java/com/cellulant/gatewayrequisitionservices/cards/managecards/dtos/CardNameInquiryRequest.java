package com.cellulant.gatewayrequisitionservices.cards.managecards.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by eyo
 * Project card-services-gateway-api
 * User: eyo
 * Date: 2019-06-16
 * Time: 15:34
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CardNameInquiryRequest implements Serializable {

    @NotNull(message = "accountAlias is required")
    private String accountAlias;
    @NotNull(message = "cardNumber is required")
    private String cardNumber;
}
