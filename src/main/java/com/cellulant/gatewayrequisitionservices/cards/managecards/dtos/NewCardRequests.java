package com.cellulant.gatewayrequisitionservices.cards.managecards.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class NewCardRequests implements Serializable {

    @NotNull(message = "{accountAlias.required}")
    private String accountAlias;
    @NotNull(message = "{pin.required}")
//    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String pin;
    @NotNull(message = "{cardScheme.required}")
    private String cardScheme;
    @NotNull(message = "{cardProduct.required}")
    private String cardProduct;
    @NotNull(message = "{requestBranch.required}")
    private String requestBranch;
    @NotNull(message = "{amountToCharge.required}")
    private String amountToCharge;
    @NotNull(message = "{cardProductDesc.required}")
    private String cardProductDesc;
    @NotNull(message = "{chargeCurrency.required}")
    private String chargeCurrency;

    @NotNull(message = "{requestType.required}")
    private String requestType;

    @NotNull(message = "{deliveryMode.required}")
    private String deliveryMode;
}
