package com.cellulant.gatewayrequisitionservices.cards.managecards.dtos;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;

/**
 * Created by sylvester
 * Project requisition-gateway-api
 * User: sylvester
 * Date: 4/2/19
 * Time: 8:05 PM
 */
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BlockCardRequest implements Serializable {

    @NotBlank(message = "{pin.required}")
    //@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String pin;
    @NotBlank(message = "{accountAlias.required}")
    private String accountAlias;
    @NotBlank(message = "{cardNumber.required}")
    private String cardNumber;
    @NotBlank(message = "{reason.required}")
    private String reason;

    private String expiryDate;

    private String date;

    private String option;

    private String description;

}

