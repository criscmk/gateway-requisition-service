package com.cellulant.gatewayrequisitionservices.cards.managecards.dtos;

import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author Denis Gitonga
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DebitCardLimitInfo implements Serializable {
    private String currentLimit;
    private String limitState;

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
