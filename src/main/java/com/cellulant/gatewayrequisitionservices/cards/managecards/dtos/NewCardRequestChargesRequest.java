package com.cellulant.gatewayrequisitionservices.cards.managecards.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class NewCardRequestChargesRequest implements Serializable {
    @NotBlank(message = "{accountAlias.required}")
    private String accountAlias;
    @NotBlank(message = "{cardScheme.required}")
    private String cardScheme;
    @NotBlank(message = "{requestType.required}")
    private String requestType;
    @NotBlank(message = "{deliveryMode.required}")
    private String deliveryMode;
    private String cardNumber;
    private String digitalDeliveryAddress;
    private String digitalAddressMobileNumber;
    private String pan;
}