package com.cellulant.gatewayrequisitionservices.cards.managecards.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ActivateCardRequest implements Serializable {

    @NotBlank(message = "{pan.required}")
    private String pan;

    @NotBlank(message = "{pin.required}")
    //@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String pin;

    @NotBlank(message = "{cardAuthenticationPin.required}")
    //@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String cardAuthenticationPin;

    @NotBlank(message = "{expDate.required}")
    private String expDate;

    @NotBlank(message = "{accountAlias.required}")
    private String accountAlias;

    @NotBlank(message = "{cardAccountNumber.required}")
    private String cardAccountNumber;

}
