package com.cellulant.gatewayrequisitionservices.cards.managecards.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

/**
 * @author Denis Gitonga
 */
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChangeCardPinRequest {
    @NotBlank(message = "{pin.required}")
//    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String pin;
    @NotBlank(message = "{accountAlias.required}")
    private String accountAlias;
    @NotBlank(message = "{expiryDate.required}")
    private String expiryDate;
    @NotBlank(message = "{pan.required}")
    private String pan;
    @NotBlank(message = "{currentPin.required}")
//    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String currentPin;
    @NotBlank(message = "{newPin.required}")
//    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String newPin;

}
