package com.cellulant.gatewayrequisitionservices.cards;

import lombok.*;
import org.hibernate.validator.constraints.NotBlank;
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class StepUpJWTRequest {

    @NotBlank(message = "{returnUrl.required}")
    private String returnUrl;
    @NotBlank(message = "{acsUrl.required}")
    private String acsUrl;
    @NotBlank(message = "{transactionId.required}")
    private String transactionId;
    @NotBlank(message = "{pareq.required}")
    private String pareq;
    @NotBlank(message = "{referenceId.required}")
    private String referenceId;


}
