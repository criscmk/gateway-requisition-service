package com.cellulant.gatewayrequisitionservices.cards.virtualcards.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;

/**
 * Created by sylvester
 * Project cards-services-gateway-api
 * User: sylvester
 * Date: 7/8/19
 * Time: 5:13 PM
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateOTPVirtualCardRequest implements Serializable {

    @NotBlank(message = "{pin.required}")
//   @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String pin;
    @NotBlank(message = "{virtualAccountNumber.required}")
    private String virtualAccountNumber;
    @NotBlank(message = "{virtualAccountToken.required}")
    private String virtualAccountAlias;
    @NotBlank(message = "{virtualAccountAlias.required}")
    private String virtualAccountToken;
    @NotBlank(message = "{affiliate.required}")
    private String affiliate;
    @NotBlank(message = "{currentOTPNumber.required}")
    private String currentOTPNumber;
    @NotBlank(message = "{newOtpNumber.required}")
    private String newOtpNumber;
    @NotBlank(message = "{accountAlias.required}")
    private String accountAlias;


}