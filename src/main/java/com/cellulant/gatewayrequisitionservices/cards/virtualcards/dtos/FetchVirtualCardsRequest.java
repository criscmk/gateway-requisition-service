package com.cellulant.gatewayrequisitionservices.cards.virtualcards.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Created by eyo
 * Project card-services-gateway-api
 * User: eyo
 * Date: 2019-06-16
 * Time: 16:11
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FetchVirtualCardsRequest implements Serializable {

    private String accountAlias;
}
