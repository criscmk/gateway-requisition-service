package com.cellulant.gatewayrequisitionservices.cards.virtualcards.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by LoiseKinyua
 * Project special-service-gateway-api
 * User: LoiseKinyua
 * Date: 4/4/19
 * Time: 9:30 AM
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FetchVirtualCardDetailsRequest implements java.io.Serializable {

    @NotBlank(message = "{virtualAccountNumber.required}")
    private String virtualAccountNumber;
    @NotBlank(message = "{virtualCardAlias.required}")
    private String virtualCardAlias;
    @NotBlank(message = "{pin.required}")
//    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String pin;
}
