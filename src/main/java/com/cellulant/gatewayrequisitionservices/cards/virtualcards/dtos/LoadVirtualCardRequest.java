package com.cellulant.gatewayrequisitionservices.cards.virtualcards.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by eyo
 * Project card-services-gateway-api
 * User: eyo
 * Date: 2019-06-16
 * Time: 16:29
 */

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoadVirtualCardRequest implements Serializable {

    @NotBlank(message = "{pin.required}")
    private String pin;
    @NotBlank(message = "{accountAlias.required}")
    private String accountAlias;
    @NotBlank(message = "{virtualAccountNumber.required}")
    private String virtualAccountNumber;
    @NotBlank(message = "{virtualAccountToken.required}")
    private String virtualAccountToken;
    @NotNull(message = "{amount.required}")
    @Min(1)
    private double amount;
    @NotNull(message = "{chargeAmount.required}")
    private double chargeAmount;
}
