package com.cellulant.gatewayrequisitionservices.cards.virtualcards.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;

/**
 * Created by sylvester
 * Project cards-services-gateway-api
 * User: sylvester
 * Date: 7/8/19
 * Time: 5:13 PM
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FetchOTPMobileNumberRequest implements Serializable {
    @NotBlank(message = "{virtualAccountNumber.required}")
    private String virtualAccountNumber;
    @NotBlank(message = "{affiliate.required}")
    private String affiliate;
}