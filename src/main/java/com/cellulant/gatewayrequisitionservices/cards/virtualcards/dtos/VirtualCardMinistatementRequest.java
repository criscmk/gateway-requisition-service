package com.cellulant.gatewayrequisitionservices.cards.virtualcards.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

/**
 * Created by LoiseKinyua
 * Project special-service-gateway-api
 * User: LoiseKinyua
 * Date: 4/5/19
 * Time: 6:37 PM
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class VirtualCardMinistatementRequest implements java.io.Serializable {

    @NotBlank(message = "{pin.required}")
    //@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String pin;
    @NotNull(message = "{virtualAccountNumber.required}")
    private String virtualAccountNumber;
    @NotBlank(message = "{virtualAccountToken.required}")
    private String virtualAccountToken;
    private String fromDate;
    private String toDate;
    @NotBlank(message = "{accountAlias.required}")
    private String accountAlias;

}