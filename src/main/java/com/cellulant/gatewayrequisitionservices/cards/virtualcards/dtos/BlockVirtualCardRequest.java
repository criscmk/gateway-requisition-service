package com.cellulant.gatewayrequisitionservices.cards.virtualcards.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by LoiseKinyua
 * Project special-service-gateway-api
 * User: LoiseKinyua
 * Date: 4/4/19
 * Time: 9:30 AM
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BlockVirtualCardRequest implements java.io.Serializable {

    @NotBlank(message = "{virtualAccountNumber.required}")
    private String virtualAccountNumber;
    @NotBlank(message = "{virtualAccountToken.required}")
    private String virtualAccountToken;
    @NotBlank(message = "{virtualCardAlias.required}")
    private String virtualCardAlias;
    @NotBlank(message = "{pin.required}")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String pin;
    @NotBlank(message = "{accountAlias.required}")
    private String accountAlias;
}
