package com.cellulant.gatewayrequisitionservices.cards.virtualcards.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * The type Fetch card types request.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FetchVirtualCardTypesRequest implements Serializable {
    /**
     * The Emcert id.
     */
    @NotNull(message = "{emcertID.required}")
    int emcertID;

}
