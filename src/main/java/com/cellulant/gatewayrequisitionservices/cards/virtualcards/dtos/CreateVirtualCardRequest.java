package com.cellulant.gatewayrequisitionservices.cards.virtualcards.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by eyo
 * Project card-services-gateway-api
 * User: eyo
 * Date: 2019-06-16
 * Time: 16:00
 */
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateVirtualCardRequest implements Serializable {
    @NotBlank(message = "{affiliateCode.required}")
    private String affiliateCode;
    @NotBlank(message = "{schemeType.required}")
    private String schemeType;
    @NotBlank(message = "{virtualCardType.required}")
    private String virtualCardType;
    @NotNull(message = "{amount.required}")
    @Min(1)
    private double amount;
    @NotBlank(message = "{sourceAccount.required}")
    private String sourceAccount;
    @NotBlank(message = "{chargeAmount.required}")
    private String chargeAmount;
    @NotBlank(message = "{totalAmount.required}")
    private String totalAmount;
    @NotBlank(message = "{currencyCode.required}")
    private String currencyCode;
    @NotBlank(message = "{otpNumber.required}")
    private String otpNumber;
}
