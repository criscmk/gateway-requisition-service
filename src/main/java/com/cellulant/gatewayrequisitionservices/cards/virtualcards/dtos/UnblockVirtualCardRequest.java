package com.cellulant.gatewayrequisitionservices.cards.virtualcards.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

/**
 * Created by LoiseKinyua
 * Project special-service-gateway-api
 * User: LoiseKinyua
 * Date: 4/4/19
 * Time: 10:06 AM
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UnblockVirtualCardRequest implements java.io.Serializable {

    @NotNull(message = "{virtualAccountNumber.required}")
    private String virtualAccountNumber;
    @NotBlank(message = "{virtualAccountToken.required}")
    private String virtualAccountToken;
    @NotBlank(message = "{pin.required}")
//    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String pin;
    @NotBlank(message = "{accountAlias.required}")
    private String accountAlias;
}