package com.cellulant.gatewayrequisitionservices.exceptions;


import com.cellulant.gatewayrequisitionservices.configurations.StatusCodeConfig;
import com.cellulant.gatewayrequisitionservices.configurations.StatusMessageServiceConfig;

/**
 * Created by  IntelliJ IDEA.
 * Project transfers-api-gateway
 * User: Sylvester Musyoki
 * Date: 5/31/19
 * Time: 12:11 PM
 */
public class InvalidMsisdnException extends AbstractException {
    /**
     * Instantiates a new Invalid msisdn exception.
     *
     * @param statusCodeConfig     the status code config
     * @param statusMessageService the status message service
     * @throws Exception the exception
     */
    public InvalidMsisdnException(StatusCodeConfig statusCodeConfig, StatusMessageServiceConfig statusMessageService) throws Exception {
        super(statusCodeConfig, statusMessageService);
        this.statusCode = statusCodeConfig.getInvalidMsisdnStatusCode();
        this.statusMessage = statusMessageService.formulateResponseMessage(this.statusCode);
    }
}
