package com.cellulant.gatewayrequisitionservices.exceptions;


import com.cellulant.gatewayrequisitionservices.configurations.StatusCodeConfig;
import com.cellulant.gatewayrequisitionservices.configurations.StatusMessageServiceConfig;

/**
 * Created by kamauwamatu
 * Project paymentsgatewayapi
 * User: kamauwamatu
 * Date: 2019-05-15
 * Time: 09:32
 */
public class InvalidUrlException extends AbstractException {
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new User not found exception.
     *
     * @param statusCodeConfig     the status code config
     * @param statusMessageService the status message service
     * @throws Exception the exception
     */
    public InvalidUrlException(
            StatusCodeConfig statusCodeConfig, StatusMessageServiceConfig statusMessageService
    ) throws Exception {
        super(statusCodeConfig, statusMessageService);
        this.statusCode = statusCodeConfig.getInvalidUrlStatusCode();
        this.statusMessage = statusMessageService.formulateResponseMessage(this.statusCode);
    }
}
