package com.cellulant.gatewayrequisitionservices.exceptions;

import com.cellulant.gatewayrequisitionservices.configurations.StatusCodeConfig;
import com.cellulant.gatewayrequisitionservices.configurations.StatusMessageServiceConfig;
import lombok.NonNull;

/**
 * Created by eyo
 * Project gateway-requisition-services
 * User: eyo
 * Date: 05/12/2019
 * Time: 9:59 AM
 */
public class DuplicateClientRequestIdException extends AbstractException {

    public DuplicateClientRequestIdException(StatusCodeConfig statusCodeConfig, @NonNull StatusMessageServiceConfig statusMessageService) {
        super(statusCodeConfig, statusMessageService);
        this.statusCode = statusCodeConfig.getDuplicateClientRequestIdExceptionStatusCode();
        this.statusMessage = statusMessageService.formulateResponseMessage(this.statusCode);
    }
}
