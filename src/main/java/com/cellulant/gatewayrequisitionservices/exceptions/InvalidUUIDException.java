package com.cellulant.gatewayrequisitionservices.exceptions;


import com.cellulant.gatewayrequisitionservices.configurations.StatusCodeConfig;
import com.cellulant.gatewayrequisitionservices.configurations.StatusMessageServiceConfig;

/**
 * Created by eyo
 * Project onboarding-gateway-api
 * User: eyo
 * Date: 2019-07-17
 * Time: 11:26
 */
public class InvalidUUIDException extends AbstractException {
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new Abstract exception.
     *
     * @param statusCodeConfig     the status code config
     * @param statusMessageService the status message service
     */
    public InvalidUUIDException(StatusCodeConfig statusCodeConfig, StatusMessageServiceConfig statusMessageService) {
        super(statusCodeConfig, statusMessageService);
        this.statusCode = statusCodeConfig.getInvalidUuidExceptionStatusCode();
        this.statusMessage = statusMessageService.formulateResponseMessage(this.statusCode);
    }
}
