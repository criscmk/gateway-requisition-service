package com.cellulant.gatewayrequisitionservices.exceptions;


import com.cellulant.gatewayrequisitionservices.configurations.StatusCodeConfig;
import com.cellulant.gatewayrequisitionservices.configurations.StatusMessageServiceConfig;

/**
 * Created by kamauwamatu
 * Project gateway
 * User: kamauwamatu
 * Date: 2019-05-02
 * Time: 10:38
 */
public abstract class AbstractException extends Exception {

    private static final long serialVersionUID = 1L;
    /**
     * The Status codes.
     */
    protected StatusCodeConfig statusCodes;

    /**
     * The Status message service.
     */
    protected StatusMessageServiceConfig statusMessageService;
    /**
     * The Status code.
     */
    protected int statusCode;
    /**
     * The Status message.
     */
    protected String statusMessage;

    /**
     * Instantiates a new Abstract exception.
     *
     * @param statusCodes          the status codes
     * @param statusMessageService the status message service
     */
    AbstractException(
            StatusCodeConfig statusCodes,
            StatusMessageServiceConfig statusMessageService
    ) {
        super();
        this.statusCodes = statusCodes;
        this.statusMessageService = statusMessageService;
    }

    /**
     * Gets serial version uid.
     *
     * @return the serial version uid
     */
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    /**
     * Gets status code.
     *
     * @return the status code
     */
    public int getStatusCode() {
        return statusCode;
    }

    /**
     * Sets status code.
     *
     * @param statusCode the status code
     */
    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * Gets status message.
     *
     * @return the status message
     */
    public String getStatusMessage() {
        return statusMessage;
    }

    /**
     * Sets status message.
     *
     * @param statusMessage the status message
     */
    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }
}
