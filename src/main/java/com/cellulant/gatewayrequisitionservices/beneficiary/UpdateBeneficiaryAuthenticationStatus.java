package com.cellulant.gatewayrequisitionservices.beneficiary;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UpdateBeneficiaryAuthenticationStatus implements Serializable {
    private String nominationID;
    private Boolean authenticationStatus;
    private String otp;
    private String otpType;

}
