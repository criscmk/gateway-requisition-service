package com.cellulant.gatewayrequisitionservices.contacts.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by kamauwamatu
 * Project lookup-gateway-api
 * User: kamauwamatu
 * Date: 2019-06-16
 * Time: 23:34
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RespContacts {

    private Long contactInfo;
    private String title;
    private String location;
    private String phoneFax;
    private String email;
    private String address;
    private String twitter;
    private String facebook;
    private String website;
    private String linkedIn;
    private String date;
    private String youtube;
    private String swiftCode;
    private String mission;
    private String vision;
}