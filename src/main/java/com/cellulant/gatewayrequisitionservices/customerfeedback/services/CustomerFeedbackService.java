package com.cellulant.gatewayrequisitionservices.customerfeedback.services;
import com.cellulant.gatewayrequisitionservices.configurations.Configs;
import com.cellulant.gatewayrequisitionservices.configurations.Constants;
import com.cellulant.gatewayrequisitionservices.customerfeedback.requests.CustomerFeedBackRequest;
import com.cellulant.gatewayrequisitionservices.dtos.responses.ResponseArray;
import com.cellulant.gatewayrequisitionservices.models.ServicesEntity;
import com.cellulant.gatewayrequisitionservices.services.ServiceImpl;
import com.cellulant.gatewayrequisitionservices.utils.SharedUtilities;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.slf4j.MDC;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.Map;

@Service
@AllArgsConstructor
public class CustomerFeedbackService {

    private final SharedUtilities sharedUtilities;
    private final ServiceImpl serviceImpl;
    private final Configs configs;

    /**
     * Process customer feedback request mono.
     *
     * @param headerParams             the header params
     * @param customerFeedBackRequestDto the customer feedback request
     * @return the mono
     * @throws Exception the exception
     */
    public Mono<ResponseArray> processCustomerFeedback(Map headerParams, CustomerFeedBackRequest customerFeedBackRequestDto) throws Exception {
        //Get action from headers
        final String action = sharedUtilities.retrieveActionFromHeaders(headerParams);
        final String uuid = String.valueOf(headerParams.get(Constants.UUID));
        //Retrieve the Service Details form Database
        final Mono<ServicesEntity> servicesEntityMono = serviceImpl.getServiceDetails((String.valueOf(headerParams.get(Constants.SERVICE))));

        return servicesEntityMono.flatMap(servicesEntity -> {
            // Get msisdn
            final Mono<String> msisdnMono = sharedUtilities.getMsisdn(headerParams, action, servicesEntity)
                    .doOnNext(msisdn -> MDC.put(Constants.MSISDN, String.valueOf(msisdn)));

            // Retrieve Wallet URL from Database using provided action
            final Mono<String> walletUrlMono = sharedUtilities.getWalletUrl(action, servicesEntity);

            final Mono<String> customerFeedBackJsonMono = Mono.just(customerFeedBackRequestDto)
                    .flatMap(customerFeedBackRequestDto1 -> Mono.fromCallable(() -> {
                        // Formulate Wallet Payload
                        ObjectMapper objectMapper = new ObjectMapper();
                        objectMapper.disable(MapperFeature.USE_ANNOTATIONS);
                        return objectMapper.writeValueAsString(customerFeedBackRequestDto1);
                    }));


            return Mono.zip(msisdnMono, walletUrlMono, customerFeedBackJsonMono)
                    .flatMap(data -> {
                        final String msisdn = data.getT1();
                        final String walletUrl = data.getT2();
                        final String hardTokenActivateRequestJson = data.getT3();

                        // update headerParams with updated msisdn
                        final Mono<Map> headerParamsMono = Mono.just(headerParams).map(map -> {
                            map.replace(Constants.MSISDN, msisdn);
                            return map;
                        });

                        return headerParamsMono.flatMap(headers -> {
                            final Mono<String> requestIDMono = sharedUtilities.getRequestID(customerFeedBackRequestDto, msisdn, headers)
                                    .doOnNext(requestID -> MDC.put(Constants.CLOUD_REQUEST_ID, String.valueOf(requestID)));

                            //Validate Uuid
                            final Mono<Void> validateUUID = sharedUtilities.validateUUID(servicesEntity, msisdn, action, uuid);

                            return validateUUID.then(requestIDMono)
                                    .flatMap(requestID -> sharedUtilities
                                            .processIntegrationResponse(headers, msisdn, requestID, servicesEntity, walletUrl, hardTokenActivateRequestJson));

                        });
                    });
        });
    }

    /**
     * Fetch Customer Feedback Services mono.
     *
     * @param headerParams the header params
     * @return the mono
     * @throws Exception the exception
     */
    public Mono<ResponseArray> fetchCustomerFeedbackServiceCodes(Map headerParams) throws Exception {
        //Get action from headers
        String action = sharedUtilities.retrieveActionFromHeaders(headerParams);
        //Retrieve the Service Details form Database
        final Mono<ServicesEntity> servicesEntityMono = serviceImpl.getServiceDetails((String.valueOf(headerParams.get(Constants.SERVICE))));

        return servicesEntityMono.flatMap(servicesEntity -> {
            // Get msisdn
            final Mono<String> msisdnMono = sharedUtilities.getMsisdn(headerParams, action, servicesEntity)
                    .doOnNext(msisdn -> MDC.put(Constants.MSISDN, String.valueOf(msisdn)));

            // Retrieve Wallet URL from Database using provided action
            final Mono<String> walletUrlMono = sharedUtilities.getWalletUrl(action, servicesEntity);

            return msisdnMono.flatMap(msisdn -> {
                // update headerParams with updated msisdn
                final Mono<Map> headerParamsMono = Mono.just(headerParams).map(map -> {
                    map.replace(Constants.MSISDN, msisdn);
                    return map;
                });

                return headerParamsMono.then(walletUrlMono)
                        .flatMap(sharedUtilities::getIntegrationLayerResponse)
                        .flatMap(sharedUtilities::handleResponse);

            });
        });
    }


}
