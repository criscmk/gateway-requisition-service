package com.cellulant.gatewayrequisitionservices.customerfeedback.requests;
import com.cellulant.gatewayrequisitionservices.validation.Conditional;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Conditional(selected = "serviceCode",values = {"other","Other","OTHER"},required = {"otherServiceCode"})
public class CustomerFeedBackRequest {
    @NotBlank(message = "{platform.required}")
    private String platform;
    @NotBlank(message = "{openDate.required}")
    private String openDate;
    @NotBlank(message = "{closeDate.required}")
    private String closeDate;
    @NotBlank(message = "{serviceCode.required}")
    private String serviceCode;
    private String otherServiceCode;
    @NotNull(message = "{experienceMetric.required}")
    @Min(0) @Max(10)
    private Integer experienceMetric;
    @NotNull(message = "{referralMetric.required}")
    @Min(0) @Max(10)
    private Integer referralMetric;

    private String additionalResponse;
    @NotNull(message = "{contactCustomer.required}")
    @Min(0)@Max(1)
    private Integer contactCustomer;
    @NotBlank(message = "accountAlias.required")
    private String accountAlias;
}
