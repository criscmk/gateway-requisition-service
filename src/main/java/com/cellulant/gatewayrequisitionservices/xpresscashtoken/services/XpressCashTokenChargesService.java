package com.cellulant.gatewayrequisitionservices.xpresscashtoken.services;

import com.cellulant.gatewayrequisitionservices.configurations.Constants;
import com.cellulant.gatewayrequisitionservices.dtos.responses.ResponseArray;
import com.cellulant.gatewayrequisitionservices.models.ServicesEntity;
import com.cellulant.gatewayrequisitionservices.services.ServiceImpl;
import com.cellulant.gatewayrequisitionservices.utils.SharedUtilities;
import com.cellulant.gatewayrequisitionservices.xpresscashtoken.request.XpressCashTokenChargesRequestDto;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.Map;

@Service
@RequiredArgsConstructor
@Slf4j
public class XpressCashTokenChargesService {
    private final SharedUtilities sharedUtilities;
    private final ServiceImpl serviceImpl;

    public Mono<ResponseArray> processFetchXpressCashTokenCharges(Map headerParams, XpressCashTokenChargesRequestDto xpressCashTokenChargesRequestDto) throws Exception{
        //Get action from headers
        final String action = sharedUtilities.retrieveActionFromHeaders(headerParams);
        final String uuid = String.valueOf(headerParams.get(Constants.UUID));
        //Retrieve the Service Details form Database
        final Mono<ServicesEntity> servicesEntityMono = serviceImpl.getServiceDetails((String.valueOf(headerParams.get(Constants.SERVICE))));

        return servicesEntityMono.flatMap(servicesEntity -> {
            // Get msisdn
            final Mono<String> msisdnMono = sharedUtilities.getMsisdn(headerParams, action, servicesEntity)
                    .doOnNext(msisdn -> MDC.put(Constants.MSISDN, String.valueOf(msisdn)));

            // Retrieve Wallet URL from Database using provided action
            final Mono<String> walletUrlMono = sharedUtilities.getWalletUrl(action, servicesEntity);

            final Mono<String> fetchXpressCashTokenChargesJsonMono = Mono.just(xpressCashTokenChargesRequestDto)
                    .flatMap(chargesRequest -> Mono.fromCallable(() -> {
                        log.info("This is the charges request [{}]", chargesRequest);
                        // Encrypt the Pin
                        //chargesRequest.setTransactionType(sharedUtilities.mapServiceToEcobankTransactionTypes(String.valueOf(donationsChargesRequest.getTransactionType())));
                        float amount = sharedUtilities.cleanupDecimalAmount(Float.parseFloat(xpressCashTokenChargesRequestDto.getAmount().replace(",","")));
                        chargesRequest.setAmount(String.valueOf(amount));
                        // Formulate Wallet Payload
                        ObjectMapper objectMapper = new ObjectMapper();
                        objectMapper.disable(MapperFeature.USE_ANNOTATIONS);
                        return objectMapper.writeValueAsString(chargesRequest);
                    }));

            return Mono.zip(msisdnMono, walletUrlMono, fetchXpressCashTokenChargesJsonMono)
                    .flatMap(data -> {

                        final String msisdn = data.getT1();
                        final String walletUrl = data.getT2();
                        final String fetchChargesJson = data.getT3();

                        // update headerParams with updated msisdn
                        final Mono<Map> headerParamsMono = Mono.just(headerParams).map(map -> {
                            map.replace(Constants.MSISDN, msisdn);
                            return map;
                        });

                        return headerParamsMono.flatMap(headers -> {
                            final Mono<String> requestIDMono = sharedUtilities.getRequestID(xpressCashTokenChargesRequestDto, msisdn, headers)
                                    .doOnNext(requestID -> MDC.put(Constants.CLOUD_REQUEST_ID, String.valueOf(requestID)));

                            //Validate Uuid
                            final Mono<Void> validateUUID = sharedUtilities.validateUUID(servicesEntity, msisdn, action, uuid);

                            return validateUUID.then(requestIDMono)
                                    .flatMap(requestID -> sharedUtilities
                                            .processIntegrationResponse(headers, msisdn, requestID, servicesEntity, walletUrl, fetchChargesJson));
                        });

                    });
        });
    }

}
