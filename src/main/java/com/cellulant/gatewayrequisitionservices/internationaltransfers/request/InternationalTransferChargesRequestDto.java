package com.cellulant.gatewayrequisitionservices.internationaltransfers.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InternationalTransferChargesRequestDto implements Serializable {
    @NotBlank(message = "{accountAlias.required}")
    private String accountAlias;
    @NotNull(message = "{amount.required}")
    private String amount;
    @NotNull(message = "{currency.required}")
    private String currency;
    @NotNull(message = "{productCode.required}")
    private String productCode;
}
