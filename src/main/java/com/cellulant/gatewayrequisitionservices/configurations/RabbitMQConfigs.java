package com.cellulant.gatewayrequisitionservices.configurations;


import lombok.AllArgsConstructor;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Configuration
@AllArgsConstructor
public class RabbitMQConfigs {

    private final CachingConnectionFactory cachingConnectionFactory;
    private final Configs configs;

    @PostConstruct
    public RabbitAdmin getRabbitAdmin() {
        RabbitAdmin admin = new RabbitAdmin(cachingConnectionFactory);
        admin.declareQueue(syncWalletCoreQueue());
        admin.declareQueue(syncRequestLoggerQueue());
        admin.declareExchange(requestLoggerServiceExchange());
        admin.declareExchange(syncCoreServiceExchange());
        admin.declareBinding(loggerBinding());
        admin.declareBinding(coreBinding());
        admin.initialize();
        return admin;
    }

    private Queue syncWalletCoreQueue() {
        return new Queue(configs.getQueueNamePrefix() + "." + configs.getSyncCoreQueue(), true);
    }


    private Queue syncRequestLoggerQueue() {
        return new Queue(configs.getQueueNamePrefix() + "." + configs.getRequestLoggerQueue(), true, false, false);
    }

    /**
     * Bind the request Queue to the Exchange and Routing Key
     *
     * @return Binding Object
     */
    private Binding loggerBinding() {
        return BindingBuilder.
                bind(syncRequestLoggerQueue()).
                to(requestLoggerServiceExchange()).
                with(configs.getQueueNamePrefix() + "." + configs.getRequestLoggerQueue());
    }

    /**
     * Bind the request Queue to the Exchange and Routing Key
     *
     * @return Binding Object
     */
    private Binding coreBinding() {
        return BindingBuilder.
                bind(syncWalletCoreQueue()).
                to(syncCoreServiceExchange()).
                with(configs.getQueueNamePrefix() + "." + configs.getSyncCoreQueue());
    }

    /**
     * Create a new Requests Topic Exchange
     *
     * @return Requests Object
     */
    private TopicExchange requestLoggerServiceExchange() {
        return new TopicExchange(configs.getExchangeName() + "." + configs.getQueueNamePrefix() + "." + configs.getRequestLoggerQueue());
    }

    private TopicExchange syncCoreServiceExchange() {
        return new TopicExchange(configs.getExchangeName() + "." + configs.getQueueNamePrefix() + "." + configs.getSyncCoreQueue());
    }

    @Bean
    public MessageConverter jsonMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }


}
