package com.cellulant.gatewayrequisitionservices.configurations;

import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.server.i18n.LocaleContextResolver;

/**
 * @author Denis Gitonga
 */
@Configuration
@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.REACTIVE)
public class CellulantWebfluxSupportConfig {

    @Bean
    LocaleContextResolver localeContextHolder(final Configs configs) {
        return new CellulantLocaleContextResolver(configs);
    }
}
