package com.cellulant.gatewayrequisitionservices.configurations;

import com.cellulant.gatewayrequisitionservices.security.TokenStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.slf4j.Logger;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestContextHolder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by kamauwamatu
 * Project gateway
 * User: kamauwamatu
 * Date: 2019-04-27
 * Time: 13:25
 */
@Configuration
//@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
public class RestTemplateConfig {
    // Initializes the logger class
    private Logger logger;
    // Initializes the Configs class
    private final Configs configs;

    public RestTemplateConfig(Logger logger, Configs configs) {
        this.logger = logger;
        this.configs = configs;
    }

    @Bean
    public PoolingHttpClientConnectionManager poolingHttpClientConnectionManager() {
        PoolingHttpClientConnectionManager result = new PoolingHttpClientConnectionManager();
        result.setMaxTotal(configs.getMaxConnectionPoolSize());
        result.setDefaultMaxPerRoute(configs.getDefaultMaxPerRouteConnectionPoolSize());
        return result;
    }

    @Bean
    public RequestConfig requestConfig() {
        return RequestConfig
                .custom()
                .setConnectionRequestTimeout(configs.getRestTemplateRequestTimeout())
                .setConnectTimeout(configs.getRestTemplateSocketTimeout())
                .setSocketTimeout(configs.getRestTemplateConnectTimeout())
                .build();
    }

    @Bean
    public CloseableHttpClient httpClient(
            PoolingHttpClientConnectionManager poolingHttpClientConnectionManager,
            RequestConfig requestConfig
    ) {
        return HttpClientBuilder
                .create()
                .setConnectionManager(poolingHttpClientConnectionManager)
                .setDefaultRequestConfig(requestConfig)
                .build();
    }

    /**
     * Rest template rest template.
     *
     * @return the rest template
     */
    @Bean
    public RestTemplate restTemplate(HttpClient httpClient) {
        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        requestFactory.setHttpClient(httpClient);
        RestTemplate restTemplate = new RestTemplate(requestFactory);

        List<ClientHttpRequestInterceptor> interceptors = restTemplate.getInterceptors();
        if (CollectionUtils.isEmpty(interceptors)) {
            interceptors = new ArrayList<>();
        }
        interceptors.add(new AuthenticationCustomInterceptor(logger, configs));
        restTemplate.setInterceptors(interceptors);

        return restTemplate;
    }
}

class AuthenticationCustomInterceptor implements ClientHttpRequestInterceptor {
    // Initializes the logger class
    private Logger logger;
    // Initializes the Configs class
    private final Configs configs;

    AuthenticationCustomInterceptor(Logger logger, Configs configs) {
        this.logger = logger;
        this.configs = configs;
    }

    @Override
    public ClientHttpResponse intercept(
            HttpRequest request, byte[] body,
            ClientHttpRequestExecution execution
    ) throws IOException {

        // Language Code Interception
        String languageCode;
        String headersMapString;
        // Headers map
        Map<String, String> headersMap = new HashMap<>();

        // Get Language Code from the context
        try {
            languageCode = RequestContextHolder.getRequestAttributes().getAttribute(Constants.LANGUAGE_CODE, 0).toString();
            headersMapString = RequestContextHolder.getRequestAttributes().getAttribute(Constants.X_HEADERS, 0).toString();
        } catch (Exception ex) {
            logger.error("Language Code Parameter Not Found in Request Context ::{} ", ex.getMessage());
            languageCode = configs.getDefaultLanguageCode();
            headersMapString = null;
        }


        HttpHeaders headers = request.getHeaders();
        String jwt = TokenStore.getToken();

        logger.debug("the jwt token to send in request: {} ", jwt);
        headers.add(Constants.X_AUTHORIZATION, "Bearer " + jwt);
        headers.add(Constants.AUTHORIZATION, "Bearer " + jwt);
        headers.add(Constants.LANGUAGE_CODE, languageCode);
        headers.add(Constants.X_HEADERS, headersMapString);
        headers.add(Constants.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        return execution.execute(request, body);
    }
}
