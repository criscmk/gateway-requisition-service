package com.cellulant.gatewayrequisitionservices.configurations.aspects;


import com.cellulant.gatewayrequisitionservices.configurations.Constants;
import com.cellulant.gatewayrequisitionservices.configurations.ObjectMapperConfiguration;
import com.cellulant.gatewayrequisitionservices.configurations.StatusCodeConfig;
import com.cellulant.gatewayrequisitionservices.dtos.responses.ResponseArray;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.MDC;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import static net.logstash.logback.argument.StructuredArguments.keyValue;

/**
 * Created by kamauwamatu
 * Project enquiries-gateway-api
 * User: kamauwamatu
 * Date: 2019-07-09
 * Time: 07:12
 */
@Aspect
@RequiredArgsConstructor
@Component
@Slf4j
public class ControllersAspect {


    @NonNull
    private final ObjectMapperConfiguration objectMapper;
    @NonNull
    private final StatusCodeConfig statusCodeConfig;

    private Map<String, String> requestHeaders = new HashMap<>();

    @Pointcut("execution(* com.cellulant.*.controllers.*.*(..)) &&" +
            " within(@org.springframework.web.bind.annotation.RestController *)")
    private void forControllerLoggable() {
    }

    /**
     * Application logging advice object.
     *
     * @param proceedingJoinPoint the proceeding join point
     * @return the object
     * @throws Throwable the throwable
     */
    @Around("forControllerLoggable()")
    public Object applicationLoggingAdvice(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {

        long startTime = System.currentTimeMillis();
        String methodName = proceedingJoinPoint.getSignature().toShortString();
        Object[] request = proceedingJoinPoint.getArgs();
        String requestHeaderString = null;
        if (request.length != 0) {

            requestHeaderString = (String) request[0];
            requestHeaders = objectMapper.objectMapper().readValue(requestHeaderString, Map.class);

        }
        String requestString = null;
        if (request.length > 2) {
            requestString = Optional.of(objectMapper.objectMapper().writeValueAsString(request[1])).orElse(null);
        }
        log.info("showing the controller request : [{}]: ", requestString);


        Object value;
        try {
            final AtomicReference<Long> startTimeRef = new AtomicReference<>();
            value = proceedingJoinPoint.proceed();
            if (value instanceof Mono<?>) {
                final Mono<?> mono = (Mono<?>) value;
                final String finalRequestString = requestString;
                return mono.doOnSubscribe(subscription -> startTimeRef.set(System.nanoTime()))
                        .doOnSuccess(o -> {
                            final long timeTaken = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startTimeRef.get());
                            ResponseArray response = (ResponseArray) (o);

                            logSuccessResponse(methodName, finalRequestString, timeTaken, response);
                        })
                        .doOnError(ex -> {
                            final long timeTaken = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startTimeRef.get());

                            logErrorResponse(methodName, finalRequestString, ex, timeTaken);
                        }).doFinally(signalType -> MDC.clear());
            }
        } catch (Throwable e) {
            long timeTaken = System.currentTimeMillis() - startTime;
            logErrorResponse(methodName, requestString, e, timeTaken);

            throw e;
        }

        long timeTaken = System.currentTimeMillis() - startTime;

        final ResponseEntity<?> responseEntity = (ResponseEntity<?>) value;
        ResponseArray response = (ResponseArray) responseEntity.getBody();

        MDC.put(Constants.HTTP_STATUS, String.valueOf(responseEntity.getStatusCode()));
        logSuccessResponse(methodName, requestString, timeTaken, response);

        MDC.clear();
        return value;
    }

    private void logErrorResponse(String methodName, String requestString, Throwable ex, long timeTaken) {
        MDC.put(Constants.MSISDN, requestHeaders.get(Constants.MSISDN));
        MDC.put(Constants.SERVICE, requestHeaders.get(Constants.SERVICE));
        MDC.put(Constants.ACTION, requestHeaders.get(Constants.ACTION));
        MDC.put(Constants.AFFILIATE, requestHeaders.get(Constants.AFFILIATE));
        MDC.put(Constants.METHOD, methodName);
        MDC.put(Constants.UUID, requestHeaders.get(Constants.UUID));
        MDC.put(Constants.OS_TYPE, requestHeaders.get(Constants.OS_TYPE));
        MDC.put(Constants.OS_VERSION, requestHeaders.get(Constants.OS_VERSION));
        MDC.put(Constants.APP_VERSION, requestHeaders.get(Constants.APP_VERSION));
        MDC.put(Constants.DEVICE_TYPE, requestHeaders.get(Constants.DEVICE_TYPE));
        MDC.put(Constants.DEVICE_MODEL, requestHeaders.get(Constants.DEVICE_MODEL));
        MDC.put(Constants.CHANNEL, requestHeaders.get(Constants.CHANNEL));
        MDC.put(Constants.TAT, String.valueOf(timeTaken));
        MDC.put(Constants.REQUEST, requestString);
        MDC.put(Constants.EXCEPTION, ex.getMessage());


        log.error("Failure Processing Request {} {} {} {} {} {} ",
                keyValue(Constants.MSISDN, requestHeaders.get(Constants.MSISDN)),
                keyValue(Constants.SERVICE, requestHeaders.get(Constants.SERVICE)),
                keyValue(Constants.ACTION, requestHeaders.get(Constants.ACTION)),
                keyValue(Constants.METHOD, methodName),
                keyValue(Constants.TAT, timeTaken),
                keyValue(Constants.EXCEPTION, ex.getMessage()));
    }

    private void logSuccessResponse(String methodName, String requestString, long timeTaken, ResponseArray response) {
        MDC.put(Constants.MSISDN, requestHeaders.get(Constants.MSISDN));
        MDC.put(Constants.SERVICE, requestHeaders.get(Constants.SERVICE));
        MDC.put(Constants.ACTION, requestHeaders.get(Constants.ACTION));
        MDC.put(Constants.AFFILIATE, requestHeaders.get(Constants.AFFILIATE));
        MDC.put(Constants.APP_VERSION, requestHeaders.get(Constants.APP_VERSION));
        MDC.put(Constants.DEVICE_TYPE, requestHeaders.get(Constants.DEVICE_TYPE));
        MDC.put(Constants.OS_TYPE, requestHeaders.get(Constants.OS_TYPE));
        MDC.put(Constants.OS_VERSION, requestHeaders.get(Constants.OS_VERSION));
        MDC.put(Constants.UUID, requestHeaders.get(Constants.UUID));
        MDC.put(Constants.DEVICE_MODEL, requestHeaders.get(Constants.DEVICE_MODEL));
        MDC.put(Constants.CHANNEL, requestHeaders.get(Constants.CHANNEL));
        MDC.put(Constants.METHOD, methodName);
        MDC.put(Constants.TAT, String.valueOf(timeTaken));
        MDC.put(Constants.REQUEST, requestString);
        try {
            MDC.put(Constants.RESPONSE, objectMapper.objectMapper().writeValueAsString(response));
        } catch (JsonProcessingException e) {
            // ignore parsing error
        }

        log.info("Successfully Processed Request {} {} {} {} {} ",
                keyValue(Constants.MSISDN, requestHeaders.get(Constants.MSISDN)),
                keyValue(Constants.SERVICE, requestHeaders.get(Constants.SERVICE)),
                keyValue(Constants.METHOD, methodName),
                keyValue(Constants.ACTION, requestHeaders.get(Constants.ACTION)),
                keyValue(Constants.TAT, timeTaken));
    }

    /**
     * Exception logging advice.
     *
     * @param joinPoint the join point
     * @param ex        the ex
     * @throws Throwable the throwable
     */
    @AfterThrowing(pointcut = "forControllerLoggable()", throwing = "ex")
    public void exceptionLoggingAdvice(JoinPoint joinPoint, Exception ex) throws Throwable {
        long startTime = System.currentTimeMillis();
        String methodName = joinPoint.getSignature().toShortString();
        Object[] request = joinPoint.getArgs();
        String requestHeaderString = null;

        if (request.length != 0) {
            requestHeaderString = (String) request[0];
            requestHeaders = objectMapper.objectMapper().readValue(requestHeaderString, Map.class);
        }
        String requestString = null;
        if (request.length > 2) {
            requestString = Optional.of(objectMapper.objectMapper().writeValueAsString(request[1])).orElse(null);
        }

        log.info("showing the controller request : [{}]: ", requestString);
        Class<?> exceptionClassType = ex.getClass();
        Object object = exceptionClassType.cast(ex);
        long timeTaken = System.currentTimeMillis() - startTime;


        MDC.put(Constants.MSISDN, requestHeaders.get(Constants.MSISDN));
        MDC.put(Constants.SERVICE, requestHeaders.get(Constants.SERVICE));
        MDC.put(Constants.ACTION, requestHeaders.get(Constants.ACTION));
        MDC.put(Constants.AFFILIATE, requestHeaders.get(Constants.AFFILIATE));
        MDC.put(Constants.CHANNEL, requestHeaders.get(Constants.CHANNEL));
        MDC.put(Constants.METHOD, methodName);
        MDC.put(Constants.TAT, String.valueOf(timeTaken));
        MDC.put(Constants.REQUEST, requestString);
        MDC.put(Constants.RESPONSE, objectMapper.objectMapper().writeValueAsString(object));
        MDC.put(Constants.STATUS_CODE, String.valueOf(statusCodeConfig.getGatewayFailureStatusCode()));


        log.error("Failure Processing Request {} {} {} {} {} {} ",
                keyValue(Constants.MSISDN, requestHeaders.get(Constants.MSISDN)),
                keyValue(Constants.SERVICE, requestHeaders.get(Constants.SERVICE)),
                keyValue(Constants.METHOD, methodName),
                keyValue(Constants.ACTION, requestHeaders.get(Constants.ACTION)),
                keyValue(Constants.TAT, timeTaken),
                keyValue(Constants.EXCEPTION, ex.toString()));

        MDC.clear();

    }


}
