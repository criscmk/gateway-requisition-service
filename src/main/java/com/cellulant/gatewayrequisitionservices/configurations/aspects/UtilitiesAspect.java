package com.cellulant.gatewayrequisitionservices.configurations.aspects;


import com.cellulant.gatewayrequisitionservices.configurations.Constants;
import com.cellulant.gatewayrequisitionservices.configurations.ObjectMapperConfiguration;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by kamauwamatu
 * Project enquiries-gateway-api
 * User: kamauwamatu
 * Date: 2019-07-09
 * Time: 07:12
 */
@Aspect
@Component
@RequiredArgsConstructor
@SuppressWarnings("Duplicates")
@Slf4j
public class UtilitiesAspect {


    @NonNull
    private final ObjectMapperConfiguration objectMapper;

    @Pointcut("execution(* com.cellulant.*.utils.*.*(..)) &&" +
            " within(@com.cellulant.gatewayrequisitionservices.configurations.aspects.annotations.UtilitiesLoggable *)" +
            "  && !@annotation(com.cellulant.gatewayrequisitionservices.configurations.aspects.annotations.NoLogging)")
    private void UtilitiesLoggable() {
    }


    /**
     * Application around advice object.
     *
     * @param proceedingJoinPoint the proceeding join point
     * @return the object
     * @throws Throwable the throwable
     */
    @Around("UtilitiesLoggable()")
    public Object applicationAroundAdvice(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {

        long startTime = System.currentTimeMillis();

        String methodName = proceedingJoinPoint.getSignature().toShortString();
        Object[] request = proceedingJoinPoint.getArgs();
        Object value = null;

        try {
            value = proceedingJoinPoint.proceed();

            if (value instanceof Mono<?>) {
                final AtomicReference<Long> startTimeRef = new AtomicReference<>();
                return ((Mono<?>) value).doOnSubscribe(subscription -> startTimeRef.set(System.nanoTime()))
                        .doOnSuccess(o -> {
                            final long timeTaken = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startTimeRef.get());
                            logSuccessResponse(methodName, request, o, timeTaken);
                        })
                        .doOnError(ex -> {
                            final long timeTaken = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startTimeRef.get());
                            logErrorResponse(methodName, request, ex, timeTaken);

                        });
            }
        } catch (Throwable e) {

            long timeTaken = System.currentTimeMillis() - startTime;
            logErrorResponse(methodName, request, e, timeTaken);

            throw e;
        }

        long timeTaken = System.currentTimeMillis() - startTime;
        final String requestString = objectMapper.objectMapper().writeValueAsString(request);
        final String valueAsString = objectMapper.objectMapper().writeValueAsString(value);
        logSuccessResponse(methodName, requestString, valueAsString, timeTaken);

        return value;
    }

    private void logSuccessResponse(String methodName, Object request, Object o, long timeTaken) {
        String responseString = "";
        try {
            responseString = objectMapper.objectMapper().writeValueAsString(o);
            MDC.put(Constants.REQUEST, objectMapper.objectMapper().writeValueAsString(request));
            MDC.put(Constants.RESPONSE, responseString);
            log.info("Returning Response : from method ::  {}  with response :: {}  with request :: {}", methodName, responseString,objectMapper.objectMapper().writeValueAsString(request));

        } catch (JsonProcessingException e) {
            // do nothing
        }
        MDC.put(Constants.METHOD, methodName);
        MDC.put(Constants.UTILITIES_TAT, String.valueOf(timeTaken));
        MDC.remove(Constants.SERVICE_TAT);

    }

    private void logErrorResponse(String methodName, Object request, Throwable ex, long timeTaken) {
        try {
            MDC.put(Constants.REQUEST, objectMapper.objectMapper().writeValueAsString(request));
        } catch (JsonProcessingException e) {
            // do nothing
        }
        MDC.put(Constants.METHOD, methodName);
        MDC.put(Constants.EXCEPTION, ex.getLocalizedMessage());
        MDC.put(Constants.UTILITIES_TAT, String.valueOf(timeTaken));
        MDC.remove(Constants.SERVICE_TAT);

        log.error("Error : in Exception : from method :: {} and Exception Message : {} ", methodName, ex.getMessage());
    }
}