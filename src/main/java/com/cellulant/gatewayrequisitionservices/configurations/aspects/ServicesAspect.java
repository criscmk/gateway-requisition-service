package com.cellulant.gatewayrequisitionservices.configurations.aspects;


import com.cellulant.gatewayrequisitionservices.configurations.Constants;
import com.cellulant.gatewayrequisitionservices.configurations.ObjectMapperConfiguration;
import com.cellulant.gatewayrequisitionservices.configurations.StatusCodeConfig;
import com.cellulant.gatewayrequisitionservices.dtos.responses.ResponseArray;
import com.cellulant.gatewayrequisitionservices.dtos.responses.ResponseArrayResultDTO;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import static net.logstash.logback.argument.StructuredArguments.keyValue;

/**
 * Created by kamauwamatu
 * Project gateway-payment-services
 * User: kamauwamatu
 * Date: 2019-07-09
 * Time: 07:12
 */
@Aspect
@Component
@RequiredArgsConstructor
@SuppressWarnings("Duplicates")
@Slf4j
public class ServicesAspect {

    @NonNull
    private final ObjectMapperConfiguration objectMapper;
    @NonNull
    private final StatusCodeConfig statusCodeConfig;

    private Map<String, String> requestHeaders = new HashMap<>();


    @Pointcut("execution(* com.cellulant.*.*.services.*.*(..)) &&  within(@org.springframework.stereotype.Service *)")
    private void forServicesLoggable() {
    }

    /**
     * Application logging advice object.
     *
     * @param proceedingJoinPoint the proceeding join point
     * @return the object
     * @throws Throwable the throwable
     */
    @Around("forServicesLoggable()")
    public Object applicationLoggingAdvice(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        long startTime = System.currentTimeMillis();
        String methodName = proceedingJoinPoint.getSignature().toShortString();
        Object[] request = proceedingJoinPoint.getArgs();
        String requestHeaderString = null;

        if (request.length != 0) {
            requestHeaderString = objectMapper.objectMapper().writeValueAsString(request[0]);
            requestHeaders = objectMapper.objectMapper().readValue(requestHeaderString, Map.class);

        }
        String requestString = null;
        if (request.length > 2) {
            requestString = Optional.of(objectMapper.objectMapper().writeValueAsString(request[1])).orElse(null);
        }

        Object value = null;
        try {
            value = proceedingJoinPoint.proceed();

            if (value instanceof Mono<?>) {
                final AtomicReference<Long> startTimeRef = new AtomicReference<>();
                final String finalRequestString = requestString;
                return ((Mono<?>) value).doOnSubscribe(subscription -> startTimeRef.set(System.nanoTime()))
                        .doOnSuccess(o -> {
                            final long timeTaken = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startTimeRef.get());
                            logSuccessResponse(methodName, finalRequestString, o, timeTaken);
                        })
                        .doOnError(e -> {
                            final long timeTaken = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startTimeRef.get());
                            logErrorResponse(methodName, finalRequestString, e, timeTaken);
                        });
            }
        } catch (Throwable e) {
            long timeTaken = System.currentTimeMillis() - startTime;

            logErrorResponse(methodName, requestString, e, timeTaken);


            throw e;
        }
        long timeTaken = System.currentTimeMillis() - startTime;
        logSuccessResponse(methodName, requestString, value, timeTaken);


        return value;
    }

    private void logErrorResponse(String methodName, String requestString, Throwable e, long timeTaken) {
        MDC.put(Constants.METHOD, methodName);
        MDC.put(Constants.SERVICE_TAT, String.valueOf(timeTaken));
        MDC.put(Constants.REQUEST, requestString);
        MDC.put(Constants.EXCEPTION, e.getMessage());
        MDC.remove(Constants.TAT);


        log.error("Failure Processing Request  {} {} {} {} {} ",
                keyValue(Constants.MSISDN, requestHeaders.get(Constants.MSISDN)),
                keyValue(Constants.SERVICE, requestHeaders.get(Constants.SERVICE)),
                keyValue(Constants.ACTION, requestHeaders.get(Constants.ACTION)),
                keyValue(Constants.SERVICE_TAT, timeTaken),
                keyValue(Constants.EXCEPTION, e.getMessage()));
    }

    private void logSuccessResponse(String methodName, String finalRequestString, Object responseObject, long timeTaken) {
        ResponseArray response = null;
        ResponseArrayResultDTO responseArrayResultDTO = null;
        String refID = null;
        String statusCode = null;
        String statusMessage = null;

        String objectAsString = null;


        try {

            objectAsString = objectMapper.objectMapper().writeValueAsString(responseObject);
            response = (ResponseArray) responseObject;
            responseArrayResultDTO = objectMapper
                    .objectMapper()
                    .readValue(objectMapper.objectMapper().writeValueAsString(response.getResult()), ResponseArrayResultDTO.class);
            refID = responseArrayResultDTO.getReferenceID();
            statusCode = String.valueOf(response.getStatusCode());
            statusMessage = String.valueOf(response.getStatusMessage());


        } catch (Exception ex) {

            refID = null;
            statusCode = null;
            statusMessage = null;

        }
        MDC.put(Constants.MSISDN, requestHeaders.get(Constants.MSISDN));
        MDC.put(Constants.SERVICE, requestHeaders.get(Constants.SERVICE));
        MDC.put(Constants.ACTION, requestHeaders.get(Constants.ACTION));
        MDC.put(Constants.AFFILIATE, requestHeaders.get(Constants.AFFILIATE));
        MDC.put(Constants.APP_VERSION, requestHeaders.get(Constants.APP_VERSION));
        MDC.put(Constants.DEVICE_TYPE, requestHeaders.get(Constants.DEVICE_TYPE));
        MDC.put(Constants.DEVICE_MODEL, requestHeaders.get(Constants.DEVICE_MODEL));
        MDC.put(Constants.CHANNEL, requestHeaders.get(Constants.CHANNEL));
        MDC.put(Constants.METHOD, methodName);
        MDC.put(Constants.STATUS_CODE, statusCode);
        MDC.put(Constants.STATUS_MESSAGE, statusMessage);
        MDC.put(Constants.SERVICE_TAT, String.valueOf(timeTaken));
        MDC.put(Constants.REQUEST, finalRequestString);
        MDC.put(Constants.REFERENCE_ID, refID);
        MDC.put(Constants.RESPONSE, objectAsString);
        MDC.remove(Constants.TAT);


        log.info("Successfully Processed Request {} {} {} {} ",
                keyValue(Constants.REQUEST, finalRequestString),
                keyValue(Constants.METHOD, methodName),
                keyValue(Constants.RESPONSE, objectAsString),
                keyValue(Constants.SERVICE_TAT, timeTaken));
    }

    /**
     * Exception logging advice.
     *
     * @param joinPoint the join point
     * @param ex        the ex
     * @throws Throwable the throwable
     */
    @AfterThrowing(pointcut = "forServicesLoggable()", throwing = "ex")
    public void exceptionLoggingAdvice(JoinPoint joinPoint, Exception ex) throws Throwable {
        long startTime = System.currentTimeMillis();
        String methodName = joinPoint.getSignature().toShortString();
        Object[] request = joinPoint.getArgs();
        String requestHeaderString = null;

        if (request.length != 0) {
            requestHeaderString = objectMapper.objectMapper().writeValueAsString(request[0]);
            requestHeaders = objectMapper.objectMapper().readValue(requestHeaderString, Map.class);
        }
        String requestString = null;
        if (request.length > 2) {
            requestString = Optional.of(objectMapper.objectMapper().writeValueAsString(request[1])).orElse(null);
        }


        Class<?> exceptionClassType = ex.getClass();
        Object object = exceptionClassType.cast(ex);
        long timeTaken = System.currentTimeMillis() - startTime;

        MDC.put(Constants.MSISDN, requestHeaders.get(Constants.MSISDN));
        MDC.put(Constants.SERVICE, requestHeaders.get(Constants.SERVICE));
        MDC.put(Constants.ACTION, requestHeaders.get(Constants.ACTION));
        MDC.put(Constants.AFFILIATE, requestHeaders.get(Constants.AFFILIATE));
        MDC.put(Constants.APP_VERSION, requestHeaders.get(Constants.APP_VERSION));
        MDC.put(Constants.DEVICE_TYPE, requestHeaders.get(Constants.DEVICE_TYPE));
        MDC.put(Constants.DEVICE_MODEL, requestHeaders.get(Constants.DEVICE_MODEL));
        MDC.put(Constants.CHANNEL, requestHeaders.get(Constants.CHANNEL));
        MDC.put(Constants.METHOD, methodName);
        MDC.put(Constants.SERVICE_TAT, String.valueOf(timeTaken));
        MDC.put(Constants.REQUEST, requestString);
        MDC.put(Constants.RESPONSE, objectMapper.objectMapper().writeValueAsString(object));
        MDC.put(Constants.STATUS_CODE, String.valueOf(statusCodeConfig.getGatewayFailureStatusCode()));


        log.error("Failure Processing Request  {} {} {} {} {} ",
                keyValue(Constants.MSISDN, requestHeaders.get(Constants.MSISDN)),
                keyValue(Constants.METHOD, methodName),
                keyValue(Constants.ACTION, requestHeaders.get(Constants.ACTION)),
                keyValue(Constants.SERVICE_TAT, timeTaken),
                keyValue(Constants.EXCEPTION, ex.toString()));

    }


}


