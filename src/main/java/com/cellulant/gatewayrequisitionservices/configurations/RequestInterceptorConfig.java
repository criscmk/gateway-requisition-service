package com.cellulant.gatewayrequisitionservices.configurations;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;
import java.util.Map;

/**
 * Created by sylvester
 * Project transfers-gateway-api
 * User: sylvester
 * Date: 6/25/19
 * Time: 12:12 PM
 */
@Configuration
public class RequestInterceptorConfig implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new RequestInterceptor());
    }

    @Bean
    public LocaleResolver localeResolver() {
        SessionLocaleResolver slr = new SessionLocaleResolver();
        slr.setDefaultLocale(Locale.US);
        return slr;
    }
}

/**
 * The type Request interceptor.
 * This interceptor retrieves language code from request headers
 * Supports internationalization
 */
@Slf4j
class RequestInterceptor implements HandlerInterceptor {


    private Configs configs;


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //Retrieve and parse header string to get language code
        Map<String, String> headers = new ObjectMapper().readValue(request.getHeader(Constants.X_HEADERS), Map.class);
        String languageCode = String.valueOf(headers.get(Constants.LANGUAGE_CODE));
        log.info("Retrieved Language Code :: {}", languageCode);

        if (languageCode == null || languageCode.isEmpty()) {
            languageCode = configs.getDefaultLanguageCode();
        }

        log.info("Retrieved Language Code :: {}", languageCode);

        //Set Locale to the language code extracted.
        LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);
        if (null != localeResolver) {
            localeResolver.setLocale(request, response, new Locale(languageCode));
        }

        //Set the language code extracted to a context holder so that it can be globally available
        RequestAttributes requestAttributes = RequestContextHolder.currentRequestAttributes();
        requestAttributes.setAttribute(Constants.LANGUAGE_CODE, languageCode, 0);
        requestAttributes.setAttribute(Constants.X_HEADERS, request.getHeader(Constants.X_HEADERS), 0);

        return true;
    }
}
