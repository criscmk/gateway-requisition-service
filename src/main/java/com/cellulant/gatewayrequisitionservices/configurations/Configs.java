package com.cellulant.gatewayrequisitionservices.configurations;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by sylvester
 * Project requisition-gateway-api
 * User: sylvester
 * Date: 4/2/19
 * Time: 7:40 PM
 */
@RefreshScope
@Component
@Configuration
@ConfigurationProperties("gateway.api")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Configs {

    @Value("${gateway.api.default.language.code}")
    public String defaultLanguageCode;
    @Value("${gateway.api.languageCodes}")
    public List languageCodes;
    private String applicationName;
    private String springApplicationName;
    private Integer failureStatusCode;
    private Integer successStatusCode;
    private String failureStatusMessage;
    private String successStatusMessage;
    private String username;
    private String password;
    private int walletSyncSuccessCode;
    private int walletAsyncSuccessCode;
    private int walletFailureCode;
    @Value("${gateway.api.walletNoRecordFoundStatusCode}")
    private int walletNoRecordFoundStatusCode;
    private String msisdnValidationUrl;
    private int validMsisdnStatusCode;

    @Value("${token.jwt.secret}")
    private String jwtSecret;

    @Value("${token.jwt.ttl}")
    private String jwtExpiration;

    @Value("${gateway.api.authenticationPassword}")
    private String authenticationPassword;

    @Value("${gateway.api.authenticationUsername}")
    private String authenticationUsername;

    @Value("${gateway.api.authenticationService}")
    private String authenticationService;

    @Value("${gateway.api.authenticationSuccessStatusCode}")
    private int authenticationSuccessStatusCode;

    @Value("${gateway.api.restTemplateRequestTimeout}")
    private int restTemplateRequestTimeout;
    @Value("${gateway.api.restTemplateSocketTimeout}")
    private int restTemplateSocketTimeout;
    @Value("${gateway.api.restTemplateConnectTimeout}")
    private int restTemplateConnectTimeout;
    @Value("${gateway.api.affiliateCode}")
    private String affiliateCode;
    @Value("${gateway.api.maxConnectionPoolSize}")
    private int maxConnectionPoolSize;
    @Value("${gateway.api.defaultMaxPerRouteConnectionPoolSize}")
    private int defaultMaxPerRouteConnectionPoolSize;

    private String queueNamePrefix;
    private String requestLoggerQueue;
    private String
            syncCoreQueue;
    private String exchangeName;
    private int failedStatusType;
    @Value("${gateway.api.confirmTimeout}")
    private int confirmTimeout;
    private int failedStatusCode;
    private int successStatusType;

    @JsonProperty("asyncMessages")
    private Map<String, String> asyncMessages = new HashMap<>();
    @JsonProperty("defaultSuccessMessage")
    private String defaultSuccessMessage;

    @JsonProperty("defaultSuccessMessage")
    private String jwtApiKey;

    @JsonProperty("jwtApiOrgUnitId")
    private String jwtApiOrgUnitId;

    @JsonProperty("jwtApiIndentifier")
    private String jwtApiIndentifier;

    @JsonProperty("jwtApiExpiration")
    private int jwtApiExpiration;

    @JsonProperty("hardTokenMockEnabled")
    private String hardTokenMockEnabled;

    @JsonProperty("hardTokenCheckPendingRequest")
    private String hardTokenCheckPendingRequest;

    @JsonProperty("updateBeneficiaryUrl}")
    private String updateBeneficiaryUrl;
}
