package com.cellulant.gatewayrequisitionservices.configurations;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Created by kamauwamatu
 * Project gateway
 * User: kamauwamatu
 * Date: 2019-05-07
 * Time: 18:55
 */
@Configuration
@Data
@PropertySource("classpath:statusCodes.properties")
public class StatusCodeConfig {

    @Value("${response.status.code.gatewaySyncSuccessCode}")
    private int gatewaySyncSuccessStatusCode;
    @Value("${response.status.code.gatewayAsyncSuccessCode}")
    private int gatewayAsyncSuccessStatusCode;
    @Value("${response.status.code.gatewayNoRecordFoundSuccessStatusCode}")
    private int gatewayNoRecordFoundSuccessStatusCode;
    @Value("${response.status.code.gatewayFailureStatusCode}")
    private int gatewayFailureStatusCode;
    @Value("${response.status.code.channelHeaderMissingStatusCode}")
    private int channelHeaderMissingStatusCode;
    @Value("${response.status.code.channelMappingNotFoundStatusCode}")
    private int channelMappingNotFoundStatusCode;
    @Value("${response.status.code.channelNotFoundStatusCode}")
    private int channelNotFoundStatusCode;
    @Value("${response.status.code.deviceIdentifierHeaderNotFoundStatusCode}")
    private int deviceIdentifierHeaderNotFoundStatusCode;
    @Value("${response.status.code.serviceNotFoundStatusCode}")
    private int serviceNotFoundStatusCode;
    @Value("${response.status.code.invalidUrlStatusCode}")
    private int invalidUrlStatusCode;
    @Value("${response.status.code.minimumAmountStatusCode}")
    private int minimumAmountStatusCode;
    @Value("${response.status.code.maximumAmountStatusCode}")
    private int maximumAmountStatusCode;
    @Value("${response.status.code.exceptionStatusCode}")
    private int exceptionStatusCode;
    @Value("${response.status.code.invalidMsisdnStatusCode}")
    private int invalidMsisdnStatusCode;

    @Value("${response.status.code.missingActionHeaderParamExceptionStatusCode}")
    private int missingActionHeaderParamExceptionStatusCode;

    @Value("${response.status.code.actionNotConfiguredExceptionStatusCode}")
    private int actionNotConfiguredExceptionStatusCode;
    @Value("${response.status.code.invalidUuidExceptionStatusCode}")
    private int invalidUuidExceptionStatusCode;
    @Value("${response.status.code.duplicateClientRequestIdException}")
    private int duplicateClientRequestIdExceptionStatusCode;


}
