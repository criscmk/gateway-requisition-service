package com.cellulant.gatewayrequisitionservices.configurations;

/**
 * Created by kamauwamatu
 * Project gateway-payment-services
 * User: kamauwamatu
 * Date: 23/08/2019
 * Time: 14:31
 */


public class Constants {

    public static final String MSISDN = "msisdn";
    public static final String UUID = "uuid";
    public static final String SERVICE = "service";
    public static final String NETWORK_ID = "network-id";
    public static final String WALLET_CHANNEL_ID = "wallet-channel-id";
    public static final String ACCESS_POINT = "access-point";
    public static final String DATE_FORMAT = "yyyy-MM-dd  HH:mm:ss";
    public static final String ACTION = "action";
    public static final String LANGUAGE_CODE = "languageCode";
    public static final String CHANNEL_ID = "channel-id";
    public static final String HTTP_METHOD_PUT = "put";
    public static final String CLOUD_REQUEST_ID = "requestID";
    public static final String METHOD = "method";
    public static final String EXCEPTION = "exception";
    public static final String RESPONSE = "response";
    public static final String REQUEST = "request";
    public static final String TAT = "tat";
    public static final String SERVICE_TAT = "serviceTat";
    public static final String UTILITIES_TAT = "utilitiesTat";
    public static final String HTTP_STATUS = "httpStatus";
    public static final String STATUS_CODE = "statusCode";
    public static final String REFERENCE_ID = "referenceID";
    public static final String X_HEADERS = "x-headers";
    public static final String AUTHORIZATION = "Authorization";
    public static final String X_AUTHORIZATION = "x-authorization";
    public static final String ACCEPT = "Accept";
    public static final String AFFILIATE = "affiliateCode";
    public static final String CHANNEL = "channel";
    public static final String ACTIVATION_KEY_HASHMAP = "^ACTIVATION_KEY^";
    public static final String APP_VERSION = "appVersion";
    public static final String DEVICE_TYPE = "deviceType";
    public static final String DEVICE_MODEL = "deviceModel";
    public static final String STATUS_MESSAGE = "statusMessage";
    public static final String STAT_MESSAGE = "downStreamMessage";
    public static final String CLIENT_REQUEST_ID = "clientRequestID";
    public static final String OS_TYPE = "osType";
    public static final String OS_VERSION = "osVersion";
    public static final String EXISTING_NOT_AUTHENTICATED_REQUEST_TYPE = "existingNotAuthenticated";
    public static final int ASYNCHRONOUS_MODE_FLAG = 0;

}
