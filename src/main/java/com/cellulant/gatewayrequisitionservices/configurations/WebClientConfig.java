package com.cellulant.gatewayrequisitionservices.configurations;

import com.cellulant.gatewayrequisitionservices.security.TokenStore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.web.reactive.function.client.WebClientCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.lang.NonNull;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.reactive.function.client.*;
import reactor.core.publisher.Mono;

import java.util.Collections;
import java.util.Locale;

/**
 * @author Denis Gitonga
 */
@Configuration
//@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.REACTIVE)
public class WebClientConfig {

    @Bean
    @Scope("prototype")
    WebClient webClient(final WebClient.Builder builder) {
        return builder.build();
    }

    @Bean
    WebClientCustomizer webClientCustomizer() {
        return new CellulantCustomizer();
    }

    @Slf4j
    static class CellulantCustomizer implements WebClientCustomizer {

        @Override
        public void customize(WebClient.Builder webClientBuilder) {
            webClientBuilder.filter(jwtTokenFilter());
            webClientBuilder.filter(xHeadersFilter());
        }

        private ExchangeFilterFunction xHeadersFilter() {
            return (request, next) -> ReactiveRequestContextHolder.getXHeaders()
                    .switchIfEmpty(Mono.just(Collections.emptyMap()))
                    .flatMap(xHeaders -> {
                        log.debug("Adding headers to WebClient: {}", xHeaders.toString());
                        final ClientRequest clientRequest = ClientRequest.from(request)
                                .headers(headers -> {
                                    if (!xHeaders.isEmpty()) {
                                        try {

                                            final String language = (String) xHeaders.getOrDefault(Constants.LANGUAGE_CODE, Locale.US.getLanguage());
                                            headers.add(Constants.LANGUAGE_CODE, language);

                                            headers.add(Constants.X_HEADERS, new ObjectMapper().writeValueAsString(xHeaders));
                                            headers.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
                                        } catch (JsonProcessingException e) {
                                            log.error("Error setting x-headers");
                                        }
                                    }
                                }).build();

                        return next.exchange(clientRequest);
                    });
        }

        private ExchangeFilterFunction jwtTokenFilter() {
            return (request, next) -> {
                final String jwt = TokenStore.getToken();

                final ClientRequest.Builder builder = ClientRequest.from(request)
                        .header("Accept", MediaType.APPLICATION_JSON_VALUE);
                if (StringUtils.isEmpty(jwt)) return next.exchange(builder.build());

                log.info("The jwt token to send in request: {}", jwt);
                builder.header("Authorization", "Bearer " + jwt);
                return next.exchange(builder.build());
            };
        }
    }
}
