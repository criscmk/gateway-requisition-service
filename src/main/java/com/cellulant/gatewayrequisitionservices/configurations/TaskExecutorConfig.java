package com.cellulant.gatewayrequisitionservices.configurations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ConcurrentTaskExecutor;

import java.util.concurrent.Executors;

/**
 * Created by sylvester
 * Project transfers-gateway-api
 * User: sylvester
 * Date: 6/21/19
 * Time: 11:37 AM
 */
@Configuration
public class TaskExecutorConfig {

    @Bean(name = "concurrentThreadPoolTaskExecutor")
    public ConcurrentTaskExecutor concurrentTaskExecutor() {
        return new ConcurrentTaskExecutor(Executors.newCachedThreadPool());
    }
}
