package com.cellulant.gatewayrequisitionservices.configurations;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.i18n.LocaleContext;
import org.springframework.context.i18n.SimpleLocaleContext;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.lang.NonNull;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.i18n.LocaleContextResolver;

import java.util.Locale;
import java.util.Map;

/**
 * @author Denis Gitonga
 */
@Slf4j
public class CellulantLocaleContextResolver implements LocaleContextResolver {


    private final Configs configs;

    public CellulantLocaleContextResolver(Configs configs) {
        this.configs = configs;
    }

    @NonNull
    @Override
    public LocaleContext resolveLocaleContext(@NonNull ServerWebExchange exchange) {

        final ServerHttpRequest request = exchange.getRequest();

        try {
            // Retrieve and parse header string to get language code
            final String headers = request.getHeaders().getFirst(Constants.X_HEADERS);
            final Map<String, Object> xHeaders = new ObjectMapper()
                    .readValue(headers, new TypeReference<>() {
                    });

            String languageCode = String.valueOf(xHeaders.get(Constants.LANGUAGE_CODE));
            log.info("Retrieved Language Code :: {}", languageCode);

            if (languageCode == null || languageCode.isEmpty()) {
                languageCode = configs.getDefaultLanguageCode();
            }

            log.info("Retrieved Language Code :: {}", languageCode);

            return new SimpleLocaleContext(new Locale(languageCode));
        } catch (Exception e) {
            log.debug("An error occurred getting xHeaders");
        }

        // resolve to default
        return new SimpleLocaleContext(Locale.US);
    }

    @Override
    public void setLocaleContext(ServerWebExchange exchange, LocaleContext localeContext) {
        throw new UnsupportedOperationException(
                "Cannot change HTTP accept header - use a different locale context resolution strategy");
    }
}
