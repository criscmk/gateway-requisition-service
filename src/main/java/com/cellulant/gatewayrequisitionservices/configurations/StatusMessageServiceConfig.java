package com.cellulant.gatewayrequisitionservices.configurations;


import com.cellulant.gatewayrequisitionservices.models.LanguagesEntity;
import com.cellulant.gatewayrequisitionservices.repositories.LanguagesRepository;
import com.cellulant.gatewayrequisitionservices.services.CacheServiceImpl;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;

import java.util.Locale;
import java.util.Optional;

/**
 * Created by kamauwamatu
 * Project api-gateway-core
 * User: kamauwamatu
 * Date: 2019-05-08
 * Time: 11:21
 */
@Service
@AllArgsConstructor
@Slf4j
public class StatusMessageServiceConfig implements MessageSourceAware {

    // The Cache service.
    @NonNull
    private final CacheServiceImpl cacheServiceImpl;

    // The Languages repository.
    @NonNull
    private final LanguagesRepository languagesRepository;

    // Initialize Resource MessageSource
    private MessageSource messageSource;

    @NonNull
    private final Configs configs;

    /**
     * Formulate response message string.
     *
     * @param statusCode the status code
     * @return the string
     */
    public String formulateResponseMessage(int statusCode) {
        // Language Code Interception
        String languageCode = null;
        String responseTemplate =  null;
        // Get Language Code from the context
        try {
            languageCode = RequestContextHolder.getRequestAttributes().getAttribute("languageCode", 0).toString();
        } catch (Exception ex) {
            log.error("Language Code Parameter Not Found in Request Context ::{} ", ex.getMessage());
            languageCode = configs.getDefaultLanguageCode();
        }

        log.info("Language Code {}", languageCode);

        int langId = this.fetchLanguageID(languageCode);
        Optional<String> optionALresponseTemplate = Optional.ofNullable(cacheServiceImpl.fetchResponseTemplate(statusCode, langId));
        // Handle Message
        if (!optionALresponseTemplate.isPresent()) {
            if (configs.getLanguageCodes().contains(languageCode)) {
                responseTemplate = messageSource.getMessage("missing.response.template", new Object[0], new Locale(languageCode));
            } else {
                responseTemplate = messageSource.getMessage("unsupported.language.response.template", new Object[0], new Locale(configs.getDefaultLanguageCode()));
            }
        }else {
            responseTemplate = optionALresponseTemplate.get();
        }

        return responseTemplate;
    }

    /**
     * Fetch language id int.
     *
     * @param langCode the lang code
     * @return the int
     */
    private int fetchLanguageID(String langCode) {

        LanguagesEntity languagesEntity = languagesRepository.findByAbbreviation(langCode);

        if (languagesEntity == null) {
            return 1;
        } else if (languagesEntity.getLanguageID() <= 0 || languagesEntity.getLanguageID() > 4) {
            return 1;
        } else {
            return languagesEntity.getLanguageID();
        }
    }


    @Override
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

}
