package com.cellulant.gatewayrequisitionservices.configurations;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

/**
 * Created by kamauwamatu
 * Project gateway
 * User: kamauwamatu
 * Date: 2019-04-27
 * Time: 13:25
 */
@Configuration
public class LoggerConfig {
    /**
     * Application logger logger.
     *
     * @return the logger
     */
    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public Logger produceLogger() {
        return LoggerFactory.getLogger(this.getClass());
    }


}
