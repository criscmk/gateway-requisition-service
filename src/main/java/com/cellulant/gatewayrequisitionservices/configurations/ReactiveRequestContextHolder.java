package com.cellulant.gatewayrequisitionservices.configurations;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpMessage;
import org.springframework.http.server.reactive.ServerHttpRequest;
import reactor.core.publisher.Mono;

import java.util.Map;

/**
 * @author Denis Gitonga
 */
public class ReactiveRequestContextHolder {
    static final Class<ServerHttpRequest> CONTEXT_KEY = ServerHttpRequest.class;


    /**
     * Gets the x-headers from the bound request
     *
     * @return map of x-headers
     */
    public static Mono<Map<String, Object>> getXHeaders() {
        return getRequest()
                .map(HttpMessage::getHeaders)
                .flatMap(headers -> Mono.fromCallable(() -> {
                    final String xHeaders = headers.getFirst(Constants.X_HEADERS);
                    return new ObjectMapper().readValue(xHeaders, new TypeReference<>() {
                    });
                }));
    }

    /**
     * Gets the language code from the bound request
     *
     * @return the language code
     */
    public static Mono<String> getLanguageCode() {
        return getXHeaders()
                .map(xHeaders -> xHeaders.get(Constants.LANGUAGE_CODE))
                .cast(String.class);
    }

    /**
     * Get the bound request
     *
     * @return the request object
     */
    public static Mono<ServerHttpRequest> getRequest() {
        return Mono.subscriberContext()
                .filter(ctx -> ctx.hasKey(CONTEXT_KEY))
                .map(ctx -> ctx.get(CONTEXT_KEY));
    }
}
