package com.cellulant.gatewayrequisitionservices.configurations.redis;

import com.cellulant.gatewayrequisitionservices.configurations.Configs;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@AllArgsConstructor
public class CachingConfiguration extends CachingConfigurerSupport {

    private Configs propertiesConfig;

    @Bean("responseMessageCustomKeyGenerator")
    @Override
    public KeyGenerator keyGenerator() {
        return new CustomKeyGenerator(propertiesConfig.getAffiliateCode());
    }
}
