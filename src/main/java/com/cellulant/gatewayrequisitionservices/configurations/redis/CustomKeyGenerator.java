package com.cellulant.gatewayrequisitionservices.configurations.redis;

import lombok.NoArgsConstructor;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;

import java.lang.reflect.Method;

@NoArgsConstructor
@Configuration
public class CustomKeyGenerator implements KeyGenerator {

    private String affiliateCode;

    public CustomKeyGenerator(String affiliateCode){
        this.affiliateCode = affiliateCode;
    }

    @Override
    public Object generate(Object o, Method method, Object... objects) {
        return String.format("%s::%s::%s",
                affiliateCode,
                method.getName(),
                StringUtils.arrayToDelimitedString(objects,"_"));
    }
}
