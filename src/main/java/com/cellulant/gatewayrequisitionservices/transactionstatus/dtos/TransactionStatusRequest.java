package com.cellulant.gatewayrequisitionservices.transactionstatus.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransactionStatusRequest {

    @NotBlank(message = "{accountAlias.required}")
    private String accountAlias;
    @NotBlank(message = "{externalRefNo.required}")
    private String externalRefNo;

}
