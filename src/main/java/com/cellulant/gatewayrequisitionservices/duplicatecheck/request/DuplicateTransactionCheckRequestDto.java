package com.cellulant.gatewayrequisitionservices.duplicatecheck.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DuplicateTransactionCheckRequestDto implements Serializable {
    @NotBlank(message = "{accountAlias.required}")
    private String accountAlias;
    @NotNull(message = "{amount.required}")
    private String amount;
    @NotNull(message = "{currency.required}")
    private String currency;
}
