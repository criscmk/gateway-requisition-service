package com.cellulant.gatewayrequisitionservices.repositories;



import com.cellulant.gatewayrequisitionservices.models.StatusCodesEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Mono;

import java.util.Optional;

/**
 * Created by kamauwamatu
 * Project api-gateway-core
 * User: kamauwamatu
 * Date: 2019-05-08
 * Time: 14:29
 */
public interface StatusCodeRepository extends JpaRepository<StatusCodesEntity, Long> {

    /**
     * Find by status code optional.
     *
     * @param statusCode the status code
     * @return the optional
     */
    Optional<StatusCodesEntity> findByStatusCode(int statusCode);


}
