package com.cellulant.gatewayrequisitionservices.repositories;

import com.cellulant.gatewayrequisitionservices.models.RequestLogs;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Created by sylvester
 * Project requisition-gateway-api
 * User: sylvester
 * Date: 4/2/19
 * Time: 7:55 PM
 */
public interface RequestLogsRepository  extends JpaRepository<RequestLogs, Long> {
    Optional<RequestLogs> findByClientRequestID(String clientRequestID);

    Optional<RequestLogs> findByClientRequestIDAndService(String clientRequestID, String service);
}
