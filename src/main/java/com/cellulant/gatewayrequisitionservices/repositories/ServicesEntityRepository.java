package com.cellulant.gatewayrequisitionservices.repositories;

import com.cellulant.gatewayrequisitionservices.models.ServicesEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * The interface Services entity repository.
 */
public interface ServicesEntityRepository extends JpaRepository<ServicesEntity, Long> {
    /**
     * Find by service code services entity.
     *
     * @param serviceCode the service code
     * @return the services entity
     */
    ServicesEntity findByServiceCode(String serviceCode);
}
