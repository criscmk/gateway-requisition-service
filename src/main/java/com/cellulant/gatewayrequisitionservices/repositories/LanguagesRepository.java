package com.cellulant.gatewayrequisitionservices.repositories;

import com.cellulant.gatewayrequisitionservices.models.LanguagesEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by sylvester
 * Project transfers-gateway-api
 * User: sylvester
 * Date: 6/20/19
 * Time: 3:01 PM
 */
public interface LanguagesRepository extends JpaRepository<LanguagesEntity, Long> {
    LanguagesEntity findByAbbreviation(String languageCode);
}
