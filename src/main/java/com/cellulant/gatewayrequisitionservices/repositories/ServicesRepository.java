package com.cellulant.gatewayrequisitionservices.repositories;

import com.cellulant.gatewayrequisitionservices.models.ServicesEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * The interface Services repository.
 */
@Repository
public interface ServicesRepository extends JpaRepository<ServicesEntity, Long> {
    /**
     * Find by service code optional.
     *
     * @param serviceCode the service code
     * @return the optional
     */
//    Optional<ServicesEntity> findByServiceCode(String serviceCode);
    ServicesEntity findByServiceCode(String serviceCode);
}
