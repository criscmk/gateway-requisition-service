package com.cellulant.gatewayrequisitionservices.repositories;

import com.cellulant.gatewayrequisitionservices.models.Products;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by LoiseKinyua
 * Project lookup-gateway-api
 * User: LoiseKinyua
 * Date: 3/25/19
 * Time: 3:15 PM
 */

@Repository
public interface ProductsRepository extends JpaRepository<Products, Long> {
}
