package com.cellulant.gatewayrequisitionservices.repositories;

import com.cellulant.gatewayrequisitionservices.models.ContactInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by LoiseKinyua
 * Project lookup-gateway-api
 * User: LoiseKinyua
 * Date: 3/22/19
 * Time: 12:49 PM
 */

@Repository
public interface ContactsRepository extends JpaRepository<ContactInfo, Long> {
}
