package com.cellulant.gatewayrequisitionservices.repositories;


import com.cellulant.gatewayrequisitionservices.models.ServiceActionsProtocolsEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ServiceActionsProtocolsRepository extends JpaRepository<ServiceActionsProtocolsEntity, Long> {
}