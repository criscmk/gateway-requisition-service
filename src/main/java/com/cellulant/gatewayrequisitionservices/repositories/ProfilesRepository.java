package com.cellulant.gatewayrequisitionservices.repositories;


import com.cellulant.gatewayrequisitionservices.models.ProfilesEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by kamauwamatu
 * Project api-gateway-core
 * User: kamauwamatu
 * Date: 2019-05-08
 * Time: 14:29
 */
public interface ProfilesRepository extends JpaRepository<ProfilesEntity, Long> {

    ProfilesEntity findByMsisdnAndUuid(String msisdn, String uuid);

    ProfilesEntity findByActivationRequestID(int activationRequestID);
}
