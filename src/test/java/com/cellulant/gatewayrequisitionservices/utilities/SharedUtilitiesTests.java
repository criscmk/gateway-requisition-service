package com.cellulant.gatewayrequisitionservices.utilities;

import com.cellulant.gatewayrequisitionservices.configurations.StatusMessageServiceConfig;
import com.cellulant.gatewayrequisitionservices.enaira.requests.ENairaAuthenticateRequest;
import com.cellulant.gatewayrequisitionservices.exceptions.*;
import com.cellulant.gatewayrequisitionservices.models.*;
import com.cellulant.gatewayrequisitionservices.repositories.RequestLogsRepository;
import com.cellulant.gatewayrequisitionservices.utils.SharedUtilities;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataIntegrityViolationException;
import reactor.test.StepVerifier;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyObject;
import static org.mockito.Mockito.when;

@Slf4j
@ExtendWith(MockitoExtension.class)
@AutoConfigureWebTestClient
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SharedUtilitiesTests {
    @Autowired
    private SharedUtilities sharedUtilities;
    @MockBean
    private StatusMessageServiceConfig statusMessageService;
    @MockBean
    private RequestLogsRepository requestLogsRepository;

    private RequestLogs requestLogs;
    private RequestLogs requestLogsWithNoPayload;

    private ENairaAuthenticateRequest eNairaAuthenticateRequest;


    // Set the Actions Entity
    private ActionsEntity actionsEntity = new ActionsEntity(
            1,
            "send-money"
    );
    //Set the Modules Entity
    private ModulesEntity modulesEntity = new ModulesEntity(
            218,
            "Transfers",
            "Transfers Module");

    //Set Service Protocol Entity
    private ServiceActionsProtocolsEntity serviceProtocolsEntity = new ServiceActionsProtocolsEntity(
            1,
            "system-user",
            0,
            0,
            true,
            actionsEntity);

    private static HashMap headerParamMap = new HashMap<>();
    private static HashMap invalidHeaderParamMap = new HashMap<>();

    private static String headerParamsString = "";
    private static String invalidHeaderParamString = "";

    private static String service = "mobile-money";

    private ServiceActionsProtocolsEntity serviceActionsProtocolsEntity = new ServiceActionsProtocolsEntity(
            3232,
            "http://wallet",
            0,
            1,
            true,
            actionsEntity

    );


    List<ServiceActionsProtocolsEntity> serviceActionsProtocolsEntityList = Arrays.asList(serviceActionsProtocolsEntity);
    //Set Service Entity
    private ServicesEntity servicesEntity = new ServicesEntity(
            664,
            "Enquiries",
            "mobile-money",
            service,
            1,
            218,
            (byte) 0,
            10,
            10000,
            (byte) 1,
            false,
            serviceActionsProtocolsEntityList,
            modulesEntity,
            1);

    @BeforeEach
    void setup() throws Exception {
        // Formulate headers
        headerParamMap.put("channel", "ussd");
        headerParamMap.put("wallet-channel-id", "1");
        headerParamMap.put("channel-id", "1");
        headerParamMap.put("service", service);
        headerParamMap.put("languageCode", "en");
        headerParamMap.put("action", "send-money");
        headerParamMap.put("msisdn", "233245326845");
        headerParamMap.put("clientRequestID", "54r878878-random");

        headerParamsString = new ObjectMapper().writeValueAsString(headerParamMap);

        eNairaAuthenticateRequest = ENairaAuthenticateRequest.builder()
                .password("test pass")
                .username("test username")
                .accountAlias("test account")
                .build();


        requestLogs = RequestLogs.builder()
                .channelId(1)
                .clientRequestID("4343-9784453")
                .service("merchant")
                .MSISDN("254718532419")
                .walletChannelId(1)
                .payload(new ObjectMapper().writeValueAsString(eNairaAuthenticateRequest))
                .requestLogID("123456972").build();

        requestLogsWithNoPayload = RequestLogs.builder()
                .channelId(1)
                .clientRequestID("4343-9784453")
                .service("merchant")
                .MSISDN("254718532419")
                .walletChannelId(1)
                .requestLogID("123456978").build();

    }

    @Test
    void onTestMinimumAmountValidation() {
        Assertions.assertThrows(MinimumAmountException.class, () -> {
            sharedUtilities.amountMinLimitValidation(servicesEntity, 5);
        });
    }

    @Test
    void onTestMaximumAmountValidation() {
        Assertions.assertThrows(MaximumAmountException.class, () -> {
            sharedUtilities.amountMaxLimitValidation(servicesEntity, 200000000000000000f);
        });
    }

    @Test
    void onTestValidMinimumAmount() throws Exception {
        Assertions.assertTrue(sharedUtilities.amountMinLimitValidation(servicesEntity, 10f));
    }

    @Test
    void onTestValidMaximumAmount() throws Exception {
        Assertions.assertTrue(sharedUtilities.amountMaxLimitValidation(servicesEntity, 500));
    }

    @Test
    void onTestGenerateActivationCode() {
        Assertions.assertEquals(6, sharedUtilities.generateActivationKey().length());
    }


    @Test
    void onTestGenerateRequestId() {
        when(requestLogsRepository.save(anyObject())).thenReturn(requestLogs);
        sharedUtilities.getRequestID(null, "254718532419", headerParamMap)
                .as(StepVerifier::create).consumeNextWith(requestId -> {
                    Assertions.assertEquals("123456972", requestId);
                }).verifyComplete();
    }

    @Test
    void onTestGenerateRequestIdForPostRequestWithPayload() {
        when(requestLogsRepository.save(anyObject())).thenReturn(requestLogs);

        sharedUtilities.getRequestID(eNairaAuthenticateRequest, "254718532419", headerParamMap)
                .as(StepVerifier::create).consumeNextWith(requestId -> {
                    Assertions.assertEquals("123456972", requestId);
                }).verifyComplete();
    }

    @Test
    void onTestHandleDuplicateRequestId() {
        when(requestLogsRepository.save(anyObject())).thenThrow(new DataIntegrityViolationException("Duplicate Client Exception"));
        when(statusMessageService.formulateResponseMessage(anyInt())).thenReturn("Duplicate Client Request Id Exception");

        sharedUtilities.getRequestID(null, "254718532419", headerParamMap).as(StepVerifier::create)
                .expectErrorMatches(throwable -> throwable instanceof DuplicateClientRequestIdException).verify();
    }

    @Test
    void onTestValidateUrlException() {
        try {
            sharedUtilities.validateUrl("test");
        } catch (Exception exception) {
            Assertions.assertTrue(exception instanceof InvalidUrlException);
        }
    }

    @Test
    void testValidUrlValidation() throws Exception {
        Assertions.assertTrue(sharedUtilities.validateUrl("https://www.w3.org/TR/2011/WD-html5-20110525/urls.html"));
    }

    @Test
    void testInvalidUrl() {
        Assertions.assertThrows(InvalidUrlException.class, () -> {
            sharedUtilities.validateUrl("test url");
        });
    }

    @Test
    void testMissingActionHeader() {
        Assertions.assertThrows(MissingActionHeaderParamException.class, () -> {
            sharedUtilities.retrieveActionFromHeaders(invalidHeaderParamMap);
        });
    }

    @Test
    void testValidHeaderParams() {
        Assertions.assertEquals("send-money",headerParamMap.get("action"));

    }
    @Test
    void testActionNotConfigured()
    {
     Assertions.assertThrows(ActionNotConfiguredException.class,()->{
         sharedUtilities.retrieveActionProtocolUrl(servicesEntity,"test");
     });
    }

    @Test
    void testEncryption(){
        Assertions.assertNotNull(sharedUtilities.encrypt("test"));
    }


    @Test
    void testRetrieveActionProtocolUrl() throws Exception {
        Assertions.assertEquals("http://wallet",sharedUtilities.retrieveActionProtocolUrl(servicesEntity,"send-money"));
    }

}
