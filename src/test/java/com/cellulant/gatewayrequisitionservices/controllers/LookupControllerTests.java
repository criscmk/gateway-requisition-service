package com.cellulant.gatewayrequisitionservices.controllers;

import com.cellulant.gatewayrequisitionservices.card.services.OrderRequest;
import com.cellulant.gatewayrequisitionservices.cardfail.dtos.FailedRequest;
import com.cellulant.gatewayrequisitionservices.cards.StepUpJWTRequest;
import com.cellulant.gatewayrequisitionservices.cards.managecards.dtos.FormDataValue;
import com.cellulant.gatewayrequisitionservices.dtos.responses.ResponseArray;
import com.cellulant.gatewayrequisitionservices.dtos.responses.ResponseArrayResultDTO;
import com.cellulant.gatewayrequisitionservices.dtos.walletdtos.WalletResponse;
import com.cellulant.gatewayrequisitionservices.models.ActionsEntity;
import com.cellulant.gatewayrequisitionservices.models.ModulesEntity;
import com.cellulant.gatewayrequisitionservices.models.ServiceActionsProtocolsEntity;
import com.cellulant.gatewayrequisitionservices.models.ServicesEntity;
import com.cellulant.gatewayrequisitionservices.services.ServiceImpl;
import com.cellulant.gatewayrequisitionservices.utils.SharedUtilities;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@AutoConfigureWebTestClient
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class LookupControllerTests {

    public static final String LOCATE_US_BRANCH_LOCATIONS = "/locate-us/branch-locations";
    public static final String LOCATE_US_ATM_LOCATIONS = "/locate-us/atm-locations";
    public static final String LOCATE_US_POS_LOCATIONS = "/locate-us/pos-locations";
    public static final String LOCATE_US_LOCATION_CATEGORIES = "/locate-us/location-categories";
    public static final String COUNTRIES_ACTIVE_COUNTRIES = "/countries/active-countries";
    public static final String IDENTITY_TYPES_FETCH = "/identity-types/fetch";
    public static final String ONBOARDING_IDENTITY_TYPES_FETCH = "/onboarding-identity-types/fetch";
    public static final String DELIVERY_MODES_FETCH = "/delivery-modes/fetch/{affiliate}";
    public static final String PRODUCTS_FETCH = "/products/fetch";
    public static final String CONTACT_INFO_FETCH = "/contact-info/fetch";
    public static final String VALIDATION_VALIDATE_CARD_NUMBER = "/validation/validate-card-number/{cardNumber}";
    public static final String VALIDATION_CREATE_CARD_JWT = "/validation/create-card-jwt";
    public static final String FAILED_REPORT = "/failed/report";
    public static final String REMOTE_CONFIGS_FETCH = "/remote-configs/fetch";
    public static final String BIN_VALIDATE_REQUEST_BIN = "bin-validate/request/{bin}";
    private static final String VALIDATION_CREATE_STEP_UP_CARD_JWT = "/validation/create-card-step-up-jwt";
    public static final String ENAIRA_TRANSFER_MODES = "/enaira/transfer-modes";
    public static final String ENAIRA_DESTINATION_OPTIONS = "/enaira/destination-options";


    private OrderRequest orderRequest;
    private FailedRequest failedRequest;
    private FailedRequest invalidFailedRequest;
    private StepUpJWTRequest stepUpJWTRequest;
    private StepUpJWTRequest invalidStepUpJWTRequest;



    private static HashMap headerParamMap = new HashMap<>();
    private static HashMap invalidHeaderParamMap = new HashMap<>();

    private static HashMap validateCardNumberParam = new HashMap<>();
    private static HashMap invalidValidateCardNumberParam = new HashMap<>();

    private static HashMap deliveryModeAffiliateParam = new HashMap<>();
    private static HashMap invalidDeliveryModeAffiliateParam = new HashMap<>();

    private static String headerParamsString = "";
    private static String invalidHeaderParamString = "";

    private static String service = "manage-cards";
    @MockBean
    SharedUtilities sharedUtilities;
    @MockBean
    private ServiceImpl servicesRepository;

    private WalletResponse walletResponse;
    private ResponseArray responseArray;


    // Token.
    private String tokenHeader = "eyJhbGciOiJIUzI1NiIsInppcCI6IkdaSVAifQ.H4sIAAAAAAAAAM2ZXW-bMBSG_4uv2RQoCUuuRgnaoq1TlKbdRdULl7qJVcCRDeqiqv99BnVa5QRqt8emN0FENs-LOef1x3lEFFdo5o8no2ASTf3QQ6K-QTMk9qIixadaEI481FwWt7Jd5CFSYJrLFu31a_v7OWOFbMVZTgSaXT2igt3WOVnM0SyaeM93v3BBZLekFhUrCF-RDRUVxxVlpexLyopW-zhrbpt-LenFf984q3f_HrFK43WqdFqyB8KfG5xfLNMVevJeCvE7dIgPB590wy-Wc7vwYPSONz-LfxzAw0ihl6ygZfvVnYz8yUjhk1KGaV7IZ7j58icKH2cZqz8ifNoNn6dxsl5cmgtQk3_J2R1tTOIIf9zH_5laZQ868NAvHqnweOMq2iPVa1p0Y_WO0j0Kjgk437KdG3x4DL-mee4GP1bwp3V-v8Z8Qxx9ftXsbyQ_YWWFMzcCxuoA7AgvqBBd4QedeqZ86KnelD_0-H_p5r9xxjFVYGPWM9QQ-N0aLhfpb0N6OD1YcVX0jmbday7oLDBXAJ0H5gqgM8FcAXwumGuwkQ3GKoDzwR-pK8EzwrMtPr4kCt8RB8n3NDncAZnxe3JRY_whFPTkolYMQGjoycZXHQmC35OLr_oRBL8nD51Eod9zDOAoBPweF3AWh34wcCA2G8lBIxHcjNXZ4BSX966MWJdtx4R16bYMWJdvw3x12TaMV5dtw3R12ZYMVxtvy2y1BdgwWm24DZPVhQ-62gXf-hniwUsdZnjwY18zPPyOz1CAje2eydE3dOybFLpg0QbFBmDyWwus0GMfqifvG1yRB7x3UWT0QzXotiTPmSX0tWzxZyfXC_-r5k9_Ac7LI5tIHwAA.R3bk9qK7EFCBgugZ65yqZbz52IHLI0dPpF-ARhElrEU";
    @Autowired
    private WebTestClient webClient;


    // Set the Actions Entity
    private ActionsEntity actionsEntity = new ActionsEntity(
            1,
            "buy"
    );
    //Set the Modules Entity
    private ModulesEntity modulesEntity = new ModulesEntity(
            218,
            "Transfers",
            "Transfers Module"
    );

    //Set Service Protocol Entity
    private ServiceActionsProtocolsEntity serviceProtocolsEntity = new ServiceActionsProtocolsEntity(
            1,
            "system-user",
            0,
            0,
            true,
            actionsEntity
    );

    //Set the service action entity List
    private List<ServiceActionsProtocolsEntity> serviceActionsProtocolsEntityList = new ArrayList<>();

    //Set Service Entity
    private ServicesEntity servicesEntity = new ServicesEntity(
            664,
            "Enquiries",
            "mobile-money",
            service,
            1,
            218,
            (byte) 0,
            10,
            10000,
            (byte) 1,
            false,
            serviceActionsProtocolsEntityList,
            modulesEntity,
            1
    );


    @BeforeEach
    void setup() throws Exception {
        // Formulate headers
        List<FormDataValue> formDataValues = new ArrayList<>();
        headerParamMap.put("channel", "ussd");
        headerParamMap.put("wallet-channel-id", "1");
        headerParamMap.put("channel-id", "1");
        headerParamMap.put("service", service);
        headerParamMap.put("languageCode", "en");
        headerParamMap.put("action", "send-money");
        headerParamMap.put("msisdn", "233245326845");
        headerParamMap.put("uuid", "233245326845");
        headerParamMap.put("action", "test");


        invalidHeaderParamMap.put("channel", "ussd");
        invalidHeaderParamMap.put("wallet-channel-id", "1");
        invalidHeaderParamMap.put("channel-id", "1");
        invalidHeaderParamMap.put("service", service);
        invalidHeaderParamMap.put("languageCode", "en");
        invalidHeaderParamMap.put("msisdn", "233245326845");

        headerParamsString = new ObjectMapper().writeValueAsString(headerParamMap);
        invalidHeaderParamString = new ObjectMapper().writeValueAsString(invalidHeaderParamMap);


        walletResponse = WalletResponse.builder().statusCode(4000).statusMessage("Success")
                .build();


        String message = "async message";
        responseArray = new ResponseArray();
        ResponseArrayResultDTO responseArrayResultDTO = new ResponseArrayResultDTO();
        responseArray.setStatusCode(5000);
        responseArray.setStatusMessage(message);
        responseArrayResultDTO.setMessage(message);
        responseArrayResultDTO.setReferenceID("5000");
        responseArrayResultDTO.setData(null);
        responseArrayResultDTO.setReferenceID("50000");
        responseArray.setResult(responseArrayResultDTO);

        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));


        when(servicesRepository.getServiceDetails(any())).thenReturn(Mono.just(servicesEntity));
        // Return Msisdn Mono.
        when(sharedUtilities.getMsisdn(any(), any(), any())).thenReturn(Mono.just("233558296125"));
        when(sharedUtilities.getWalletUrl(any(), any())).thenReturn(Mono.just("https://test"));
        when(sharedUtilities.msisdnValidation(anyString())).thenReturn(Mono.just("233558296125"));


        // Handle Encryption.
        when(sharedUtilities.encrypt(any())).thenReturn("abcdcdfereerer");
        when(sharedUtilities.getRequestID(any(), any(), any())).thenReturn(Mono.just("12335484188551"));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.processIntegrationResponse(any(), any(), any(), any(), any(), any())).thenReturn(Mono.just(responseArray));
        when(sharedUtilities.getIntegrationLayerResponse(anyString(), anyString(), anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.getIntegrationLayerResponse(anyCollection(), anyString(), anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));



        //QueryParams
        validateCardNumberParam.put("cardNumber", "1424453553");
        invalidValidateCardNumberParam.put("cardNumber", "");

        deliveryModeAffiliateParam.put("affiliate", "ke");
        invalidDeliveryModeAffiliateParam.put("affiliate", "");

        //Dtos
        orderRequest = OrderRequest.builder()
                .amount("12334")
                .currencyCode("eke")
                .orderNumber("sample")
                .build();

        failedRequest = FailedRequest.builder()
                .externalApi("test api")
                .responseCode("5000")
                .responseMessage("success")
                .service("send money")
                .build();

        invalidFailedRequest = FailedRequest.builder()
                .externalApi("test api")
                .responseCode("5000")
                .responseMessage("success")
                .build();

        stepUpJWTRequest = StepUpJWTRequest.builder()
                .acsUrl("sample url")
                .pareq("test")
                .referenceId("refid")
                .transactionId("transid")
                .returnUrl("test url").build();

        invalidStepUpJWTRequest = StepUpJWTRequest.builder()
                .acsUrl("sample url")
                .pareq("test")
                .referenceId("refid")
                .returnUrl("test url").build();

    }

    //    LOCATE_US_BRANCH_LOCATIONS = "/locate-us/branch-locations";
    @Test
    void locateUsBranchLocationSuccessfully() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));

        this.webClient.get()
                .uri(LOCATE_US_ATM_LOCATIONS)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);

    }

    @Test
    void locateUsBranchLocationFailedOnInvalidAuthorizationHeader() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));

        this.webClient.get()
                .uri(LOCATE_US_ATM_LOCATIONS)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();

    }

    //    LOCATE_US_BRANCH_LOCATIONS = "/locate-us/branch-locations";
    @Test
    void locateUsBranchLocationsSuccessfully() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));

        this.webClient.get()
                .uri(LOCATE_US_BRANCH_LOCATIONS)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void locateUsBranchLocationsFailedOnInvalidAuthorizationHeader() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));

        this.webClient.get()
                .uri(LOCATE_US_BRANCH_LOCATIONS)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();

    }

    //    LOCATE_US_POS_LOCATIONS = "/locate-us/pos-locations";
    @Test
    void locateUsPosLocationsSuccessfully() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));

        this.webClient.get()
                .uri(LOCATE_US_POS_LOCATIONS)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void locateUsPosLocationsFailedOnInvalidHeaderParameters() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));

        this.webClient.get()
                .uri(LOCATE_US_POS_LOCATIONS)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();

    }

    //    LOCATE_US_LOCATION_CATEGORIES = "/locate-us/location-categories";
    @Test
    void locateUsLocationCategoriesSuccessfully() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));

        this.webClient.get()
                .uri(LOCATE_US_LOCATION_CATEGORIES)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void locateUsLocationCategoriesFailedOnInvalidAuthorizationHeader() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));

        this.webClient.get()
                .uri(LOCATE_US_LOCATION_CATEGORIES)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();

    }

    //    COUNTRIES_ACTIVE_COUNTRIES = "/countries/active-countries";
    @Test
    void activeCountriesFetchedSuccessfully() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));

        this.webClient.get()
                .uri(COUNTRIES_ACTIVE_COUNTRIES)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void activeCountriesFetchedFailedOnInvalidAuthorizationHeader() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));

        this.webClient.get()
                .uri(COUNTRIES_ACTIVE_COUNTRIES)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();
    }

    //    IDENTITY_TYPES_FETCH = "/identity-types/fetch";
    @Test
    void identityTypesFetchSuccessfully() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));

        this.webClient.get()
                .uri(IDENTITY_TYPES_FETCH)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void identityTypesFetchFailedOnInvalidAuthorizationHeader() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));

        this.webClient.get()
                .uri(IDENTITY_TYPES_FETCH)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();

    }

    //ONBOARDING_IDENTITY_TYPES_FETCH = "/onboarding-identity-types/fetch";
    @Test
    void onboardingIdentityTypesFetchedSuccessfully() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));

        this.webClient.get()
                .uri(ONBOARDING_IDENTITY_TYPES_FETCH)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void onboardingIdentityTypesFetchFailedOnInvalidAuthorizationHeader() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));

        this.webClient.get()
                .uri(ONBOARDING_IDENTITY_TYPES_FETCH)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();

    }

    //    DELIVERY_MODES_FETCH = "/delivery-modes/fetch/{affiliate}";
    @Test
    void deliveryModesFetchedSuccessfully() {
        when(sharedUtilities.returnSuccessResponse(any())).thenReturn(Mono.just(responseArray));
        this.webClient.get()
                .uri(DELIVERY_MODES_FETCH, deliveryModeAffiliateParam)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void deliveryModesFetchFailedOnInvalidQueryParam() {

        when(sharedUtilities.returnSuccessResponse(any())).thenReturn(Mono.just(responseArray));
        this.webClient.get()
                .uri(DELIVERY_MODES_FETCH, invalidDeliveryModeAffiliateParam)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555);
    }

    @Test
    void deliveryModesFetchFailedOnInvalidAuthorizationHeader() {
        when(sharedUtilities.returnSuccessResponse(any())).thenReturn(Mono.just(responseArray));
        this.webClient.get()
                .uri(DELIVERY_MODES_FETCH, invalidDeliveryModeAffiliateParam)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();
    }

    //     PRODUCTS_FETCH = "/products/fetch";
    @Test
    void productsFetchSuccessfully() {
        when(sharedUtilities.returnSuccessResponse(any())).thenReturn(Mono.just(responseArray));

        this.webClient.get()
                .uri(PRODUCTS_FETCH)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void productsFetchFailedOnInvalidAuthorizationHeader() {

        when(sharedUtilities.returnSuccessResponse(any())).thenReturn(Mono.just(responseArray));
        this.webClient.get()
                .uri(PRODUCTS_FETCH)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();
    }

    //    CONTACT_INFO_FETCH = "/contact-info/fetch";
    @Test
    void contactInfoFetchSuccessfully() {
        when(sharedUtilities.returnSuccessResponse(any())).thenReturn(Mono.just(responseArray));
        this.webClient.get()
                .uri(CONTACT_INFO_FETCH)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    //VALIDATION_VALIDATE_CARD_NUMBER = "/validation/validate-card-number/{cardNumber}";
    @Test
    void validationValidateCardNumberSuccessfully() {

        when(sharedUtilities.returnSuccessResponse(any())).thenReturn(Mono.just(responseArray));
        this.webClient.get()
                .uri(VALIDATION_VALIDATE_CARD_NUMBER, validateCardNumberParam)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);

    }

    @Test
    void validationValidateFailedOnInvalidQueryParam() {
        when(sharedUtilities.returnSuccessResponse(any())).thenReturn(Mono.just(responseArray));
        this.webClient.get()
                .uri(VALIDATION_VALIDATE_CARD_NUMBER, invalidValidateCardNumberParam)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555);


    }

    @Test
    void validationValidateCardNumberFailedOnInvalidAuthorizationHeader() {
        when(sharedUtilities.returnSuccessResponse(any())).thenReturn(Mono.just(responseArray));
        this.webClient.get()
                .uri(VALIDATION_VALIDATE_CARD_NUMBER, invalidValidateCardNumberParam)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();

    }

    //    VALIDATION_CREATE_CARD_JWT = "validation/create-card-jwt";

    @Test
    void validationCreateCardJWTSuccessGivenCorrectParameters() {
        when(sharedUtilities.handleJwtResponse(anyString())).thenReturn(responseArray);
        this.webClient.post()
                .uri(VALIDATION_CREATE_CARD_JWT)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(orderRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void validationCreateCardJWTFailedOnInvalidAuthorizationHeader() {
        this.webClient.post()
                .uri(VALIDATION_CREATE_CARD_JWT)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(orderRequest))
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();
    }

    //    FAILED_REPORT = "/failed/report";
    @Test
    void failedReportSuccessfullyGivenCorrectParameters() {
        when(sharedUtilities.getIntegrationLayerResponse(any(FailedRequest.class), anyString(), anyString())).thenReturn(Mono.just(walletResponse));
        this.webClient.post()
                .uri(FAILED_REPORT)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(failedRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);

    }


    @Test
    void failedReportFailedOnInvalidParameters() {

        this.webClient.post()
                .uri(FAILED_REPORT)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidFailedRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555)
                .jsonPath("$.result.data[0]").isEqualTo("{service.required}");

    }

    @Test
    void failedReportFailedOnInvalidAuthorizationHeader() {

        this.webClient.post()
                .uri(FAILED_REPORT)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidFailedRequest))
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();
    }

    //     REMOTE_CONFIGS_FETCH = "/remote-configs/fetch";
    @Test
    void remoteConfigsFetchSuccessfully() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));

        this.webClient.get()
                .uri(REMOTE_CONFIGS_FETCH)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void remoteConfigsFetchFailedOnInvalidAuthorizationHeader() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));

        this.webClient.get()
                .uri(REMOTE_CONFIGS_FETCH)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();
    }

    //   VALIDATION_CREATE_STEP_UP_CARD_JWT = "validation/create-card-step-up-jwt";

    @Test
    void validationCreateStepUpCardJWTSuccessGivenCorrectParameters() {
        when(sharedUtilities.handleJwtResponse(any())).thenReturn(responseArray);
        this.webClient.post()
                .uri(VALIDATION_CREATE_STEP_UP_CARD_JWT)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(stepUpJWTRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void validationCreateStepUpCardJWTFailedOnInvalidParameters() {
        when(sharedUtilities.handleJwtResponse(any())).thenReturn(responseArray);
        this.webClient.post()
                .uri(VALIDATION_CREATE_STEP_UP_CARD_JWT)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidStepUpJWTRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555);
    }

    @Test
    void validationCreateStepUpCardJWTFailedOnInvalidAuthorizationHeader() {
        this.webClient.post()
                .uri(VALIDATION_CREATE_STEP_UP_CARD_JWT)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidStepUpJWTRequest))
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();
    }

    //    ENAIRA_TRANSFER_MODES = "/enaira/transfer-modes";
    @Test
    void enairaTransferModesFetchedSuccessfully() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));

        this.webClient.get()
                .uri(ENAIRA_TRANSFER_MODES)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);

    }

    @Test
    void enairaTransferModesFetchedFailedOnInvalidAuthorizationHeader() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));

        this.webClient.get()
                .uri(ENAIRA_TRANSFER_MODES)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();

    }

    //    ENAIRA_DESTINATION_OPTIONS = "/enaira/destination-options";
    @Test
    void eNairaDestinationOptionsFetchedSuccessfully() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));

        this.webClient.get()
                .uri(ENAIRA_DESTINATION_OPTIONS)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void eNairaDestinationOptionsFetchFailedOnInvalidAuthorizationHeader() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));

        this.webClient.get()
                .uri(ENAIRA_DESTINATION_OPTIONS)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();
    }


}

