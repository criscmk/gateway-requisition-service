package com.cellulant.gatewayrequisitionservices.controllers;

import com.cellulant.gatewayrequisitionservices.balanceenquiry.requests.BalanceEnquiryBulkRequest;
import com.cellulant.gatewayrequisitionservices.balanceenquiry.requests.BalanceEnquiryRequest;
import com.cellulant.gatewayrequisitionservices.cards.managecards.dtos.FormDataValue;
import com.cellulant.gatewayrequisitionservices.dtos.responses.ResponseArray;
import com.cellulant.gatewayrequisitionservices.dtos.responses.ResponseArrayResultDTO;
import com.cellulant.gatewayrequisitionservices.dtos.walletdtos.WalletResponse;
import com.cellulant.gatewayrequisitionservices.enaira.requests.ENairaBalanceEnquiryBulkRequest;
import com.cellulant.gatewayrequisitionservices.enaira.requests.ENairaMiniStatementRequest;
import com.cellulant.gatewayrequisitionservices.forex.requests.ForexRequest;
import com.cellulant.gatewayrequisitionservices.ministatement.requests.MiniStatementRequest;
import com.cellulant.gatewayrequisitionservices.models.ActionsEntity;
import com.cellulant.gatewayrequisitionservices.models.ModulesEntity;
import com.cellulant.gatewayrequisitionservices.models.ServiceActionsProtocolsEntity;
import com.cellulant.gatewayrequisitionservices.models.ServicesEntity;
import com.cellulant.gatewayrequisitionservices.services.ServiceImpl;
import com.cellulant.gatewayrequisitionservices.transactionstatus.dtos.TransactionStatusRequest;
import com.cellulant.gatewayrequisitionservices.utils.SharedUtilities;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@AutoConfigureWebTestClient
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class EnquiriesControllerTests {

    public static final String BALANCE_ENQUIRY_REQUEST = "/balance-enquiry/request";
    public static final String BALANCE_ENQUIRY_MULTIPLE = "/balance-enquiry/multiple";
    public static final String RECENT_ACTIVITY_FETCH = "/recent-activity/fetch";
    public static final String MINI_STATEMENT_REQUEST = "/mini-statement/request";
    public static final String TRANSACTION_STATUS_FETCH = "/transaction-status/fetch";
    public static final String FOREX_RATES = "/forex/rates";
    public static final String ENAIRA_BALANCE_ENQUIRY_MULTIPLE = "/enaira/balance-enquiry";
    public static final String ENAIRA_MINI_STATEMENT_REQUEST = "/enaira/mini-statement";

    private BalanceEnquiryRequest balanceEnquiryRequest;
    private BalanceEnquiryRequest invalidBalanceEnquiryRequest;
    private BalanceEnquiryBulkRequest balanceEnquiryBulkRequest;
    private BalanceEnquiryBulkRequest invalidBalanceEnquiryBulkRequest;
    private MiniStatementRequest miniStatementRequest;
    private MiniStatementRequest invalidMiniStatementRequest;
    private TransactionStatusRequest transactionStatusRequest;
    private TransactionStatusRequest invalidTransactionStatusRequest;
    private ForexRequest forexRequest;
    private ForexRequest invalidForexRequest;
    private ENairaBalanceEnquiryBulkRequest eNairaBalanceEnquiryBulkRequest;
    private ENairaBalanceEnquiryBulkRequest invalidENairaBalanceEnquiryBulkRequest;
    private ENairaMiniStatementRequest eNairaMiniStatementRequest;
    private ENairaMiniStatementRequest invalidENairaMiniStatementRequest;


    private static HashMap headerParamMap = new HashMap<>();
    private static HashMap invalidHeaderParamMap = new HashMap<>();

    private static String headerParamsString = "";
    private static String invalidHeaderParamString = "";

    private static String service = "manage-cards";
    @MockBean
    SharedUtilities sharedUtilities;
    @MockBean
    private ServiceImpl servicesRepository;

    private WalletResponse walletResponse;
    private ResponseArray responseArray;

    // Token.
    private String tokenHeader = "eyJhbGciOiJIUzI1NiIsInppcCI6IkdaSVAifQ.H4sIAAAAAAAAAM2ZXW-bMBSG_4uv2RQoCUuuRgnaoq1TlKbdRdULl7qJVcCRDeqiqv99BnVa5QRqt8emN0FENs-LOef1x3lEFFdo5o8no2ASTf3QQ6K-QTMk9qIixadaEI481FwWt7Jd5CFSYJrLFu31a_v7OWOFbMVZTgSaXT2igt3WOVnM0SyaeM93v3BBZLekFhUrCF-RDRUVxxVlpexLyopW-zhrbpt-LenFf984q3f_HrFK43WqdFqyB8KfG5xfLNMVevJeCvE7dIgPB590wy-Wc7vwYPSONz-LfxzAw0ihl6ygZfvVnYz8yUjhk1KGaV7IZ7j58icKH2cZqz8ifNoNn6dxsl5cmgtQk3_J2R1tTOIIf9zH_5laZQ868NAvHqnweOMq2iPVa1p0Y_WO0j0Kjgk437KdG3x4DL-mee4GP1bwp3V-v8Z8Qxx9ftXsbyQ_YWWFMzcCxuoA7AgvqBBd4QedeqZ86KnelD_0-H_p5r9xxjFVYGPWM9QQ-N0aLhfpb0N6OD1YcVX0jmbday7oLDBXAJ0H5gqgM8FcAXwumGuwkQ3GKoDzwR-pK8EzwrMtPr4kCt8RB8n3NDncAZnxe3JRY_whFPTkolYMQGjoycZXHQmC35OLr_oRBL8nD51Eod9zDOAoBPweF3AWh34wcCA2G8lBIxHcjNXZ4BSX966MWJdtx4R16bYMWJdvw3x12TaMV5dtw3R12ZYMVxtvy2y1BdgwWm24DZPVhQ-62gXf-hniwUsdZnjwY18zPPyOz1CAje2eydE3dOybFLpg0QbFBmDyWwus0GMfqifvG1yRB7x3UWT0QzXotiTPmSX0tWzxZyfXC_-r5k9_Ac7LI5tIHwAA.R3bk9qK7EFCBgugZ65yqZbz52IHLI0dPpF-ARhElrEU";
    @Autowired
    private WebTestClient webClient;

    // Set the Actions Entity
    private ActionsEntity actionsEntity = new ActionsEntity(
            1,
            "buy"
    );
    //Set the Modules Entity
    private ModulesEntity modulesEntity = new ModulesEntity(
            218,
            "Transfers",
            "Transfers Module"
    );

    //Set Service Protocol Entity
    private ServiceActionsProtocolsEntity serviceProtocolsEntity = new ServiceActionsProtocolsEntity(
            1,
            "system-user",
            0,
            0,
            true,
            actionsEntity
    );

    //Set the service action entity List
    private List<ServiceActionsProtocolsEntity> serviceActionsProtocolsEntityList = new ArrayList<>();

    //Set Service Entity
    private ServicesEntity servicesEntity = new ServicesEntity(
            664,
            "Enquiries",
            "mobile-money",
            service,
            1,
            218,
            (byte) 0,
            10,
            10000,
            (byte) 1,
            false,
            serviceActionsProtocolsEntityList,
            modulesEntity,
            1
    );

    @BeforeEach
    void setup() throws Exception {
        // Formulate headers
        List<FormDataValue> formDataValues = new ArrayList<>();
        headerParamMap.put("channel", "ussd");
        headerParamMap.put("wallet-channel-id", "1");
        headerParamMap.put("channel-id", "1");
        headerParamMap.put("service", service);
        headerParamMap.put("languageCode", "en");
        headerParamMap.put("action", "send-money");
        headerParamMap.put("msisdn", "233245326845");
        headerParamMap.put("uuid", "233245326845");
        headerParamMap.put("action", "test");


        invalidHeaderParamMap.put("channel", "ussd");
        invalidHeaderParamMap.put("wallet-channel-id", "1");
        invalidHeaderParamMap.put("channel-id", "1");
        invalidHeaderParamMap.put("service", service);
        invalidHeaderParamMap.put("languageCode", "en");
        invalidHeaderParamMap.put("msisdn", "233245326845");

        headerParamsString = new ObjectMapper().writeValueAsString(headerParamMap);
        invalidHeaderParamString = new ObjectMapper().writeValueAsString(invalidHeaderParamMap);


        walletResponse = WalletResponse.builder().statusCode(4000).statusMessage("Success")
                .build();


        String message = "async message";
        responseArray = new ResponseArray();
        ResponseArrayResultDTO responseArrayResultDTO = new ResponseArrayResultDTO();
        responseArray.setStatusCode(5000);
        responseArray.setStatusMessage(message);
        responseArrayResultDTO.setMessage(message);
        responseArrayResultDTO.setReferenceID("5000");
        responseArrayResultDTO.setData(null);
        responseArrayResultDTO.setReferenceID("50000");
        responseArray.setResult(responseArrayResultDTO);

        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));


        when(servicesRepository.getServiceDetails(any())).thenReturn(Mono.just(servicesEntity));
        // Return Msisdn Mono.
        when(sharedUtilities.getMsisdn(any(), any(), any())).thenReturn(Mono.just("233558296125"));
        when(sharedUtilities.getWalletUrl(any(), any())).thenReturn(Mono.just("https://test"));
        when(sharedUtilities.msisdnValidation(anyString())).thenReturn(Mono.just("233558296125"));


        // Handle Encryption.
        when(sharedUtilities.encrypt(any())).thenReturn("abcdcdfereerer");
        when(sharedUtilities.getRequestID(any(), any(), any())).thenReturn(Mono.just("12335484188551"));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.processIntegrationResponse(any(), any(), any(), any(), any(), any())).thenReturn(Mono.just(responseArray));
        when(sharedUtilities.getIntegrationLayerResponse(anyString(), anyString(), anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));

        //DTOs
        balanceEnquiryRequest = BalanceEnquiryRequest.builder()
                .accountAlias("Test Account")
                .pin("080808").build();

        invalidBalanceEnquiryRequest = BalanceEnquiryRequest.builder()
                .pin("080808").build();

        balanceEnquiryBulkRequest = BalanceEnquiryBulkRequest.builder()
                .pin("080808")
                .accountAlias("Test Account")
                .accountNumbers("234588838383")
                .build();

        invalidBalanceEnquiryBulkRequest = BalanceEnquiryBulkRequest.builder()
                .accountAlias("Test Account")
                .accountNumbers("234588838383")
                .build();

        miniStatementRequest = MiniStatementRequest.builder()
                .pin("080808")
                .accountAlias("test account")
                .fromDate("1656547200000")
                .toDate("1456547200000")
                .build();

        invalidMiniStatementRequest = MiniStatementRequest.builder()
                .accountAlias("test account")
                .fromDate("1656547200000")
                .toDate("1456547200000")
                .build();

        transactionStatusRequest = TransactionStatusRequest.builder()
                .accountAlias("test account")
                .externalRefNo("test ref")
                .build();

        invalidTransactionStatusRequest = TransactionStatusRequest.builder()
                .accountAlias("test account")
                .build();

        forexRequest = ForexRequest.builder()
                .baseCurrency("test")
                .quoteCurrency("test").build();

        invalidForexRequest = ForexRequest.builder()
                .quoteCurrency("test").build();

        eNairaBalanceEnquiryBulkRequest = ENairaBalanceEnquiryBulkRequest.builder()
                .accountAlias("test account")
                .authorizationToken("test token").build();

        invalidENairaBalanceEnquiryBulkRequest = ENairaBalanceEnquiryBulkRequest.builder()
                .accountAlias("test account")
                .build();

        eNairaMiniStatementRequest = ENairaMiniStatementRequest.builder()
                .accountAlias("test account")
                .authorizationToken("test authorization token").build();

        invalidENairaMiniStatementRequest = ENairaMiniStatementRequest.builder()
                .authorizationToken("test authorization token").build();


    }

    //    BALANCE_ENQUIRY_REQUEST = "/balance-enquiry/request";
    @Test
    void balanceEnquirySuccessfullyGivenCorrectParameters() {
        this.webClient.post()
                .uri(BALANCE_ENQUIRY_REQUEST)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(balanceEnquiryRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void balanceEnquiryFailedOnInvalidParameters() {
        this.webClient.post()
                .uri(BALANCE_ENQUIRY_REQUEST)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidBalanceEnquiryRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555)
                .jsonPath("$.result.data[0]").isEqualTo("Account Required");

    }


    @Test
    void balanceEnquiryFailedOnInvalidAuthorizationHeader() {
        this.webClient.post()
                .uri(BALANCE_ENQUIRY_REQUEST)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidBalanceEnquiryRequest))
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();
    }

    //    BALANCE_ENQUIRY_MULTIPLE = "/balance-enquiry/multiple";
    @Test
    void balanceEnquiryMultipleSuccessfullyGivenCorrectParams() {
        this.webClient.post()
                .uri(BALANCE_ENQUIRY_MULTIPLE)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(balanceEnquiryBulkRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void balanceEnquiryMultipleFailedGivenInvalidParams() {
        this.webClient.post()
                .uri(BALANCE_ENQUIRY_MULTIPLE)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidBalanceEnquiryBulkRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555)
                .jsonPath("$.result.data[0]").isEqualTo("pin is a required field");

    }

    @Test
    void balanceEnquiryMultipleFailedOnInvalidAuthorizationHeader() {
        this.webClient.post()
                .uri(BALANCE_ENQUIRY_MULTIPLE)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(balanceEnquiryBulkRequest))
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();

    }

    //RECENT_ACTIVITY_FETCH = "/recent-activity/fetch";
    @Test
    void fetchRecentActivitySuccessfullyGivenCorrectParameters() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString(), anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));

        this.webClient.get()
                .uri(RECENT_ACTIVITY_FETCH)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void fetchRecentActivityFailedOnInvalidHeaderParams() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString(), anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));

        this.webClient.get()
                .uri(RECENT_ACTIVITY_FETCH)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().isUnauthorized()
                .expectBody();

    }

    //    MINI_STATEMENT_REQUEST = "/mini-statement/request";
    @Test
    void getMiniStatementSuccessfullyGivenCorrectParameters() {
        this.webClient.post()
                .uri(MINI_STATEMENT_REQUEST)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(miniStatementRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);

    }

    @Test
    void getMiniStatementFailedOnInvalidParameters() {
        this.webClient.post()
                .uri(MINI_STATEMENT_REQUEST)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidMiniStatementRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555)
                .jsonPath("$.result.data[0]").isEqualTo("pin is a required field");
    }

    @Test
    void getMiniStatementFailedOnInvalidAuthorizationHeader() {
        this.webClient.post()
                .uri(MINI_STATEMENT_REQUEST)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidMiniStatementRequest))
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();
    }

    //TRANSACTION_STATUS_FETCH = "/transaction-status/fetch";
    @Test
    void transactionStatusFetchSuccessfullyGivenCorrectParameters() {

        this.webClient.post()
                .uri(TRANSACTION_STATUS_FETCH)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(transactionStatusRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void transactionStatusFetchFailedOnInvalidParameters() {

        this.webClient.post()
                .uri(TRANSACTION_STATUS_FETCH)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidTransactionStatusRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555)
                .jsonPath("$.result.data[0]").isEqualTo("{externalRefNo.required}");

    }

    @Test
    void transactionStatusFetchFailedOnInvalidAuthorizationHeader() {

        this.webClient.post()
                .uri(TRANSACTION_STATUS_FETCH)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidTransactionStatusRequest))
                .exchange()
                .expectStatus().isUnauthorized()
                .expectBody();
    }

    //    FOREX_RATES = "/forex/rates";
    @Test
    void fetchForexRatesSuccessfullyGivenCorrectParameters() {

        this.webClient.post()
                .uri(FOREX_RATES)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(forexRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void fetchForexRatesFailedOnInvalidParameters() {

        this.webClient.post()
                .uri(FOREX_RATES)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidForexRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555)
                .jsonPath("$.result.data[0]").isEqualTo("{baseCurrency.required}");

    }

    @Test
    void fetchForexRatesFailedOnInvalidAuthorizationHeader() {

        this.webClient.post()
                .uri(FOREX_RATES)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidForexRequest))
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();

    }

    //    ENAIRA_BALANCE_ENQUIRY_MULTIPLE = "/enaira/balance-enquiry";
    @Test
    void enairaBalanceEnquirySuccessfullyGivenCorrectParameters() {

        this.webClient.post()
                .uri(ENAIRA_BALANCE_ENQUIRY_MULTIPLE)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(eNairaBalanceEnquiryBulkRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void enairaBalanceEnquiryFailedOnInvalidParameters() {

        this.webClient.post()
                .uri(ENAIRA_BALANCE_ENQUIRY_MULTIPLE)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidENairaBalanceEnquiryBulkRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555)
                .jsonPath("$.result.data[0]").isEqualTo("{authorizationToken.required}");

    }

    @Test
    void enairaBalanceEnquiryFailedOnInvalidAuthorizationHeader() {
        this.webClient.post()
                .uri(ENAIRA_BALANCE_ENQUIRY_MULTIPLE)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidENairaBalanceEnquiryBulkRequest))
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();
    }

    //    ENAIRA_MINI_STATEMENT_REQUEST = "/enaira/mini-statement";
    @Test
    void EnairaMiniStatementRequestFetchedSuccessfullyGivenCorrectParameters() {
        this.webClient.post()
                .uri(ENAIRA_MINI_STATEMENT_REQUEST)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(eNairaMiniStatementRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);

    }

    @Test
    void EnairaMiniStatementRequestFailedOnInvalidParameters() {
        this.webClient.post()
                .uri(ENAIRA_MINI_STATEMENT_REQUEST)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidENairaMiniStatementRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555)
                .jsonPath("$.result.data[0]").isEqualTo("Account Required");


    }

    @Test
    void EnairaMiniStatementRequestFailedOnInvalidAuthorizationHeader() {
        this.webClient.post()
                .uri(ENAIRA_MINI_STATEMENT_REQUEST)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidENairaMiniStatementRequest))
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();



    }

}
