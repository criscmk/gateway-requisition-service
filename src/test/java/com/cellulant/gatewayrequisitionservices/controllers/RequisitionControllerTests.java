package com.cellulant.gatewayrequisitionservices.controllers;

import com.cellulant.gatewayrequisitionservices.cards.managecards.dtos.FormDataValue;
import com.cellulant.gatewayrequisitionservices.chequeservices.dtos.ConfirmChequeRequest;
import com.cellulant.gatewayrequisitionservices.chequeservices.dtos.RequestChequeBookRequest;
import com.cellulant.gatewayrequisitionservices.chequeservices.dtos.StopChequeBookRequest;
import com.cellulant.gatewayrequisitionservices.customerfeedback.requests.CustomerFeedBackRequest;
import com.cellulant.gatewayrequisitionservices.dtos.responses.ResponseArray;
import com.cellulant.gatewayrequisitionservices.dtos.responses.ResponseArrayResultDTO;
import com.cellulant.gatewayrequisitionservices.dtos.walletdtos.WalletResponse;
import com.cellulant.gatewayrequisitionservices.enaira.requests.ENairaAuthenticateRequest;
import com.cellulant.gatewayrequisitionservices.enaira.requests.ENairaNameInquiryRequest;
import com.cellulant.gatewayrequisitionservices.enaira.requests.ENairaPreAuthorizationRequest;
import com.cellulant.gatewayrequisitionservices.fullstatement.requests.FullStatementRequest;
import com.cellulant.gatewayrequisitionservices.hardtoken.requests.*;
import com.cellulant.gatewayrequisitionservices.internationaltransfers.request.InternationalTransferChargesRequestDto;
import com.cellulant.gatewayrequisitionservices.internationaltransfers.services.InternationalTransferChargesService;
import com.cellulant.gatewayrequisitionservices.loanterms.requests.LoanTermsRequest;
import com.cellulant.gatewayrequisitionservices.models.ActionsEntity;
import com.cellulant.gatewayrequisitionservices.models.ModulesEntity;
import com.cellulant.gatewayrequisitionservices.models.ServiceActionsProtocolsEntity;
import com.cellulant.gatewayrequisitionservices.models.ServicesEntity;
import com.cellulant.gatewayrequisitionservices.services.ServiceImpl;
import com.cellulant.gatewayrequisitionservices.termdeposit.requests.TermDepositRequest;
import com.cellulant.gatewayrequisitionservices.utils.SharedUtilities;
import com.cellulant.gatewayrequisitionservices.xpresscashtoken.request.XpressCashTokenChargesRequestDto;
import com.cellulant.gatewayrequisitionservices.xpresscashtoken.services.XpressCashTokenChargesService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@AutoConfigureWebTestClient
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RequisitionControllerTests {
    public static final String CHEQUE_SERVICES_STOP = "/cheque-services/stop";
    public static final String CHEQUE_SERVICES_CONFIRM = "/cheque-services/confirm";
    public static final String CHEQUE_SERVICES_BOOK_LEAVES = "/cheque-services/book-leaves";
    public static final String CHEQUE_SERVICES_REQUEST = "/cheque-services/request";
    public static final String FULL_STATEMENT_REQUEST = "/full-statement/request";
    public static final String TERM_DEPOSIT_REQUEST = "/term-deposit/request";
    public static final String LOAN_INFORMATION_REQUEST = "/loan-information/request";
    public static final String CHEQUE_SERVICES_BRANCHES_FETCH = "/cheque-services/branches-fetch";
    public static final String HARD_TOKEN_REQUEST_TYPES = "/hard-token/request-types";
    public static final String HARD_TOKEN_DELIVERY_MODES = "/hard-token/delivery-modes";
    public static final String HARD_TOKEN_BRANCHES = "/hard-token/branches";
    public static final String HARD_TOKEN_CHARGES = "/hard-token/charges";
    public static final String HARD_TOKEN_REQUEST = "/hard-token/request";
    public static final String HARD_TOKEN_ACTIVATE = "/hard-token/activate";
    public static final String HARD_TOKEN_VALIDATE = "/hard-token/validate-token";
    public static final String HARD_TOKEN_CHECK_PENDING_REQUEST = "/hard-token/check-pending-request";
    public static final String ENAIRA_AUTHENTICATE_REQUEST = "/enaira/authenticate";

    public static final String ENAIRA_PRE_AUTHORIZATION = "/enaira/preauthorization";
    public static final String ENAIRA_NAME_INQUIRY = "/enaira/name-inquiry";
    public static final String CUSTOMERS_ACTIVE_BANNER = "/customers/active-banner";
    public static final String CUSTOMER_FEEDBACK_CREATE = "/customer-feedback/create";
    public static final String XPRESS_CASH_TOKEN_CHARGES = "/xpress-cash-token-account/charges";
    public static final String INTERNATIONAL_TRANSFER_CHARGES = "/international-transfer/charges";

    private StopChequeBookRequest stopChequeBookRequest;
    private StopChequeBookRequest invalidStopChequeBookRequest;
    private ConfirmChequeRequest confirmChequeRequest;
    private ConfirmChequeRequest invalidConfirmChequeRequest;
    private RequestChequeBookRequest requestChequeBookRequest;
    private RequestChequeBookRequest invalidRequestChequeBookRequest;
    private FullStatementRequest fullStatementRequest;
    private FullStatementRequest invalidFullStatementRequest;
    private TermDepositRequest termDepositRequest;
    private TermDepositRequest invalidTermDepositRequest;
    private LoanTermsRequest loanTermsRequest;
    private LoanTermsRequest invalidLoanTermsRequest;
    private HardTokenChargeRequest hardTokenChargeRequest;
    private HardTokenChargeRequest invalidHardTokenChargeRequest;
    private HardTokenRequest hardTokenRequest;
    private HardTokenRequest invalidHardTokenRequest;
    private HardTokenActivateRequest hardTokenActivateRequest;
    private HardTokenActivateRequest invalidHardTokenActivateRequest;
    private HardTokenValidateRequest hardTokenValidateRequest;
    private HardTokenValidateRequest invalidHardTokenValidateRequest;
    private HardTokenCheckPendingRequest hardTokenCheckPendingRequest;
    private HardTokenCheckPendingRequest invalidHardTokenCheckPendingRequest;
    private ENairaAuthenticateRequest eNairaAuthenticateRequest;
    private ENairaAuthenticateRequest invalidENairaAuthenticateRequest;
    private ENairaPreAuthorizationRequest eNairaPreAuthorizationRequest;
    private ENairaPreAuthorizationRequest invalidENairaPreAuthorizationRequest;
    private ENairaNameInquiryRequest eNairaNameInquiryRequest;
    private ENairaNameInquiryRequest invalidENairaNameInquiryRequest;
    private CustomerFeedBackRequest customerFeedBackRequest;
    private CustomerFeedBackRequest invalidCustomerFeedBackRequest;
    private XpressCashTokenChargesRequestDto xpressCashTokenChargesRequestDto;
    private XpressCashTokenChargesRequestDto invalidXpressCashTokenChargesRequestDto;
    private InternationalTransferChargesRequestDto internationalTransferChargesRequestDto;
    private InternationalTransferChargesRequestDto invalidInternationalTransferChargesRequestDto;

    private static HashMap headerParamMap = new HashMap<>();
    private static HashMap invalidHeaderParamMap = new HashMap<>();

    private static String headerParamsString = "";
    private static String invalidHeaderParamString = "";

    private static String service = "manage-cards";
    @MockBean
    SharedUtilities sharedUtilities;
    @MockBean
    private ServiceImpl servicesRepository;

    private WalletResponse walletResponse;
    private ResponseArray responseArray;


    // Token.
    private String tokenHeader = "eyJhbGciOiJIUzI1NiIsInppcCI6IkdaSVAifQ.H4sIAAAAAAAAAM2ZXW-bMBSG_4uv2RQoCUuuRgnaoq1TlKbdRdULl7qJVcCRDeqiqv99BnVa5QRqt8emN0FENs-LOef1x3lEFFdo5o8no2ASTf3QQ6K-QTMk9qIixadaEI481FwWt7Jd5CFSYJrLFu31a_v7OWOFbMVZTgSaXT2igt3WOVnM0SyaeM93v3BBZLekFhUrCF-RDRUVxxVlpexLyopW-zhrbpt-LenFf984q3f_HrFK43WqdFqyB8KfG5xfLNMVevJeCvE7dIgPB590wy-Wc7vwYPSONz-LfxzAw0ihl6ygZfvVnYz8yUjhk1KGaV7IZ7j58icKH2cZqz8ifNoNn6dxsl5cmgtQk3_J2R1tTOIIf9zH_5laZQ868NAvHqnweOMq2iPVa1p0Y_WO0j0Kjgk437KdG3x4DL-mee4GP1bwp3V-v8Z8Qxx9ftXsbyQ_YWWFMzcCxuoA7AgvqBBd4QedeqZ86KnelD_0-H_p5r9xxjFVYGPWM9QQ-N0aLhfpb0N6OD1YcVX0jmbday7oLDBXAJ0H5gqgM8FcAXwumGuwkQ3GKoDzwR-pK8EzwrMtPr4kCt8RB8n3NDncAZnxe3JRY_whFPTkolYMQGjoycZXHQmC35OLr_oRBL8nD51Eod9zDOAoBPweF3AWh34wcCA2G8lBIxHcjNXZ4BSX966MWJdtx4R16bYMWJdvw3x12TaMV5dtw3R12ZYMVxtvy2y1BdgwWm24DZPVhQ-62gXf-hniwUsdZnjwY18zPPyOz1CAje2eydE3dOybFLpg0QbFBmDyWwus0GMfqifvG1yRB7x3UWT0QzXotiTPmSX0tWzxZyfXC_-r5k9_Ac7LI5tIHwAA.R3bk9qK7EFCBgugZ65yqZbz52IHLI0dPpF-ARhElrEU";
    @Autowired
    private WebTestClient webClient;

    // Set the Actions Entity
    private ActionsEntity actionsEntity = new ActionsEntity(
            1,
            "buy"
    );
    //Set the Modules Entity
    private ModulesEntity modulesEntity = new ModulesEntity(
            218,
            "Transfers",
            "Transfers Module"
    );

    //Set Service Protocol Entity
    private ServiceActionsProtocolsEntity serviceProtocolsEntity = new ServiceActionsProtocolsEntity(
            1,
            "system-user",
            0,
            0,
            true,
            actionsEntity
    );

    //Set the service action entity List
    private List<ServiceActionsProtocolsEntity> serviceActionsProtocolsEntityList = new ArrayList<>();

    //Set Service Entity
    private ServicesEntity servicesEntity = new ServicesEntity(
            664,
            "Enquiries",
            "mobile-money",
            service,
            1,
            218,
            (byte) 0,
            10,
            10000,
            (byte) 1,
            false,
            serviceActionsProtocolsEntityList,
            modulesEntity,
            1
    );

    @BeforeEach
    void setup() throws Exception {
        // Formulate headers
        List<FormDataValue> formDataValues = new ArrayList<>();
        headerParamMap.put("channel", "ussd");
        headerParamMap.put("wallet-channel-id", "1");
        headerParamMap.put("channel-id", "1");
        headerParamMap.put("service", service);
        headerParamMap.put("languageCode", "en");
        headerParamMap.put("action", "send-money");
        headerParamMap.put("msisdn", "233245326845");
        headerParamMap.put("uuid", "233245326845");
        headerParamMap.put("action", "test");


        invalidHeaderParamMap.put("channel", "ussd");
        invalidHeaderParamMap.put("wallet-channel-id", "1");
        invalidHeaderParamMap.put("channel-id", "1");
        invalidHeaderParamMap.put("service", service);
        invalidHeaderParamMap.put("languageCode", "en");
        invalidHeaderParamMap.put("msisdn", "233245326845");

        headerParamsString = new ObjectMapper().writeValueAsString(headerParamMap);
        invalidHeaderParamString = new ObjectMapper().writeValueAsString(invalidHeaderParamMap);


        walletResponse = WalletResponse.builder().statusCode(4000).statusMessage("Success")
                .build();


        String message = "async message";
        responseArray = new ResponseArray();
        ResponseArrayResultDTO responseArrayResultDTO = new ResponseArrayResultDTO();
        responseArray.setStatusCode(5000);
        responseArray.setStatusMessage(message);
        responseArrayResultDTO.setMessage(message);
        responseArrayResultDTO.setReferenceID("5000");
        responseArrayResultDTO.setData(null);
        responseArrayResultDTO.setReferenceID("50000");
        responseArray.setResult(responseArrayResultDTO);

        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));


        when(servicesRepository.getServiceDetails(any())).thenReturn(Mono.just(servicesEntity));
        // Return Msisdn Mono.
        when(sharedUtilities.getMsisdn(any(), any(), any())).thenReturn(Mono.just("233558296125"));
        when(sharedUtilities.getWalletUrl(any(), any())).thenReturn(Mono.just("https://test"));
        when(sharedUtilities.msisdnValidation(anyString())).thenReturn(Mono.just("233558296125"));


        // Handle Encryption.
        when(sharedUtilities.encrypt(any())).thenReturn("abcdcdfereerer");
        when(sharedUtilities.getRequestID(any(), any(), any())).thenReturn(Mono.just("12335484188551"));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.processIntegrationResponse(any(), any(), any(), any(), any(), any())).thenReturn(Mono.just(responseArray));
        when(sharedUtilities.getIntegrationLayerResponse(anyString(), anyString(), anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.getIntegrationLayerResponse(anyCollection(), anyString(), anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));

        //Dtos
        stopChequeBookRequest = StopChequeBookRequest.builder()
                .accountAlias("test acount")
                .pin("080808")
                .chequeNumbers("test cheque")
                .reason("not valid")
                .startLeaveNo("883737")
                .endLeaveNo("010101")
                .build();

        invalidStopChequeBookRequest = StopChequeBookRequest.builder()
                .accountAlias("test acount")
                .chequeNumbers("test cheque")
                .reason("not valid")
                .startLeaveNo("883737")
                .endLeaveNo("010101")
                .build();

        confirmChequeRequest = ConfirmChequeRequest.builder()
                .accountAlias("test account")
                .chequeNumber("test cheque")
                .reason("test")
                .beneficiaryName("muthui")
                .amount(20000)
                .chequeDate("12th june")
                .pin("080808")
                .build();

        invalidConfirmChequeRequest = ConfirmChequeRequest.builder()
                .accountAlias("test account")
                .chequeNumber("test cheque")
                .reason("test")
                .beneficiaryName("muthui")
                .amount(20000)
                .chequeDate("12th june")
                .build();

        requestChequeBookRequest = RequestChequeBookRequest.builder()
                .accountAlias("test account")
                .branchName("Ongata branch")
                .pin("080808")
                .branchCode("022")
                .leaves("test")
                .reason("test")
                .build();

        invalidRequestChequeBookRequest = RequestChequeBookRequest.builder()
                .accountAlias("test account")
                .branchName("Ongata branch")
                .branchCode("022")
                .leaves("test")
                .reason("test")
                .build();

        fullStatementRequest = FullStatementRequest.builder()
                .accountAlias("test account")
                .email("muthui@test.com")
                .startDate("2019-6-18")
                .endDate("2021-07-18")
                .pin("080808")
                .build();

        invalidFullStatementRequest = FullStatementRequest.builder()
                .accountAlias("test account")
                .email("muthui@test.com")
                .startDate("2019-6-18")
                .endDate("2021-07-18")
                .build();

        termDepositRequest = TermDepositRequest.builder()
                .accountAlias("test account")
                .customerId("customer 001")
                .build();

        invalidTermDepositRequest = TermDepositRequest.builder()
                .customerId("customer 001")
                .build();

        loanTermsRequest = LoanTermsRequest.builder()
                .accountAlias("test account")
                .customerId("test id")
                .pin("080808").build();

        invalidLoanTermsRequest = LoanTermsRequest.builder()
                .accountAlias("test account")
                .customerId("test id")
                .build();

        hardTokenChargeRequest = HardTokenChargeRequest.builder()
                .modeOfDelivery("test mode")
                .build();

        invalidHardTokenChargeRequest = HardTokenChargeRequest.builder()
                .build();

        hardTokenRequest = HardTokenRequest.builder()
                .pin("080808")
                .accountAlias("test account")
                .requestType("test")
                .modeOfDelivery("test mode")
                .pickUpBranch("Ongata")
                .build();

        invalidHardTokenRequest = HardTokenRequest.builder()
                .accountAlias("test account")
                .requestType("test")
                .modeOfDelivery("test mode")
                .pickUpBranch("Ongata")
                .build();

        hardTokenActivateRequest = HardTokenActivateRequest.builder()
                .tokenResponse1("response 1")
                .tokenResponse2("response 2")
                .pin("080808")
                .accountAlias("test account")
                .serialNumber("test serial")
                .build();

        invalidHardTokenActivateRequest = HardTokenActivateRequest.builder()
                .tokenResponse1("response 1")
                .tokenResponse2("response 2")
                .accountAlias("test account")
                .serialNumber("test serial")
                .build();

        hardTokenValidateRequest = HardTokenValidateRequest.builder()
                .tokenResponse("test response")
                .pin("080808")
                .accountAlias("test account")
                .build();

        invalidHardTokenValidateRequest = HardTokenValidateRequest.builder()
                .tokenResponse("test response")
                .accountAlias("test account")
                .build();

        hardTokenCheckPendingRequest = HardTokenCheckPendingRequest.builder()
                .accountAlias("test account").build();

        invalidHardTokenCheckPendingRequest = HardTokenCheckPendingRequest.builder().build();

        eNairaAuthenticateRequest = ENairaAuthenticateRequest.builder()
                .accountAlias("test account")
                .username("test user")
                .password("Pass1900!")
                .build();

        invalidENairaAuthenticateRequest = ENairaAuthenticateRequest.builder()
                .username("test user")
                .password("Pass1900!")
                .build();

        eNairaPreAuthorizationRequest = ENairaPreAuthorizationRequest.builder()
                .authorizationToken("test token")
                .accountAlias("test account")
                .build();

        invalidENairaPreAuthorizationRequest = ENairaPreAuthorizationRequest.builder()
                .authorizationToken("test token")
                .build();

        eNairaNameInquiryRequest = ENairaNameInquiryRequest.builder()
                .accountAlias("test account")
                .accountInfo("test info")
                .build();

        invalidENairaNameInquiryRequest = ENairaNameInquiryRequest.builder()
                .accountInfo("test info")
                .build();

        customerFeedBackRequest = CustomerFeedBackRequest.builder()
                .platform("test")
                .accountAlias("Test Account")
                .additionalResponse("Test Response")
                .contactCustomer(1)
                .closeDate("test date")
                .openDate("test date")
                .experienceMetric(1)
                .referralMetric(1)
                .otherServiceCode("")
                .serviceCode("test service")
                .build();

        customerFeedBackRequest = CustomerFeedBackRequest.builder()
                .platform("test")
                .accountAlias("Test Account")
                .additionalResponse("Test Response")
                .contactCustomer(3)
                .closeDate("test date")
                .openDate("test date")
                .experienceMetric(12)
                .referralMetric(12)
                .otherServiceCode("")
                .serviceCode("test service")
                .build();

        invalidCustomerFeedBackRequest = CustomerFeedBackRequest.builder()
                .platform("test")
                .additionalResponse("Test Response")
                .contactCustomer(3)
                .closeDate("test date")
                .openDate("test date")
                .experienceMetric(12)
                .referralMetric(12)
                .otherServiceCode("")
                .serviceCode("test service")
                .build();

        xpressCashTokenChargesRequestDto = XpressCashTokenChargesRequestDto.builder()
                .accountAlias("test account")
                .amount("100")
                .currency("NGN")
                .build();

        invalidXpressCashTokenChargesRequestDto = XpressCashTokenChargesRequestDto.builder()
                .accountAlias("test account")
                .currency("NGN")
                .build();

        internationalTransferChargesRequestDto = InternationalTransferChargesRequestDto.builder()
                .accountAlias("test account")
                .amount("100")
                .currency("NGN")
                .productCode("test product")
                .build();

        invalidInternationalTransferChargesRequestDto = InternationalTransferChargesRequestDto.builder()
                .accountAlias("test account")
                .amount("100")
                .currency("NGN")
                .build();
    }

    //     CHEQUE_SERVICES_STOP = "/cheque-services/stop";
    @Test
    void chequeServicesStopSuccessfullyOnCorrectParameters() {
        this.webClient.post()
                .uri(CHEQUE_SERVICES_STOP)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(stopChequeBookRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);

    }

    @Test
    void chequeServicesStopFailedOnInvalidParameters() {
        this.webClient.post()
                .uri(CHEQUE_SERVICES_STOP)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidStopChequeBookRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555)
                .jsonPath("$.result.data[0]").isEqualTo("pin is a required field");

    }

    @Test
    void chequeServicesStopFailedOnInvalidAuthorizationHeader() {
        this.webClient.post()
                .uri(CHEQUE_SERVICES_STOP)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidStopChequeBookRequest))
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();

    }

    //    CHEQUE_SERVICES_CONFIRM = "/cheque-services/confirm";
    @Test
    void chequeServicesConfirmSuccessfullyGivenCorrectParameters() {
        this.webClient.post()
                .uri(CHEQUE_SERVICES_CONFIRM)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(confirmChequeRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);

    }

    @Test
    void chequeServicesConfirmFailedOnInvalidParameters() {
        this.webClient.post()
                .uri(CHEQUE_SERVICES_CONFIRM)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidConfirmChequeRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555)
                .jsonPath("$.result.data[0]").isEqualTo("pin is a required field");


    }

    @Test
    void chequeServicesConfirmFailedOnInvalidAuthorizationHeader() {
        this.webClient.post()
                .uri(CHEQUE_SERVICES_CONFIRM)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(confirmChequeRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);

    }

    //    CHEQUE_SERVICES_BOOK_LEAVES = "/cheque-services/book-leaves";
    @Test
    void chequeServiceBookLeavesSuccessfully() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));

        this.webClient.get()
                .uri(CHEQUE_SERVICES_BOOK_LEAVES)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void chequeServiceBookLeavesFailedOnInvalidAuthorizationHeader() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));

        this.webClient.get()
                .uri(CHEQUE_SERVICES_BOOK_LEAVES)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();

    }

    //    CHEQUE_SERVICES_REQUEST = "/cheque-services/request";
    @Test
    void chequeServicesRequestSuccessfullyGivenCorrectParameters() {

        this.webClient.post()
                .uri(CHEQUE_SERVICES_REQUEST)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(requestChequeBookRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);


    }

    @Test
    void chequeServicesRequestFailedOnInvalidParameters() {
        this.webClient.post()
                .uri(CHEQUE_SERVICES_REQUEST)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidRequestChequeBookRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555)
                .jsonPath("$.result.data[0]").isEqualTo("pin is a required field");

    }

    @Test
    void chequeServicesRequestFailedOnInvalidAuthorizationHeader() {
        this.webClient.post()
                .uri(CHEQUE_SERVICES_REQUEST)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidRequestChequeBookRequest))
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();

    }

    //    FULL_STATEMENT_REQUEST = "/full-statement/request";
    @Test
    void fullStatementRequestSuccessfullyGivenCorrectParameters() {

        this.webClient.post()
                .uri(FULL_STATEMENT_REQUEST)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(fullStatementRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);

    }

    @Test
    void fullStatementRequestFailedOnInvalidParameters() {

        this.webClient.post()
                .uri(FULL_STATEMENT_REQUEST)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidFullStatementRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555)
                .jsonPath("$.result.data[0]").isEqualTo("pin is a required field");


    }

    @Test
    void fullStatementRequestFailedOnInvalidAuthorizationHeader() {

        this.webClient.post()
                .uri(FULL_STATEMENT_REQUEST)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidFullStatementRequest))
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();
    }

    //    TERM_DEPOSIT_REQUEST = "/term-deposit/request";
    @Test
    void termDepositRequestSuccessfullyGivenCorrectParameters() {
        this.webClient.post()
                .uri(TERM_DEPOSIT_REQUEST)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(termDepositRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);

    }

    @Test
    void termDepositRequestFailedOnInvalidParameters() {
        this.webClient.post()
                .uri(TERM_DEPOSIT_REQUEST)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidTermDepositRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555)
                .jsonPath("$.result.data[0]").isEqualTo("Account Required");


    }

    @Test
    void termDepositRequestFailedOnInvalidAuthorizationHeader() {
        this.webClient.post()
                .uri(TERM_DEPOSIT_REQUEST)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidTermDepositRequest))
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();

    }

    //    LOAN_INFORMATION_REQUEST = "/loan-information/request";
    @Test
    void loanInformationRequestFetchedSuccessfullyGivenCorrectParameters() {
        this.webClient.post()
                .uri(LOAN_INFORMATION_REQUEST)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(loanTermsRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);

    }

    @Test
    void loanInformationRequestFailedOnInvalidParamters() {
        this.webClient.post()
                .uri(LOAN_INFORMATION_REQUEST)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidLoanTermsRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555)
                .jsonPath("$.result.data[0]").isEqualTo("pin is a required field");

    }

    @Test
    void loanInformationRequestFailedOnInvalidAuthorizationHeader() {
        this.webClient.post()
                .uri(LOAN_INFORMATION_REQUEST)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidLoanTermsRequest))
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();

    }

    //    CHEQUE_SERVICES_BRANCHES_FETCH = "/cheque-services/branches-fetch";
    @Test
    void chequeServicesBranchesFetchedSuccessfullyGivenCorrectParameters() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));

        this.webClient.get()
                .uri(CHEQUE_SERVICES_BRANCHES_FETCH)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);

    }

    @Test
    void chequeServicesBranchesFetchFailedOnInvalidAuthorizationHeader() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));

        this.webClient.get()
                .uri(CHEQUE_SERVICES_BRANCHES_FETCH)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();

    }

    //    HARD_TOKEN_REQUEST_TYPES = "/hard-token/request-types";
    @Test
    void hardTokenRequestTypesFetchedSuccessfully() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));

        this.webClient.get()
                .uri(HARD_TOKEN_REQUEST_TYPES)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void hardTokenRequestTypesFetchFailedOnInvalidAuthorizationHeader() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));

        this.webClient.get()
                .uri(HARD_TOKEN_REQUEST_TYPES)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();
    }

    //    HARD_TOKEN_DELIVERY_MODES = "/hard-token/delivery-modes";
    @Test
    void hardTokenDeliveryModesFetchedSuccessfully() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));

        this.webClient.get()
                .uri(HARD_TOKEN_DELIVERY_MODES)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void hardTokenDeliveryModesFetchFailedOnInvalidAuthorizationHeader() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));

        this.webClient.get()
                .uri(HARD_TOKEN_DELIVERY_MODES)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();
    }

    //    HARD_TOKEN_BRANCHES = "/hard-token/branches";
    @Test
    void hardTokenBranchesFetchSuccessfully() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString(), anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));

        this.webClient.get()
                .uri(HARD_TOKEN_BRANCHES)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void hardTokenBranchesFetchFailedOnInvalidAuthorizationHeader() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));

        this.webClient.get()
                .uri(HARD_TOKEN_BRANCHES)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();
    }

    //    HARD_TOKEN_CHARGES = "/hard-token/charges";
    @Test
    void hardTokenChargesFetchSuccessfullyGivenCorrectParameters() {
        when(sharedUtilities.getIntegrationLayerResponse(any(hardTokenChargeRequest.getClass()), anyString(), anyString())).thenReturn(Mono.just(walletResponse));
        this.webClient.post()
                .uri(HARD_TOKEN_CHARGES)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(hardTokenChargeRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);

    }

    @Test
    void hardTokenChargesFetchSuccessfullyOnInvalidParameters() {
        this.webClient.post()
                .uri(HARD_TOKEN_CHARGES)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidHardTokenChargeRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555)
                .jsonPath("$.result.data[0]").isEqualTo("{modeOfDelivery.required}");

    }

    @Test
    void hardTokenChargesFetchFailedOnInvalidAuthorizationHeader() {
        this.webClient.post()
                .uri(HARD_TOKEN_CHARGES)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidHardTokenChargeRequest))
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();

    }

    //    HARD_TOKEN_REQUEST = "/hard-token/request";
    @Test
    void hardTokenRequestSuccessfullyGivenCorrectParameters() {
        this.webClient.post()
                .uri(HARD_TOKEN_REQUEST)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(hardTokenRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);

    }

    @Test
    void hardTokenRequestOnInvalidParameters() {
        this.webClient.post()
                .uri(HARD_TOKEN_REQUEST)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidHardTokenRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555)
                .jsonPath("$.result.data[0]").isEqualTo("pin is a required field");

    }

    //    HARD_TOKEN_ACTIVATE = "/hard-token/activate";
    @Test
    void hardTokenActivateSuccessfullyGivenCorrectParameters() {
        this.webClient.post()
                .uri(HARD_TOKEN_ACTIVATE)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(hardTokenActivateRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);

    }

    @Test
    void hardTokenActivateFailedOnInvalidParameters() {
        this.webClient.post()
                .uri(HARD_TOKEN_ACTIVATE)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidHardTokenActivateRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555)
                .jsonPath("$.result.data[0]").isEqualTo("pin is a required field");

    }

    @Test
    void hardTokenActivateFailedOnInvalidAuthorizationHeader() {
        this.webClient.post()
                .uri(HARD_TOKEN_ACTIVATE)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidHardTokenActivateRequest))
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();

    }

    //    HARD_TOKEN_VALIDATE = "/hard-token/validate-token";
    @Test
    void hardTokenValidateSuccessfullyGivenCorrectParameters() {
        this.webClient.post()
                .uri(HARD_TOKEN_VALIDATE)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(hardTokenValidateRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void hardTokenValidateFailedOnInvalidParameters() {
        this.webClient.post()
                .uri(HARD_TOKEN_VALIDATE)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidHardTokenValidateRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555)
                .jsonPath("$.result.data[0]").isEqualTo("pin is a required field");
    }

    @Test
    void hardTokenValidateFailedOnInvalidAuthorizationHeader() {
        this.webClient.post()
                .uri(HARD_TOKEN_VALIDATE)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidHardTokenValidateRequest))
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();
    }

    //    HARD_TOKEN_CHECK_PENDING_REQUEST = "/hard-token/check-pending-request";
    @Test
    void hardTokenCheckPendingRequestSuccessfullyGivenCorrectParameters() {
        this.webClient.post()
                .uri(HARD_TOKEN_CHECK_PENDING_REQUEST)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(hardTokenCheckPendingRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);

    }

    @Test
    void hardTokenCheckPendingRequestOnInvalidParameters() {
        this.webClient.post()
                .uri(HARD_TOKEN_CHECK_PENDING_REQUEST)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidHardTokenCheckPendingRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555)
                .jsonPath("$.result.data[0]").isEqualTo("Account Alias is a required field");


    }


    @Test
    void hardTokenCheckPendingRequestOnInvalidAuthorizationHeader() {
        this.webClient.post()
                .uri(HARD_TOKEN_CHECK_PENDING_REQUEST)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(hardTokenCheckPendingRequest))
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();

    }

    //    ENAIRA_AUTHENTICATE_REQUEST = "/enaira/authenticate";
    @Test
    void eNairaAuthenticateRequestSuccessfullyGivenCorrectParameters() {
        this.webClient.post()
                .uri(ENAIRA_AUTHENTICATE_REQUEST)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(eNairaAuthenticateRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void eNairaAuthenticateRequestFailedOnInvalidParameters() {
        this.webClient.post()
                .uri(ENAIRA_AUTHENTICATE_REQUEST)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidENairaAuthenticateRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555)
                .jsonPath("$.result.data[0]").isEqualTo("account Alias is required");

    }

    @Test
    void eNairaAuthenticateRequestFailedOnInvalidAuthorizationHeader() {
        this.webClient.post()
                .uri(ENAIRA_AUTHENTICATE_REQUEST)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidENairaAuthenticateRequest))
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();

    }

    //    ENAIRA_PRE_AUTHORIZATION = "/enaira/preauthorization";
    @Test
    void eNairaPreAuthorizationSuccessfullyGivenCorrectParameters() {
        this.webClient.post()
                .uri(ENAIRA_PRE_AUTHORIZATION)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(eNairaPreAuthorizationRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);

    }

    @Test
    void eNairaPreAuthorizationFailedOnInvalidParameters() {
        this.webClient.post()
                .uri(ENAIRA_PRE_AUTHORIZATION)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidENairaPreAuthorizationRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555)
                .jsonPath("$.result.data[0]").isEqualTo("accountAlias is required");


    }

    @Test
    void eNairaPreAuthorizationFailedOnInvalidAuthorizationHeader() {
        this.webClient.post()
                .uri(ENAIRA_PRE_AUTHORIZATION)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidENairaPreAuthorizationRequest))
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();
    }

    //    ENAIRA_NAME_INQUIRY = "/enaira/name-inquiry";
    @Test
    void eNairaNameInquirySuccessfullylGivenCorrectParameters() {
        this.webClient.post()
                .uri(ENAIRA_NAME_INQUIRY)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(eNairaNameInquiryRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);

    }

    @Test
    void eNairaNameInquiryFailedOnInvalidParameters() {
        this.webClient.post()
                .uri(ENAIRA_NAME_INQUIRY)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidENairaNameInquiryRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555)
                .jsonPath("$.result.data[0]").isEqualTo("accountAlias is required");


    }

    @Test
    void eNairaNameInquiryFailedOnInvalidAuthorizationHeader() {
        this.webClient.post()
                .uri(ENAIRA_NAME_INQUIRY)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidENairaNameInquiryRequest))
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();


    }

    //    CHEQUE_SERVICES_BOOK_LEAVES = "/cheque-services/book-leaves";
    @Test
    void fetchActiveCustomerBannerSuccess() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString(), anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));

        this.webClient.get()
                .uri(CUSTOMERS_ACTIVE_BANNER)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void customerFeedbackSuccessGivenCorrectParameters() {
        this.webClient.post()
                .uri(CUSTOMER_FEEDBACK_CREATE)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(customerFeedBackRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void customerFeedbackSuccessGivenInvalidParameters() {
        this.webClient.post()
                .uri(CUSTOMER_FEEDBACK_CREATE)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidCustomerFeedBackRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555);
    }

    @Test
    void xpressCashTokenChargesGivenCorrectParameters() {
        this.webClient.post()
                .uri(XPRESS_CASH_TOKEN_CHARGES)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(xpressCashTokenChargesRequestDto))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void xpressCashTokenChargesGivenInCorrectParameters() {
        this.webClient.post()
                .uri(XPRESS_CASH_TOKEN_CHARGES)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidXpressCashTokenChargesRequestDto))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void internationalTransferChargesGivenCorrectParameters() {
        this.webClient.post()
                .uri(INTERNATIONAL_TRANSFER_CHARGES)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(internationalTransferChargesRequestDto))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void internationalTransferChargesGivenInCorrectParameters() {
        this.webClient.post()
                .uri(INTERNATIONAL_TRANSFER_CHARGES)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidInternationalTransferChargesRequestDto))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555);
    }
}
