package com.cellulant.gatewayrequisitionservices.controllers;

import com.cellulant.gatewayrequisitionservices.cards.managecards.dtos.*;
import com.cellulant.gatewayrequisitionservices.cards.virtualcards.dtos.*;
import com.cellulant.gatewayrequisitionservices.dtos.responses.ResponseArray;
import com.cellulant.gatewayrequisitionservices.dtos.responses.ResponseArrayResultDTO;
import com.cellulant.gatewayrequisitionservices.dtos.walletdtos.WalletResponse;
import com.cellulant.gatewayrequisitionservices.models.*;
import com.cellulant.gatewayrequisitionservices.services.ServiceImpl;
import com.cellulant.gatewayrequisitionservices.utils.SharedUtilities;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@AutoConfigureWebTestClient
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ManageCardServicesControllerTests {

    private static final String MANAGE_CARDS_BLOCK = "/manage-cards/block";
    private static final String MANAGE_CARDS_ACTIVE_CARDS = "/manage-cards/active-cards";
    private static final String MANAGE_CARDS_UNBLOCK = "/manage-cards/unblock";
    private static final String MANAGE_CARDS_BLOCK_CARD_OPTIONS = "/manage-cards/block-card-options";
    private static final String MANAGE_CARDS_BLOCK_CARD_REASONS = "/manage-cards/block-card-reasons";
    private static final String TRAVEL_NOTIFICATION_REQUEST = "/travel-notification/request";
    private static final String TRAVEL_NOTIFICATION_COUNTRIES = "/travel-notification/countries";
    private static final String PREPAID_CARD_FUND = "/prepaid-card/fund";
    private static final String MANAGE_CARDS_CARD_NAME_INQUIRY = "/manage-cards/card-name-inquiry";


    private static final String MANAGE_CARDS_NEW_CARD_REQUEST_TYPES = "/manage-cards/request-types";
    private static final String MANAGE_CARDS_RENEWAL_CARD_REQUEST_TYPES = "/manage-cards/request-renewal-types";
    private static final String MANAGE_CARDS_NEW_CARD_SCHEME_TYPES = "/manage-cards/scheme-types";
    private static final String MANAGE_CARDS_NEW_CARDS_FETCH_BRANCHES = "/manage-cards/bank-branches";
    private static final String MANAGE_CARDS_NEW_CARDS_FETCH_DELIVERY_MODES = "/manage-cards/delivery-modes";
    private static final String MANAGE_CARDS_NEW_CARDS_FETCH_CARD_CHARGES = "/manage-cards/fetch-request-charges";
    private static final String MANAGE_CARDS_NEW_CARD_REQUESTS = "/manage-cards/request-card";
    private static final String MANAGE_CARDS_FETCH_CARDS_BY_CIF = "/manage-cards/fetch-cards-cif";
    private static final String MANAGE_CARDS_ACTIVATE_CARDS = "/manage-cards/activate-card";
    private static final String MANAGE_CARDS_RENEW_CARD = "/manage-cards/renew-card";

    private static final String MANAGE_CARDS_FETCH_DEBIT_CARD_LIMITS = "/manage-cards/fetch-debit-card-limits";
    private static final String MANAGE_CARDS_UPDATE_DEBIT_CARD_LIMITS = "/manage-cards/update-debit-card-limits";
    private static final String MANAGE_CARDS_CHANGE_CARD_PIN = "/manage-cards/change-card-pin";
    private static final String MANAGE_CARDS_RESET_CARD_PIN = "/manage-cards/reset-card-pin";
    private static final String MANAGE_CARDS_DEBIT_CARD_LIMIT_STATES = "/manage-cards/debit-card-limit-states";

    public static final String VIRTUAL_CARDS_CREATE = "/virtual-cards/create";
    public static final String VIRTUAL_CARDS_CLOSE_CARD = "/virtual-cards/close-card";
    public static final String VIRTUAL_CARDS_FETCH_CARDS = "/virtual-cards/fetch-cards";
    public static final String VIRTUAL_CARDS_CARD_DETAILS = "/virtual-cards/card-details";
    public static final String VIRTUAL_CARDS_FETCH_TYPES = "/virtual-cards/fetch-types";
    public static final String VIRTUAL_CARDS_MINI_STATEMENT = "/virtual-cards/mini-statement";
    public static final String VIRTUAL_CARDS_UNBLOCK_CARD = "/virtual-cards/unblock-card";
    public static final String VIRTUAL_CARDS_BALANCE = "/virtual-cards/balance";
    public static final String VIRTUAL_CARDS_GET_OTP_NUMBER = "/virtual-cards/get-otp-number";
    public static final String VIRTUAL_CARDS_UPDATE_OTP_NUMBER = "/virtual-cards/update-otp-number";
    public static final String VIRTUAL_CARDS_LOAD = "/virtual-cards/load";




    private BlockCardRequest blockCardRequest;
    private BlockCardRequest invalidBlockCardRequest;
    private FetchActiveCardsRequest fetchActiveCardsRequest;
    private FetchActiveCardsRequest invalidFetchActiveCardsRequest;
    private UnBlockCardRequest unBlockCardRequest;
    private UnBlockCardRequest invalidUnblockCardRequest;
    private TravelNotificationRequest travelNotificationRequest;
    private TravelNotificationRequest invalidTravelNotificationRequest;
    private FundPrepaidCardRequest fundPrepaidCardRequest;
    private FundPrepaidCardRequest invalidFundPrepaidCardRequest;
    private CardNameInquiryRequest cardNameInquiryRequest;
    private CardNameInquiryRequest invalidCardNameInquiryRequest;
    private NewCardRequestChargesRequest newCardRequestChargesRequest;
    private NewCardRequestChargesRequest invalidNewCardRequestChargesRequest;
    private NewCardRequests newCardRequests;
    private NewCardRequests invalidNewCardRequests;
    private FetchActiveCardsByCIFRequest fetchActiveCardsByCIFRequest;
    private FetchActiveCardsByCIFRequest invalidFetchActiveCardsByCIFRequest;
    private ActivateCardRequest activateCardRequest;
    private ActivateCardRequest invalidActivateCardRequest;
    private RenewCardRequest renewCardRequest;
    private RenewCardRequest invalidRenewCardRequest;
    private UpdateDebitCardLimitRequest updateDebitCardLimitRequest;
    private UpdateDebitCardLimitRequest invalidUpdateDebitCardLimitRequest;
    private FetchDebitCardLimitRequest fetchDebitCardLimitRequest;
    private FetchDebitCardLimitRequest invalidFetchDebitCardLimitRequest;
    private ChangeCardPinRequest changeCardPinRequest;
    private ChangeCardPinRequest invalidChangeCardPinRequest;
    private ResetCardPinRequest resetCardPinRequest;
    private ResetCardPinRequest invalidResetCardPinRequest;
    private CreateVirtualCardRequest createVirtualCardRequest;
    private CreateVirtualCardRequest invalidCreateVirtualCardRequest;
    private CloseVirtualCardRequest closeVirtualCardRequest;
    private CloseVirtualCardRequest invalidCloseVirtualCardRequest;
    private FetchVirtualCardsRequest fetchVirtualCardsRequest;
    private FetchVirtualCardsRequest invalidFetchVirtualCardsRequest;
    private FetchVirtualCardDetailsRequest fetchVirtualCardDetailsRequest;
    private FetchVirtualCardDetailsRequest invalidFetchVirtualCardDetailsRequest;
    private VirtualCardMinistatementRequest virtualCardMinistatementRequest;
    private VirtualCardMinistatementRequest invalidVirtualCardMinistatementRequest;
    private UnblockVirtualCardRequest unblockVirtualCardRequest;
    private UnblockVirtualCardRequest invalidUnblockVirtualCardRequest;
    private VirtualCardBalanceRequest virtualCardBalanceRequest;
    private VirtualCardBalanceRequest invalidVirtualCardBalanceRequest;
    private FetchOTPMobileNumberRequest fetchOTPMobileNumberRequest;
    private FetchOTPMobileNumberRequest invalidFetchOTPMobileNumberRequest;
    private UpdateOTPVirtualCardRequest updateOTPVirtualCardRequest;
    private UpdateOTPVirtualCardRequest invalidUpdateOTPVirtualCardRequest;
    private LoadVirtualCardRequest loadVirtualCardRequest;
    private LoadVirtualCardRequest invalidLoadVirtualCardRequest;


    private static HashMap headerParamMap = new HashMap<>();
    private static HashMap invalidHeaderParamMap = new HashMap<>();

    private static String headerParamsString = "";
    private static String invalidHeaderParamString = "";

    private static String service = "manage-cards";
    @MockBean
    SharedUtilities sharedUtilities;
    @MockBean
    private ServiceImpl servicesRepository;

    private WalletResponse walletResponse;
    private ResponseArray responseArray;

    // Token.
    private String tokenHeader = "eyJhbGciOiJIUzI1NiIsInppcCI6IkdaSVAifQ.H4sIAAAAAAAAAM2ZXW-bMBSG_4uv2RQoCUuuRgnaoq1TlKbdRdULl7qJVcCRDeqiqv99BnVa5QRqt8emN0FENs-LOef1x3lEFFdo5o8no2ASTf3QQ6K-QTMk9qIixadaEI481FwWt7Jd5CFSYJrLFu31a_v7OWOFbMVZTgSaXT2igt3WOVnM0SyaeM93v3BBZLekFhUrCF-RDRUVxxVlpexLyopW-zhrbpt-LenFf984q3f_HrFK43WqdFqyB8KfG5xfLNMVevJeCvE7dIgPB590wy-Wc7vwYPSONz-LfxzAw0ihl6ygZfvVnYz8yUjhk1KGaV7IZ7j58icKH2cZqz8ifNoNn6dxsl5cmgtQk3_J2R1tTOIIf9zH_5laZQ868NAvHqnweOMq2iPVa1p0Y_WO0j0Kjgk437KdG3x4DL-mee4GP1bwp3V-v8Z8Qxx9ftXsbyQ_YWWFMzcCxuoA7AgvqBBd4QedeqZ86KnelD_0-H_p5r9xxjFVYGPWM9QQ-N0aLhfpb0N6OD1YcVX0jmbday7oLDBXAJ0H5gqgM8FcAXwumGuwkQ3GKoDzwR-pK8EzwrMtPr4kCt8RB8n3NDncAZnxe3JRY_whFPTkolYMQGjoycZXHQmC35OLr_oRBL8nD51Eod9zDOAoBPweF3AWh34wcCA2G8lBIxHcjNXZ4BSX966MWJdtx4R16bYMWJdvw3x12TaMV5dtw3R12ZYMVxtvy2y1BdgwWm24DZPVhQ-62gXf-hniwUsdZnjwY18zPPyOz1CAje2eydE3dOybFLpg0QbFBmDyWwus0GMfqifvG1yRB7x3UWT0QzXotiTPmSX0tWzxZyfXC_-r5k9_Ac7LI5tIHwAA.R3bk9qK7EFCBgugZ65yqZbz52IHLI0dPpF-ARhElrEU";
    @Autowired
    private WebTestClient webClient;
    // Set the Actions Entity
    private ActionsEntity actionsEntity = new ActionsEntity(
            1,
            "buy"
    );
    //Set the Modules Entity
    private ModulesEntity modulesEntity = new ModulesEntity(
            218,
            "Transfers",
            "Transfers Module"
    );

    //Set Service Protocol Entity
    private ServiceActionsProtocolsEntity serviceProtocolsEntity = new ServiceActionsProtocolsEntity(
            1,
            "system-user",
            0,
            0,
            true,
            actionsEntity
    );

    //Set the service action entity List
    private List<ServiceActionsProtocolsEntity> serviceActionsProtocolsEntityList = new ArrayList<>();

    //Set Service Entity
    private ServicesEntity servicesEntity = new ServicesEntity(
            664,
            "Enquiries",
            "mobile-money",
            service,
            1,
            218,
            (byte) 0,
            10,
            10000,
            (byte) 1,
            false,
            serviceActionsProtocolsEntityList,
            modulesEntity,
            1
    );


    @BeforeEach
    void setup() throws Exception {
        // Formulate headers
        List<FormDataValue> formDataValues = new ArrayList<>();
        headerParamMap.put("channel", "ussd");
        headerParamMap.put("wallet-channel-id", "1");
        headerParamMap.put("channel-id", "1");
        headerParamMap.put("service", service);
        headerParamMap.put("languageCode", "en");
        headerParamMap.put("action", "send-money");
        headerParamMap.put("msisdn", "233245326845");
        headerParamMap.put("uuid", "233245326845");
        headerParamMap.put("action", "test");


        invalidHeaderParamMap.put("channel", "ussd");
        invalidHeaderParamMap.put("wallet-channel-id", "1");
        invalidHeaderParamMap.put("channel-id", "1");
        invalidHeaderParamMap.put("service", service);
        invalidHeaderParamMap.put("languageCode", "en");
        invalidHeaderParamMap.put("msisdn", "233245326845");

        headerParamsString = new ObjectMapper().writeValueAsString(headerParamMap);
        invalidHeaderParamString = new ObjectMapper().writeValueAsString(invalidHeaderParamMap);


        walletResponse = WalletResponse.builder().statusCode(4000).statusMessage("Success")
                .build();


        String message = "async message";
        responseArray = new ResponseArray();
        ResponseArrayResultDTO responseArrayResultDTO = new ResponseArrayResultDTO();
        responseArray.setStatusCode(5000);
        responseArray.setStatusMessage(message);
        responseArrayResultDTO.setMessage(message);
        responseArrayResultDTO.setReferenceID("5000");
        responseArrayResultDTO.setData(null);
        responseArrayResultDTO.setReferenceID("50000");
        responseArray.setResult(responseArrayResultDTO);

        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));


        when(servicesRepository.getServiceDetails(any())).thenReturn(Mono.just(servicesEntity));
        // Return Msisdn Mono.
        when(sharedUtilities.getMsisdn(any(), any(), any())).thenReturn(Mono.just("233558296125"));
        when(sharedUtilities.getWalletUrl(any(), any())).thenReturn(Mono.just("https://test"));
        when(sharedUtilities.msisdnValidation(anyString())).thenReturn(Mono.just("233558296125"));


        // Handle Encryption.
        when(sharedUtilities.encrypt(any())).thenReturn("abcdcdfereerer");
        when(sharedUtilities.getRequestID(any(), any(), any())).thenReturn(Mono.just("12335484188551"));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.processIntegrationResponse(any(), any(), any(), any(), any(), any())).thenReturn(Mono.just(responseArray));
        when(sharedUtilities.getIntegrationLayerResponse(anyString(), anyString(), anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));

        // DTOs.
        blockCardRequest = BlockCardRequest.builder()
                .pin("080808")
                .accountAlias("XPRESS")
                .cardNumber("0004458754481848")
                .reason("Another test")
                .date("date")
                .option("options")
                .description("testing").build();

        invalidBlockCardRequest = BlockCardRequest.builder()
                .accountAlias("XPRESS")
                .cardNumber("0004458754481848")
                .reason("Another test")
                .date("date")
                .option("options")
                .description("testing").build();

        fetchActiveCardsRequest = FetchActiveCardsRequest.builder()
                .accountAlias("XPRESS").build();

        invalidFetchActiveCardsRequest = FetchActiveCardsRequest.builder().build();

        unBlockCardRequest = UnBlockCardRequest.builder()
                .pin("080808")
                .accountAlias("XPRESS")
                .cardNumber("45687894125464844")
                .reason("testing").build();

        invalidUnblockCardRequest = UnBlockCardRequest.builder()
                .accountAlias("XPRESS")
                .cardNumber("45687894125464844")
                .reason("testing").build();

        travelNotificationRequest = TravelNotificationRequest.builder()
                .pin("080808")
                .accountAlias("Testing Xpress")
                .cardAccounts("5454545454787874")
                .travelDestination("Kinoo")
                .travelDate("Tuesday, 5th May 2021")
                .returnDate("Wednesday, 11th October 2022")
                .purposeOfTravel("Work")
                .build();

        invalidTravelNotificationRequest = TravelNotificationRequest.builder()
                .accountAlias("Testing Xpress")
                .cardAccounts("5454545454787874")
                .travelDestination("Kinoo")
                .travelDate("Tuesday, 5th May 2021")
                .returnDate("Wednesday, 11th October 2022")
                .purposeOfTravel("Work")
                .build();
        formDataValues.add(FormDataValue.builder()
                .fieldName("billerCode").fieldValue("GHS-242").dataType("string").build());

        fundPrepaidCardRequest = FundPrepaidCardRequest.builder()
                .pin("080808")
                .billerCode("test")
                .accountAlias("XPRESS")
                .amount(Float.parseFloat("250.25"))
                .formDataValue(formDataValues)
                .productCode("GPV")
                .narration("Testing")
                .nominate("yes").build();


        invalidFundPrepaidCardRequest = FundPrepaidCardRequest.builder()
                .pin("080808")
                .accountAlias("XPRESS")
                .amount(Float.parseFloat("250.25"))
                .formDataValue(formDataValues)
                .productCode("GPV")
                .narration("Testing")
                .nominate("yes").build();

        cardNameInquiryRequest = CardNameInquiryRequest.builder()
                .accountAlias("Xpress Account")
                .cardNumber("0022667700").build();


        invalidCardNameInquiryRequest = CardNameInquiryRequest.builder()
                .accountAlias("XPRESS").build();

        newCardRequestChargesRequest = NewCardRequestChargesRequest.builder()
                .accountAlias("XPRESS")
                .cardNumber("45687894125464844")
                .cardScheme("VISA")
                .deliveryMode("BRANCHPICKUP")
                .requestType("NEWCARD")
                .digitalAddressMobileNumber("test")
                .digitalDeliveryAddress("Ongata Rongai")
                .pan("test")
                .build();

        invalidNewCardRequestChargesRequest = NewCardRequestChargesRequest.builder()
                .cardNumber("45687894125464844")
                .cardScheme("VISA")
                .deliveryMode("BRANCHPICKUP")
                .requestType("NEWCARD")
                .digitalAddressMobileNumber("test")
                .build();

        newCardRequests = NewCardRequests.builder()
                .accountAlias("1441XXXXX0335 - AMOUNT BLOCKED")
                .pin("080808")
                .cardScheme("VISA")
                .cardProduct("404587")
                .requestType("H98")
                .amountToCharge("12.0")
                .cardProductDesc("OngataVisaClassics")
                .chargeCurrency("GHS")
                .requestType("NEWCARD")
                .requestBranch("Ongata Branch")
                .deliveryMode("BRANCHPICKUP").build();

        invalidNewCardRequests = NewCardRequests.builder()
                .accountAlias("1441XXXXX0335 - AMOUNT BLOCKED")
                .cardScheme("VISA")
                .cardProduct("404587")
                .requestType("H98")
                .amountToCharge("12.0")
                .cardProductDesc("GhanaVisaClassic")
                .chargeCurrency("GHS")
                .requestType("NEWCARD")
                .requestBranch("Ongata Branch")
                .deliveryMode("BRANCHPICKUP").build();

        fetchActiveCardsByCIFRequest = FetchActiveCardsByCIFRequest.builder()
                .accountAlias("0010184641549601").build();

        invalidFetchActiveCardsByCIFRequest = FetchActiveCardsByCIFRequest.builder()
                .build();

        activateCardRequest = ActivateCardRequest.builder()
                .pan("Fg0+s+5B0U+kmDC+KU9VQ6JP1Zdt2FBj")
                .pin("080808")
                .cardAuthenticationPin("020202")
                .expDate("1656547200000")
                .accountAlias("1441XXXXX0335 - AMOUNT BLOCKED")
                .cardAccountNumber("1441000690335").build();

        invalidActivateCardRequest = ActivateCardRequest.builder()
                .pin("080808")
                .cardAuthenticationPin("020202")
                .expDate("1656547200000")
                .accountAlias("1441XXXXX0335 - AMOUNT BLOCKED")
                .cardAccountNumber("1441000690335").build();

        renewCardRequest = RenewCardRequest.builder()
                .pan("Fg0+s+5B0U+kmDC+KU9VQ6JP1Zdt2FBj")
                .pin("080808")
                .expiryDate("1656547200000")
                .accountAlias("Bancassurance 2")
                .requestType("RENEWAL")
                .deliveryMode("BRANCHPICKUP")
                .cardAccountNumber("1441000229735").build();


        invalidRenewCardRequest = RenewCardRequest.builder()
                .pin("080808")
                .expiryDate("1656547200000")
                .accountAlias("Bancassurance 2")
                .requestType("RENEWAL")
                .deliveryMode("BRANCHPICKUP")
                .cardAccountNumber("1441000229735").build();

        updateDebitCardLimitRequest = UpdateDebitCardLimitRequest.builder()
                .pan("nmly43GbqANJhp62ET0zMBAxtAUMLgk4")
                .pin("080808")
                .expiryDate("1656547200000")
                .accountAlias("1441XXXXXX5288 - ACTIVE").build();


        invalidUpdateDebitCardLimitRequest = UpdateDebitCardLimitRequest.builder()
                .pan("nmly43GbqANJhp62ET0zMBAxtAUMLgk4")
                .expiryDate("1656547200000")
                .accountAlias("1441XXXXXX5288 - ACTIVE").build();

        fetchDebitCardLimitRequest = FetchDebitCardLimitRequest.builder()
                .pan("nmly43GbqANJhp62ET0zMBAxtAUMLgk4")
                .expiryDate("1656547200000")
                .currency("eke").build();


        invalidFetchDebitCardLimitRequest = FetchDebitCardLimitRequest.builder()
                .expiryDate("1656547200000")
                .currency("eke").build();

        changeCardPinRequest = ChangeCardPinRequest.builder()
                .pin("080808")
                .currentPin("111111")
                .newPin("080808")
                .accountAlias("Testing Xpress")
                .expiryDate("1656547200000")
                .pan("nmly43GbqANJhp62ET0zMBAxtAUMLgk4")
                .build();

        invalidChangeCardPinRequest = ChangeCardPinRequest.builder()
                .pin("080808")
                .currentPin("111111")
                .accountAlias("Testing Xpress")
                .expiryDate("1656547200000")
                .pan("nmly43GbqANJhp62ET0zMBAxtAUMLgk4")
                .build();

        resetCardPinRequest = ResetCardPinRequest.builder()
                .pin("080808")
                .cardAuthenticationPin("1122")
                .accountAlias("1441XXXXX0335 - AMOUNT BLOCKED")
                .pan("76FWOI2+x+b2uykW+3MWs6JP1Zdt2FBj")
                .cardAccountNumber("1441000755074")
                .expiryDate("1656547200000").build();

        invalidResetCardPinRequest = ResetCardPinRequest.builder()
                .pan("Fg0+s+5B0U+kmDC+KU9VQ6JP1Zdt2FBj")
                .pin("080808")
                .expiryDate("1656547200000")
                .accountAlias("1441XXXXX0335 - AMOUNT BLOCKED")
                .cardAccountNumber("1441000690335").build();

        createVirtualCardRequest = CreateVirtualCardRequest.builder()
                .virtualCardType("GENERAL PURPOSE CARD")
                .affiliateCode("436")
                .amount(34000.00)
                .chargeAmount("130")
                .sourceAccount("3444545454")
                .otpNumber("32343434")
                .schemeType("VISA")
                .currencyCode("GHS")
                .totalAmount("234000").build();

        invalidCreateVirtualCardRequest = CreateVirtualCardRequest.builder()
                .virtualCardType("GENERAL PURPOSE CARD")
                .affiliateCode("436")
                .amount(34000.00)
                .chargeAmount("130")
                .sourceAccount("3444545454")
                .otpNumber("32343434")
                .schemeType("VISA")
                .totalAmount("234000").build();

        closeVirtualCardRequest = CloseVirtualCardRequest.builder()
                .virtualAccountNumber("9900990597040538")
                .accountAlias("010XXX0431906_GHS_SAV")
                .destAccountNumber("1441000968393")
                .pin("080808")
                .virtualAccountToken("384ef3ace1b94197b2d2ef51ee3f9b31")
                .build();

        invalidCloseVirtualCardRequest = CloseVirtualCardRequest.builder()
                .virtualAccountNumber("9900990597040538")
                .accountAlias("010XXX0431906_GHS_SAV")
                .destAccountNumber("1441000968393")
                .virtualAccountToken("384ef3ace1b94197b2d2ef51ee3f9b31")
                .build();

        fetchVirtualCardsRequest = FetchVirtualCardsRequest.builder()
                .accountAlias("23232xxx3231xxx")
                .build();

        invalidFetchVirtualCardsRequest = FetchVirtualCardsRequest.builder()
                .build();

        fetchVirtualCardDetailsRequest = FetchVirtualCardDetailsRequest.builder()
                .virtualCardAlias("test")
                .virtualAccountNumber("9900990597040538")
                .pin("080808")
                .build();

        invalidFetchVirtualCardDetailsRequest = FetchVirtualCardDetailsRequest.builder()
                .virtualCardAlias("test")
                .virtualAccountNumber("9900990597040538")
                .build();

        virtualCardMinistatementRequest = VirtualCardMinistatementRequest.builder()
                .virtualAccountNumber("9900990597040538")
                .accountAlias("test")
                .virtualAccountToken("test")
                .fromDate("test date")
                .pin("080808")
                .toDate("test date")
                .build();

        invalidVirtualCardMinistatementRequest = VirtualCardMinistatementRequest.builder()
                .virtualAccountNumber("9900990597040538")
                .virtualAccountToken("test")
                .fromDate("test date")
                .pin("080808")
                .toDate("test date")
                .build();

        unblockVirtualCardRequest = UnblockVirtualCardRequest.builder()
                .virtualAccountNumber("9900990597040538")
                .pin("080808")
                .accountAlias("test alias")
                .virtualAccountToken("test")
                .build();

        invalidUnblockVirtualCardRequest = UnblockVirtualCardRequest.builder()
                .virtualAccountNumber("9900990597040538")
                .pin("080808")
                .accountAlias("test alias")
                .build();

        virtualCardBalanceRequest = VirtualCardBalanceRequest.builder()
                .virtualAccountNumber("9900990597040538")
                .accountAlias("Test Alias")
                .affiliate("GHS")
                .virtualAccountToken("test token")
                .pin("080808")
                .build();

        invalidVirtualCardBalanceRequest = VirtualCardBalanceRequest.builder()
                .virtualAccountNumber("9900990597040538")
                .accountAlias("Test Alias")
                .affiliate("GHS")
                .virtualAccountToken("test token")
                .build();

        fetchOTPMobileNumberRequest = FetchOTPMobileNumberRequest.builder()
                .virtualAccountNumber("9900990597040538")
                .affiliate("GHS").build();

        invalidFetchOTPMobileNumberRequest = FetchOTPMobileNumberRequest.builder()
                .virtualAccountNumber("9900990597040538")
                .build();

        updateOTPVirtualCardRequest = UpdateOTPVirtualCardRequest.builder()
                .virtualAccountAlias("Test")
                .currentOTPNumber("909090")
                .newOtpNumber("909090")
                .pin("080808")
                .affiliate("GHS")
                .accountAlias("test VirtualCard")
                .virtualAccountToken("Test Token")
                .virtualAccountNumber("9900990597040538")
                .build();

        invalidUpdateOTPVirtualCardRequest = UpdateOTPVirtualCardRequest.builder()
                .virtualAccountAlias("Test")
                .currentOTPNumber("909090")
                .pin("080808")
                .affiliate("GHS")
                .newOtpNumber("909090")
                .virtualAccountToken("Test Token")
                .virtualAccountNumber("9900990597040538")
                .build();

        loadVirtualCardRequest = LoadVirtualCardRequest.builder()
                .virtualAccountNumber("9900990597040538")
                .amount(200.43)
                .virtualAccountToken("test Token")
                .chargeAmount(8292)
                .accountAlias("test")
                .pin("080808")
                .build();

        invalidLoadVirtualCardRequest = LoadVirtualCardRequest.builder()
                .virtualAccountNumber("9900990597040538")
                .amount(200.43)
                .virtualAccountToken("test Token")
                .chargeAmount(8292)
                .accountAlias("test")
                .build();


    }


    // MANAGE_CARDS_BLOCK = "/manage-cards/block";
    @Test
    void blockCardIsCompletedSuccessfullyGivenCorrectParameters() {
        this.webClient.post()
                .uri(MANAGE_CARDS_BLOCK)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(blockCardRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void blockCardFailsOnInvalidRequestParameters() {
        this.webClient.post()
                .uri(MANAGE_CARDS_BLOCK)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidBlockCardRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555)
                .jsonPath("$.result.data[0]").isEqualTo("pin is a required field");
    }


    @Test
    void blockCardFailsOnInvalidAuthorizationToken() {
        this.webClient.post()
                .uri(MANAGE_CARDS_BLOCK)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidBlockCardRequest))
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();
    }

    // MANAGE_CARDS_ACTIVE_CARDS = "/manage-cards/active-cards";
    @Test
    void activeCardsAreFetchedSuccessfullyGivenCorrectParameters() {
        this.webClient.post()
                .uri(MANAGE_CARDS_ACTIVE_CARDS)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(fetchActiveCardsRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }


    @Test
    void activeCardsFetchFailsOnInvalidRequestParameters() {
        this.webClient.post()
                .uri(MANAGE_CARDS_ACTIVE_CARDS)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidFetchActiveCardsRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555)
                .jsonPath("$.result.data[0]").isEqualTo("Account Required");
    }


    @Test
    void activeCardsFetchFailsOnInvalidAuthorizationToken() {
        this.webClient.post()
                .uri(MANAGE_CARDS_ACTIVE_CARDS)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidFetchActiveCardsRequest))
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();
    }


    // MANAGE_CARDS_UNBLOCK = "/manage-cards/unblock";
    @Test
    void unblockCardsAreFetchedSuccessfullyGivenCorrectParameters() {
        this.webClient.post()
                .uri(MANAGE_CARDS_UNBLOCK)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(unBlockCardRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }


    @Test
    void unblockCardsFetchFailsOnInvalidRequestParameters() {
        this.webClient.post()
                .uri(MANAGE_CARDS_UNBLOCK)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidUnblockCardRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555)
                .jsonPath("$.result.data[0]").isEqualTo("pin is a required field");
    }


    @Test
    void unblockCardsFetchFailsOnInvalidAuthorizationToken() {
        this.webClient.post()
                .uri(MANAGE_CARDS_UNBLOCK)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidUnblockCardRequest))
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();
    }

    // MANAGE_CARDS_BLOCK_CARD_OPTIONS = "/manage-cards/block-card-options";
    @Test
    void testBlockCardOptionsFetchSuccessfully() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));
        this.webClient.get()
                .uri(MANAGE_CARDS_BLOCK_CARD_OPTIONS)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void testBlockCardOptionsFailsOnMissingAuthorizationHeader() {
        this.webClient.get()
                .uri(MANAGE_CARDS_BLOCK_CARD_OPTIONS)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();
    }


    // MANAGE_CARDS_BLOCK_CARD_REASONS = "/manage-cards/block-card-reasons";
    @Test
    void testBlockCardReasonsFetchSuccessfully() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));
        this.webClient.get()
                .uri(MANAGE_CARDS_BLOCK_CARD_REASONS)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void testBlockCardReasonsFailsOnMissingAuthorizationHeader() {
        this.webClient.get()
                .uri(MANAGE_CARDS_BLOCK_CARD_REASONS)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();
    }


    // TRAVEL_NOTIFICATION_REQUEST = "/travel-notification/request";
    @Test
    void travelNotificationCompletedSuccessfullyGivenCorrectParameters() {
        this.webClient.post()
                .uri(TRAVEL_NOTIFICATION_REQUEST)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(travelNotificationRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void travelNotificationFailsOnInvalidRequestParameters() {
        this.webClient.post()
                .uri(TRAVEL_NOTIFICATION_REQUEST)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidTravelNotificationRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555)
                .jsonPath("$.result.data[0]").isEqualTo("pin is a required field");
    }


    @Test
    void travelNotificationFailsOnInvalidAuthorizationToken() {
        this.webClient.post()
                .uri(TRAVEL_NOTIFICATION_REQUEST)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidTravelNotificationRequest))
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();
    }

    // TRAVEL_NOTIFICATION_COUNTRIES = "/travel-notification/countries";
    @Test
    void testNotificationCountriesFetchSuccessfully() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));
        this.webClient.get()
                .uri(TRAVEL_NOTIFICATION_COUNTRIES)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void testNotificationCountriesFailsOnMissingAuthorizationHeader() {
        this.webClient.get()
                .uri(TRAVEL_NOTIFICATION_COUNTRIES)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();
    }


    // PREPAID_CARD_FUND = "/prepaid-card/fund";

    @Test
    void fundPrepaidCardSuccessfullyGivenCorrectParameters() {
        this.webClient.post()
                .uri(PREPAID_CARD_FUND)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(fundPrepaidCardRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }


    @Test
    void fundPrepaidCardFailsOnInvalidRequestParameters() {
        this.webClient.post()
                .uri(PREPAID_CARD_FUND)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidFundPrepaidCardRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555)
                .jsonPath("$.result.data[0]").isEqualTo("Biller code is a required field");
    }


    @Test
    void fundPrepaidCardFailsOnInvalidAuthorizationToken() {
        this.webClient.post()
                .uri(PREPAID_CARD_FUND)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidFundPrepaidCardRequest))
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();
    }

    // MANAGE_CARDS_NAME_INQUIRY = "/manage-cards/card-name-inquiry";
    @Test
    void NameInquiryFetchedSuccessfullyGivenCorrectParameters() {
        when(sharedUtilities.getRequestID((CardNameInquiryRequest) any(), any(), any())).thenReturn(Mono.just("12335484188551"));

        this.webClient.post()
                .uri(MANAGE_CARDS_CARD_NAME_INQUIRY)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(cardNameInquiryRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);

    }

    @Test
    void nameInquiryFailsOnInvalidRequestParameters() {
        this.webClient.post()
                .uri(MANAGE_CARDS_CARD_NAME_INQUIRY)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidCardNameInquiryRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555)
                .jsonPath("$.result.data[0]").isEqualTo("cardNumber is required");
    }

    @Test
    void NameInquiryFetchedFailedOnIncorrectAuthorizationToken() {
        this.webClient.post()
                .uri(MANAGE_CARDS_CARD_NAME_INQUIRY)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidCardNameInquiryRequest))
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();
    }

    // NEW CARDS REQUEST TYPES = "/manage-cards/request-types";
    @Test
    void newCardsRequestTypesFetchSuccessfully() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));
        this.webClient.get()
                .uri(MANAGE_CARDS_NEW_CARD_REQUEST_TYPES)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void newCardsRequestTypesFetchFailedOnInvalidAuthorizationToken() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));
        this.webClient.get()
                .uri(MANAGE_CARDS_NEW_CARD_REQUEST_TYPES)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();
    }


    // Renewal CARDS REQUEST TYPES = "/manage-cards/request-renewal-types";
    @Test
    void renewalCardsRequestTypesFetchSuccessfully() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));
        this.webClient.get()
                .uri(MANAGE_CARDS_RENEWAL_CARD_REQUEST_TYPES)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void renewalCardsRequestTypesFetchFailedOnInvalidAuthorizationToken() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));
        this.webClient.get()
                .uri(MANAGE_CARDS_RENEWAL_CARD_REQUEST_TYPES)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();
    }

    //NEW CARD SCHEME TYPES = "/manage-cards/scheme-types";
    @Test
    void newCardSchemeTypesFetchSuccessfully() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString(), anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));
        this.webClient.get()
                .uri(MANAGE_CARDS_NEW_CARD_SCHEME_TYPES)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);


    }

    @Test
    void newCardSchemeTypesFetchFailedOnInvalidAuthorizationToken() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));
        webClient.get()
                .uri(MANAGE_CARDS_NEW_CARD_SCHEME_TYPES)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();

    }

    //    NEW CARDS FETCH BRANCHES = "/manage-cards/bank-branches";
    @Test
    void newCardsBranchesFetchSuccessfully() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString(), anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));
        this.webClient.get()
                .uri(MANAGE_CARDS_NEW_CARDS_FETCH_BRANCHES)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);


    }

    @Test
    void newCardsBranchesFetchFailedOnInvalidAuthorizationToken() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));
        webClient.get()
                .uri(MANAGE_CARDS_NEW_CARDS_FETCH_BRANCHES)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();

    }

    //    NEW CARDS FETCH DELIVERY MODES = "/manage-cards/delivery-modes";
    @Test
    void newCardDeliveryModesFetchSuccessfully() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));
        this.webClient.get()
                .uri(MANAGE_CARDS_NEW_CARDS_FETCH_DELIVERY_MODES)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void newCardDeliveryModesFailedOnInvalidAuthorizationToken() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));
        this.webClient.get()
                .uri(MANAGE_CARDS_NEW_CARDS_FETCH_DELIVERY_MODES)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().isUnauthorized()
                .expectBody();
    }

    // NEW CARD FETCH CARD CHARGES = "/manage-cards/fetch-request-charges";
    @Test
    void newCardsChargesFetchedSuccessfullyGivenCorrectParameters() {
//        when(sharedUtilities.validateUUID(any(),anyString(),anyString(),anyString())).thenReturn((Mono.empty()));
        this.webClient.post()
                .uri(MANAGE_CARDS_NEW_CARDS_FETCH_CARD_CHARGES)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(newCardRequestChargesRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void newCardsChargesFetchedFailedOnInvalidRequestParameters() {
        this.webClient.post()
                .uri(MANAGE_CARDS_NEW_CARDS_FETCH_CARD_CHARGES)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidNewCardRequestChargesRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555)
                .jsonPath("$.result.data[0]").isEqualTo("Account Required");


    }

    @Test
    void newCardChargesFetchFailedOnInValidAuthorizationToken() {
        this.webClient.post()
                .uri(MANAGE_CARDS_NEW_CARDS_FETCH_CARD_CHARGES)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().isUnauthorized()
                .expectBody();
    }

    //MANAGE_CARDS_NEW_CARD_REQUESTS = "/manage-cards/request-card"
    @Test
    void newCardRequestCardCompletedSuccessfullyGivenCorrectParameters() {
        this.webClient.post()
                .uri(MANAGE_CARDS_NEW_CARD_REQUESTS)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(newCardRequests))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void newCardRequestCardFailedOnInvalidRequestParameters() {
        this.webClient.post()
                .uri(MANAGE_CARDS_NEW_CARD_REQUESTS)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidNewCardRequests))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555)
                .jsonPath("$.result.data[0]").isEqualTo("pin is a required field");
    }

    @Test
    void newCardRequestCardFailedOnInvalidAuthorizationToken() {
        this.webClient.post()
                .uri(MANAGE_CARDS_NEW_CARD_REQUESTS)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidNewCardRequests))
                .exchange()
                .expectStatus().isUnauthorized()
                .expectBody();

    }

    // FETCH CARDS BY CIF = "/manage-cards/fetch-cards-cif";
    @Test
    void fetchCardsByCIFFetchedSuccessfullyGivenCorrectParameters() {
        this.webClient.post()
                .uri(MANAGE_CARDS_FETCH_CARDS_BY_CIF)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(fetchActiveCardsByCIFRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void fetchCardsByCIFFailedOnInvalidRequestParameters() {
        this.webClient.post()
                .uri(MANAGE_CARDS_FETCH_CARDS_BY_CIF)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidFetchActiveCardsByCIFRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555)
                .jsonPath("$.result.data[0]").isEqualTo("Account Required");

    }

    @Test
    void fetchCardsByCIFFailedOnInvalidAuthorizationToken() {
        this.webClient.post()
                .uri(MANAGE_CARDS_FETCH_CARDS_BY_CIF)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(fetchActiveCardsByCIFRequest))
                .exchange()
                .expectStatus().isUnauthorized()
                .expectBody();

    }

    //    MANAGE_CARDS_ACTIVATE_CARDS = "/manage-cards/activate-card";
    @Test
    void activateCardSuccessfullyGivenCorrectParameters() {
        this.webClient.post()
                .uri(MANAGE_CARDS_ACTIVATE_CARDS)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(activateCardRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void activateCardFailedGivenInvalidParameters() {
        this.webClient.post()
                .uri(MANAGE_CARDS_ACTIVATE_CARDS)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidActivateCardRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555);
    }

    @Test
    void activateCardFailedGivenInvalidAuthorizationToken() {
        this.webClient.post()
                .uri(MANAGE_CARDS_ACTIVATE_CARDS)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidActivateCardRequest))
                .exchange()
                .expectStatus().isUnauthorized()
                .expectBody();

    }

    //    RENEW CARD = "/manage-cards/renew-card";
    @Test
    void renewCardSuccessfulGivenCorrectParams() {
        this.webClient.post()
                .uri(MANAGE_CARDS_RENEW_CARD)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(renewCardRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void renewCardFailedOnInvalidParameters() {
        this.webClient.post()
                .uri(MANAGE_CARDS_RENEW_CARD)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidRenewCardRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555)
                .jsonPath("$.result.data[0]").isEqualTo("{pan.required}");
    }

    @Test
    void renewCardFailedOnInvalidAuthorizationToken() {
        this.webClient.post()
                .uri(MANAGE_CARDS_RENEW_CARD)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidRenewCardRequest))
                .exchange()
                .expectStatus().isUnauthorized()
                .expectBody();
    }

    //    FETCH DEBIT CARD LIMITS = "/manage-cards/fetch-debit-card-limits";
    @Test
    void debitCardLimitsFetchedSuccessfullyGivenCorrectParameters() {
        when(sharedUtilities.getIntegrationLayerResponse(any(fetchDebitCardLimitRequest.getClass()),anyString(),anyString())).thenReturn(Mono.just(walletResponse));
        webClient.post()
                .uri(MANAGE_CARDS_FETCH_DEBIT_CARD_LIMITS)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(fetchDebitCardLimitRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);

    }

    @Test
    void debitCardLimitsFetchFailedOnInvalidParameters() {
        webClient.post()
                .uri(MANAGE_CARDS_FETCH_DEBIT_CARD_LIMITS)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidFetchDebitCardLimitRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555)
                .jsonPath("$.result.data[0]").isEqualTo("{pan.required}");

    }

    @Test
    void debitCardLimitsFetchFailedOnInvalidAuthorizationToken() {
        webClient.post()
                .uri(MANAGE_CARDS_FETCH_DEBIT_CARD_LIMITS)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidFetchDebitCardLimitRequest))
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();

    }
//    UPDATE DEBIT CARD LIMITS = "/manage-cards/update-debit-card-limits";

    @Test
    void updateDebitCardLimitsUpdatedSuccessfullyGivenCorrectParameters() {
        webClient.post()
                .uri(MANAGE_CARDS_UPDATE_DEBIT_CARD_LIMITS)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(updateDebitCardLimitRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void updateDebitCardLimitsFailedGivenInvalidParameters() {
        webClient.post()
                .uri(MANAGE_CARDS_UPDATE_DEBIT_CARD_LIMITS)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidUpdateDebitCardLimitRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555)
                .jsonPath("$.result.data[0]").isEqualTo("pin is a required field");

    }

    @Test
    void updateDebitCardLimitsFailedGivenInvalidAuthorizationToken() {
        webClient.post()
                .uri(MANAGE_CARDS_UPDATE_DEBIT_CARD_LIMITS)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidUpdateDebitCardLimitRequest))
                .exchange()
                .expectStatus().isUnauthorized()
                .expectBody();
    }

    //    CHANGE CARD PIN = "/manage-cards/change-card-pin";
    @Test
    void changeCardPinSuccessfullyGivenCorrectParameters() {
        webClient.post()
                .uri(MANAGE_CARDS_CHANGE_CARD_PIN)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(changeCardPinRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void changeCardPinFailedGivenInvalidParameters() {
        this.webClient.post()
                .uri(MANAGE_CARDS_CHANGE_CARD_PIN)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidChangeCardPinRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555)
                .jsonPath("$.result.data[0]").isEqualTo("{newPin.required}");
    }

    @Test
    void changeCardPinFailedOnInvalidAuthorizationToken() {
        webClient.post()
                .uri(MANAGE_CARDS_CHANGE_CARD_PIN)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidChangeCardPinRequest))
                .exchange()
                .expectStatus().isUnauthorized()
                .expectBody();
    }

    //    RESET CARD PIN = "/manage-cards/reset-card-pin";
    @Test
    void resetPinSuccessfullyGivenCorrectParameters() {
        this.webClient.post()
                .uri(MANAGE_CARDS_RESET_CARD_PIN)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(resetCardPinRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void resetPinFailedGivenInvalidParameters() {
        this.webClient.post()
                .uri(MANAGE_CARDS_RESET_CARD_PIN)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidResetCardPinRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555)
                .jsonPath("$.result.data[0]").isEqualTo("{cardAuthenticationPin.required}");
    }

    @Test
    void resetPinFailedOnInvalidAuthorizationHeader() {
        webClient.post()
                .uri(MANAGE_CARDS_RESET_CARD_PIN)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidResetCardPinRequest))
                .exchange()
                .expectStatus().isUnauthorized()
                .expectBody();
    }

    //DEBIT CARD LIMIT STATES = "/manage-cards/debit-card-limit-states"
    @Test
    void debitCardLimitStateFetchedSuccessfully() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));
        this.webClient.get()
                .uri(MANAGE_CARDS_DEBIT_CARD_LIMIT_STATES)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void debitCardLimitStateFetchFailedOnInvalidAuthorizationHeader() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));
        this.webClient.get()
                .uri(MANAGE_CARDS_DEBIT_CARD_LIMIT_STATES)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();

    }

    //    VIRTUAL CARDS CREATE = "/virtual-cards/create";
    @Test
    void createVirtualCardSuccessfullyGivenCorrectParameters() {
        this.webClient.post()
                .uri(VIRTUAL_CARDS_CREATE)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(createVirtualCardRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void createVirtualCardFailedOnInvalidParameters() {
        this.webClient.post()
                .uri(VIRTUAL_CARDS_CREATE)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidCreateVirtualCardRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555)
                .jsonPath("$.result.data[0]").isEqualTo("Currency code is required");
    }

    @Test
    void createVirtualCardsFailedOnInvalidAuthorizationHeader() {
        this.webClient.post()
                .uri(VIRTUAL_CARDS_CREATE)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidCreateVirtualCardRequest))
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();
    }

    //    VIRTUAL CARDS CLOSE CARD = "/virtual-cards/close-card";
    @Test
    void closeVirtualCardSuccessfullyGivenCorrectParameters() {
        this.webClient.post()
                .uri(VIRTUAL_CARDS_CLOSE_CARD)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(closeVirtualCardRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void closeVirtualCardFailedOnInvalidParameters() {
        this.webClient.post()
                .uri(VIRTUAL_CARDS_CLOSE_CARD)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidCloseVirtualCardRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555)
                .jsonPath("$.result.data[0]").isEqualTo("pin is a required field");
    }

    @Test
    void setCloseVirtualCardRequestOnInvalidAuthorizationHeader() {
        this.webClient.post()
                .uri(VIRTUAL_CARDS_CLOSE_CARD)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidCloseVirtualCardRequest))
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();

    }

    //VIRTUAL CARDS FETCH CARDS = "/virtual-cards/fetch-cards";
    @Test
    void fetchVirtualCardsSuccessfullyGivenCorrectParameters() {
        this.webClient.post()
                .uri(VIRTUAL_CARDS_FETCH_CARDS)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(fetchVirtualCardsRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);

    }


    @Test
    void fetchVirtualCardRequestFailedOnInvalidAuthorizationHeader() {
        this.webClient.post()
                .uri(VIRTUAL_CARDS_FETCH_CARDS)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidFetchVirtualCardsRequest))
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();
    }

    //    VIRTUAL CARDS_CARD DETAILS = "/virtual-cards/card-details";
    @Test
    void getVirtualCardDetailsSuccessfullyGivenCorrectParameters() {
        this.webClient.post()
                .uri(VIRTUAL_CARDS_CARD_DETAILS)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(fetchVirtualCardDetailsRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void getVirtualCardDetailsFailedOnInvalidParameters() {
        this.webClient.post()
                .uri(VIRTUAL_CARDS_CARD_DETAILS)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidFetchVirtualCardDetailsRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555)
                .jsonPath("$.result.data[0]").isEqualTo("pin is a required field");
    }

    @Test
    void getVirtualCardDetailsFailedOnInvalidAuthorizationToken() {
        this.webClient.post()
                .uri(VIRTUAL_CARDS_CARD_DETAILS)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidFetchVirtualCardDetailsRequest))
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();

    }

    //VIRTUAL_CARDS_FETCH_TYPES = "/virtual-cards/fetch-types";
    @Test
    void fetchVirtualCardsTypesSuccessfully() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString(), anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));
        this.webClient.get()
                .uri(VIRTUAL_CARDS_FETCH_TYPES)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);

    }

    @Test
    void fetchVirtualCardsTypesFailedOnInvalidAuthorizationToken() {
        when(sharedUtilities.formulateHeaderHashMap(any())).thenReturn(Mono.just(headerParamMap));
        when(sharedUtilities.validateUUID(any(), any(), any(), any())).thenReturn(Mono.empty());
        when(sharedUtilities.getIntegrationLayerResponse(anyString(), anyString())).thenReturn(Mono.just(walletResponse));
        when(sharedUtilities.handleResponse(walletResponse)).thenReturn(Mono.just(responseArray));
        this.webClient.get()
                .uri(VIRTUAL_CARDS_FETCH_TYPES)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();
    }

    //    VIRTUAL CARDS MINI STATEMENT = "/virtual-cards/mini-statement";
    @Test
    void fetchVirtualCardsMiniStatementSuccessfullyGivenCorrectParameters() {
        this.webClient.post()
                .uri(VIRTUAL_CARDS_MINI_STATEMENT)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(virtualCardMinistatementRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);
    }

    @Test
    void fetchVirtualCardsMiniStatementFailedOnInvalidParameters() {
        this.webClient.post()
                .uri(VIRTUAL_CARDS_MINI_STATEMENT)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidVirtualCardMinistatementRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555)
                .jsonPath("$.result.data[0]").isEqualTo("Account Required");


    }


    @Test
    void fetchVirtualCardsMiniStatementFailedOnInvalidAuthorizationHeader() {

        this.webClient.post()
                .uri(VIRTUAL_CARDS_MINI_STATEMENT)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidVirtualCardMinistatementRequest))
                .exchange()
                .expectStatus().isUnauthorized()
                .expectBody();
    }

    //    VIRTUAL_CARDS_UNBLOCK_CARD = "/virtual-cards/unblock-card";
    @Test
    void virtualCardUnblockCardSuccessfullyGivenCorrectParameters() {
        this.webClient.post()
                .uri(VIRTUAL_CARDS_UNBLOCK_CARD)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(unblockVirtualCardRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);

    }

    @Test
    void virtualCardUnblockFailedGivenInvalidParameters() {
        this.webClient.post()
                .uri(VIRTUAL_CARDS_UNBLOCK_CARD)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidUnblockVirtualCardRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555)
                .jsonPath("$.result.data[0]").isEqualTo("{virtualAccountToken.required}");

    }

    @Test
    void virtualCardUnblockFailedGivenInvalidAuthenticationHeader() {
        this.webClient.post()
                .uri(VIRTUAL_CARDS_UNBLOCK_CARD)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(unblockVirtualCardRequest))
                .exchange()
                .expectStatus().isUnauthorized()
                .expectBody();
    }

    //   VIRTUAL_CARDS_BALANCE = "/virtual-cards/balance";
    @Test
    void getVirtualCardsBalanceSuccessfullyGivenCorrectParameters() {
        this.webClient.post()
                .uri(VIRTUAL_CARDS_BALANCE)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(virtualCardBalanceRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);

    }

    @Test
    void getVirtualCardsBalanceOnInvalidParameters() {

        this.webClient.post()
                .uri(VIRTUAL_CARDS_BALANCE)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidVirtualCardBalanceRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555)
                .jsonPath("$.result.data[0]").isEqualTo("pin is a required field");

    }

    @Test
    void getVirtualCardsBalanceOnInvalidAuthorizationHeader() {
        this.webClient.post()
                .uri(VIRTUAL_CARDS_BALANCE)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(virtualCardBalanceRequest))
                .exchange()
                .expectStatus().isUnauthorized()
                .expectBody();
    }

    //VIRTUAL_CARDS_GET_OTP_NUMBER = "/virtual-cards/get-otp-number"
    @Test
    void virtualCardGetOtpNumberSuccessfullyGivenCorrectParameters() {

        this.webClient.post()
                .uri(VIRTUAL_CARDS_GET_OTP_NUMBER)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(fetchOTPMobileNumberRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);

    }

    @Test
    void virtualCardGetOtpNumberFailedOnInvalidParameters() {

        this.webClient.post()
                .uri(VIRTUAL_CARDS_GET_OTP_NUMBER)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidFetchOTPMobileNumberRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555)
                .jsonPath("$.result.data[0]").isEqualTo("Affiliate is required");


    }

    @Test
    void virtualCardGetOtpNumberFailedOnInvalidAuthorizationHeader() {

        this.webClient.post()
                .uri(VIRTUAL_CARDS_GET_OTP_NUMBER)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidFetchOTPMobileNumberRequest))
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();

    }

    //    VIRTUAL_CARDS_UPDATE_OTP_NUMBER = "/virtual-cards/update-otp-number"
    @Test
    void virtualCardsUpdateOtpNumberSuccessfullyGivenCorrectParameters() {
        this.webClient.post()
                .uri(VIRTUAL_CARDS_UPDATE_OTP_NUMBER)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(updateOTPVirtualCardRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);

    }

    @Test
    void virtualCardsUpdateOtpNumberFailedOnInvalidParameters() {
        this.webClient.post()
                .uri(VIRTUAL_CARDS_UPDATE_OTP_NUMBER)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidUpdateOTPVirtualCardRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555)
                .jsonPath("$.result.data[0]").isEqualTo("Account Required");

    }

    @Test
    void virtualCardsUpdateOtpNumberInvalidOnInvalidHeaderParams() {
        this.webClient.post()
                .uri(VIRTUAL_CARDS_UPDATE_OTP_NUMBER)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidUpdateOTPVirtualCardRequest))
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();
    }

    //    VIRTUAL_CARDS_LOAD = "/virtual-cards/load"
    @Test
    void virtualCardsLoadSuccessfullyGivenCorrectParameters() {
        this.webClient.post()
                .uri(VIRTUAL_CARDS_LOAD)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(loadVirtualCardRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5000);

    }

    @Test
    void virtualCardsLoadSuccessfullyOnInvalidParameters() {
        this.webClient.post()
                .uri(VIRTUAL_CARDS_LOAD)
                .header("Authorization", "Bearer " + tokenHeader)
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(invalidLoadVirtualCardRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo(5555)
                .jsonPath("$.result.data[0]").isEqualTo("pin is a required field");

    }

    @Test
    void virtualCardsLoadSuccessfullyOnInvalidAuthorizationHeader() {
        this.webClient.post()
                .uri(VIRTUAL_CARDS_LOAD)
                .header("Authorization", "Bearer " + "tokenHeader")
                .header("x-headers", headerParamsString)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(updateOTPVirtualCardRequest))
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody();

    }



}
