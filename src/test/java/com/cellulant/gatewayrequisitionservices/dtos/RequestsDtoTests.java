package com.cellulant.gatewayrequisitionservices.dtos;

import com.cellulant.gatewayrequisitionservices.dtos.core.CoreRequest;
import com.cellulant.gatewayrequisitionservices.dtos.core.ServiceRequest;
import com.cellulant.gatewayrequisitionservices.dtos.walletdtos.Payload;
import com.cellulant.gatewayrequisitionservices.dtos.walletdtos.WalletPayload;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import javax.validation.constraints.AssertTrue;
import java.util.HashMap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class RequestsDtoTests {

    private CoreRequest coreRequest = new CoreRequest();
    private ServiceRequest serviceRequest = new ServiceRequest();
    private WalletPayload walletPayload = new WalletPayload();
    private Payload payload = new Payload();

    @BeforeEach
    void setUp() {

        serviceRequest = new ServiceRequest("080808", "test account", "Transfer", "customers", 45, "send-money", new HashMap<>());

        coreRequest = new CoreRequest("32323131", "32323131", 0, "233249971997", "1", "23423113", 0, "29923y923y", 2, "en", "system", "lipuka", serviceRequest);

        walletPayload = new WalletPayload("32323131", 2, "233245326845", "dasdnasjfasf", 2, "dasdsads", 2, "ussd", 2, "en", 1);

        payload = new Payload("balance-enquiry", "test account", "233245326845", "dasdnasjfasf", 20f, "dasdsads", new HashMap<>());

    }

    @Test
    void testRequestForServiceRequest() {
        assertEquals("test account", serviceRequest.getAccountAlias());
        assertNotNull(serviceRequest.getAmount());
    }

    @Test
    void testRequestFor_CoreRequest() {
        assertNotNull(coreRequest.getMsisdn());
        assertEquals("32323131", coreRequest.getCloudPacketID());
    }

    @Test
    void testRequestFor_WalletPayloadRequest()
    {
        assertEquals("32323131",walletPayload.getCloudRequestID());
        assertNotNull(walletPayload.getPayload());
    }

    @Test
    void testRequestFor_payLoadRequest()
    {
        assertNotNull(payload.getModuleName());
        assertEquals(payload.getAccountAlias(),"test account");

    }

}
