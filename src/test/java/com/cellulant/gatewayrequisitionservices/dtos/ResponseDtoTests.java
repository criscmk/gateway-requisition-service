package com.cellulant.gatewayrequisitionservices.dtos;

import com.cellulant.gatewayrequisitionservices.dtos.responses.MsisdnValidationResponse;
import com.cellulant.gatewayrequisitionservices.dtos.responses.RabbitMQResponse;
import com.cellulant.gatewayrequisitionservices.dtos.responses.ResponseArray;
import com.cellulant.gatewayrequisitionservices.dtos.responses.ResponseArrayResultDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ResponseDtoTests {
    private MsisdnValidationResponse msisdnValidationResponse = new MsisdnValidationResponse();
    private ResponseArray responseArray = new ResponseArray();
    private ResponseArrayResultDTO responseArrayResultDTO = new ResponseArrayResultDTO();
    private RabbitMQResponse rabbitMQResponse = new RabbitMQResponse();
    @BeforeEach
    void setUp() {
        msisdnValidationResponse = MsisdnValidationResponse.builder()
                .originalNumber("233249971997")
                .numberType("MOBILE")
                .localNumber("0249971997")
                .internationalNumber("233249971997")
                .countryCode("GH")
                .countryDialCode("233")
                .country("EGH")
                .network("")
                .hasNumberPortability("true")
                .numberType("")
                .note(" Number Validation Succeeded using msisdn exception")
                .build();

        responseArray = ResponseArray.builder()
                .statusCode(5000)
                .statusMessage("Dear Customer, Your Request was successfully processed")
                .result( new Object())
                .build();

        responseArrayResultDTO = ResponseArrayResultDTO.builder()
                .message("Dear Customer, Your Request was successfully processed")
                .referenceID("12232442")
                .data(new ArrayList())
                .build();

        rabbitMQResponse = RabbitMQResponse.builder()
                .statusCode(5000)
                .statusMessage("Dear Customer, Your Request was successfully processed")
                .statusType( 1)
                .resultsList(List.of(new Object()))
                .build();
    }
    @Test
    void testResponseforMsisdnValidationResponse() {

        assertNotNull(msisdnValidationResponse.toString());
        assertEquals("233249971997", msisdnValidationResponse.getInternationalNumber());
    }

    @Test
    void testResponse_ResponseArray() {

        assertNotNull(responseArray.toString());
        assertEquals("5000", responseArray.getStatusCode().toString());
    }

    @Test
    void testResponse_ResponseArrayResultDTO() {

        assertNotNull(responseArrayResultDTO.toString());
        assertEquals("12232442", responseArrayResultDTO.getReferenceID());
    }


    @Test
    void testResponse_RabbitMQResponse() {

        assertNotNull(rabbitMQResponse.toString());
        assertEquals("5000", String.valueOf(rabbitMQResponse.getStatusCode()));
    }


}
